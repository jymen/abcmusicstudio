//
//  SyntaxColoringTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 2/15/19 week 7.
//  Copyright © 2019 jymengant. All rights reserved.
//

import XCTest
import CocoaLumberjack

class SyntaxColoringTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCodableStyle() {
      let attStr = NSMutableAttributedString(string : "Big Nerd Ranch")
      attStr.addAttribute(NSAttributedString.Key.font, value: NSFont.boldSystemFont(ofSize:22), range: NSRangeFromString(attStr.string))
      attStr.addAttribute(NSAttributedString.Key.foregroundColor, value: NSColor.red, range: NSRangeFromString(attStr.string))
      var style : [ColoringType : Style] = [ColoringType : Style]()
      style[.Title] = Style(inf:loc.tr("Placeholder"),[StyleElement(key: .foregroundColor , value : NSColor.black),
                                                       StyleElement(key: .font , value : NSFont.boldSystemFont(ofSize: 12))
        ])
      //let style = StyleElement(key: .foregroundColor , value : NSColor.gray)
      //let styles = Style(inf:"Comments",[style])
      // let style : [String: String] =  [String: String]()
      
      let encoder = JSONEncoder()
      encoder.outputFormatting = .prettyPrinted
      do {
        let encoded = try! encoder.encode(style)
        let str = String(decoding: encoded, as: UTF8.self)
        DDLogDebug("data1 : \(str)")
        let decoder = JSONDecoder()
        let data = str.data(using: .utf8)
        XCTAssert( data != nil )
        let decoded : [ColoringType : Style] =
          try decoder.decode([ColoringType : Style].self,from:data!)
        XCTAssert( decoded[.Title] != nil  )
      } catch {
        print(error.localizedDescription)
      }
      let attributedText = NSAttributedString(string: "Hello, playground", attributes: [
        .foregroundColor: NSColor.red,
        .backgroundColor: NSColor.green,
        .ligature: 1,
        .strikethroughStyle: 1
        ])
      
      // retrieve attributes
      let attributes = attStr.attributes(at: 1, effectiveRange: nil)
      
      // iterate each attribute
      for attr in attributes {
        print(attr.key, attr.value)
      }
      let newStyle = Style(attributed: attributedText)
      print("new style from attributed : \(newStyle.info)")
    }


}
