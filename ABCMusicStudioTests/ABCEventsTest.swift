//
//  ABCEventsTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 8/24/18 week 34.
//  Copyright © 2018 jymengant. All rights reserved.
//

import XCTest

class ABCEventsTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

  
    func testEvent() {
      // This is an example of a functional test case.
      // Use XCTAssert and related functions to verify your tests produce the correct results.
      print( "Entering testEvent" )
      let manager = EventManager.shared
      let evt : ABCEvent = ABCEvent(type:.TreeElementRemoved,msg: "test")
      let listener : (_ data: ABCEvent) -> () = {
         data in
           print( "entering listener" )
           XCTAssert( data.type == .TreeElementRemoved)
           XCTAssert( data.msg as! String  == "test")
           print( "leaving listener" )
      }
      _ = manager.add(listener:listener)
      manager.emit(event: evt)
      print( "leaving testEvent" )
    }

}
