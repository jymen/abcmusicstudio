//
//  SubstringTests.swift
//  ABCStudioTests
//
//  Created by jymengant on 1/29/19 week 5.
//  Copyright © 2019 jymengant. All rights reserved.
//

import XCTest

class SubstringTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSubString() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
      let test = "Hello USA 🇺🇸!!! Hello Brazil 🇧🇷!!!"
      var res = test[safe: 10]   // "🇺🇸"
      XCTAssertEqual(res, "🇺🇸")
      let res1 = test[11]   // "!"
      XCTAssertEqual(res1, "!")
      var res2 = test[10...]   // "🇺🇸!!! Hello Brazil 🇧🇷!!!"
      XCTAssertEqual(res2, "🇺🇸!!! Hello Brazil 🇧🇷!!!")
      let i = 10
      let j = 12
      res2 = test[i..<j]   // "🇺🇸!"
      XCTAssertEqual(res2, "🇺🇸!")
      res2 = test[10...12]   // "🇺🇸!!"
      XCTAssertEqual(res2, "🇺🇸!!")
      res2 = test[...10]   // "Hello USA 🇺🇸"
      XCTAssertEqual(res2,  "Hello USA 🇺🇸")
      res2 = test[..<10]   // "Hello USA "
      XCTAssertEqual(res2,  "Hello USA ")
      res = test.first   // "H"
      XCTAssertEqual(res, "H")
      res = test.last    // "!"
      XCTAssertEqual(res, "!")

      // Subscripting the Substring
      _ = test[...][...3]  // "Hell"
      
      // Note that they all return a Substring of the original String.
      // To create a new String you need to add .string as follow
      _ = test[10...].string  // "🇺🇸!!! Hello Brazil 🇧🇷!!!"
    }


}
