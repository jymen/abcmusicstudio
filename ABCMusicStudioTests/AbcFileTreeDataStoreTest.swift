//
//  AbcFileTreeDataStoreTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 6/2/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import XCTest


class AbcFileTreeDataStoreTest: XCTestCase {
  
  var prefs : PreferenceManager? = nil
  var dataStore : ABCTreeJsonDataStore? = nil
  let minimal : String = """
  {
    "root" : {
      "infos" : {
        "title": "root Folder" ,
        "creation": 573495800.37878299,
        "storageURL": "file:///Users/jymen" ,
        "Comments": "An optional comment area",
        "encoding": 1,
      },
    },
  }
  """
  
  let dataTest : String = """
  {
    "root" : {
      "infos" : {
        "title": "root Folder" ,
        "creation": 573495800.37878299,
        "storageURL": "file:///Users/jymen" ,
        "Comments": "An optional comment area",
        "encoding": 1,
      },
     "folders" : [
      {
          "infos" : {
          "title": "Jigs and Slip Jigs" ,
          "creation": 573495800.37878299 ,
          "storageURL": "file:///Users/jymen" ,
          "encoding": 1,
        },
      },
    ],
    "files" : [
      {
        "infos" : {
        "title": "Jigs and Slip Jigs" ,
        "creation": 573495800.37878299 ,
        "storageURL": "file:///Users/jymen" ,
        "encoding": 1,
         },
      },
      {
        "infos" : {
        "title": "locate me" ,
        "creation": 573495800.37878299 ,
        "storageURL": "file:///Users/jymen" ,
        "encoding": 1,
         },
       },
      ],
    },
  }
  """

  let dataTest1 : String = """
  {
    "infos": {
      "title":"Une jeune fille",
      "creation":573592772.46399295,
        "storageURL": "file:///Users/jymen" ,
      "comments":"",
       "encoding":1,
     },
  }
  """
  
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
      prefs = PreferenceManager()
      do {
        try prefs!.checkPreferences()
      } catch {
        XCTAssert(false)
      }
      dataStore = ABCTreeJsonDataStore()

      
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDecodeEnCodeAndsearch() {
      do {
        let rootData = try dataStore!.decode(jsonSource: dataTest)
        XCTAssertNotNil( rootData )
        let abcTop = rootData as? ABCFolder
        XCTAssertNotNil(abcTop)
        let root = abcTop?.root

        XCTAssertEqual(root!.infos.title , "root Folder" )
        let encoded : String? = try ABCTreeJsonDataStore.encode(source: root,encoding:.utf8)
        XCTAssertNotNil( encoded )
        print("\(encoded!)")
        let findLeaf = Leaf(title: "locate me" , creation: Date(), comments:"" ,encoding: .utf8 ,type: ABCResType.ABCScore)
        XCTAssertNotNil( findLeaf )
        let myParent = root?.lookForParent(candidate: findLeaf )
        XCTAssertNotNil( myParent )
        let notFoundLeaf = Leaf(title: "not found" , creation: Date(), comments:"" ,encoding: .utf8 , type: ABCResType.ABCScore)
        let myNotFoundParent = root?.lookForParent(candidate: notFoundLeaf )
        XCTAssertNil( myNotFoundParent )
        let fileData = try ABCTreeJsonDataStore.decodeTreeElement(jsonSource: dataTest1)
        XCTAssertNotNil( fileData )
      } catch {
         print("error : \(error)")
         XCTAssert(false)
      }
    }
  
  func testAddItem() {
    do {
      let rootData = try dataStore!.decode(jsonSource: dataTest)
      XCTAssertNotNil( rootData )
      let curFolder = rootData as? ABCFolder
      XCTAssertNotNil( curFolder )
      // add group
      let curGroupData : ABCResourcePanelData = ABCResourcePanelData(type:.Group,title:"testGroup",creation:Date())
      _ = curFolder!.root.addItem(data: curGroupData, content: "toto")
      
    } catch {
      XCTAssert(false)
    }

  }
}
