//
//  Base64EncoderTest.swift
//  ABCMusicStudioTests
//
//  Created by jymengant on 3/15/20 week 11.
//  Copyright © 2020 jymengant. All rights reserved.
//

import XCTest

class Base64EncoderTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEncodingDecoding() {
      let bundle = Bundle(for: type(of: self))
      if let url = bundle.url(forResource: "TopViolin", withExtension: "png", subdirectory: "testdata") {
        let encoder = Base64Encoder()
        let str64 = encoder.encode(url: url)
        XCTAssert( str64 != nil)
        let data = encoder.decode(str64: str64!)
        XCTAssert(data != nil)
      }
      if let url = bundle.url(forResource: "Mazurkas", withExtension: "m4a", subdirectory: "testdata") {
        let encoder = Base64Encoder()
        let str64 = encoder.encode(url: url)
        XCTAssert( str64 != nil)
        let data = encoder.decode(str64: str64!)
        XCTAssert(data != nil)
      }
    }

}
