//
//  UnicodeTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 12/16/18 week 50.
//  Copyright © 2018 jymengant. All rights reserved.
//

import XCTest

class UnicodeTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testBadProtocol() {
      // test bad unicode protocol
      // Use XCTAssert and related functions to verify your tests produce the correct results.
      let result = Unicode(encoding:"unknown")
      XCTAssert( result.encoding == .utf8) // wrong is defaulted to Utf8

      //  XCTFail("Expecting exception here on invalid encoding")
      let str : String = "哈哈123abc"
      // print ( str ) comment to prevent gitlab failures
      let unicode  = Unicode(encoding:"utf8")
      let encoded = unicode.encode(str: str)
      XCTAssert( encoded != nil )
      // print( encoded! ) comment to prevent gitlab failures
      let decoded = unicode.decode(str: encoded!)
      XCTAssert( decoded != nil )
      // print (decoded!) comment to prevent gitlab failures
    }
  
  func testStaticFunctions(){
    print(Unicode.sortedEncodingsKeys)
    let utf8Pos = Unicode.encodingIndex(encoding: .utf8)
    print("utf8 index  :\(utf8Pos)")
    let utf8 = Unicode.encoding(atIndex: 5)
    XCTAssert( utf8 == .utf8)
    let utf8Pos2 = Unicode.encodingIndex(strEncoding: "utf8")
    XCTAssert( utf8Pos2 == 5)
  }
  


}
