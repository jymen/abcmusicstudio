//
//  ExportedABCFolder.swift
//  ABCMusicStudioTests
//
//  Created by jymengant on 3/18/20 week 12.
//  Copyright © 2020 jymengant. All rights reserved.
//

import XCTest

class ExportedABCFolderTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExported() {
      let bundle = Bundle(for: type(of: self))
      if let url = bundle.url(forResource: "testcatalogue", withExtension: "abcmusicstudio", subdirectory: "testdata") {
      
        let dataStore = ABCTreeJsonDataStore()
        let fu = FileUtils()
        do {
          let json = try fu.read(url: url, backupEncoding: .utf8)
          if let repos = try dataStore.decode(jsonSource: json.0) as? ABCFolder {
            let exported = ExportedABCSource(source: repos.root)
            let dirPath = NSTemporaryDirectory()
            XCTAssert( fu.canWriteAt(dirPath:dirPath) )
            let tmpFile = dirPath + "/temp.json"
            let destUrl = URL(fileURLWithPath: tmpFile, isDirectory: false)

            exported.exportArchive(dest: destUrl, exportType: ExportOptions(extra: true, sound: true, icon: true) )
          }
        } catch {
          print("error: ", error)
          XCTAssert(false)
        }
      } else {
        XCTAssert(false)
      }
      
    }
  


}
