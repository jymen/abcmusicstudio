//
//  AnyCodableTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 4/13/19 week 15.
//  Copyright © 2019 jymengant. All rights reserved.
//

import XCTest

class AnyCodableTest: XCTestCase {
  
  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }
  
  func testMinimum() {
    let jsondoc = """
{
  "className": "testClass",
  "fx": "fx"
}
"""
    do {
      let result : EntryPointArgs = try JSONDecoder().decode(EntryPointArgs.self, from: jsondoc.data(using: .utf8)!)
      XCTAssert(result.className == "testClass")
      let jsonEncoder = JSONEncoder()

      let encoded : Data? = try jsonEncoder.encode(result)
      let strEncoded = String(data: encoded!, encoding: .utf8)
      XCTAssert(strEncoded  != nil)
      print("ENCODED : \(String(describing: strEncoded))")
    } catch {
      XCTFail("DECODING ERROR :\(error) ")
    }
    
  }
  
  func testWithInputArgs() {
    let jsondoc = """
{
  "className": "testClass",
  "fx": "fx",
  "fxArgs" : ["abc" , 123 , true]
}
"""
    do {
      let result : EntryPointArgs = try JSONDecoder().decode(EntryPointArgs.self, from: jsondoc.data(using: .utf8)!)
      XCTAssert(result.className == "testClass")
      let abc = result.fxArgs![0].value as! String
      XCTAssert( abc == "abc" )
      let d123 = result.fxArgs![1].value as! Int
      XCTAssert( d123 == 123 )
      let dTrue = result.fxArgs![2].value as! Bool
      XCTAssert( dTrue == true )
      let jsonEncoder = JSONEncoder()

      let encoded : Data? = try jsonEncoder.encode(result)
      let strEncoded = String(data: encoded!, encoding: .utf8)
      XCTAssert(strEncoded  != nil)
      print("ENCODED : \(String(describing: strEncoded))")

    } catch {
      XCTFail("DECODING ERROR :\(error) ")
    }
    
  }
  
  func testFullCases() {
    let jsondoc = """
{
  "className": "testClass",
  "fx": "fx",
  "fxArgs": ["abc",123,true , { "toto" : 123 } ],
  "fxRet" : {
    "error" : "test error" ,
    "data" : {
      "StringValue" : "String value",
      "intvalue" : 123 ,
      "double" :123.123,
      "boolval" : true
    }
  }
}
"""
    do {
      let result : EntryPointArgs = try JSONDecoder().decode(EntryPointArgs.self, from: jsondoc.data(using: .utf8)!)
      XCTAssert(result.className == "testClass")
      let abc = result.fxArgs![0].value as! String
      XCTAssert( abc == "abc" )
      let d123 = result.fxArgs![1].value as! Int
      XCTAssert( d123 == 123 )
      let dTrue = result.fxArgs![2].value as! Bool
      XCTAssert( dTrue == true )
      if let fxRet = result.fxRet {
        if let data = fxRet.data["intvalue"] {
          XCTAssert( data.value as! Int == 123)
        }
        if let data = fxRet.data["double"] {
          XCTAssert( data.value as! Double == 123.123)
        }
        if let data = fxRet.data["boolval"] {
          XCTAssert( data.value as! Bool == true)
        }
        if let data = fxRet.data["StringValue"] {
          XCTAssert( data.value as! String == "String value")
        }
        XCTAssert( fxRet.error == "test error")
        let jsonEncoder = JSONEncoder()
        
        let encoded : Data? = try jsonEncoder.encode(result)
        let strEncoded = String(data: encoded!, encoding: .utf8)
        XCTAssert(strEncoded  != nil)
        print("ENCODED : \(String(describing: strEncoded))")

      } else {
        XCTAssert(false)
      }
    } catch {
      XCTFail("DECODING ERROR :\(error) ")
    }
  }
}
