//
//  ABCStudioTests.swift
//  ABCStudioTests
//
//  Created by jymengant on 5/26/18 week 21.
//  Copyright © 2018 jymengant. All rights reserved.
//

import XCTest

class ABCStudioTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
  func testVerySimple() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
  }
  
  func testSampleEchoProx() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let echo = SampleEcho() ;
    echo.verysimple()  // minimalist call
    // now with unsafe pointers
    let result = echo.echo(arg:"hello")
    assert( result == "hello")
  }
  
  
}
