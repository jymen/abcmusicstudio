//
//  ABCParserTests.swift
//  ABCStudioTests
//
//  Created by jymengant on 9/19/18 week 38.
//  Copyright © 2018 jymengant. All rights reserved.
//

import XCTest
import CocoaLumberjack

class ABCParserTests: XCTestCase {
  
  var abcSrc : String? = nil
  
  func loadABC(name: String) -> String? {
    let bundle = Bundle(for: type(of: self))
    let path = bundle.path(forResource: name, ofType: "txt" , inDirectory: "testdata")
    XCTAssert(path != nil)
    do {
      let abcData = try String(contentsOfFile: path!, encoding: String.Encoding.ascii)
      return abcData
    } catch {
      XCTFail("Error loading abc Source :\(error) ")
    }
    return nil
  }
  
  func doParse( src : String? ) throws -> SyntaxError?  {
    XCTAssert(src != nil)
    let parser : ABCParser = ABCParser(src:src!)
    let abcIR  = try parser.parse()
    if ( abcIR.errors != nil ) {
      // just syntax parsing ABC errors to handle
      let error = abcIR.errors!
      let token = error.token
      DDLogDebug("text offsets : \(token.start) , length : \(token.length)   ")
      let content = src![token.start..<token.stop]
      DDLogDebug("text content : \(content)")
    }
    return abcIR.errors
  }
  
  
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testEndOfSource()  {
    let abcSource = "%%vocalfont Bookman-Light 12\n\r%%wordsfont Bookman-Light 14"
    do {
      let error =  try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testComments() {
    let abcSource = """
        %%vocalfont Bookman-Light 12
        %%wordsfont Bookman-Light 14
        %%composerfont Bookman-Light 12
        %%titlefont Luminari 26

        This file contains 32 Carolan Tunes (#1 - #32).

        You can find more abc tune files at http://www.norbeck.nu/abc/
        I've transcribed them as I have learnt them, which does not necessarily mean
        that I play them that way nowadays. Many of the tunes include variations and
        different versions. If there is a source (S:) or discography (D:) included the
        version transcribed might still not be exactly as that source played the tune,
        since I might have changed the tune around a bit when I learnt it.
        The tunes were learnt from sessions, from friends or from recordings.
        When I've included discography, it's often just a reference to what recordings
        the tune appears on.

        Last updated 18 October 2018.

        (c) Copyright 1997-2018 Henrik Norbeck. This file:
        - May be distributed freely (with restrictions below).
        - May not be used for commercial purposes (such as printing a tune book to sell).
        - This file (or parts of it) may not be made available on a web page for
        download without permission from me.
        - This copyright notice must be kept, except when e-mailing individual tunes.
        - May be printed on paper for personal use.
        - Questions? E-mail: henrik@norbeck.nu
        R:carolan
        C:Turlough O'Carolan (1670-1738)
        Z:id:hn-carolan-%X

        X:1
        T:Planxty Fanny Powers
        T:Fanny Powers
        R:carolan
        C:Turlough O'Carolan (1670-1738)
        Z:id:hn-carolan-1
        M:3/4
        L:1/4
        Q:1/4=160
        K:G
        D |: G2D | G>AB | c2B | A2G | GFE | DFD | F2G | A2c |
        B>AG | Bcd | e2A | A>BG | GFE | DGF | G3 |1 G2D :|2 G2d ||
        |: dB/c/d | dB/c/d | G>BG | GBd | ec/d/e | ec/d/e | A>cA | AcA |
        B>cd | efg | f>ga | d2c | B>AG | DGF | G3 |1 G2d :|2 G2 ||
        P:variations
        D |: G2D | G>AB | c2B | A2G | F>GE | DFD | F2G | A>Bc |
        B>AG | B>cd | e2A | A2G | GFE | DGF | G3 |1 G2D :|2 G>AB/c/ ||
        |: dB/c/d | dB/c/d | G>AG | GBd | ec/d/e | ec/d/e | A>BA | ABc |
        B>cd | e>fg | f>ga | d2c | B>AG | A/B/cF | G3 |1 G>AB/c/ :|2 G2 ||

        - A final comment line

        """
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
    
    
  }
  
  
  func testExample() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let abcSource = loadABC(name:"firstsample")
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testTuneCollection() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let abcSource = loadABC(name:"TuneCollectionTest")
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testOperation() {
    let abcSource = loadABC(name:"firstsample")

    let expectation = self.expectation(description: "Operation")
    let opStub : ParsingOperationStub = ParsingOperationStub(src: abcSource!)
    opStub.getIr().done { (ir) in
      DDLogDebug("returning from parsing")
      XCTAssert( ir.errors == nil )
      XCTAssert(ir.countTunes > 0)
      expectation.fulfill()
    }.catch { (err) in
      XCTFail("NO errors are expected here")
    }
    // Wait for the expectation to be fullfilled, or time out
    // after 5 seconds. This is where the test runner will pause.
    waitForExpectations(timeout: 15, handler: nil)
    DDLogDebug("leaving testOperation")

  }
  
  func testVoices() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let abcSource = loadABC(name:"TextVoices")
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testVoices1() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let abcSource = loadABC(name:"TextVoices1")
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testFerneGeliebte() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let abcSource = loadABC(name:"dieFerneGeliebte")
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testIREncoding() {
    var abcSource = "I:abc-charset utf8"
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
    }
    abcSource = "I:abc-charset utf8"
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testTuneNumber() {
    var abcSource = "X:ABC"
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error != nil)
      print("OK checked non numerical : \(String(describing: error?.message))")
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
    abcSource = "X:1"
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testVersion() {
    let abcSource = "%abc-2.1"
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testSlide() {
    let abcSource = """
    X:1
    (AB)
    """
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testInformationTypeField() {
    let abcSource = """
    X:1
    T:Paddy O'Rafferty
    K:G
    """
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testChords() {
    let abcSource = """
    X:1
    K:G
    [=EG][GB][FA]) f[FA])([FA] | !p! [B,EGB]2 (B2 A2) | [G,EG]2 x[K:treble] x2 | x[K:bass] x2 | !p! [B,EGB]2 (B2 A2) |$ x2 (B3 c) |
    """
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  

  func testNotes() {
    let abcSource = """
    X:1
    K:G
    D |: G2D | G>AB | c<2B |
    P:variations
    (_AB ^cd//) A2 A3

    - A final comment line
    """
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testMinimalVoice() {
    let abcSource = """
      V:* clef=bass
      X:1
      %%score (T1 T2) (B1 B2)
      V:T1  clef=treble-8  space=+10 name=\"Tenore I\"   snm=\"T.I\"
      V:T2  clef=treble-8  name="Tenore II"  snm="T.II"
      K:G
      [V:T1] (B2c2 d2g2)  | f6e2      | (d2c2 d2)e2 | d4 c2z2 |
      [V:T2]       x8      | z2B2 c2d2 | e3e (d2c2)  | H d6    ||
    """
    do {
      let error = try doParse(src:abcSource)
      if let err = error {
        DDLogDebug("ERROR : \(err.message)")
      }
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testheader() {
    let abcSource = """
    %%vocalfont Bookman-Light 12
    %%wordsfont Bookman-Light 14
    %%composerfont Bookman-Light 12
    %%titlefont Luminari 26
    """
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testInformationFields() {
    let abcSource = """
    X: 1
    T: Drag her round the road
    Z:
    S:
    R: reel
    M: 4/4
    L: 1/8
    K: G
    """
    do {
      let error = try doParse(src:abcSource)
      XCTAssert(error == nil)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testScoreNotes() {
    let abcSource = """
    || A2 AA G2  c2| cc A2 G2 E2 | G2 A2 A2 D2 |  E2 F2 A2 G2 F2 D2||
    w: C'é tait trois gar çons d'Al le ma a gne s'y pro me nant dans la cam pa a gne
    """
    do {
      _ = try doParse(src:abcSource)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
  func testBadError() {
    let abcSource = """
    X: 1
    T: Denis Murphy
    R: polka
    M: 4/4
    Q: 120
    L: 1/8
    K: G
    |: d2 ed B2 d2 | G2 d2 B2 d2 | e2 ge d2 B2 |[1 A2 G2 E4 :|[2 A2 G2 G4|
    |: B2 d2 e2 ge | d2 B2 A2 G2 | [1B2 d2 e2 ge | d2 B2 A4 :|][2 d2 ed B2 A2 | A2 G2 G4 ]|
    """
    do {
      _ = try doParse(src:abcSource)
    } catch {
      XCTFail("ABC parsing error :\(error) ")
    }
  }
  
}
