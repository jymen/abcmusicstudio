//
//  abcm2psTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 8/29/19 week 35.
//  Copyright © 2019 jymengant. All rights reserved.
//

import XCTest
import CocoaLumberjack

class abcm2psTest: XCTestCase {
  
  var homeTestData : String?

    override func setUp() {
      // Put setup code here. This method is called before the invocation of each test method in the class.
      // Provide here absolute path to test data directory
      homeTestData = ABCTestEnvironment().homeTestLocation
      do {
        try preferences.checkPreferences()
      } catch {
        XCTFail()
      }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

  func testSampleEcho() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let echo = SampleEcho()
    // call very simple first
    echo.verysimple()
    let toEcho : String = "This has been echoed"
    let ret = echo.echo(arg: toEcho)
    XCTAssert( ret == toEcho)
  }
  
  
  //
  // simple test returning usages and options
  //
  func testSampleCallMain() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let testFile  = homeTestData! + "./firstsample.abc"
    let abc2PS = ABC2CModules(candidate: URL(fileURLWithPath: testFile))
    // call very simple first
    let ret: ( Int32 , String ) = abc2PS.callAbc2PsMain(arguments: ["-v"])
    XCTAssert( ret.0 == 0)
    XCTAssert( ret.1.count > 0)
    DDLogDebug("\(ret.1)")
  }
  
  //
  // test ps production
  //
  func testSVGEPSProduction() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let testFile  = homeTestData! + "/firstsample.abc"
    let abc2PS = ABC2CModules(candidate: URL(fileURLWithPath: testFile))
    // emit ps for sampleabc
    let ret: ( Int32 , String ) = abc2PS.emitForPrinting(type: .EPS)
    XCTAssert( ret.0 == 0)
    XCTAssert( ret.1.count == 0)
    DDLogDebug("\(ret.1)")
    let ret1: ( Int32 , String ) = abc2PS.emitForPrinting(type: .SVG)
    XCTAssert( ret1.0 == 0)
    XCTAssert( ret1.1.count == 0)
    DDLogDebug("\(ret1.1)")
  }
  
}
