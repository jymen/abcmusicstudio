//
//  ABCScraperTest.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/30/20 week 22.
//  Copyright © 2020 jymengant. All rights reserved.
//

import XCTest

class ABCScraperTest: XCTestCase {
  
  override func setUpWithError() throws {
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDownWithError() throws {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }
  
  func testLoadPage() throws {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let scrapper = ABCSCrapper(site: .TheSession )
    let expectation = XCTestExpectation(description: "Download html page")
    try scrapper.load( webUrl: "https://thesession.org/tunes/search?q=old+gray+goose",
      parser : { html in
        
        expectation.fulfill()
      }
    )
    wait(for: [expectation], timeout: 60.0)
    XCTAssert(scrapper.html != nil)
  }
  
  func testPerformanceExample() throws {
    // This is an example of a performance test case.
    self.measure {
      // Put the code you want to measure the time of here.
    }
  }
  
  
  
}
