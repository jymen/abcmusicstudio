//
//  AudioPlayerTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 11/8/19 week 45.
//  Copyright © 2019 jymengant. All rights reserved.
//

import XCTest

class AudioPlayerTest: XCTestCase {
  
  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }
  
  func testAudioExample() {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    let player = AudioPlayer()
    player.setupAudio()
    let bundle = Bundle(for: type(of: self))
    if let url = bundle.url(forResource: "Intro", withExtension: "mp4", subdirectory: "testdata") {
      player.audioFileURL = url
      player.playTapped()
      sleep(60)
    } else {
      XCTFail()
    }

    
  }
  
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      // Put the code you want to measure the time of here.
    }
  }
  
}
