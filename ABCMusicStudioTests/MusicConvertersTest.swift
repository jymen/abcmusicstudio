//
//  XmlMusicConverterTest.swift
//  ABCMusicStudioTests
//
//  Created by jymengant on 11/27/20 week 48.
//  Copyright © 2020 jymengant. All rights reserved.
//

import XCTest
import CocoaLumberjack

let abcSource = """
    %%vocalfont Bookman-Light 12
    %%wordsfont Bookman-Light 14
    %%composerfont Bookman-Light 12
    %%titlefont Luminari 26

    This file contains 32 Carolan Tunes (#1 - #32).

    You can find more abc tune files at http://www.norbeck.nu/abc/
    I've transcribed them as I have learnt them, which does not necessarily mean
    that I play them that way nowadays. Many of the tunes include variations and
    different versions. If there is a source (S:) or discography (D:) included the
    version transcribed might still not be exactly as that source played the tune,
    since I might have changed the tune around a bit when I learnt it.
    The tunes were learnt from sessions, from friends or from recordings.
    When I've included discography, it's often just a reference to what recordings
    the tune appears on.

    Last updated 18 October 2018.

    (c) Copyright 1997-2018 Henrik Norbeck. This file:
    - May be distributed freely (with restrictions below).
    - May not be used for commercial purposes (such as printing a tune book to sell).
    - This file (or parts of it) may not be made available on a web page for
    download without permission from me.
    - This copyright notice must be kept, except when e-mailing individual tunes.
    - May be printed on paper for personal use.
    - Questions? E-mail: henrik@norbeck.nu
    R:carolan
    C:Turlough O'Carolan (1670-1738)
    Z:id:hn-carolan-%X

    X:1
    T:Planxty Fanny Powers
    T:Fanny Powers
    R:carolan
    C:Turlough O'Carolan (1670-1738)
    Z:id:hn-carolan-1
    M:3/4
    L:1/4
    Q:1/4=160
    K:G
    D |: G2D | G>AB | c2B | A2G | GFE | DFD | F2G | A2c |
    B>AG | Bcd | e2A | A>BG | GFE | DGF | G3 |1 G2D :|2 G2d ||
    |: dB/c/d | dB/c/d | G>BG | GBd | ec/d/e | ec/d/e | A>cA | AcA |
    B>cd | efg | f>ga | d2c | B>AG | DGF | G3 |1 G2d :|2 G2 ||
    P:variations
    D |: G2D | G>AB | c2B | A2G | F>GE | DFD | F2G | A>Bc |
    B>AG | B>cd | e2A | A2G | GFE | DGF | G3 |1 G2D :|2 G>AB/c/ ||
    |: dB/c/d | dB/c/d | G>AG | GBd | ec/d/e | ec/d/e | A>BA | ABc |
    B>cd | e>fg | f>ga | d2c | B>AG | A/B/cF | G3 |1 G>AB/c/ :|2 G2 ||

    - A final comment line

    """


class MusicConvertersTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

  func testPing() throws {
    let converter = MusicConverters()
    let result = converter.ping(echo:"check abcstudioconverter")
    XCTAssert(result.starts(with: "AbcStudioConverter"))
  }

  private func testMusicXml(source: String , ofType: String ) throws {
    let converter = MusicConverters()
    let bundle = Bundle(for: type(of: self))
    let stringPath = bundle.path(forResource: source, ofType: ofType , inDirectory: "testdata")
    
    if let stringPath = stringPath {
      let xmlBuffer = try String(contentsOfFile: stringPath)
      let result = try converter.xmlMusicToABC(xmlStuff:  xmlBuffer)
      XCTAssert(result != nil)
      // print(result!)

    } else {
      XCTAssert(false)
    }
  }
  
  func testXmlMusic2ABC() throws {
    try testMusicXml(source: "carolanTest", ofType: "musicxml")
  }
  
  func testXmlMusicSample2ABC() throws {
    try testMusicXml(source: "TestForXmlMusicConversion", ofType: "musicxml")
  }
  
  func testComplexXmlMusicSample2ABC() throws {
    try testMusicXml(source: "reve", ofType: "musicxml")
  }
  
  func testKerryReel2ABC() throws {
    try testMusicXml(source: "AKerryReel", ofType: "xml")
  }
  
  func testMuseScoreToABC() throws {
    let converter = MusicConverters()
    let bundle = Bundle(for: type(of: self))
    let stringPath = bundle.path(forResource: "SampleMuseScoreLA", ofType: "mscx" , inDirectory: "testdata")
    
    if let stringPath = stringPath {
      let xmlBuffer = try String(contentsOfFile: stringPath)
      let result = try converter.musescoreToABC(musescoreStuff:  xmlBuffer)
      XCTAssert(result != nil)
      print(result!)
    } else {
      XCTAssert(false)
    }
  }
  
  func testReadXmlMusic() throws {
    let converter = MusicConverters()
    let bundle = Bundle(for: type(of: self))
    // async zipped
    var url = bundle.url(forResource: "VideZippe", withExtension: "mxl" , subdirectory: "testdata")
    let expectation = XCTestExpectation(description: "Unzip / ")
    if let url = url {
      converter.read(xmlUrl: url).done({ (data) in
        XCTAssert( data.count > 0)
        expectation.fulfill()
      }).catch({
        error in
        print("FAILURE: \(error)")
        XCTAssert(false)
      })
      wait(for: [expectation], timeout: 100000.0)
    }
    // sync unziped
    url = bundle.url(forResource: "sampleMusicXml", withExtension: "xml" , subdirectory: "testdata")
    if let url = url {
      let expectXml = XCTestExpectation(description: "readXML / ")
      converter.read(xmlUrl: url).done({ (data) in
        XCTAssert( data.count > 0)
        expectXml.fulfill()
      }).catch({
        error in
        print("FAILURE: \(error)")
        XCTAssert(false)
      })
      wait(for: [expectXml], timeout: 100000.0)
    }
  }
  
  func testMuseScoreNote() throws {
    let key = ABCScoreKey(key: 0)
    // B
    let note = MuseScoreNote(pitch: 71, tpc: 19)
    let (strN , octv)  = note.pitch2Note(key)
    XCTAssert(strN == "B")
    XCTAssert(octv == 5)
    let abcN = try note.toABCNote(key: key)
    XCTAssert(abcN == "B")
    // C
    let note1 = MuseScoreNote(pitch: 72, tpc: 14)
    let (strN1 , octv1)  = note1.pitch2Note(key)
    XCTAssert(strN1 == "C")
    XCTAssert(octv1 == 6)
    let abcN1 = try note1.toABCNote(key: key)
    XCTAssert(abcN1 == "c")
    // E
    let note2 = MuseScoreNote(pitch: 76, tpc: 18)
    let (strN2 , octv2)  = note2.pitch2Note(key)
    XCTAssert(strN2 == "E")
    XCTAssert(octv2 == 6)
    let abcN2 = try note2.toABCNote(key: key)
    XCTAssert(abcN2 == "e")
  }
  
  private func testMuseScore( source: String , ext : String) {
    let converter = MusicConverters()
    let bundle = Bundle(for: type(of: self))
    let url = bundle.url(forResource: source, withExtension: ext, subdirectory: "testdata")
    if let url = url {
      let expectation = XCTestExpectation(description: "Unzip / ")
      converter.read(xmlUrl: url).done({
        museXml in
        do {
          let abcStuff = try  converter.musescoreToABC(musescoreStuff: museXml)
           XCTAssert(abcStuff != nil)
           // print("ABCStuff=\(abcStuff!)")
          expectation.fulfill()
        } catch {
          DDLogDebug("MuseScore MultiStaff error : \(error)")
          XCTAssert(false)
        }
      }).catch({
        error in
        print("FAILURE: \(error)")
        XCTAssert(false)
      })
      wait(for: [expectation], timeout: 100000.0)
    }
  }
  
  func testSampleMuseScoreLA() throws {
    testMuseScore(source: "SampleMuseScoreLA",ext: "mscx")
  }
  
  
  func testSampleMuseScore2() throws {
    testMuseScore(source: "Swallowtail_Jig_Maondolin",ext: "mscx")
  }
  
  func testMultiStaffMuseScore() throws {
    testMuseScore(source: "Duet_Violin__Cello",ext: "mscx")
  }

  func testComplexMultiStaffMuseScore() throws {
    testMuseScore(source: "Bach_Minuet_in_G_Major_BWV_Anh._114",ext: "mscx")
  }


  func testABC2xml() throws {
    let converter = MusicConverters()
    do {
      let result = try converter.abcToXmlMusic(abcContent: abcSource)
      XCTAssert(result != nil)
      // print (result!)
    } catch {
      DDLogDebug("ABC2XML error : \(error)")
      XCTAssert(false)
    }
  }


}
