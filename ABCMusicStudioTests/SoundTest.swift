//
//  SoundTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 12/15/19 week 50.
//  Copyright © 2019 jymengant. All rights reserved.
//

import XCTest

class SoundTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTuneData() {
      let tuneData = Sound(
       url: nil,
       image: nil,
       title: "The Old gray goose",
       description: "Exemple de description"
      )
      tuneData.tuneSize = 145210
      tuneData.tunePosition = tuneData.tuneSize / 2
      let relative = tuneData.convertToRelative()
      let absolute = tuneData.convertFromRelative()
      print( "relative :\(relative) absolute : \(absolute)")
      XCTAssert( tuneData.tuneSize / 2 ==  absolute )
      let currentTime = tuneData.getCurrentTime()
      let remainingTime = tuneData.getCurrentTime()
      print( "currentTime :\(currentTime) remainingTime : \(remainingTime)")
    }


}
