//
//  ABCLocalizerTest.swift
//  ABCStudioTests
//
//  Created by jymengant on 6/6/18 week 23.
//  Copyright © 2018 jymengant. All rights reserved.
//

import XCTest

class ABCLocalizerTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
      let localizer = Localizer(resource: "Localizable")
        let translated = localizer.tr("Ttes_Translation_NotFound")
        XCTAssertEqual(translated , "Ttes Translation NotFound" )
    }


}
