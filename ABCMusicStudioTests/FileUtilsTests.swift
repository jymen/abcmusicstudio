//
//  FileUtilsTests.swift
//  ABCStudioTests
//
//  Created by jymengant on 2/23/19 week 8.
//  Copyright © 2019 jymengant. All rights reserved.
//

import XCTest
import CocoaLumberjack

class FileUtilsTests: XCTestCase {
  

    override func setUp() {
      // Put setup code here. This method is called before the invocation of each test method in the class.
      do {
        try preferences.checkPreferences()
      } catch {
        DDLogDebug("error: \(error.localizedDescription)")
        XCTAssert(false)
      }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
  
    func testApplicationDirectory() {
      let fu = FileUtils()
      let appDir = fu.applicationDirectory
      XCTAssert(appDir != nil)
      DDLogDebug("application directory : \(appDir?.absoluteString ?? "NIL")")
    }
  
    func testDatabaseStuff() {
      let fu = FileUtils()
      do {
        let _ = try fu.listDatabases()
      } catch {
        DDLogDebug("error: \(error.localizedDescription)")
        XCTAssert(false)
      }
    }

    func testBuildFolder() {
      let fu = FileUtils()
      let url : URL = URL(fileURLWithPath: "/Users/jymen/Music/atelierChantGallo/Rolland Brou 2019",
                          isDirectory: true)
      let fileUrl : URL = URL(fileURLWithPath: "/Users/jymen/test", isDirectory: false)
      do {
        let f : Folder? = try fu.folderFromFileSystem(url: fileUrl,filter: ".abc" , recurse: true)
        XCTAssert(f == nil)
        let folder : Folder? = try fu.folderFromFileSystem(url: url,filter: ".abc" , recurse: true)
        XCTAssert(folder != nil)

      } catch {
        XCTFail("Error building folder from file System :\(error) ")
      }
    }
  
  func testChangeFName() {
    let url : URL = URL(fileURLWithPath: "/Users/jymen/Development/ABCMusicStudio/ABCMusicStudio/testdata/caden.html",
                        isDirectory: false)
    let fu = FileUtils()
    var exists = fu.existFile(url: url, directory: false)
    XCTAssert(exists == true)
    let renamed = fu.changeFName(url: url)
    exists = fu.existFile(url: renamed, directory: false)
    XCTAssert(exists == false)
  }
  
  func testFileUtils() {
    // test fileUtils module
    let bundle = Bundle(for: type(of: self))
    let fu = FileUtils()
    do {
      let txt1  = try fu.loadTextFile(bundle: bundle, name: "firstsample", ofType: "txt", directory: "testdata" , encoding: .ascii)
      XCTAssert(txt1.0.count > 0)
      let txt2  = try fu.loadTextFile(bundle: bundle, name: "testEncoding", ofType: "abc", directory: "testdata" )
      XCTAssert(txt2.0.count > 0)
      let txt3 = try fu.loadTextFile(bundle: bundle, name: "TuneCollectionTest", ofType: "abc", directory: "testdata" )
      XCTAssert(txt3.0.count > 0)
      
    } catch {
      XCTFail("Error loading test Source from bundle :\(error) ")
    }
  }
  
  func testScandirectories() {
    // test fileUtils module
    let fu = FileUtils()
    let rootUrl = URL(fileURLWithPath: "/Users/jymen/Music/atelierChantGallo")
    do {
      let abcFolder = try fu.folderFromFileSystem(url: rootUrl, filter: ".abc", recurse: true)
      XCTAssert(abcFolder != nil)
    } catch {
      XCTFail("Error scanning directories :\(error) ")
    }
  }
  

}
