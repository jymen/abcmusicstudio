//
//  PreferenceManagerTests.swift
//  ABCStudioTests
//
//  Created by jymengant on 6/13/18 week 24.
//  Copyright © 2018 jymengant. All rights reserved.
//

import XCTest

class PreferenceManagerTests: XCTestCase {

    let prefs : PreferenceManager = PreferenceManager()
  
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testInit() {
      do {
        try prefs.checkPreferences()
        let json : String? = prefs.getRepoName()
        XCTAssert(json != nil)
      } catch {
        let myErr = error as! ABCFailure
        print("UNEXPECTED error : \(String(describing: myErr.message))")
        XCTAssert(false)
      }
    }

}
