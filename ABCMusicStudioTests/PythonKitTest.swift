//
//  PithonKitTest.swift
//  ABCMusicStudioTests
//
//  Created by jymengant on 11/26/20 week 48.
//  Copyright © 2020 jymengant. All rights reserved.
//

import XCTest
import PythonKit


class PithonKitTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
  
  func testSysPython() throws {
    let sys = Python.import("sys")
    print("Python \(sys.version_info.major).\(sys.version_info.minor)")
    print("Python Version: \(sys.version)")
    print("Python Encoding: \(sys.getdefaultencoding().upper())")
  }


  func testAbcToXml() throws {
  }

  func testXml2Abc() throws {
  }


}
