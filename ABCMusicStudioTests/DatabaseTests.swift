//
//  DatabaseTests.swift
//  ABCMusicStudioTests
//
//  Created by jymengant on 3/23/20 week 13.
//  Copyright © 2020 jymengant. All rights reserved.
//

import XCTest
import CocoaLumberjack

class DatabaseTests: XCTestCase {
  var dataStore : ABCTreeJsonDataStore? = nil
  var rootData : Any?
  
  let dataTest : String = """
  {
    "root" : {
      "infos" : {
        "title": "root Folder" ,
        "creation": 573495800.37878299,
        "storageURL": "file:///Users/jymen" ,
        "comments": "An optional comment area",
        "encoding": 1,
      },
     "folders" : [
      {
          "infos" : {
          "title": "Jigs and Slip Jigs" ,
          "creation": 573495800.37878299 ,
          "storageURL": "file:///Users/jymen" ,
          "encoding": 1,
        },
      },
    ],
    "files" : [
      {
        "infos" : {
        "title": "Jigs and Slip Jigs" ,
        "creation": 573495800.37878299 ,
        "storageURL": "file:///Users/jymen" ,
        "encoding": 1,
         },
      },
      {
        "infos" : {
        "title": "locate me" ,
        "creation": 573495800.37878299 ,
        "storageURL": "file:///Users/jymen" ,
        "encoding": 1,
         },
       },
      ],
    },
  }
  """
  
  
  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    do {
      try preferences.checkPreferences()
      dataStore = ABCTreeJsonDataStore()
      rootData = try dataStore!.decode(jsonSource: dataTest)
      XCTAssertNotNil( rootData )
    } catch {
      XCTAssert(false)
    }
    
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }
  
  func testMongo() {
    let mongo = MongoDriver(name: "ABCTestDataBase" )
    var connected = mongo.connect(to: "mongodb://localhost:27000")
    XCTAssert(!connected)
    connected = mongo.connect(to: "mongodb://localhost:27017")
    XCTAssert(connected)
    do {
      try mongo.dropDatabase()
      try mongo.createDatabase()
      try mongo.begin(collection: "Hello")
      let jsonStr = """
        { "hello" : "ABC world" }
        """
      let id = try mongo.insert(db:"ABCTestDataBase",collection: "Hello", jsonStr: jsonStr)
      DDLogDebug("id inserted : \(id)")
      // select ALL
      let cursor = try mongo.select(db: "ABCTestDataBase", collection: "Hello")
      let jsonData = try mongo.fetch(cursor: cursor)
      DDLogDebug("data returned : \(jsonData ?? "NO DATA")")
      try mongo.close(cursor: cursor)
      // select 1
      let selection = "{ \"_id\" : { \"$oid\" : \"\(id)\"}}"
      let cursor1 = try mongo.select(db: "ABCTestDataBase", collection: "Hello",selection: selection)
      let jsonData1 = try mongo.fetch(cursor: cursor1)
      DDLogDebug("data returned : \(jsonData1 ?? "NO DATA")")
      try mongo.close(cursor: cursor1)
      // close colection
      try mongo.end(db: "ABCTestDataBase", collection: "Hello")
      mongo.terminate()
    } catch {
      DDLogDebug("ERROR: \(mongo.dbError ?? "DbError is empty")")
      XCTAssert(false)
    }
  }
  
  func compare(source: Folder , to : Folder ) {
    let sourceLeaf = source.getLeaf()
    let toLeaf = to.getLeaf()
    XCTAssert(sourceLeaf.title == toLeaf.title)
    let srcNbChild = source.folders?.count
    let toNbChild = to.folders?.count
    XCTAssert(srcNbChild == toNbChild)
    let srcNbFChild = source.files?.count
    let toNbFChild = to.files?.count
    XCTAssert(srcNbFChild == toNbFChild)
  }
  
  func testSqlite() {
    let abcTop = rootData as? ABCFolder
    XCTAssertNotNil(abcTop)
    let root = abcTop?.root
    let temp = "/Users/jymen/temp/sqlite"
    let fm = FileManager.default
    var directory = fm.temporaryDirectory
    directory = URL(string: temp)!
    let sqlite = SqliteDriver(dbPath: directory, name: "testDataBase")
    do {
      try sqlite.deleteDatabase() //delete previous
      try sqlite.createDatabase() //create or open
      // try sqlite.dropDataModel() // cleanup first
      // try sqlite.createDataModel()
      // populate sample
      var rootFolder = ABCSqlFolder(folder: root , parent: nil)
      try rootFolder.store(dbDriver: sqlite)
      sqlite.closeDatabase()
      // get sample back
      try sqlite.createDatabase() // open again
      rootFolder = ABCSqlFolder()
      try rootFolder.load(dbDriver: sqlite, id: ABCOid(oid: 0))
      let rFolder = rootFolder.folder
      compare(source:root! , to: rFolder! )
      // update and delete
      rootFolder.folder?.infos.title = "new updated title"
      try rootFolder.update(dbDriver: sqlite)
      rootFolder = ABCSqlFolder()
      try rootFolder.load(dbDriver: sqlite, id: ABCOid(oid: 0))
      XCTAssert(rootFolder.folder?.infos.title == "new updated title")
      // try delete root
      do {
        try rootFolder.delete(dbDriver: sqlite)
        XCTAssert(false) // must thow on root
      } catch {}
      let todelete = rootFolder.folder?.folders![0]
      let fdeleted = ABCSqlFolder(folder: todelete)
      try fdeleted.delete(dbDriver: sqlite)
      sqlite.terminate()
    } catch {
      DDLogDebug("ERROR: \(error)")
      XCTAssert(false)
    }
  }
  
  func testRealmIo() {
    let realmDriver = RealmDriver(name: "testDataBase")
    do {
      let realm = try realmDriver.createDatabase()
    } catch {
      DDLogDebug("ERROR: \(error)")
      XCTAssert(false)
    }
  }
  
  
  func testRepository() {
    let repo = ABCRepository(db: ABCDb(name: "repositoryTest", type: .SqlLite))
    do {
      try repo.mount()
      
      repo.unmount()
    } catch {
      DDLogDebug("ERROR: \(error)")
      XCTAssert(false)
    }
  }
  
  func testImportFromJson() {
    let bundle = Bundle(for: type(of: self))
    let path = bundle.path(forResource: "testcatalogue", ofType: "abcmusicstudio" , inDirectory: "testdata")
    XCTAssert(path != nil)
    let jsonStore = ABCTreeJsonDataStore()
    let fu = FileUtils()
    do {
      let jsonSource = try fu.read(absolute: path!)
      try jsonStore.deserialize(jsonSource)
      let newReq = AbcDbRequestFactory(dbName: "repositoryImportTest", dbType: .SqlLite)
      try newReq.deleteDb()
      try newReq.createDb()
      XCTAssert(jsonStore.data != nil)
      try newReq.dbImport(root: jsonStore.data!.root)
      // Try to read back imported
      // and compare content with original
      let query = AbcDbRequestFactory(dbName: "repositoryImportTest", dbType: .SqlLite)
      query.query = ABCQuery(isFolder: true,  oid: ABCOid(oid: 0))
      query.build(operation: .Load)
      let result: Folder = try AbcJson.decode(jsonSource: try query.exec(), encoding: .utf8)
      XCTAssert(result.folders != nil)
      XCTAssert(jsonStore.data!.root.folders?.count == result.folders!.count)
    } catch {
      print(error)
      XCTFail("Error loading json Source :\(error) ")
    }
  }
  
  func testJsonRequest() {
    let jsonDDL : String =
    """
     {
      "core" :
      {
        "name" : "repositoryTest" ,
        "type" : "SqlLite"
      }
     }
    """
    let jsonRepoTest : String =
    """
    {
      "core": {
        "name" : "repositoryTest" ,
        "type" : "SqlLite"
      },
      "operation" :
      {
        "op" : 0 ,
        "candidate" : {
          "isFolder" : true ,
          "folder" : {
            "infos" :
            {
              "title" : "this is the root folder"
            }
          }
        }
      }
    }
    """
    let request = ABCDbJsonRequest()
    
    let newReq = AbcDbRequestFactory(dbName: "repositoryTest", dbType: .SqlLite)
    newReq.folder = Folder(infos: Leaf(title: "this is the root folder", creation: Date(), id: ABCOid(oid: 0),encoding: .utf8, type: .Group))
    do {
      _ = try request.doRequest(fx: .DeleteDatabase , jsonRepo: jsonDDL )
      _ = try request.doRequest(fx: .CreateDatabase , jsonRepo: jsonDDL )
      do {
        _ = try request.doRequest(fx: .StorageOperation , jsonRepo : "invalid" )
        DDLogDebug("Exception is expected => assert false")
        XCTAssert(false)
      } catch {
        DDLogDebug("expected exception here")
        let json = try request.doRequest(fx: .StorageOperation , jsonRepo: jsonRepoTest)
        let rootOid : ABCOid = try AbcJson.decode(jsonSource: json, encoding: .utf8)
        newReq.folder?.oid = rootOid
        newReq.build(operation: .Delete)
        do {
          try _ = newReq.exec()
        }  catch {
          // root can't be deleted
          newReq.folder = Folder(
            infos:
            Leaf(title: "Jigs folder", creation: Date(),encoding: .utf8, type: .Group)
          )
          newReq.parent = rootOid
          newReq.build(operation: .Add)
          var newOid : ABCOid  = try AbcJson.decode(jsonSource: try newReq.exec(), encoding: .utf8)
          // test deletion on folder
          newReq.folder?.oid = newOid
          newReq.build(operation: .Delete) // should work now
          _  = try newReq.exec()
          // add it again
          newReq.build(operation: .Add)
          newOid = try AbcJson.decode(jsonSource: try newReq.exec(), encoding: .utf8)
          newReq.folder = nil
          let goose = Leaf(title: "the old gray goose", creation: Date(),encoding: .utf8, type: .ABCScore)
          goose.storageURL = URL(string: "file:///Users/jymen/Music/irish%20tunes/fiddle/Sessions/jigs/the%20old%20gray%20goose.abc")
          newReq.file = File(infos: goose)
          newReq.parent = newOid
          newReq.build(operation: .Add)
          newOid = try AbcJson.decode(jsonSource: try newReq.exec(), encoding: .utf8)
          newReq.file?.oid = newOid
          newReq.build(operation: .Delete) // delete goose finally
          let _ = try newReq.exec()
          // add again after deletion
          newReq.build(operation: .Add)
          newOid = try AbcJson.decode(jsonSource: try newReq.exec(), encoding: .utf8)
          newReq.file = nil
          // get file back
          newReq.query = ABCQuery(isFolder: false,  oid: newOid)
          newReq.build(operation: .Load)
          let result: Folder = try AbcJson.decode(jsonSource: try newReq.exec(), encoding: .utf8)
          // get folder back
        }
      }
    } catch {
      DDLogDebug("ERROR: \(error)")
      XCTAssert(false)
    }
    
  }
  
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      // Put the code you want to measure the time of here.
    }
  }
  
}
