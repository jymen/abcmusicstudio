//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"
//: Playground - noun: a place where people can play

import Cocoa

let str1: NSString = str as NSString
let array = [1,2,4,8]
let array1 : NSArray = array as NSArray
let swiftArray : [Int] = array1 as! [Int]
print("test \(str)")
print("test \(swiftArray)")
NSLog("test NSLOG message")
print("after NSLog")


struct Employee  {
  var name: String
  var id: Int
  var favoriteToy: Toy
  
  enum CodingKeys: String, CodingKey {
    case id = "employeeId"
    case name
    case gift
  }
}

extension Employee: Encodable {
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(name, forKey: .name)
    try container.encode(id, forKey: .id)
    try container.encode(favoriteToy.name, forKey: .gift)
  }
}

extension Employee: Decodable {
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    name = try values.decode(String.self, forKey: .name)
    id = try values.decode(Int.self, forKey: .id)
    let gift = try values.decode(String.self, forKey: .gift)
    favoriteToy = Toy(name: gift)
  }
}

struct Toy: Codable {
  var name: String
}
let toy1 = Toy(name: "Teddy Bear");
let employee1 = Employee(name: "John Appleseed", id: 7, favoriteToy: toy1)
let jsonEncoder = JSONEncoder()
let jsonData = try jsonEncoder.encode(employee1)
let jsonString = String(data: jsonData, encoding: .utf8)
print(jsonString!)
let jsonDecoder = JSONDecoder()
let employee2 = try jsonDecoder.decode(Employee.self, from: jsonData)

let myJson = """
{
 "title": "The Old Gray Goose" ,
 "creation": "31/12/2017",
 "storageURL": "file:///Users/jymen"
}
""".data(using: .utf8)!

let myAbc = """
{
   "root" : {
     "infos" : {
        "title": "root Folder" ,
        "creation": "2017-11-16" ,
        "storageURL": "file:///Users/jymen" ,
     },
     "folders" : [
       {
         "infos" : {
            "title": "Jigs and Slip Jigs" ,
            "creation": "2017-11-16" ,
            "storageURL": "file:///Users/jymen" ,
         },
       },
     ],
     "files" : [
        {
          "file" : {
           "title": "Jigs and Slip Jigs" ,
           "creation": "2017-11-16" ,
           "storageURL": "file:///Users/jymen" ,
          },
       },
     ],
   },
}
""".data(using: .utf8)!

struct Leaf : Codable {
  let title: String
  let creation: String
  let storageURL : URL
}

struct File : Codable {
  let file: Leaf
}

struct Folder : Codable {
  let infos: Leaf
  let folders : [Folder]?
  let files : [File]?
}

struct ABCFolder : Codable {
  let root : Folder
}

print("toto")
let decoder = JSONDecoder()
do {
  let decoded1 = try decoder.decode(ABCFolder.self, from: myAbc)
  let decoded = try decoder.decode(Leaf.self, from: myJson)
  print(decoded)
  print(decoded1)
} catch {
  print("SEVERE JSON PARSE ERROR : \(error)")
}
print("titi")




