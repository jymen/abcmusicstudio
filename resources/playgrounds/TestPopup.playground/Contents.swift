//: A Cocoa based Playground to present user interface

import AppKit
import PlaygroundSupport

let nibFile = NSNib.Name("MyTest")
var topLevelObjects : NSArray?

Bundle.main.loadNibNamed(nibFile, owner:nil, topLevelObjects: &topLevelObjects)
let views = (topLevelObjects as! Array<Any>).filter { $0 is NSView }

// Present the view in Playground
PlaygroundPage.current.liveView = views[0] as! NSView


class MessagePopover : NSView {
  
  let msgArea : NSTextField = NSTextField()
  
  
  init() {
    super.init(frame: NSMakeRect(5, 5, 200, 20) )
    msgArea.backgroundColor = .red
    self.addSubview(msgArea)
  }
  
  required init?(coder decoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func info(_ msg : String ) {
    msgArea.stringValue = msg
  }
  
}


class MessagePopoverController : NSViewController {
  
  var popOver : MessagePopover! =  MessagePopover()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    popOver = MessagePopover()
  }
  
  func setInfo( msg: String ) {
    popOver.info(msg)
  }
  
  
}

func showLittlePopoverWithMessage(sender: AnyObject, message: String) {
  let controller = NSViewController()
  controller.view = NSView(frame: CGRect(x: CGFloat(10), y: CGFloat(10), width: CGFloat(100), height: CGFloat(50)))
  
  let popover = NSPopover()

  popover.contentViewController = controller
  //popover.contentSize = controller.view.frame.size
  popover.contentSize = NSSize(width: 20, height: 20) // Or whatever size you want, perhaps based on the size of the content controller

  
  popover.behavior = .applicationDefined
  popover.animates = true
  
  // let txt = NSTextField(frame: NSMakeRect(5,5,50,22))
  // let txt = NSTextField(frame: NSMakeRect(100,50,50,22))
  // let txt = NSTextField(frame: controller.view.frame)
  //txt.stringValue = message
  //txt.textColor = NSColor.white.withAlphaComponent(0.95)
  //controller.view.addSubview(txt)
  //txt.sizeToFit()
  popover.show(relativeTo: sender.bounds, of: sender as! NSView, preferredEdge: NSRectEdge.maxY)
}


func toto() {
let containerView = NSView( frame: CGRect(x:0,y:0,width:512,height:512))
//containerView.wantsLayer = true
//containerView.layer!.backgroundColor = .white
// containerView.backgroundColor = .white
let popup  = MessagePopover()
popup.info("un très long message")
var newBut = NSButton(frame: NSRect(x: 10, y: 300, width: 150, height: 30))
newBut.title = "press me!"
let myTextField = NSTextField(frame: NSMakeRect(10,10,100,20))
myTextField.stringValue = "Test de remplissage de texte"
myTextField.isEditable = false
myTextField.textColor = .orange
myTextField.isBordered = false

PlaygroundPage.current.liveView = containerView
containerView.addSubview(newBut)
// containerView.addSubview(myTextField)
// showLittlePopoverWithMessage(sender:newBut ,message:"test message")

let controller = NSViewController()
let myview = NSView(frame: CGRect(x: CGFloat(10), y: CGFloat(10), width: CGFloat(100), height: CGFloat(50)))
myview.wantsLayer = true
myview.layer!.backgroundColor = .white

controller.view = myview
let popover = NSPopover()

popover.contentViewController = controller
//popover.contentSize = controller.view.frame.size
popover.contentSize = NSSize(width: 200, height: 200) // Or whatever size you want, perhaps based on the size of the content controller


popover.behavior = .applicationDefined
popover.animates = true

// let txt = NSTextField(frame: NSMakeRect(5,5,50,22))
// let txt = NSTextField(frame: NSMakeRect(100,50,50,22))
// let txt = NSTextField(frame: controller.view.frame)
//txt.stringValue = message
//txt.textColor = NSColor.white.withAlphaComponent(0.95)
//controller.view.addSubview(txt)
//txt.sizeToFit()
popover.show(relativeTo: newBut.bounds, of: newBut, preferredEdge: NSRectEdge.maxY)
}

