let primaryColor = UIColor(red: 0.81, green: 0.08, blue: 0.16, alpha: 1.0);
let primaryLightColor = UIColor(red: 1.00, green: 0.34, blue: 0.33, alpha: 1.0);
let primaryDarkColor = UIColor(red: 0.58, green: 0.00, blue: 0.00, alpha: 1.0);
let primaryTextColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.0);
let secondaryColor = UIColor(red: 0.77, green: 0.81, blue: 0.76, alpha: 1.0);
let secondaryLightColor = UIColor(red: 0.97, green: 1.00, blue: 0.96, alpha: 1.0);
let secondaryDarkColor = UIColor(red: 0.58, green: 0.62, blue: 0.57, alpha: 1.0);
let secondaryTextColor = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.0);