//: [Previous](@previous)

import AudioKitPlaygrounds
import AudioKit
import AudioKitUI
import PlaygroundSupport

// 1. Create an oscillator
let oscillator = AKOscillator()
oscillator.rampDuration = 0.2
oscillator.frequency = 500
// 2. Start the AudioKit 'engine'
AudioKit.output = oscillator
AudioKit.start()

// 3. Start the oscillator
oscillator.start()

AKPlaygroundLoop(every: 0.5) {
  oscillator.frequency =
    oscillator.frequency == 500 ? 100 : 500
}

//PlaygroundPage.current.needsIndefiniteExecution = true
//: [Next](@next)
