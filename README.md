#  ABCMusicStudio is an ABC score GUITOOL for MAC OSX And IOS

MacOsx version has been made available first with following features :

# 1.0.3 Available features

- Full support of MacOsx latest Cocoa features 
- Light and Dark themes 
- Multilingual support : English and French 
- MIDI support 
- ABC Editor with customizable syntax coloring 
- import of complementary information MP3 and Images stuff  
- Direct read of XmlMusic and MuseScore files and immediate conversion to ABC format 
- Score transposition 
- Multiple ABC repositories (stored in independant JSON format)

# Features under development 

- Database storage for ABC repositories 
- IOS transfert to IPAD / IPHONE of ABC
- Internet ABC Tune search engine for direct import 


Android version to be developped later
