//
//  mftext.h
//  Abc2Midi
//
//  Created by jymengant on 10/4/19 week 40.
//  Copyright © 2019 jymengant. All rights reserved.
//

#ifndef mftext_h
#define mftext_h

void initfuncs(void);
void midifile(void);

FILE * efopen(char *name, char*mode) ;
void abc2MidiError(char *s) ;


#endif /* mftext_h */
