//
//  nullfuncs.h
//  ABCStudio
//
//  Created by jymengant on 10/16/19 week 42.
//  Copyright © 2019 jymengant. All rights reserved.
//

#ifndef nullfuncs_h
#define nullfuncs_h

/* Functions to be called while processing the MIDI file. */
int (*Mf_getc)(void) = NULLFUNC;
void (*Mf_error)(char *s) = NULLFUNC;
void (*Mf_header)(int format,int ntrks, int division) = NULLFUNC;
void (*Mf_trackstart)(void) = NULLFUNC;
void (*Mf_trackend)(void)= NULLFUNC;
void (*Mf_noteon)(int chan, int c1, int c2) = NULLFUNC;
void (*Mf_noteoff)(int chan, int c1, int c2) = NULLFUNC;
void (*Mf_pressure)(int chan, int c1, int c2) = NULLFUNC;
void (*Mf_parameter)(int chan, int c1, int c2) = NULLFUNC;
void (*Mf_pitchbend)(int chan, int c1, int c2) = NULLFUNC;
void (*Mf_program)(int chan, int c1) = NULLFUNC;
void (*Mf_chanpressure)(int chan, int c1) = NULLFUNC;
void (*Mf_sysex)(int l , char * s) = NULLFUNC;
void (*Mf_arbitrary)(int l , char * s) = NULLFUNC;
void (*Mf_metamisc)(int type , int len , char * s) = NULLFUNC;
void (*Mf_seqnum)(int num) = NULLFUNC;
void (*Mf_eot)(void) = NULLFUNC;
void (*Mf_smpte)(int hr,int mn,int se,int fr,int ff) = NULLFUNC;
void (*Mf_tempo)(long t) = NULLFUNC;
void (*Mf_timesig)(int nn,int dd,int cc,int bb) = NULLFUNC;
void (*Mf_keysig)(int sf, int mi) = NULLFUNC;
void (*Mf_seqspecific)(int type, int leng) = NULLFUNC;
void (*Mf_text)(int type , int len , char * s) = NULLFUNC;

/* Functions to implement in order to write a MIDI file */
int (*Mf_putc)( char c) = NULLFUNC;
long (*Mf_writetrack)(int trck) = NULLFUNC;
int (*Mf_writetempotrack)(int trck) = NULLFUNC;



#endif /* nullfuncs_h */
