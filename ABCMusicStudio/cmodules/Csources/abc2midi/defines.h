//
//  defines.h
//  Abc2Midi
//
//  Created by jymengant on 10/12/19 week 41.
//  Copyright © 2019 jymengant. All rights reserved.
//
#ifndef defines_h
#define defines_h

#ifndef NULLPTR
#define NULLPTR ((void *)0)
#endif

#define NULLFUNC NULLPTR
#define ANSILIBS

#define PUBLIC
#define PRIVATE static

#endif /* defines_h */
