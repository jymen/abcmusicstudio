//
//  store.h
//  Abc2Midi
//
//  Created by jymengant on 10/13/19 week 41.
//  Copyright © 2019 jymengant. All rights reserved.
//

#ifndef store_h
#define store_h

#define VERSION "3.97 June 10 2017 abc2midi"

/* enables reading V: indication in header */
#define XTEN1 1

#include "abc.h"
#include "parseabc.h"
#include "parser2.h"
#include "midifile.h"
#include "genmidi.h"

#include <stdio.h>
#include <math.h>

#ifdef __MWERKS__
#define __MACINTOSH__ 1
#endif /* __MWERKS__ */

#ifdef __MACINTOSH__
int setOutFileCreator(char *fileName,unsigned long theType,
                      unsigned long theCreator);
#endif /* __MACINTOSH__ */
/* define USE_INDEX if your C libraries have index() instead of strchr() */
#ifdef USE_INDEX
#define strchr index
#endif

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/*int snprintf(char *str, size_t size, const char *format, ...);*/
int load_stress_parameters(char *);

#define MAXLINE 500
#define INITTEXTS 20
#define INITWORDS 20
#define MAXCHANS 16

extern long writetrack(int xtrack);
extern void init_drum_map(void);
extern void init_stresspat( void );


#endif /* store_h */
