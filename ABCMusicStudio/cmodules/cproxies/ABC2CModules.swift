//
//  ABC2Ps.swift
//  ABCStudio
//
//  Created by jymengant on 8/29/19 week 35.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation

enum PrintingType {
  case EPS
  case SVG
}

class ABC2CModules {
  
  let candidate : URL
  var tuneList : [String]? = nil
  var outUrl : URL? = nil
  
  
  init( candidate : URL) {
    self.candidate = candidate
  }
  
  
  private func withArrayOfCStrings<R>(
    _ args: [String],
    _ body: ([UnsafeMutablePointer<CChar>?]) -> R
    ) -> R {
    var cStrings = args.map { strdup($0) }
    cStrings.append(nil)
    defer {
      cStrings.forEach { free($0) }
    }
    return body(cStrings)
  }

  //
  // this mimics the call to abcm2ps main utility program
  //
  func callAbc2PsMain (arguments: [String]) -> (Int32 , String) {
    let count : Int32 = Int32(arguments.count)
    return withArrayOfCStrings(arguments) {
      argv in
      // returned msgArea
      let ptrMessage : UnsafeMutablePointer<CChar>? = UnsafeMutablePointer<CChar>.allocate(capacity: 4096)
      ptrMessage!.initialize(to:0)
      let retval = myAbc2Psmain(count, argv , ptrMessage  )
      let message = String(cString: ptrMessage!)
      return (retval , message)
    }
  }
  
  //
  // XHTML + SVG / EPS emitting from abc score
  //
  func emitForPrinting(type: PrintingType ,  dest : URL? = nil ) -> (Int32 , String) {
    let printDir = preferences.printPath
    var sType = "-X"
    var sFileType = ".html"
    if ( type == .EPS ) {
      sType = "-E"
      sFileType = ".eps"
    }
    var outFile = printDir!.path + "/out" + sFileType
    if let dest = dest {
      outFile = dest.path + "/out" + sFileType
    }
    self.outUrl = URL(fileURLWithPath: outFile)
    var arguments = [
      "unused_old_pgm_name" ,
      sType,
      self.candidate.path ,
      "-O" ,
      outFile
    ]
    if let tuneList = self.tuneList {
      arguments.append("-e")
      var tunes = ""
      var comma = false
      for tune in tuneList {
        if ( comma ) {
          tunes += ","
        }
        tunes += String(tune)
        comma = true
      }
      arguments.append(tunes)
    }
    return callAbc2PsMain(arguments: arguments)
  }
  
}
