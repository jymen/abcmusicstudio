//
//  SampleEcho.swift
//  ABCStudio
//
//  Created by jymengant on 5/26/18 week 21.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation

class SampleEcho {
  
  init() {
  }
  
  func verysimple() {
    verySimple()
  }
  
  /**
    just wrap C echo function
  */
  func  echo( arg : String ) -> String {
    // let cArg : UnsafeMutablePointer<Int8>! = arg.
    let cstr = (arg as NSString).utf8String
    // CALL C function
    let result : UnsafePointer<Int8> = sampleEcho(cstr) ;
    let ret = String(cString: result)
    return ret ;
  }
  
}
