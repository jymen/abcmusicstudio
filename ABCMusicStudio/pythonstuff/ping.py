#
# this module is a sample used for check
#
class Ping:
    """Sample ping"""
    # Performance tuning: we construct a *lot* of these, so keep this
    # constructor as small and fast as possible
    def __init__( self ):
      return

    def ping(self, msg ):
      return "received :" + msg    
