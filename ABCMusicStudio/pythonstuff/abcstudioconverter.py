#
# Tiny wrapper around ABC2cml / Xml2Abc
#

import sys , types
_VERSION_ = '1.0.0'

PY_3 = sys.version.startswith('3')
if PY_3:
  from io import StringIO
  uni_type = str
else:
  from io import BytesIO as StringIO
  uni_type = types.UnicodeType

class ConverterOptions :
    """ Option class for xml2abc """
    def __init__( self ):
      self.c = 0       # set credit text filter to C
      self.b = 5       # BPL: max number of bars per line 
      self.d = 0       # set L:1/D", default=0, metavar='D'
      self.p = ''      # pageformat PFMT (cm) = scale, pageheight, pagewidth, leftmargin, rightmargin, topmargin, botmargin
      self.i = None    # read xml file from standard input
      self.stm = False # translate stem directions
      self.j = None    # switch for compatibility with javascript version
      self.m = 0       # 0 -> no %%MIDI, 1 -> minimal %%MIDI, 2-> all %%MIDI
      self.o = ''      # store abc files in DIR
      self.n  = 0      # CPL: max number of characters per line (default 100)
      self.v1 = None   # start-stop directions allways to first voice of staff
      self.s = None    # shift node heads 3 units left in a tab staff
      self.u = None    # unfold simple repeats
      self.t = None    # translate perc- and tab-staff to ABC code with %%map, %%voicemap
      self.v = 0       # set volta typesetting behaviour to V
      self.ped = True  # skip all pedal directions
      self.x = None    # output no line breaks


class AbcStudioConverter:
    """ ABC Studio front end class to xml2abc/abc2xml """
    def __init__( self ):
      print("AbcStudioConverter : " +_VERSION_ + " initing")
      return

    def ping(self, msg ):
      return "AbcStudioConverter : " +_VERSION_ + " received : " + msg    

    def _decodeInput ( self,data_string):
      try:        
        enc = 'utf-8';   unicode_string = data_string.decode (enc)
      except:
        try:    
          enc = 'latin-1'; unicode_string = data_string.decode (enc)
        except: 
          return ( None , 'data not encoded in utf-8 nor in latin-1')
      return ( unicode_string , None )

    def _readfile (self, fobj):
      try:
        encoded_data = fobj.read ()
        fobj.close ()
        return (encoded_data , None) if type (encoded_data) == uni_type else self._decodeInput (encoded_data)
      except Exception as e:
        return None , repr(e)


    def _checkInput( self, fCandidate , ftype ) :
      import os
      from zipfile import ZipFile 
      fobj = None
      _ , ext = os.path.splitext (fCandidate)
      if ext.lower () not in ftype:
        return ( (None , 'input file %s, it should have extension %s' % fCandidate , ftype) )
      if os.path.isdir (fCandidate):
        return ( (None , 'argument is directory %s. Only files are accepted' % fCandidate))
      elif ext.lower () == '.mxl':        # extract .xml file from .mxl file
        z = ZipFile(fCandidate)
        for n in z.namelist():          # assume there is always an xml file in a mxl archive !!
          if (n[:4] != 'META') and (n[-4:].lower() == '.xml'):
            fobj = z.open (n)
            break   # assume only one MusicXML file per archive
        else:
            fobj = open (fCandidate, 'rb') 
      else:
        # open regular xml file
        fobj = open (fCandidate, 'rb') 
      return ( fobj , None )

    def toXML(self , candidate , isBuffer ) :
      import abc2xml as converter
      error = None
      inputAbc = None
      if not isBuffer :
        fObj , error = self._checkInput(candidate, ('.abc'))
        if fObj != None :
          inputAbc , error = self._readfile(fObj)
      else : 
        inputAbc = candidate
      if error != None :
        return ( None , error)
      returned = converter.getXmlScores(inputAbc)
      return returned , None

    def toABC( self , buffer ) :
      import xml2abc as converter
      # old_stdout = sys.stdout
      # sys.stdout = mystdout = StringIO()
      # setup options
      options = ConverterOptions()
      parser = converter.Parser(options)  

      converter.abcOut = converter.ABCoutput ('none.abc', '', 0 , options)
      parser.debugParse(buffer)
      # abcConversion = mystdout.getvalue()
      # sys.stdout = old_stdout
      # return abcConversion , None
      return "done" , None
#----------------
# Main Program
#----------------
if __name__ == '__main__':
  """ just for a quick testing """
  import io
  converter = AbcStudioConverter()
  f = io.open("carolanTest.musicxml", mode="r", encoding="utf-8")
  xmlBuffer = f.read()
  returned , error = converter.toABC(xmlBuffer)
  if error != None:
    print("ERROR %s" , error)
    sys.exit(12)
  returned , error = converter.toXML(returned,True)  
  if error != None:
    print("ERROR %s" , error)
    sys.exit(12)
  print (returned)
