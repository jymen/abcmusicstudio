//
//  AudioPlayer.swift
//  ABCStudio
//
//  Created by jymengant on 11/8/19 week 45.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation
import AVFoundation
import Combine
import CocoaLumberjack


class AudioPlayer {
  
 
  // for external communication purposes
  // @Published var playing : Bool = false
  
  var engine = AVAudioEngine()
  var player = AVAudioPlayerNode()
  var rateEffect = AVAudioUnitTimePitch()

  var audioBuffer: AVAudioPCMBuffer?

  // MARK: other properties
  var audioFormat: AVAudioFormat?
  var audioSampleRate: Float = 0
  var audioLengthSeconds: Double = 0
  var audioLengthSamples: AVAudioFramePosition = 0
  var needsFileScheduled = true
  
  var updater : DisplayLink?
  
  var currentFrame: AVAudioFramePosition {
    guard let lastRenderTime = player.lastRenderTime,
      let playerTime = player.playerTime(forNodeTime: lastRenderTime) else {
        return 0
    }

    return playerTime.sampleTime
  }
  
  var seekFrame: AVAudioFramePosition = 0
  var currentPosition: AVAudioFramePosition = 0
  var progressBarPos : Double = 0.0
  let pauseImageHeight: Float = 26.0
  let minDb: Float = -80.0

  var playerData : PlayerObserver?
  var lastSliderPos : Double?
  
  init( observer : PlayerObserver? = nil ) {
    self.playerData = observer
  }
  
  
  func seek(to time: Float) {
      guard let audioFile = audioFile,
        let _ = updater else {
        return
      }
      player.stop()

      // seekFrame = AVAudioFramePosition(time * audioSampleRate)
      seekFrame = AVAudioFramePosition(time)
      seekFrame = max(seekFrame, 0)
      seekFrame = min(seekFrame, audioLengthSamples)
      let lengthSamples = Float(audioLengthSamples) - Float(seekFrame)
      currentPosition = seekFrame
    
    
      player.scheduleSegment(audioFile, startingFrame: seekFrame, frameCount: AVAudioFrameCount(lengthSamples), at: nil, completionHandler:nil
        //{self.player.pause()}
      )
      self.needsFileScheduled = false // prevent positoin reset
      //player.play()
  }
  
  private func resetSlider( position : Double ) {
    if let sound = playerData as? Sound{
      // sound.sliderValue =  ( Double(position) / Double(audioLengthSamples) ) * 100
      sound.setTunePosition(offset: Double(position) )
    }
  }
  
  private func  endOfRecord() {
    self.stop()
    updater?.pause()
    disconnectVolumeTap()
    resetSlider(position:0)
    self.needsFileScheduled = true
  }
  
  func cancelCurrentAudio() {
    endOfRecord()
  }
  
  private func updateUI() {
    if let sound = playerData as? Sound {
      currentPosition = currentFrame + seekFrame
      currentPosition = max(currentPosition, 0)
      currentPosition = min(currentPosition, audioLengthSamples)
      
      sound.setTunePosition(offset: Double(currentPosition) )
      print( "current position :  \(currentPosition) of \(audioLengthSamples)")

      if currentPosition >= audioLengthSamples {
        endOfRecord()
      }
    }
  }
  

  //
  // compute VuMeter data
  //
  private func scaledPower(power: Float) -> Float {
    // check power is not Nan
    guard power.isFinite else { return 0.0 }

    if power < minDb {
      return 0.0
    } else if power >= 1.0 {
      return 1.0
    } else {
      return (abs(minDb) - abs(power)) / abs(minDb)
    }
  }
  
  func initUIUpdater() throws {
    updater = DisplayLink()
    guard  let updater = updater else {
      throw ABCFailure(message: "AUDIO Gui updater DisplayLink init failure", kind: .AUDIOFailure)
    }
    // inited => populate callback
    updater.callback = updateUI
  }
  
  private func checkUiUpdater() {
    if let _ = updater {
      return
    }
    do {
      try initUIUpdater()
    } catch let error as ABCFailure{
      if ModalAlert().show(question: "Sound init error", text: "\(String(describing: error.message))", style: .Critical){}
    } catch {}
  }


  var audioFile: AVAudioFile? {
    didSet {
      if let audioFile = audioFile {
        if let sound = playerData {
          audioLengthSamples = audioFile.length
          sound.tuneSize = Double(audioLengthSamples)
          audioFormat = audioFile.processingFormat
          audioSampleRate = Float(audioFormat?.sampleRate ?? 44100)
          audioLengthSeconds = Double(audioLengthSamples) / Double(audioSampleRate)
          sound.duration = audioLengthSeconds
        }
      }
    }
  }
  
  var audioFileURL: URL? {
    didSet {
      if let audioFileURL = audioFileURL {
        print("new audio file selected : \(audioFileURL.absoluteString)")
        audioFile = try? AVAudioFile(forReading: audioFileURL)
        // scheule new audio file
        scheduleAudioFile()
        // seek to beginning
        seek(to: 0)
      }
    }
  }

  //
  // check for recording avalable for current score
  //
  func isRecordAvailable() -> Bool  {
    if let _  =  audioFileURL {
      return true
    }
    return false
  }
  
  func setupAudio() {
    engine.attach(player)
    engine.attach(rateEffect)
    engine.connect(player, to: rateEffect, format: audioFormat)
    engine.connect(rateEffect, to: engine.mainMixerNode, format: audioFormat)

    engine.prepare()

    do {
      try engine.start()
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  func scheduleAudioFile() {
    guard let audioFile = audioFile else { return }
    print ( "AUDIOFILE scheduled")
    seekFrame = 0
    player.scheduleFile(audioFile, at: nil) { [weak self] in
      self?.needsFileScheduled = true
    }
  }
  
  func connectVolumeTap() {
    let format = engine.mainMixerNode.outputFormat(forBus: 0)
    engine.mainMixerNode.installTap(onBus: 0, bufferSize: 1024, format: format) { buffer, when in

      guard let channelData = buffer.floatChannelData,
        let _ = self.updater else {
          return
      }

      let channelDataValue = channelData.pointee
      let channelDataValueArray = stride(from: 0,
                                         to: Int(buffer.frameLength),
                                         by: buffer.stride).map{ channelDataValue[$0] }
      let map = channelDataValueArray.map{ $0 * $0 }.reduce(0, +)
      let rms = sqrt(map / Float(buffer.frameLength))
      let avgPower = 20 * log10(rms)
      
      DispatchQueue.main.async {
        if let playerData = self.playerData as? Sound {
           playerData.meterLevel = self.scaledPower(power: avgPower)
        }
      }
    }
  }

  
  func disconnectVolumeTap() {
    engine.mainMixerNode.removeTap(onBus: 0)
    if let playerData = self.playerData as? Sound {
      playerData.meterLevel = 0
    }
  }

  //
  // update soundSpeed
  //
  func speedhasChanged(newSpeed: Float) {
    print("new speed : \(newSpeed)")
    rateEffect.rate = newSpeed
  }
  
  func stop() {
    player.stop()
    if let sound = playerData as? Sound {
      sound.isPlaying = false
    }
  }

  func isPlaying() -> Bool {
    return player.isPlaying
  }
  
  func stopCurrentPlay() {
    if player.isPlaying {
      // stop UI regresh
      updater?.pause()
      player.pause()
    }
  }

  func playTapped() {
    checkUiUpdater()
    if player.isPlaying {
      stopCurrentPlay()
    } else {
      if needsFileScheduled {
        needsFileScheduled = false
        scheduleAudioFile()
      }
      // start UI refresh
      player.play()
      updater?.start()

    }
  }
}
