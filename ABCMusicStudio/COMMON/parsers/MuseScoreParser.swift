//
//  MusicScoreParser.swift
//  ABCMusicStudio
//
//  Created by jymengant on 12/6/20 week 49.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import SwiftyXMLParser
import CocoaLumberjack

private let MUSESCORE = "museScore"
private let MUSESCOREVERSION = "version"
private let MSCVERSION = "mscVersion"
private let MUSESCORE_SCORE = "Score"
private let MUSESCORE_DIVISION = "Division"
private let MUSESCORE_METATAG = "metaTag"
private let MUSESCORE_NAME = "name"
private let MUSESCORE_ARRANGER = "arranger"
private let MUSESCORE_COMPOSER = "composer"
private let MUSESCORE_COMPOSER1 = "Composer"
private let MUSESCORE_COPYRIGHTS = "copyright"
private let MUSESCORE_CREATIONDATE = "creationDate"
private let MUSESCORE_LYRICIST = "lyricist"
private let MUSESCORE_MOVEMENTNUMBER = "movementNumber"
private let MUSESCORE_MOVEMENTTITLE = "movementTitle"
private let MUSESCORE_PLATFORM = "platform"
private let MUSESCORE_POET = "poet"
private let MUSESCORE_SOURCE = "source"
private let MUSESCORE_TRANSLATOR = "translator"
private let MUSESCORE_WORKNUMBER = "workNumber"
private let MUSESCORE_WORKTITLE = "workTitle"
private let MUSESCORE_STAFF = "Staff"
private let MUSESCORE_STAFFTYPE = "StaffType"
private let MUSESCORE_GROUP = "group"
private let MUSESCORE_TABLATURE = "tablature"
private let MUSESCORE_LID = "lid"
private let MUSESCORE_SLUR = "Slur"
private let MUSESCORE_FRET = "fret"
private let MUSESCORE_STRING = "string"
private let MUSESCORE_ACCIACCATURA = "acciaccatura"
private let MUSESCORE_LINKEDMAIN = "linkedMain"
private let MUSESCORE_NUMBER = "number"
private let MUSESCORE_VBOX = "VBox"
private let MUSESCORE_PART = "Part"
private let MUSESCORE_ID = "id"
private let MUSESCORE_TRACKNAME = "trackName"
private let MUSESCORE_INSTRUMENT = "Instrument"
private let MUSESCORE_LONGNAME = "longName"
private let MUSESCORE_SHORTNAME = "shortName"
private let MUSESCORE_HEIGHT = "height"
private let MUSESCORE_TEXT = "Text"
private let MUSESCORE_TEXT1 = "text"
private let MUSESCORE_STYLE = "style"
private let MUSESCORE_MEASURE = "Measure"
private let MUSESCORE_STARTREPEAT = "startRepeat"
private let MUSESCORE_ENDREPEAT = "endRepeat"
private let MUSESCORE_VOICE = "voice"
private let MUSESCORE_TIMESIG = "TimeSig"
private let MUSESCORE_KEYSIG = "KeySig"
private let MUSESCORE_CLEFSIG = "Clef"
private let MUSESCORE_CLEFTYPE = "concertClefType"
private let MUSESCORE_ACCIDENTAL = "Accidental"
private let MUSESCORE_SIGN = "sigN"
private let MUSESCORE_SIGD = "sigD"
private let MUSESCORE_CHORD = "Chord"
private let MUSESCORE_DURATIONTYPE = "durationType"
private let MUSESCORE_NOTE = "Note"
private let MUSESCORE_PITCH = "pitch"
private let MUSESCORE_TPC = "tpc"
private let MUSESCORE_REST = "Rest"
private let MUSESCORE_DURATION = "duration"
private let MUSESCORE_TITLE = "Title"
private let MUSESCORE_SUBTITLE = "Subtitle"
private let MUSESCORE_DEFAULTCLE = "defaultClef"
private let MUSESCORE_BEAMMODE = "BeamMode"
private let MUSESCORE_BEGIN = "begin"
private let MUSESCORE_NO = "no"
private let MUSESCORE_AUTOPLACE = "autoplace"
private let MUSESCORE_DOTS = "dots"
private let MUSESCORE_SPANNER = "Spanner"
private let MUSESCORE_ORIGINALFORMAT = "originalFormat"
private let MUSESCORE_ARTICULATION = "Articulation"
private let ACCIDENTAL_SUBTYPE = "subtype"
private let ACCIDENTAL_SHARP = "accidentalSharp"
private let ACCIDENTAL_FLAT = "accidentalFlat"
private let MUSESCORE_VISIBLE = "visible"
private let MUSESCORE_STEMDIRECTION = "StemDirection"

struct MuseScoreNote {
  var pitch : Int
  var tpc : Int
  var alteration : ABCAlteration?
  
  private func checkAltered(note: Int , on : ABCScoreKey) -> String {
    let notes = [ "A", "_B" , "B" , "C" , "^C" ,"D",  "_E" , "E" , "F","^F" ,"G", "^G"]
    switch note {
    case 1 : // Sib La#
      if ( on.key > 4 ) {
        return "A"
      }
      if ( on.key < 0 ) {
        return "B"
      }
    case 4 : // Reb do#
      if ( on.key > 1 ) {
        return "C"
      }
      if ( on.key < -3 ) {
        return "B"
      }
    case 6 : // Mib Re#
      if ( on.key > 3 ) {
        return "D"
      }
      if ( on.key < -1 ) {
        return "E"
      }
    case 9 : // Solb F#
      if ( on.key > 0 ) {
        return "F"
      }
      if ( on.key < -4 ) {
        return "G"
      }
    case 11 : // Sol# Lab
      if ( on.key > 2 ) {
        return "G"
      }
      if ( on.key < -2 ) {
        return "A"
      }
    default :
      break
    }
    return notes[note]
  }
  
  func pitch2Note(_ key: ABCScoreKey) -> ( String , Int){
    let octv : Int = (pitch / 12) 
    let noteNumber = pitch - 21
    let convertedNote : Int = noteNumber % 12
    let note : String  = checkAltered(note: convertedNote, on: key)
    return (note , octv)
  }
  
  func toABCNote(key: ABCScoreKey) throws -> String {
    let (strn , octv) = pitch2Note(key)
    var returned = ""
    switch ( octv ) {
    case 1 :
      returned = "\(strn),,,,"
      break
    case 2 :
      returned = "\(strn),,,"
      break
    case 3 :
      returned = "\(strn),,"
      break
    case 4 :
      returned = "\(strn),"
      break
    case 5 :
      returned = strn
      break
    case 6 :
      returned = strn.lowercased()
      break
    case 7 :
      returned = "\(strn.lowercased())'"
      break
    default :
      throw ABCFailure(message:"MuseScore note pitch : \(pitch) : can't be converted to ABC note",kind:.XMLMusicError)
    }
    return returned
  }
}


struct MuseScoreText {
  let style : String?
  let text : String?
}


class MuseScoreVBOX : XmlParserUtils{
  var height : Int?
  var texts : [MuseScoreText] = []
  
  private func buildText( parent : XML.Accessor ) throws {
    for text in parent[MUSESCORE_TEXT] {
      let style = text[MUSESCORE_STYLE].text
      let text1 = text[MUSESCORE_TEXT1].text
      texts.append(MuseScoreText(style: style, text: text1))
    }
  }
  
  func parseContent(_ vbox : XML.Accessor ) throws {
    self.height = try toInt( tag: MUSESCORE_HEIGHT, from: vbox[MUSESCORE_HEIGHT].text)
    try buildText(parent: vbox)
  }
  
  func getText( style: String ) -> String? {
    for text in texts {
      if ( text.style == style ) {
        return text.text
      }
    }
    return nil
  }
  
}


struct MuseScoreDuration {
  let durationType : String
  let duration : String?
}

enum MuseScoreBeam : String  {
  case no
  case begin
}


class MuseScoreChord :  XmlParserUtils , ABCEmittingActor{
  
  
  var duration : MuseScoreDuration?
  var beam : MuseScoreBeam? = nil
  var notes  = [MuseScoreNote]()
  var dots : Int = 0

  var isRest : Bool {
    get {
      if notes.count == 0 {
        return true
      }
      return false
    }
  }
  
  func parseContent(_ chordXml : XML.Element ) throws {
    
    let children = chordXml.childElements
    for child in children {
      switch child.name {
      case MUSESCORE_SPANNER ,
           MUSESCORE_ARTICULATION ,
           MUSESCORE_STEMDIRECTION ,
           MUSESCORE_LINKEDMAIN,
           MUSESCORE_ACCIACCATURA,
           MUSESCORE_SLUR,
           MUSESCORE_LID :
        break // ignored for the moment
      case MUSESCORE_DOTS :
        dots = try toInt(tag:MUSESCORE_DOTS,from:child.text)!
        break
      case MUSESCORE_DURATIONTYPE :
        duration = MuseScoreDuration(durationType: child.text!,duration:nil)
        break
      case MUSESCORE_BEAMMODE :
        beam = MuseScoreBeam(rawValue: child.text!)
        break
      case MUSESCORE_NOTE :
        let noteChildren = child.childElements
        var pitch : Int?
        var tpc: Int?
        for noteChild in noteChildren {
          switch noteChild.name {
          case MUSESCORE_AUTOPLACE ,
               MUSESCORE_LINKEDMAIN,
               MUSESCORE_LID,
               MUSESCORE_STRING,
               MUSESCORE_FRET,
               MUSESCORE_SPANNER :
            break  // ignored for the moment
          case MUSESCORE_PITCH :
            pitch = try toInt(tag:MUSESCORE_PITCH,from:noteChild.text)
            break
          case MUSESCORE_ACCIDENTAL :
            break // ignored accidental handled in pitch for musescore
          case MUSESCORE_TPC :
            tpc = try toInt(tag:MUSESCORE_TPC,from:noteChild.text)
            break
          default :
            throw ABCFailure(message:"MuseScore \(noteChild.name) unexpected tag in \(MUSESCORE_NOTE)",kind:.XMLMusicError)
          }
        }
        guard let pitc = pitch else {
          throw ABCFailure(message:"MuseScore \(MUSESCORE_PITCH) expected in \(MUSESCORE_NOTE) not found",kind:.XMLMusicError)
        }
        guard let tcp = tpc else {
          throw ABCFailure(message:"MuseScore \(MUSESCORE_TPC) expected in \(MUSESCORE_NOTE) not found",kind:.XMLMusicError)
        }
        notes.append(MuseScoreNote(pitch: pitc, tpc: tcp , alteration: nil))
        break
      default :
        throw ABCFailure(message:"MuseScore \(child.name) unexpected tag in \(MUSESCORE_CHORD)",kind:.XMLMusicError)
      }
    }
  }
  
  private func emitNote(_ emitter: ABCEmitter , n: MuseScoreNote , duration: ScoreABCDuration) throws {
    let abcN = try n.toABCNote(key:emitter.curKSignature)
    let abcNote = ScoreABCNote(duration: duration,note:abcN,dots:dots, alteration: nil,tied:nil)
    // Beam emit must happen here
    if ( emitter.inBeam ) {
      if let beam = beam {
        if ( beam == .begin || beam == .no ) {
          emitter.inBeam = false // stop previous beam
        }
      }
    }
    emitter.emit(note: abcNote)
    if let beam = beam {
      if ( beam == .begin ) {
        emitter.emitBeam(ABCBeam(type: .Begin, number: 0, tuplet: false))
      }
    }
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    let dur = ScoreABCDuration(rawValue:"d\(duration!.durationType)")
    guard let durat = dur else {
      throw ABCFailure(message:"MuseScore \(MUSESCORE_DURATIONTYPE) expected value \(duration!.durationType)",kind:.XMLMusicError)
    }
    if (isRest) {
      let abcNote = ScoreABCNote(duration: durat,note:nil,dots:dots,alteration: nil,tied:nil)
      emitter.emit(note:abcNote)
    } else {
      if ( notes.count > 1 ) {
        emitter.append("[")
        emitter.insideChord = true
        for note in notes {
          try emitNote(emitter, n: note, duration: durat)
        }
        emitter.append("]")
        emitter.insideChord = false 
      } else {
        try emitNote(emitter, n: notes[0], duration: durat)
      }
    }
  }
}



class MuseScoreVoice :  XmlParserUtils , ABCEmittingActor {
  
  var timeSig : ScoreABCTimeSig? = nil
  var accidental : Int?
  var clef : ABCScoreClef? = nil
  var chords : [MuseScoreChord] = []
  var isFirstMeasure : Bool = false
  
  private func getClef(from: String?) throws -> ABCScoreClef  {
    if let from  = from {
      if from.starts(with: "F") {
        return ABCScoreClef(rawValue: "F")!
      }
      if from.starts(with: "C") {
        return ABCScoreClef(rawValue: "C")!
      }
      if from.starts(with: "G") {
        return ABCScoreClef(rawValue: "G")!
      }
      throw ABCFailure(message:"MuseScore \(MUSESCORE_CLEFTYPE) invalid clef type \(from)",kind:.XMLMusicError)
    }
    return ABCScoreClef(rawValue: "G")!
  }
  
  func parseContent(_ voiceXml : XML.Accessor ) throws {
    let keySig = voiceXml[MUSESCORE_KEYSIG]
    if ( isOK(keySig) ) {
      accidental = try toInt(tag: MUSESCORE_ACCIDENTAL,from: keySig[MUSESCORE_ACCIDENTAL].text)
    }
    let clefSig = voiceXml[MUSESCORE_CLEFSIG]
    if ( isOK(clefSig) ) {
      clef = try getClef(from: keySig[MUSESCORE_CLEFTYPE].text)
    }
    let tSig = voiceXml[MUSESCORE_TIMESIG]
    if ( isOK(tSig) ) {
      let  sign = try toInt(tag: MUSESCORE_SIGN,from: tSig[MUSESCORE_SIGN].text)
      guard let uwsign = sign else {
        throw ABCFailure(message:"MuseScore \(MUSESCORE_SIGN) expected in \(MUSESCORE_TIMESIG) not found",kind:.XMLMusicError)
      }
      let sigd = try toInt(tag: MUSESCORE_SIGD,from: tSig[MUSESCORE_SIGD].text)
      guard let uwsigd = sigd else {
        throw ABCFailure(message:"MuseScore \(MUSESCORE_SIGD) expected in \(MUSESCORE_TIMESIG) not found",kind:.XMLMusicError)
      }
      timeSig = ScoreABCTimeSig(sigN: uwsign, sigD: uwsigd)
    }
    let voiceElem = voiceXml.element
    let children = voiceElem!.childElements
    for element in children {
      switch element.name {
      case MUSESCORE_TIMESIG :
        break // already handled
      case MUSESCORE_CHORD :
        let chord = MuseScoreChord()
        try chord.parseContent(element)
        chords.append(chord)
        break
      case MUSESCORE_REST :
        let children = element.childElements
        var durationType : String?
        var duration : String?
        for child in children {
          switch child.name {
          case MUSESCORE_VISIBLE :
            break
          case MUSESCORE_DURATIONTYPE :
            durationType = child.text
            break
          case MUSESCORE_DURATION :
            duration = child.text
            break
          default :
            throw ABCFailure(message:"MuseScore \(child.name) unexpected tag in \(MUSESCORE_REST) not found",kind:.XMLMusicError)
          }
        }
        guard let dType = durationType else {
          throw ABCFailure(message:"MuseScore \(MUSESCORE_DURATIONTYPE) expected in \(MUSESCORE_REST) not found",kind:.XMLMusicError)
        }
        let rest = MuseScoreChord()
        rest.duration = MuseScoreDuration(durationType: dType,duration:duration)
        chords.append(rest)
        break
      default :
        DDLogDebug("\(element.name) Found")
        break
      }
    }
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    if ( !isFirstMeasure ) {
      // clef change or signature change
      if let accidental = accidental {
        if let clef = clef {
          try emitter.checkVoiceChanges(clef: clef, signature: ABCScoreKey(key: accidental))
        }
      }
    }
    if let timeSig = timeSig {
      if ( timeSig != emitter.curTimeSig ) {
        // only if timesig has changed
        emitter.curTimeSig = timeSig
        emitter.append("[")
        emitter.emitTimeSig()
        emitter.append("]")
      }
    }
    for chord in chords {
      try chord.emitABC(emitter)
    }
  }

}


class MuseScoreMeasure :  XmlParserUtils , ABCEmittingActor {
  
  var vBox : MuseScoreVBOX? = nil
  var repeatStart : Bool = false
  var repeatEnd : Bool = false
  var voices = [MuseScoreVoice]()
  var isFirstMeasure : Bool = false
  var isLastMeasure : Bool = false
  
  init( vBox : MuseScoreVBOX?) {
    self.vBox = vBox
  }
  
  private func add( voiceXml: XML.Accessor ) throws {
    let curVoice = MuseScoreVoice()
    try curVoice.parseContent(voiceXml)
    voices.append(curVoice)
  }
  
  func parseContent(_ mesureXml : XML.Accessor ) throws {
    let repet = mesureXml[MUSESCORE_STARTREPEAT]
    if ( isOK(repet)) {
      self.repeatStart = true
    }
    let endRepet = mesureXml[MUSESCORE_ENDREPEAT]
    if ( isOK(endRepet)) {
      self.repeatEnd = true
    }
    let voicesXml =  mesureXml[MUSESCORE_VOICE]
    if ( isOK(voicesXml)) {
      for voiceXml in voicesXml {
        try self.add(voiceXml: voiceXml)
      }
    } else {
      // singlevoice Case => parse Mesure parent instead
      try self.add(voiceXml: mesureXml)
    }
  }
  
  func getVBoxValue( key : String) -> String? {
    if let box = vBox {
      return box.getText(style: key)
    }
    return nil
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    if ( repeatStart ) {
      emitter.emitRepeat(start: true)
    } else {
      emitter.emitMeasure()
    }
    for voice in self.voices {
      voice.isFirstMeasure = isFirstMeasure
      try voice.emitABC(emitter)
    }
    if ( repeatEnd ) {
      emitter.emitRepeat(start: false)
    } else {
      if ( isLastMeasure ) {
        emitter.emitMeasure()
      }
    }
  }

}


class MuseScoreStaff : XmlParserUtils , ABCEmittingActor {

  var number : Int?
  
  var parent : MuseScorePart?
  var mesures = [MuseScoreMeasure]()
  var defaultCle : ABCScoreClef?
  var instrument : ScoreInstrument? = nil
  var trackName : String?
  var nicName : String?
  var tablature : Bool = false
  var voiceDetails : ScoreVoice {
    get {
      return ScoreVoice(id: number!, clef: defaultCle, name: instrument?.longName, snm: instrument?.shortName,nicName: nicName!)
    }
  }

  
  init( number : Int? , defClef : String? ) {
    self.number = number
    if let defClef = defClef {
      defaultCle = ABCScoreClef(rawValue: defClef)
    } else {
      defaultCle = .G
    }
  }
  
  func getMeasure(number : Int) -> MuseScoreMeasure? {
    let curNum =  0
    for mesure in mesures {
      if ( curNum == number ) {
        return mesure
      }
    }
    return nil
  }
  
  func parseContent( staff : XML.Accessor ) throws {
    var curvBox : MuseScoreVBOX? = nil
    let vboxXml = staff[MUSESCORE_VBOX]
    if ( isOK(vboxXml)) {
      curvBox = MuseScoreVBOX()
      try curvBox!.parseContent(vboxXml)
    }
    let mesuresXml = staff[MUSESCORE_MEASURE]
    var count = 0
    var lastMeasure : MuseScoreMeasure? = nil
    for mesureXml in mesuresXml {
      let curMeasure = MuseScoreMeasure(vBox: curvBox)
      if ( count == 0 ) {
        curMeasure.isFirstMeasure = true
      }
      try curMeasure.parseContent(mesureXml)
      mesures.append(curMeasure)
      count += 1
      lastMeasure = curMeasure
    }
    lastMeasure?.isLastMeasure = true
  }
  
  //
  // ignore tablature for example
  //
  func checkTablature( staff : XML.Accessor ) {
    let typeXml = staff[MUSESCORE_STAFFTYPE]
    if ( isOK(typeXml) ) {
      if let attrs = typeXml.element?.attributes {
        let group = attrs[MUSESCORE_GROUP]
        if ( group == MUSESCORE_TABLATURE ) {
          self.tablature = true
        }
      }
    }
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    for mesure in mesures {
      try mesure.emitABC(emitter)
    }
  }
  
  
}



class MuseScorePart :  XmlParserUtils , ABCEmittingActor {

  var staffs = [MuseScoreStaff]()
  var isMultiStaff : Bool {
    get {
      if ( staffs.count > 1 ) {
        return true
      }
      return false
    }
  }
  
  func add( staff: MuseScoreStaff) {
    staff.parent = self
    staffs.append(staff)
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    for staff in staffs {
      try staff.emitABC(emitter)
    }
  }

}

class MuseScoreMultiVoiceEmitter : ABCEmittingActor {
  
  var staffs = [MuseScoreStaff]()
  var curPos : Int = 0
  var maxMeasure : Int
  
  private var hasFinished : Bool {
    for staff in staffs {
      if ( staff.mesures.count > curPos ) {
        return false
      }
    }
    return true
  }
  
  init( max: Int ) {
    self.maxMeasure = max
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    var limit = curPos + maxMeasure
    while ( !hasFinished && ( curPos < limit ) ) {
      emitter.emitCommentLine("\(curPos+1)")
      for staff in staffs {
        let curVoice = staff.voiceDetails
        curVoice.emitLine(emitter: emitter)
        for ii in curPos..<limit {
          if ( ii < staff.mesures.count ) {
            let curMesure = staff.mesures[ii]
            try curMesure.emitABC(emitter)
          }
        }
        emitter.emitMeasure()
        emitter.eol()
      }
      curPos = limit
      limit = curPos + maxMeasure
    }
  }

}


class MuseScoreIR {
  
  var version : String?
  var division : Int?
  var arranger : String?
  var composer : String?
  var copyrights : String?
  var creationDate : String?
  var lyricist : String?
  var movementNumber : String?
  var movementTitle : String?
  var platform : String?
  var poet : String?
  var source : String?
  var translator : String?
  var workNumber : String?
  var workTitle : String?
  
  var parts : [MuseScorePart] = []
  var  isMultiPart : Bool {
    get {
      if ( parts.count <= 1 ) {
        if ( parts[0].isMultiStaff ) {
          return true 
        }
        return false
      }
      return true
    }
  }

  var parseCompleted : Bool = false
  
  private func getFirstStaff() -> MuseScoreStaff? {
    if parts.isEmpty {
      return nil
    }
    return parts[0].staffs[0]
  }
  
  private func getFirstMeasure() -> MuseScoreMeasure? {
    let staff = getFirstStaff()
    if let staff = staff {
      return staff.getMeasure(number: 0)
    }
    return nil
  }
  
  private func  lookInVBOX(key : String ) -> String? {
    let firstMeasure = getFirstMeasure()
    if let first = firstMeasure {
      return first.getVBoxValue(key: key)
    }
    return nil
  }

  private func  getFirstTimeSig() -> ScoreABCTimeSig? {
    let firstMeasure = getFirstMeasure()
    if let first = firstMeasure {
      return first.voices[0].timeSig
    }
    return nil
  }

  private func getTitles() -> [String] {
    var returned = [String]()
    if let title = workTitle {
      returned.append(title)
    }
    // look for first measure VBOX if any
    let title = lookInVBOX(key: MUSESCORE_TITLE)
    if let title = title {
      returned.append(title)
    }
    let subtitle = lookInVBOX(key: MUSESCORE_SUBTITLE)
    if let subtitle = subtitle {
      returned.append(subtitle)
    }
    return  returned
  }
 
  private func getComposer() -> String? {
    if let composer = composer {
      if ( !composer.isEmpty ) {
        return composer
      }
    }
    return lookInVBOX(key: MUSESCORE_COMPOSER1)
  }
  
  private func getHeaderInfos() throws -> ( ABCScoreClef? , ABCScoreKey , ScoreABCTimeSig?) {
    let staff = getFirstStaff()
    var timeSig : ScoreABCTimeSig? = nil
    var cle : ABCScoreClef? = nil
    var key : ABCScoreKey = ABCScoreKey(key: 0)
    if let staff = staff {
      cle =  staff.defaultCle
    }
    let firstMeasure = getFirstMeasure()
    if let firstMeasure = firstMeasure {
      let v = firstMeasure.voices[0]
      timeSig = v.timeSig
      if let acc = v.accidental {
        key = ABCScoreKey(key: acc)
      } 
      return (cle , key , timeSig)
    }
    throw ABCFailure(message:"unable to get first Clef and Key",kind:.XMLMusicError)
  }
  
  func add( part: MuseScorePart) {
    parts.append(part)
  }
  
  func emitABC() throws -> String {
    if ( parseCompleted ) {
      DDLogDebug("Emitting ABC for MuseScore  ...")
      let emitter = ABCEmitter()
      emitter.emitHeader(number: 1)
      let titles = getTitles()
      for title in titles {
        emitter.emitTitle(name: title)
      }
      if let composer = getComposer() {
        emitter.emitComposer(name: composer)
      }
      // ending abc header
      let headerData = try getHeaderInfos()
      if let sig = headerData.0 {
        emitter.currentClef = sig
      }
      emitter.emitNoteLength(l: 8) // default to 1/8
      emitter.curKSignature = headerData.1
      // loop on parts
      if ( isMultiPart ) {
        emitter.isMultiPart = isMultiPart
        emitter.append("%%score ")
        for ii in 1...parts.count {
          let curPart = parts[ii-1]
          for jj in 1...curPart.staffs.count {
            let staff = curPart.staffs[jj-1]
            staff.nicName = "T\(ii)\(jj)"
            emitter.append("(\(staff.nicName!)) ")
          }
        }
        emitter.eol()
        for part in parts {
          for staff in part.staffs {
            let v = staff.voiceDetails
            emitter.emitHeader(voice: v)
          }
        }
      }
      emitter.curTimeSig = self.getFirstTimeSig()
      emitter.emitTimeSig()
      emitter.eol()
      // K: must be the last abc line in head
      try emitter.emitClef()
      emitter.eol()
      if ( isMultiPart ) {
        let multiVoice = MuseScoreMultiVoiceEmitter(max:4)
        for part in parts {
          for staff in part.staffs {
            multiVoice.staffs.append(staff)
          }
        }
        try multiVoice.emitABC(emitter)
      } else {
        let part = parts[0]
        try part.emitABC(emitter)
        emitter.eol()
      }
      DDLogDebug("Emitting ABC for MuseScore ENDED")
      return emitter.buffer
    } else {
      throw ABCFailure(message:"MuseScore source has not been parsed",kind:.XMLMusicError)
    }
  }
}

class MuseScoreParser : XmlParserUtils {
  
  let ir : MuseScoreIR  = MuseScoreIR()
  
  private func buildMetaTag( parent : XML.Accessor ) throws {
    let metas = parent[MUSESCORE_METATAG]
    if ( isOK(metas) ) {
      for meta in metas {
        let curName = meta.attributes[MUSESCORE_NAME]
        if let curName = curName {
          switch (curName) {
          case MUSESCORE_ARRANGER :
            ir.arranger = meta.text
          case MUSESCORE_COMPOSER :
            ir.composer = meta.text
          case MUSESCORE_CREATIONDATE :
            ir.creationDate = meta.text
          case MUSESCORE_LYRICIST :
            ir.lyricist = meta.text
          case MUSESCORE_MOVEMENTNUMBER :
            ir.movementNumber = meta.text
          case MUSESCORE_COPYRIGHTS :
            ir.copyrights = meta.text
          case MUSESCORE_MOVEMENTTITLE :
            ir.movementTitle = meta.text
          case MUSESCORE_PLATFORM :
            ir.platform = meta.text
          case MUSESCORE_POET :
            ir.poet = meta.text
          case MUSESCORE_SOURCE :
            ir.source = meta.text
          case MUSESCORE_TRANSLATOR :
            ir.translator = meta.text
          case MUSESCORE_WORKNUMBER :
            ir.workNumber = meta.text
          case MUSESCORE_WORKTITLE :
            ir.workTitle = meta.text
          case MSCVERSION ,
               MUSESCORE_ORIGINALFORMAT :
            break // unused
          default :
            throw ABCFailure(message:"MuseScore \(curName) not a MuseScore metaTag",kind:.XMLMusicError)
          }
        }
      }
    }
  }
  
  private func buildParts( parent : XML.Accessor ) throws {
    let partXml = parent[MUSESCORE_PART]
    for part in partXml {
      var curPart : MuseScorePart? = nil
      let instrument = part[MUSESCORE_INSTRUMENT]
      let staffsXml = part[MUSESCORE_STAFF]
      curPart = MuseScorePart()
      for staffXml in staffsXml {
        let staffId = try toInt(tag: MUSESCORE_ID , from: staffXml.attributes[MUSESCORE_ID])
        let defCle = staffXml[MUSESCORE_DEFAULTCLE].text
        let curStaff = MuseScoreStaff(number: staffId, defClef: defCle)
        curStaff.checkTablature(staff: staffXml )
        if ( !curStaff.tablature ) {
          try curStaff.parseContent(staff: staffXml)
          curPart?.add(staff: curStaff)
          if ( isOK(instrument)) {
            let longName = instrument[MUSESCORE_LONGNAME].text
            let shortName = instrument[MUSESCORE_SHORTNAME].text
            let trackName = instrument[MUSESCORE_TRACKNAME].text
            curStaff.instrument = ScoreInstrument(longName: toString(longName),
                                                 shortName: toString(shortName),
                                                 trackName: toString(trackName))
          }
        }
      }
      ir.add(part: curPart!)
    }
  }
  
  private func buildStaffs( parent : XML.Accessor , parts : [MuseScorePart]) throws  {
    let staffXml = parent[MUSESCORE_STAFF]
    for xmlStaff in staffXml {
      let staffNumber = try toInt(tag: MUSESCORE_ID,from: xmlStaff.attributes[MUSESCORE_ID])
      if let staffNumber = staffNumber {
        for part in parts {
          for curStaff in part.staffs {
            if ( !curStaff.tablature ) {
              if ( curStaff.number == staffNumber ) {
                try curStaff.parseContent(staff: xmlStaff)
                break
              }
            }
          }
        }
      } else {
        throw ABCFailure(message:"MuseScore \(MUSESCORE_ID) missing in Staff",kind:.XMLMusicError)
      }
    }
  }
  
  private func buildMusicTree( root : XML.Accessor ) throws {
    let muse = root[MUSESCORE]
    if isOK(muse) {
      let msVersion = muse.attributes[MUSESCOREVERSION]?.trim()
      if let msVersion = msVersion {
        ir.version = msVersion
        DDLogInfo("parsing MuseScore source version \(msVersion)")
      }
      let score = muse[MUSESCORE_SCORE]
      if ( isOK(score )) {
        let division = score[MUSESCORE_DIVISION]
        if ( isOK(division)) {
          ir.division = try toInt(tag: MUSESCORE_DIVISION, from: division.text)
        }
        // <metaTag>
        try buildMetaTag(parent: score)
        // <Part>
        try buildParts(parent: score)
        // <Staff>
        try buildStaffs(parent: score, parts: ir.parts)
      }
    } else {
      throw ABCFailure(message:"MuseScore \(MUSESCORE) tag not found",kind:.XMLMusicError)
    }
  }
  
  func parse( xmlSource : String ) throws {
    do {
      let xml = try XML.parse(xmlSource)
      try buildMusicTree(root: xml)
      ir.parseCompleted = true 
    } catch {
      throw ABCFailure(message:"MusicScore parsing error : \(error)",kind:.XMLMusicError)
    }
  }
  
  func emitABC() throws -> String {
    return try ir.emitABC()
  }
 
}
