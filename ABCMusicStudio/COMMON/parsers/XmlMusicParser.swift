//
//  XmlMusicParser.swift
//  ABCStudio
//
//  Created by jymengant on 5/25/19 week 21.
//  Copyright © 2019 jymengant. All rights reserved.
//
import Foundation
import CocoaLumberjack
import SwiftyXMLParser

private let XMLMUSICSCORE = "score-partwise"
private let PART = "part"
private let WORK = "work"
private let WORK_NUMBER = "work-number"
private let WORK_TITLE = "work-title"
private let MOVEMENT_TITLE = "movement-title"
private let IDENTIFICATION = "identification"
private let CREATOR = "creator"
private let MEASURE = "measure"
private let MEASURE_NUMBER = "number"
private let MEASURE_WIDTH = "width"
private let MEASURE_IMPLICIT = "implicit"
private let MEASURE_NONCONTROLING = "width"
private let MEASURE_ATTRIBUTES = "attributes"
private let MEASURE_BARLINE = "barline"
private let MEASURE_BACKUP = "backup"
private let BARLINE_REPEAT = "repeat"
private let BARLINE_LOCATION = "location"
private let REPEAT_DIRECTION = "direction"
private let ATTRIBUTES_KEY = "key"
private let ATTRIBUTES_CLE = "clef"
private let CLEF_NUMBER = "number"
private let ATTRIBUTES_DIVISION = "divisions"
private let ATTRIBUTE_FIFTH = "fifths"
private let ATTRIBUTE_TIME = "time"
private let ATTRIBUTE_STAVES = "staves"
private let ATTRIBUTE_BEATS = "beats"
private let ATTRIBUTE_BEATTYPE = "beat-type"
private let ATTRIBUTE_SIGN = "sign"
private let ATTRIBUTE_LINE = "line"
private let ATTRIBUTE_KEYMODE = "mode"
private let ATTRIBUTE_KEYCANCEL = "cancel"
private let XNOTE  = "note"
private let XNOTE_LYRIC  = "lyric"
private let XNOTE_TEXT  = "text"
private let XNOTE_TYPE  = "type"
private let XNOTE_ACCIDENTAL  = "accidental"
private let XNOTE_DOT = "dot"
private let XNOTE_CHORD = "chord"
private let XNOTE_NOTATIONS = "notations"
private let XNOTE_TIED = "tied"
private let XNOTE_BEAM = "beam"
private let XNOTE_TIED_START = "start"
private let XNOTE_TIED_STOP = "stop"
private let XNOTE_REST = "rest"
private let XNOTE_TUPLET = "tuplet"
private let XNOTE_MEASURE = "measure"
private let XPITCH = "pitch"
private let XREST  = "rest"
private let XDURATION = "duration"
private let XSTAFF = "staff"
private let XVOICE = "voice"
private let XSTEM = "stem"
private let XSTEP = "step"
private let XOCTAVE = "octave"
private let BLINE_BARSTYLE = "bar-style"
private let BLINE_REPEAT = "repeat"
private let XMUSIC_PART_LIST = "part-list"
private let XMUSIC_DIRECTION = "direction"
private let XMUSIC_DIRECTION_TYPE = "direction-type"
private let XMUSIC_DYNAMICS = "dynamics"
private let XMUSIC_PP = "pp"
private let XMUSIC_PPP = "ppp"
private let XMUSIC_PPPP = "pppp"
private let XMUSIC_PPPPP = "ppppp"
private let XMUSIC_PPPPPP = "pppppp"
private let XMUSIC_SCOREPART = "score-part"
private let XMUSIC_PARTNAME = "part-name"
private let XMUSIC_ID = "id"
private let XMUSIC_WORDS = "words"
private let XMUSIC_WEDGE = "wedge"
private let XMUSIC_YES = "yes"
private let XMUSIC_SINGLE = "staff"
private let XMUSIC_START = "start"
private let XMUSIC_STOP = "stop"
private let XMUSIC_BEGIN = "begin"
private let XMUSIC_END = "end"
private let XMUSIC_CONTINUE = "continue"
private let XMUSIC_SCOREINSTRUMENT = "score-instrument"
private let SCOREINSTRUMENT_NAME = "instrument-name"
private let SCOREINSTRUMENT_SOUND = "instrument-sound"

enum XMusicStep: String {
  case A
  case B
  case C
  case D
  case E
  case F
  case G
}

class XMusicWork : ABCEmittingActor {
  let number : String
  var titles : [String] = []
  
  init(number : String) {
    self.number = number
  }
  
  func add(title : String) {
    titles.append(title)
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    for title in  titles {
      emitter.emitTitle(name: title)
    }
    let replaced = number.replacingOccurrences(of: "\n", with: " ")
      emitter.emitTitle(name: replaced)
  }
}

struct XMusicPitch   {
  var step: XMusicStep!
  var octave: Int!
  var sharp : Bool = false
  var flat : Bool = false
  
  func toAbcNote() throws  -> String? {
    var returned = step.rawValue
    switch octave {
    case 0 :
      returned = returned+",,,,"
      break
    case 1 :
      returned = returned+",,,"
      break
    case 2 :
      returned = returned+",,"
      break
    case 3 :
      returned = returned+","
      break
    case 4 :
      break
    case 5 :
      returned = returned.lowercased()
      break
    case 6 :
      returned = returned.lowercased()+"'"
      break
    case 7 :
      returned = returned.lowercased()+"''"
      break
    case 8 :
      returned = returned.lowercased()+"'''"
      break
    case 9 :
      returned = returned.lowercased()+"''''"
      break
    default :
      throw ABCFailure(message:"Unexpected octave value : \(octave ?? -1)",kind:.XMLMusicError)
    }
    if sharp {
      returned = "^" + returned
    }
    if flat {
      returned = "_" + returned
    }
    return returned
  }
}

enum XMusicStem: String {
  case up
  case down
}

class XMusicNote : ABCEmittingActor  {
  var pitch: XMusicPitch?
  var duration: Int!
  var divisions: Int = 1
  var voice: Int?
  var isRest: Bool = false
  var isFullMRest: Bool = false
  var type: ScoreABCDuration?
  var dots : Int = 0
  var isChord : Bool = false
  var endChord : Bool = false
  var staff : Int = 0
  var text : String? = nil
  var wedge : ABCWedge? = nil
  var tied : ABCTied? = nil
  var beam : ABCBeam? = nil
  var alteration : ABCAlteration? = nil
  
  func emitABC(_ emitter: ABCEmitter) throws {
    var note : String? = nil
    if let pitch = pitch {
      note = try pitch.toAbcNote()
    }
    if ( isFullMRest ) {
      // Full Measure rest
      type = .dmeasure
    }
    if let text = text {
      emitter.emitWord(text)
    }
    
    if let wedge = wedge {
      emitter.emitWedge(wedge)
    }
    
    if ( type == nil ) {
      let timeSig = emitter.curTimeSig
      type = ScoreABCDuration.toABC(timeSig: timeSig!, duration: duration)
      if ( type == nil ) {
        throw ABCFailure(message:"\(self) unable to compute type duration ",kind:.XMLMusicError)
      }
    }
    if let beam = beam {
      switch beam.type! {
      case .Begin :
        if ( beam.number > 1 ) {
          emitter.emitBeam(beam)
        }
        break
      default :
        break
      }
    }
    
    let abcNote = ScoreABCNote(duration: type!, note: note, dots: dots,alteration: alteration,tied: tied)
    emitter.emit(note: abcNote)
    
    if let beam = beam {
      switch beam.type! {
      case .End :
        emitter.inBeam = false
        emitter.append(" ") // blank at end of nplets
        break
      default :
        break
      }
    }

  }

}

struct XMusicTime : ABCEmittingActor {
  var beats: Int?
  var beatType: Int?
  
  func emitABC(_ emitter: ABCEmitter) throws {
    if let beats = beats {
      emitter.time(beats: beats, beatsType: beatType!)
    }
  }
}

enum XMusicSign: String {
  case G
  case F
  case C
  case percussion
  case TAB
  case jianpu
  case none
}

struct XMusicClef {

  var number: Int
  var sign: XMusicSign?
  var line: Int?
  
  init( number: Int , sign : String? = nil , line : Int? = nil ) {
    self.number = number
    if let sign = sign {
      self.sign = XMusicSign(rawValue:sign)
    }
    self.line = line
  }
}

class XMusicAttributes : ABCEmittingActor {
  var divisions: Int?
  var keyFifth: Int?
  var keyMode: String?
  var keyCancel: Int?
  var time: XMusicTime?
  var voices : [XMusicClef] = []

  func emitABC(_ emitter: ABCEmitter) throws {
    if let time = time {
      try time.emitABC(emitter)
    }
    // emitting Key stuff for first measure only for the moment
    // NB K: must be the last element in ABC header
    var strKey = "K:C "
    if let keyFifth = keyFifth {
      switch keyFifth {
      case 1:
        strKey = "K:G "
      case 2:
        strKey = "K:D "
      case 3:
        strKey = "K:A "
      case 4:
        strKey = "K:E "
      case 5:
        strKey = "K:B "
      case 6:
        strKey = "K:F# "
      case 7:
        strKey = "K:C# "
      case 0 :
        break
      case -1:
        strKey = "K:F "
      case -2:
        strKey = "K:Bb "
      case -3:
        strKey = "K:Eb "
      case -4:
        strKey = "K:Ab "
      case -5:
        strKey = "K:Db "
      case -6:
        strKey = "K:Gb "
      case -7:
        strKey = "K:Cb "
      default :
        DDLogError("keyFifth : \(keyFifth) NOT IMPLEMENTED YET")
        assert(false)
      }
    }
    var strClef = "clef=G "
    let clef = voices[emitter.curVoiceNumber]
    if let sign = clef.sign {
      switch sign {
      case XMusicSign.G :
        break
      case XMusicSign.F :
        strClef = "clef=F4 "
        break
      case XMusicSign.C :
        strClef = "clef=C4 "
        break
      default :
        DDLogError("key : \(sign) NOT IMPLEMENTED YET")
        assert(false)
      }
    }
    if let line  = clef.line {
      strClef += String(line)
    }
    strKey += strClef
    emitter.append(strKey)
    emitter.eol()
  }
  
  func add( voice : XMusicClef ) {
    voices.append(voice)
  }
}

enum BarlineLocation : String {
  typealias RawValue = String
  case left
  case right
}

enum XMusicBarStyle : String  {
  case forward
  case backward
}

class XMusicBarLine : ABCEmittingActor {
  var starting : Bool = true
  var barStyle : XMusicBarStyle? = nil
  
  init( starting : Bool ) {
    self.starting = starting
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    if let barStyle = barStyle {
      switch barStyle {
      case .forward :
        emitter.emitRepeat(start: true)
        break
      case .backward :
        emitter.emitRepeat(start: false)
        break
      }
    } else {
      // default to ||
      emitter.emitMeasure(bold: true)
    }
  }
}

class XMusicMeasure : ABCEmittingActor , NSCopying {
  
  let number : Int
  var implicit : Bool = false
  var nonControlling : Bool = false
  let width : Float?
  var notes = [XMusicNote]()
  var attributes : XMusicAttributes? = nil
  var barLine : XMusicBarLine? = nil
  var directionType : String? = nil
  var pp : Int = 0
  var last : Bool = false
  
  init( n : Int , impl : Bool , nContr : Bool , width : Float?) {
    self.number = n
    self.implicit = impl
    self.nonControlling = nContr
    self.width = width
  }
  
  func add( note: XMusicNote ) {
    notes.append(note)
  }
  
  private func emitMeasureStart(_ emitter: ABCEmitter ) throws{
    if let barLine = barLine {
      if ( barLine.starting ) {
        try barLine.emitABC(emitter)
      } else {
        emitter.emitMeasure()
      }
    } else {
      emitter.emitMeasure()
    }
    if ( pp >= 1) {
      emitter.emitPp(count: pp)
    }
  }
  
  private func openChord(_ emitter: ABCEmitter) -> Bool{
    emitter.emitChord(start: true)
    return true
  }
  
  private func closeChord(_ emitter: ABCEmitter) -> Bool {
    emitter.emitChord(start: false)
    return false
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    try emitMeasureStart(emitter)
    var curNote : XMusicNote? = nil
    var nextNote : XMusicNote? = nil
    var chordStarted : Bool = false
    if let dirType = directionType {
      emitter.emitDirection(dirType)
    }
    for note in notes {
      if ( nextNote == nil ) {
        nextNote = note
      } else {
        curNote = nextNote
        nextNote = note
        if let divisions = attributes?.divisions {
          curNote?.divisions = divisions
        }
        if ( curNote!.isChord && !chordStarted ) {
          chordStarted = openChord(emitter)
        }
        try curNote?.emitABC(emitter)
        if ( curNote!.endChord ||
             ( curNote!.isChord && nextNote!.isRest )
        ) {
          chordStarted = closeChord(emitter)
        }
      }
    }
    try nextNote?.emitABC(emitter)
    // finally close chord before leaving measure if needed
    if ( chordStarted ) {
      chordStarted = false
      emitter.emitChord(start: false)
    }
    
    // close measure as well
    if let barLine = barLine {
      if ( !barLine.starting ) {
        try barLine.emitABC(emitter)
      } else {
        if ( last ) {
          emitter.emitMeasure()
        }
      }
    } else {
      if ( last ) {
        emitter.emitMeasure()
      }
    }
  }
  
  func copy(with zone: NSZone? = nil) -> Any {
    let copy = XMusicMeasure(n:self.number, impl : self.implicit , nContr : self.nonControlling , width : self.width)
    copy.attributes = self.attributes
    copy.barLine = self.barLine
    return copy
  }

}

struct XMusicInstrument {
  let id : String?
  var name : String?
  var sound : String?
  
  init( id: String ) {
    self.id = id
  }
}

class XMusicStaff : ABCEmittingActor {
  let id : String
  var instrument : XMusicInstrument?
  var clef : XMusicClef?
  var measures : [XMusicMeasure] = []
  var nicName : String?
  
  var curMeasure : XMusicMeasure? {
    get {
      if ( measures.count == 0 ) {
        return nil
      } else {
        return measures[measures.count-1]
      }
    }
  }
  var voiceDetails : ScoreVoice {
    get {
      let cl = clef!.sign?.rawValue
      let abcClef = ABCScoreClef(rawValue: cl!)
      let v = ScoreVoice(id: 1, clef: abcClef, name: self.instrument!.name, snm: id , nicName : self.nicName!)
      return v
    }
  }

  init(id : String ) {
    self.id = id
  }
  
  func add( note: XMusicNote ) {
    if let curMeasure = curMeasure {
      curMeasure.add(note:note)
    }
  }
  
  func add( measure : XMusicMeasure ) {
    measures.append(measure)
  }

  func emitABC(_ emitter: ABCEmitter) throws {
    for ii in  0...measures.count-1  {
      let measure = measures[ii]
      if ii == measures.count-1 {
        measure.last = true
      }
      emitter.nbBeams = 0 // reset
      try measure.emitABC(emitter)
    }
  }
}

class XMusicPart : ABCEmittingActor {
  
  let id: String
  var name : String? = nil
  var instruments = [XMusicInstrument]()
  var staffs = [XMusicStaff]()
  var attributes : XMusicAttributes? = nil
  var nbStaffs : Int {
    get {
      return staffs.count
    }
  }
  var isMultiVoice : Bool {
    get {
      if nbStaffs > 1 {
        return true
      }
      return false
    }
  }
  
  init( id: String ) {
    self.id = id
  }
  
  func add( mesure : XMusicMeasure  ) throws {
    if staffs.count == 0 {
      // entering first measure
      if let attributes = mesure.attributes {
        let voices = attributes.voices
        if ( !voices.isEmpty ) {
          for ii in 0...voices.count-1 {
            let staff = XMusicStaff(id: voices[ii].sign!.rawValue)
            if ( instruments.count > ii ) {
              staff.instrument = instruments[ii]
            }
            staff.clef = voices[ii]
            staffs.append(staff)
          }
        } else {
          // allocate default staff
          let staff = XMusicStaff( id: XMUSIC_SINGLE )
          staff.instrument = XMusicInstrument(id : XMUSIC_SINGLE)
          staff.clef = XMusicClef(number: attributes.keyFifth!)
          attributes.add(voice: staff.clef!)
          staffs.append(staff)
        }
      } else {
        throw ABCFailure(message:"Missing attributes on first Measure",kind:.XMLMusicError)
      }
    }
    var nbStaff = 0
    for staff in staffs {
      // allocate dedicated measures in multi staffs
      var toAdd = mesure
      if ( nbStaff > 0 ) {
        toAdd = mesure.copy() as! XMusicMeasure
      }
      staff.add(measure: toAdd)
      nbStaff += 1
    }
  }
  
  func add( note : XMusicNote ) {
    // add to single or multiple staff depending on note staff
    // index
    if staffs.count > 1 {
      DDLogDebug("in")
    }
    let noteStaff = note.staff
    let curStaff = staffs[noteStaff]
    curStaff.add(note: note)
  }
  
  func add( instrument : XMusicInstrument ) {
    instruments.append(instrument)
  }
    
  func emitABC(_ emitter: ABCEmitter) throws {
    for staff in staffs {
      try staff.emitABC(emitter)
    }
  }
  
  func emitABCHeader(_ emitter: ABCEmitter ) throws  {
    for ii in 1...nbStaffs {
      let staff = staffs[ii-1]
      staff.nicName = "\(id)\(ii)"
      emitter.emitVoiceHead(name: staff.nicName! )
    }
  }
  
}

private class XmlMusicMultiVoiceEmitter : ABCEmittingActor {
  
  var parts : [XMusicPart] = []
  var curPos : Int = 0
  var maxMeasure : Int
  private var hasFinished : Bool {
    for part in parts {
      for staff in part.staffs {
        if ( staff.measures.count > curPos ) {
          return false
        }
      }
    }
    return true
  }

  
  init( max: Int ) {
    self.maxMeasure = max
  }
  
  func emitABC(_ emitter: ABCEmitter) throws {
    var limit = curPos + maxMeasure
    while ( !hasFinished && ( curPos < limit ) ) {
      emitter.emitCommentLine("\(curPos+1)")
      for part in parts {
        for staff in part.staffs {
          let curVoice = staff.voiceDetails
          curVoice.emitLine(emitter: emitter)
          for ii in curPos..<limit {
            if ( ii < staff.measures.count ) {
              let curMesure = staff.measures[ii]
              if ( ii == staff.measures.count-1 ) {
                curMesure.last = true 
              }
              try curMesure.emitABC(emitter)
            }
          }
          emitter.eol()
          emitter.emitWords()
        }
      }
      curPos = limit
      limit = curPos + maxMeasure
    }
  }

}

class XmlMusicIR {
  
  var work : XMusicWork?
  var creator : String?
  var parts : [XMusicPart] = []
  var parseCompleted : Bool = false
  var isMultiPart : Bool {
    get {
      if ( parts.count <= 1 ) {
        if ( parts[0].isMultiVoice ) {
          return true
        }
        return false
      }
      return true
    }
  }
  var attributes : XMusicAttributes? {
    get {
      if  ( parts.count > 0 ) {
        return parts[0].attributes
      }
      return nil
    }
  }
  
  func getPartFor(id : String ) -> XMusicPart?  {
    for part in parts {
      if ( id == part.id ) {
        return part
      }
    }
    return nil
  }
  
  func addWork( _ number : String ) {
    self.work = XMusicWork(number: number)
  }
  
  func addTitle(_ title: String) {
    if self.work == nil {
      addWork("1")
    }
    self.work?.add(title: title)
  }
  
  func add( part : XMusicPart ) {
    self.parts.append(part)
  }
  
  func emitABC() throws -> String {
    if ( parseCompleted ) {
      DDLogDebug("Emitting ABC for XmlMusic  ...")
      let emitter = ABCEmitter()
      emitter.emitHeader(number: 1)
      if let work = self.work {
        try work.emitABC(emitter)
      }
      if let creator = creator {
        // avoid CRLF
        let replaced = creator.replacingOccurrences(of: "\n", with: " ")
        emitter.emitComposer(name: replaced)
      }
      emitter.emitNoteLength(l: 8) // default to 1/8
      // ending abc header
      if ( isMultiPart ) {
        emitter.isMultiPart = isMultiPart
        emitter.append("%%score ")
        for part in parts {
          try part.emitABCHeader(emitter)
        }
        emitter.eol()
        for part in parts {
          for staff in part.staffs {
            let v = staff.voiceDetails
            emitter.emitHeader(voice: v)
          }
        }
      }
      // K: must be the last abc line in head
      // emit it with attributes
      if let attribs = self.attributes {
        try attribs.emitABC(emitter)
      } else {
        throw ABCFailure(message:"xmlmusic has no attributes",kind:.XMLMusicError)
      }
      if ( isMultiPart ) {
        let multiVoice = XmlMusicMultiVoiceEmitter(max:4)
        for part in parts {
          multiVoice.parts.append(part)
        }
        try multiVoice.emitABC(emitter)
      } else {
        for part in parts {
          try part.emitABC(emitter)
        }
      }
      DDLogDebug("Emitting ABC for XmlMusic ENDED")
      return emitter.buffer
    } else {
      throw ABCFailure(message:"xmlmusic source has not been parsed",kind:.XMLMusicError)
    }
  }
}

class XmlMusicParser : XmlParserUtils {
  
  let ir : XmlMusicIR  = XmlMusicIR()
  
  
  private func getElements( _ parent: XML.Element , _ key : String ) -> [XML.Element] {
    var returned = [XML.Element]()
    let children = parent.childElements
    for child in children {
      if ( child.name == key ) {
        returned.append(child)
      }
    }
    return returned
  }
  
  private func getFirstElement ( _ parent: XML.Element ,_ key : String ) -> XML.Element? {
    let returned = getElements(parent,key)
    if returned.isEmpty {
      return nil
    }
    return returned[0]
  }
  
  private func buildNote(_ note:XML.Element ) throws -> XMusicNote {
    let returned = XMusicNote()
    let pitch = getFirstElement(note,XPITCH)
    if let pitch = pitch  {
      var p = XMusicPitch()
      let step = getFirstElement(pitch,XSTEP)
      if let step = step {
        p.step = XMusicStep(rawValue: step.text!.trim())
      }
      let octave = getFirstElement(pitch,XOCTAVE)
      if let octave = octave {
        p.octave =  try toInt(tag: XOCTAVE, from: octave.text!.trim())
      }
      returned.pitch = p
    }
    let alter = getFirstElement(note,XNOTE_ACCIDENTAL)
    if let alter = alter {
      if let txt = alter.text {
        switch txt.trim() {
        case "natural" :
          returned.alteration = .Natural
          break
        case "flat" :
          returned.alteration = .Flat
          break
        case "sharp" :
          returned.alteration = .Sharp
          break
        default :
          break
        }
      }
    }
    returned.dots = getElements(note,XNOTE_DOT).count
    let restXml =  getFirstElement(note,XNOTE_REST)
    if let restXml = restXml {
      returned.pitch = nil
      returned.isRest = true
      let fullMeasure = restXml.attributes[XNOTE_MEASURE]
      if let full = fullMeasure {
        if ( full == XMUSIC_YES ) {
          returned.isFullMRest = true
        }
      }
    }
    let chordXml =  getFirstElement(note,XNOTE_CHORD)
    if let _ = chordXml {
      returned.isChord = true
    }
    let duration = getFirstElement(note,XDURATION)
    if let duration = duration  {
      returned.duration = try toInt(tag: XDURATION, from: duration.text!.trim())
    }
    let staffXml = getFirstElement(note,XSTAFF)
    if let staffXml = staffXml {
      if let curStaff =  try toInt(tag: XSTAFF, from: staffXml.text!.trim()) {
        returned.staff = curStaff-1
      } else {
        returned.staff = 0
      }
    }
    let voiceXml = getFirstElement(note,XVOICE)
    if let voiceXml = voiceXml {
      returned.voice = try toInt(tag: XVOICE, from: voiceXml.text!.trim())!
    }
    let type = getFirstElement(note,XNOTE_TYPE)
    if let type = type {
      returned.type = ScoreABCDuration(rawValue:"d"+type.text!)
    }
    let lyric = getFirstElement(note,XNOTE_LYRIC)
    if let lyric = lyric {
      let text = getFirstElement(lyric,XNOTE_TEXT)
      if let text = text {
        returned.text = text.text
      }
    }
    let notations = getFirstElement(note, XNOTE_NOTATIONS)
    var isTuplet = false
    if let notations = notations {
      let tied = getFirstElement(notations, XNOTE_TIED)
      if let tied = tied {
        if let startStop = tied.attributes[XNOTE_TYPE] {
          returned.tied = ABCTied(rawValue: startStop)
        }
      }
      let tuplet = getFirstElement(notations, XNOTE_TUPLET)
      if let _ = tuplet {
        isTuplet = true
      }
    }
    let beams = getFirstElement(note, XNOTE_BEAM)
    if let beams = beams {
      let converted = beams.text!.capitalizingFirstLetter()
      let beam  = ABCBeamType(rawValue: converted)
      returned.beam = ABCBeam(type: beam, number: 0 , tuplet: isTuplet)
    }
    return returned
  }
  
  private func buildBarLine (_ bLine : XML.Element ) throws -> XMusicBarLine {
    let position = bLine.attributes[BARLINE_LOCATION]
    var startMesure = true
    if let position = position {
      if ( position == "right" ) {
        startMesure = false
      }
    }
    let returned = XMusicBarLine(starting : startMesure)
    let repet = getFirstElement(bLine, BARLINE_REPEAT)
    if let repet = repet {
      if let rDir = repet.attributes[REPEAT_DIRECTION] {
        returned.barStyle = XMusicBarStyle(rawValue: rDir)
      }
    }
    return returned
  }
  
  private func insertBackup( note: XMusicNote , notes : [XMusicNote] , duration: Int) -> [XMusicNote] {
    var returned = notes
    var curDur = 0
    let curStaff = note.staff
    let isRest = note.isRest
    var inChord : Bool = false
    for ii in (0...returned.count-1).reversed() {
      if ( notes[ii].staff == curStaff || isRest ){
        let curNote = notes[ii]
        if ( curNote.isChord && !curNote.endChord ) {
          inChord = true
        } else {
          inChord = false
        }
        if ( curDur == duration ) {
          // insertion point reached
          if ( !note.isRest ) {
            note.isChord = curNote.isChord // inherit chord
            note.type = curNote.type // safely inherit duration as well
            note.dots = curNote.dots // and dots
          }
          returned.insert(note , at: ii) // insert before ii index
          break // leave for loop
        }
        if ( !inChord ) {
          curDur += curNote.duration
        }
      }
    }
    return returned
  }
    
  private func collectNotes( _ mesure : XML.Element ) throws -> [XMusicNote] {
    var returned = [XMusicNote]()
    let children = mesure.childElements
    var backUp : Int? = nil
    var staffPrevNote : [Int:XMusicNote] = [:]
    var prevNote : XMusicNote? = nil
    var curNote : XMusicNote? = nil
    for child in children {
      switch( child.name ) {
      case XNOTE:
        prevNote = curNote
        curNote = try buildNote(child)
        if let prevNote = prevNote {
          staffPrevNote[prevNote.staff] = prevNote
        }
        if ( curNote!.isChord ) {
          prevNote?.isChord = true
        } else {
          if let prevNote = prevNote {
            if ( prevNote.isChord ) {
              for ( _ , prev ) in staffPrevNote {
                // close chord
                if ( prev.isChord ) {
                  prev.endChord = true
                }
              }
            }
          }
        }
        if let backUp = backUp {
          returned = insertBackup(note: curNote! , notes: returned , duration : backUp)
        } else {
          returned.append(curNote!)
        }
        break
      case MEASURE_BACKUP :
        let duration = getFirstElement(child,XDURATION)
        if let duration = duration {
          backUp = try toInt(tag: XDURATION, from: duration.text!.trim())
        }
        break
      default :
        break
      }
    }
    if ( !self.ir.isMultiPart ) {
      if let curNote = curNote {
        if ( curNote.isChord ) {
          curNote.endChord = true // for close chord on last measure note
        }
      }
    }
    return returned
  }
  
  //
  // Beams and texts
  //
  private func buildComplement( notes: [XMusicNote] ) {
    var beamStart : XMusicNote? = nil
    var beamCount : Int = 0
    var inChord : Bool = false
    for ii in 0...notes.count-1 {
      inChord = false
      let note = notes[ii]
      if ( ii < notes.count-1 ) {
        inChord = notes[ii+1].isChord
      } else {
        inChord = notes[ii].isChord
      }
      // Empty text on measure containing words
      // replaced by "-"
      if ( ii > 0 ) {
        if let _ = notes[ii-1].text {
          if notes[ii].text == nil {
            notes[ii].text = "-"
          }
        }
      }
      if (!inChord ) {
        if let beam = note.beam {
          switch beam.type! {
          case .Begin :
            beamStart = note
            beamCount += 1
            break
          case .End :
            // end Beam
            beamStart?.beam = ABCBeam(type: .Begin, number: beamCount+1 , tuplet: false)
            beamCount = 0
            break
          case .Continue :
            beamCount += 1
            break
          }
        }
      }
    }
  }
  
  private func buildMeasure(_ mesure : XML.Element ,
                            _  forPart : XMusicPart
  ) throws {
    let number = mesure.attributes[MEASURE_NUMBER]
    if let number = number {
      let num = try toInt(tag: MEASURE_NUMBER, from: number)
      let w = try toNumber(tag:MEASURE_WIDTH ,from: mesure.attributes[MEASURE_WIDTH])
      let i = yesNoToBool(from: mesure.attributes[MEASURE_IMPLICIT])
      let n = yesNoToBool(from: mesure.attributes[MEASURE_NONCONTROLING])
      
      let returned = XMusicMeasure(n: num!, impl: i, nContr: n, width: w)
      
      let barLine = getFirstElement(mesure,MEASURE_BARLINE)
      if let barLine = barLine  {
        returned.barLine = try buildBarLine(barLine)
      }
      let attribs = getFirstElement(mesure,MEASURE_ATTRIBUTES)
      if let attribs = attribs {
        returned.attributes = try buildAttributes(attribs)
        forPart.attributes = returned.attributes
      }
      let notes = try self.collectNotes(mesure)
      
      try forPart.add(mesure: returned)
      
      for note in notes {
        forPart.add(note: note)
      }
      
      buildComplement(notes: notes)
      
      let children = mesure.childElements
      var noteIndex : Int = 0
      for child in children {
        switch ( child.name ) {
        case XNOTE :
          if ( noteIndex < notes.count-1 ) {
            noteIndex += 1
          }
          break
        case XMUSIC_DIRECTION :
          let dirType = getFirstElement(child, XMUSIC_DIRECTION_TYPE)
          let words = getFirstElement(dirType!,XMUSIC_WORDS)
          if let words = words {
            returned.directionType = words.text!.trim()
          } else {
            let dynamics = getFirstElement(dirType!,XMUSIC_DYNAMICS)
            if let dynamics = dynamics {
              let children = dynamics.childElements
              for child in children {
                switch child.name {
                case XMUSIC_PP :
                  returned.pp = 2
                  break
                case XMUSIC_PPP :
                  returned.pp = 3
                  break
                case XMUSIC_PPPP :
                  returned.pp = 4
                  break
                case XMUSIC_PPPPP :
                  returned.pp = 5
                  break
                case XMUSIC_PPPPPP :
                  returned.pp = 6
                  break
                default :
                  break
                }
              }
            }
          }
          
          let wedges = getFirstElement(dirType!,XMUSIC_WEDGE)
          if let wedges = wedges {
            let type = wedges.attributes[XNOTE_TYPE]
            if ( noteIndex >= notes.count ) {
              
            }
            let note = notes[noteIndex]
            note.wedge = ABCWedge(rawValue: type!)
          }
        default:
          break
        }
      }
    } else {
      throw ABCFailure(message:"xmlmusic : expecting 'number' tag in measure not found",kind:.XMLMusicError)
    }
  }
  
  private func buildAttibutesTime(_ time : XML.Element ) throws -> XMusicTime {
    let beats = try toInt(tag: ATTRIBUTE_BEATS,from: getFirstElement(time, ATTRIBUTE_BEATS)?.text)
    let beatType = try toInt(tag: ATTRIBUTE_BEATTYPE,from: getFirstElement(time, ATTRIBUTE_BEATTYPE)?.text)
    return XMusicTime(beats: beats, beatType: beatType)
  }
  
  private func buildAttibutesClef(_ clef : XML.Element ) throws -> XMusicClef {
    let number: Int = try toInt(tag: ATTRIBUTE_LINE, from: clef.attributes[CLEF_NUMBER]?.trim()) ?? 1
    let sign = getFirstElement(clef, ATTRIBUTE_SIGN)?.text
    let line = try toInt(tag: ATTRIBUTE_LINE,from: getFirstElement(clef, ATTRIBUTE_LINE)?.text)
    return XMusicClef(number: number, sign: sign, line: line)
  }
  
  private func buildAttributes(_ attribs : XML.Element ) throws -> XMusicAttributes {
    let attr = XMusicAttributes()
    let divisions = getFirstElement(attribs,ATTRIBUTES_DIVISION)
    if let divisions = divisions {
      attr.divisions = try toInt(tag : ATTRIBUTES_DIVISION,from: divisions.text!.trim())
    }
    let key = getFirstElement(attribs,ATTRIBUTES_KEY)
    if let key = key {
      let fifth = getFirstElement(key,ATTRIBUTE_FIFTH)
      if let fifth = fifth {
        attr.keyFifth = try toInt( tag :ATTRIBUTE_FIFTH,from: fifth.text!.trim())
      }
      let keyCancel = getFirstElement(key,ATTRIBUTE_KEYCANCEL)
      if let keyCancel = keyCancel {
        attr.keyCancel = try toInt( tag :ATTRIBUTE_KEYCANCEL,  from: keyCancel.text!.trim())
      }
      let keyMode = getFirstElement(key,ATTRIBUTE_KEYMODE)
      if let keyMode = keyMode {
        attr.keyMode = keyMode.text!.trim()
      }
    }
    let clefs = getElements(attribs, ATTRIBUTES_CLE)
    for clef in clefs {
      attr.add(voice: try buildAttibutesClef(clef))
    }
    let time = getFirstElement(attribs,ATTRIBUTE_TIME)
    if let time = time  {
      attr.time = try buildAttibutesTime(time)
    }
    return attr
  }
  
  private func buildPart( id : String ,partXml : XML.Element ) throws -> XMusicPart {
    let returned = XMusicPart(id: id)
    let children = partXml.childElements
    for child in children {
      switch( child.name ) {
      case XMUSIC_PARTNAME :
        returned.name = child.text
        break
      case XMUSIC_SCOREINSTRUMENT :
        var instrument = XMusicInstrument(id: child.attributes[XMUSIC_ID]!)
        let instChildren = child.childElements
        for instXml in instChildren {
          switch ( instXml.name) {
          case SCOREINSTRUMENT_NAME :
            instrument.name = instXml.text
          case SCOREINSTRUMENT_SOUND :
            instrument.sound = instXml.text
          default :
            break
          }
          returned.add(instrument: instrument)
        }
        break
      default:
        break
      }
    }
    return returned
  }
  
  private func buildMusicTree( root : XML.Accessor ) throws {
    let score = root[XMLMUSICSCORE]
    if ( !isOK(score) ) {
      DDLogError("error : Not an XmlMusic structure")
      throw ABCFailure(message:"xmlmusic : expecting score-partwise tag not found",kind:.XMLMusicError)
    }
    let work = score[WORK]
    if ( isOK(work)  ) {
      DDLogDebug("work: \(work)")
      let n = work[WORK_NUMBER].trimedText
      let t = work[WORK_TITLE].trimedText
      if let n = n {
        ir.addWork(n)
      }
      if let t = t {
        ir.addTitle(t)
      }
    }
    let movTitleXml = score[MOVEMENT_TITLE]
    if ( isOK(movTitleXml) ) {
      ir.addTitle(movTitleXml.text!)
    }
    let creator = score[IDENTIFICATION,CREATOR]
    if ( isOK(creator)  ) {
      ir.creator = creator.trimedText
    }
    let partList = score[XMUSIC_PART_LIST]
    if ( isOK(partList) ) {
      let partsXml = partList[XMUSIC_SCOREPART]
      for partXml in partsXml {
        let partId = partXml.attributes[XMUSIC_ID]
        ir.add(part:  try self.buildPart(id: partId! , partXml: partXml.element!))
      }
    }
    // put additional infos in created parts
    for part in score[ PART ] {
      let partId = part.attributes[XMUSIC_ID]!
      let currentPart = ir.getPartFor(id: partId)
      guard let curPart = currentPart else {
        throw ABCFailure(message:"part id : \(partId) not found twice in xml tree",kind:.XMLMusicError)
      }
      let all = part[MEASURE].all
      if let all = all {
        for mesure in all  {
          try self.buildMeasure(mesure,curPart)
        }
      }
    }
  }
  
  
  func parse( xmlSource : String ) throws {
    do {
      let xml = try XML.parse(xmlSource)
      try buildMusicTree(root: xml)
      ir.parseCompleted = true
    } catch {
      throw ABCFailure(message:"xmlmusic parsing error : \(error)",kind:.XMLMusicError)
    }
  }
  
  func emitABC() throws -> String {
    return try ir.emitABC()
  }
  
}

extension String {
  func trim() -> String {
    return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
  }
}

extension XML.Accessor {
  var trimedText : String? {
    get {
      if let text = self.text {
        return text.trim()
      }
      return nil
    }
  }
}
