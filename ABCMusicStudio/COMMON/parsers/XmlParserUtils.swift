//
//  XmlParserUtils.swift
//  ABCMusicStudio
//
//  Created by jymengant on 12/6/20 week 49.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import SwiftyXMLParser
import CocoaLumberjack

class XmlParserUtils  {
  
  func isOK(_ element : XML.Accessor ) -> Bool {
    if ( element.error == nil ) {
      return true
    }
    return false
  }
  
  func yesNoToBool(from : String? )  -> Bool {
    if let from = from {
      let upper = from.lowercased()
      if upper == "yes" {
        return true
      }
    }
    return false // default when empty
  }

  func toInt(tag : String , from : String? ) throws -> Int? {
    if let from = from {
      let returned = Int(from)
      if let returned = returned {
        return returned
      }
      throw ABCFailure(message:"xmlmusic : invalid integer value for tag \(tag)",kind:.XMLMusicError)
    }
    return nil
  }
  
  func toNumber(tag : String , from : String? ) throws -> Float? {
    if let from = from {
      let returned = Float(from)
      if let returned = returned {
        return returned
      }
      throw ABCFailure(message:"xmlmusic : invalid float value for tag \(tag)",kind:.XMLMusicError)
    }
    return nil
  }
  
  func toString(_ str: String? ) -> String {
    if let str = str {
      return str
    }
    return ""
  }
  

}
