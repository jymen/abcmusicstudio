//
//  ABCParser.swift
//  ABCStudio
//
//  Created by jymengant on 9/16/18 week 37.
//  Copyright © 2018 jymengant. All rights reserved.
//
import Foundation
import CocoaLumberjack

private let _DEBUG_ : Bool = false

enum ABCTokenType {
  case ABCString
  case ABCText
  case ABCVersion
  case ABCEncoding
  case StyleSheetDirective
  case InvalidToken
  case EndOfSource
  case NotStarted
  case Comment
  case FreeText
  case Note
  case NoteSeparator
  case Bar
  case ChordEnded
  case DoubleBar
  case RepeatBarStart
  case RepeatBarEnd
  case InformationField
  case BracketedInformationField
  case PlaceHolder
  case EmptyLine
  case StartTie
  case Final
  case FinalEnded
  case EndTie
}

struct ABCNote {
  let note : String
  let octave : Int
  let duration : Int?
  let tied : Bool
  let accidental : Accidental
  let chord : Bool
  let decorator : String?
  let column : Int
  let line : Int
  let startOffset : Int
  let endOffset : Int
}

enum ParsedData {
  case Note ( note: ABCNote )
  case StdContent (data: String )
  
  func getNote() -> ABCNote? {
    switch self {
    case .Note(let note):
      return note
    default:
      return nil
    }
  }
  
  func getStd() -> String? {
    switch self {
    case .StdContent(let str):
      return str
    default:
      return nil
    }
  }

}

struct ABCToken {
  let start : Int
  let stop : Int
  let token : String?
  let type : ABCTokenType
  let content : ParsedData?
  let line : Int
  let col : Int
  let length : Int

  init( start : Int = 0, stop: Int = 0, token : String? = nil , type : ABCTokenType , content : ParsedData? = nil , line : Int , col : Int) {
    self.start = start
    self.stop  = stop
    self.token = token
    self.type = type
    self.content = content
    self.line = line
    self.col = col
    self.length = stop-start+1
  }
  
  func myDebug(src: String) -> ABCToken {
    if ( _DEBUG_ ) {
      // let sub = src[self.start...self.start+self.length]  // Using string extension utils
      let range : NSRange = NSRange(location : self.start , length: self.length)
      let str = src.substring(with: range)
      DDLogDebug("line : \(line) : start:\(self.start) : length : \(self.length) : type : \(type) = \(str ?? "")")
    }
    return self
  }
  
  func toString(src: String) -> String? {
    let range : NSRange = NSRange(location : self.start , length: self.length)
    return String(src.substring(with: range) ?? "")
  }
}


class ParseUtils {

  static func belongs( char : Character , to: CharacterSet ) -> Bool {
    var scalarIt = char.unicodeScalars.makeIterator()
    var cur = scalarIt.next()
    while ( cur != nil ) {
      if to.contains(cur!) {
        return true
      }
      cur = scalarIt.next()
    }
    return false
  }
}

enum TuneContext {
  case FileHeader
  case Header
  case Score
}

enum Accidental {
  case Natural
  case Sharp
  case Flat
  case DoubleSharp
  case DoubleFlat
}

class ABCParser {
  var tokenPos : Int = 0
  let lastBound : String.Index
  let lastOffs : Int
  var abcSrc : String
  var curChar : Character
  var curOffs : Int = 0
  var curBound : String.Index
  var nextBound : String.Index
  var nextChar : Character
  var curToken : String
  var endOfLine : Bool = false
  var newLine : Bool = false
  var chordStarted : Bool = false
  var finalStarted : Bool = false
  var completed : Bool = false
  var line : Int = 0
  var col  : Int = 0
  var eof : Bool = false
  // store lines offset for later NSView usage
  var linesOffsets : [Int] = [0]
  
  init ( src : String ) {
    abcSrc = src
    lastBound = abcSrc.endIndex
    curBound = abcSrc.index(abcSrc.startIndex, offsetBy: 0)
    nextBound = curBound
    curChar = abcSrc[curBound]
    nextChar = abcSrc[nextBound]
    curToken = String(curChar)
    linesOffsets[0] = -1
    linesOffsets.insert(curOffs, at:line )
    lastOffs = abcSrc.distance(from: abcSrc.startIndex, to: lastBound)
  }
  
  private func debugLinesOffset()  {
    var ii = 0
    for cur in linesOffsets {
      DDLogDebug("index \(ii) : \(cur)")
      ii+=1
    }
  }
  
  /**
    used when specific encoding is requested 
  */
  func replaceSource( src : String ) {
    self.abcSrc = src
  }
  
  private func buildABCToken( start : Int = 0,
    stop: Int = 0,
    token : String? = nil ,
    type : ABCTokenType ,
    content : ParsedData? = nil ,
    line : Int ,
    col : Int) -> ABCToken {
    let returned = ABCToken(start: start, stop: stop, token: token, type: type, content: content, line: line, col: col)
    return returned.myDebug(src: self.abcSrc)
  }
  
  private func doEndOfLine() {
    line += 1
    linesOffsets.insert(tokenPos, at:line ) // next line init
    newLine = true
  }
  
  private func isEndOfLine( _ char: Character ) -> Bool {
    let newLines = CharacterSet.newlines
    if (  ParseUtils.belongs(char: char , to: newLines ) ) {
      endOfLine = true
    } else {
      endOfLine = false
    }
    return endOfLine
  }
  
  private func isCharacter( char : Character ) -> Bool {
    let letters = CharacterSet.letters
    return ParseUtils.belongs( char: char , to:letters)
  }
  
  private func isLowerCase( char : Character ) -> Bool {
    let letters = CharacterSet.lowercaseLetters
    return ParseUtils.belongs( char: char , to:letters)
  }
  
  private func isNumerical( char : Character ) -> Bool {
    let numerical = CharacterSet.decimalDigits
    return ParseUtils.belongs( char: char , to:numerical)
  }
  
  private func isNoteLength( char : Character ) -> Bool {
    let numbers = CharacterSet(charactersIn: "123456789/<>")
    return ParseUtils.belongs( char: char , to:numbers)
  }
  
  private func isNoteOctave( char : Character ) -> Bool {
    let numbers = CharacterSet(charactersIn: "',")
    return ParseUtils.belongs( char: char , to:numbers)
  }
  
  private func isInformationSet( char: Character ) -> Bool {
    let informationChars = CharacterSet(charactersIn: "ABCDFGHIKLMmNOPQRrSTUVWwXZ")
    return ParseUtils.belongs(char: char, to:informationChars)
  }
  
  private func isDecoratorSet( char: Character ) -> Bool {
    let decoratorChars = CharacterSet(charactersIn: ".~HLMOPSTuv")
    return ParseUtils.belongs(char: char, to:decoratorChars)
  }
  
  private func isScoreNotationSet( char : Character ) -> Bool {
    let informationChars = CharacterSet(charactersIn: "|:[]()")
    return ParseUtils.belongs(char: char, to:informationChars)
  }
  
  private func isAccidentalSet( char : Character ) -> Bool {
    let informationChars = CharacterSet(charactersIn: "^=_")
    return ParseUtils.belongs(char: char, to:informationChars)
  }
  
  private func isEof() -> Bool {
    if ( curBound == lastBound ) {
      return true
    }
    return false
  }
  
  private func isNoteSet( char: Character ) -> Bool {
    let noteChars = CharacterSet(charactersIn: "AaBbCcDdEeFfGgzx")
    return ParseUtils.belongs(char: char, to:noteChars)
  }
    
  private func advance() -> Bool {
    if ( eof || ( curBound == lastBound ) ) {
      curChar = " "
      return true
    }
    var endOfSource : Bool = false
    curChar = nextChar
    curBound = nextBound
    col += 1
    curToken += String(curChar)
    curOffs = abcSrc.distance(from: abcSrc.startIndex, to: curBound)
    tokenPos += 1
    if ( tokenPos < lastOffs ) {
      nextBound = abcSrc.index(abcSrc.startIndex, offsetBy: tokenPos)
      nextChar = abcSrc[nextBound]
    } else {
      endOfSource = true
    }
    return endOfSource
  }

  private func jumpEOL() -> String {
    var returned = ""
    var commentFound : Bool = false
    while ( !isEndOfLine(curChar) && !isEof() ) {
      eof = advance()
      if ( !eof ) {
        if ( nextChar == "%")  {
          // inlined comment
          commentFound = true // assume comment until EOL
        }
        if ( !commentFound ) {
          if ( !isEndOfLine(curChar) ) {
            returned += String(curChar)
          }
        }
      } else {
        if ( !isEndOfLine(curChar)) {
          returned += String(curChar)
        }
        return returned.trimmingCharacters(in: .whitespaces) // EOF reached
      }
    }
    // eof = advance() // bypass \n
    doEndOfLine()
    return returned.trimmingCharacters(in: .whitespaces)
  }
  
  private func expect( sequence: String ) -> Bool {
    var sequencePos : Int = 0
    eof = advance()
    while (!isEndOfLine(curChar) && !eof) {
      if ( curChar == " " ) {
        eof = advance()
      } else {
        let curSequence = sequence[sequencePos]
        if ( curSequence == curChar ) {
          sequencePos += 1
          if ( sequencePos >= sequence.count ) {
            return true
          }
          eof = advance()
        } else {
          return false
        }
      }
    }
    return false
  }
  
  private func parseNote() -> ABCNote?{
    let note = curChar
    var strNote = String(curChar)
    var noteLength : Int? = nil
    var octave : Int = 0
    var dottedPrev : Int = 0
    var dottedNext : Int = 0
    var tied : Bool = true
    var decorator: String? = nil
    var accidental : Accidental = .Natural
    let startOffset : Int = curOffs
    
    if ( isDecoratorSet(char: curChar)) {
      decorator = String(curChar)
      eof = advance()
      // bypass spaces afer decorator if any
      while ( curChar == " " && !eof ) {
        eof = advance()
      }
    } else if ( curChar == "!") {
      decorator = ""
      eof = advance()
      while ( curChar != "!" ) {
        decorator?.append(curChar)
        eof = advance()
      }
      eof = advance()
      while ( curChar == " ") {
        eof = advance()
      }
    }
    
    // starting chord may happen here
    if ( curChar == "[" ) {
      self.chordStarted = true
      eof = advance()
    }
    
    if ( isAccidentalSet(char: curChar)) {
      switch curChar {
      case "^" :
        if (nextChar == "^") {
          accidental = .DoubleSharp
          eof = advance()
        } else {
          accidental = .Sharp
        }
        eof = advance()
        strNote = String(curChar)
        break
      case "_" :
        if (nextChar == "_") {
          accidental = .DoubleFlat
          eof = advance()
        } else {
          accidental = .Flat
        }
        eof = advance()
        strNote = String(curChar)
        break
      case "=" :
        accidental = .Natural
        eof = advance()
        break
      default :
        accidental = .Natural
        break
      }
    }
    
    if ( !isNoteSet(char: curChar) ) {
      return nil
    }
    let noteCol : Int = col // keep col position for voice handling
    if isLowerCase(char: note) {
      octave += 1
    }
    while ( isNoteOctave(char: nextChar)) {
      eof = advance()
      if ( curChar == "," ){
        octave -= 1
      } else { // ' character
        octave += 1
      }
    }
    
    if ( isNoteLength(char: nextChar)) {
      eof = advance()
      if ( curChar == "/" ) {
        noteLength = -1  // reduce note duration scale 
        while ( nextChar == "/" ) {
          noteLength! -= 1
          eof = advance()
        }
      } else if (curChar == ">" || curChar == "<") {
        let curDotted = curChar
        while  ( curChar == curDotted ) {
          if ( curDotted == ">") {
            dottedPrev += 1
          } else {
            dottedNext += 1
          }
          eof = advance()
        }
      } else {
        noteLength = Int(String(curChar))
        if ( noteLength == nil) {
          return nil
        }
      }
    }
    
    if ( nextChar == " ") {
      tied = false
      eof = advance()
    }
    let endOffset : Int = curOffs
    return ABCNote(note:strNote,
                   octave:octave,
                   duration:noteLength,
                   tied:tied,
                   accidental: accidental,
                   chord: chordStarted,
                   decorator: decorator,
                   column: noteCol ,
                   line: line ,
                   startOffset: startOffset ,
                   endOffset: endOffset
                  )
    
  }
  
  func nextToken( context: TuneContext ) -> ABCToken {
    // fisrt check end of source reached
    curToken = ""
    
    while ( (nextChar == " " || nextChar == "\t" ) && !eof ) {
      curToken = ""
      eof = advance()
    }

    // Now get non blank character
    eof = advance()
    // trap any endofLine Next before parsing
    if ( isEndOfLine(curChar) ) { // empty line case
      doEndOfLine()
      eof = advance()
    }
    
    if ( eof  && (curToken == "")) { // eof + curChar handled since eof happens on nextChar
      return buildABCToken(token:String(curChar),type:.EndOfSource,content:ParsedData.StdContent(data: ""), line: line , col: col)
    }
 
    let start = curOffs
    if newLine {
      col = 0
      newLine = false 
    }

    if ( nextChar == "]" ) {
      // a final may also close here ... or not
      // since closing is optional : just ignore it
      if ( !chordStarted ) {
        eof = advance()
      }
    }
    
    switch curChar {
    case "%" :
      if ( nextChar == "%" ) {
        eof = advance() // bypass directive second token
        // Directive
        curToken = jumpEOL()
        return buildABCToken(start:start,stop:curOffs,type:.StyleSheetDirective,content:ParsedData.StdContent(data: curToken), line: line , col: col)
      } else if ( expect(sequence:"abc") ) {
        eof = advance()
        curToken = jumpEOL()
        return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.ABCVersion,content:ParsedData.StdContent(data: curToken), line: line , col: col)
      } else {
        // Comment just bypass
        curToken = jumpEOL()
        return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.Comment,content:ParsedData.StdContent(data: curToken), line: line , col: col)
      }
      
    default:
      // check for potential lineBreaks
      if (( curChar == "!" || curChar == "$" ) && (self.eof)) {
        eof = advance()  // bypass 
      }
      
      var maybeInformation : Bool = true
      if ( isNoteSet(char: curChar) && ( context == .Score) ) {
        // G:|| sequence may lead to information token to be misinterpreted inside score 
        maybeInformation = false
      }
      if ( (maybeInformation) && ( isInformationSet(char: curChar) && nextChar == ":") ) {
        let informationToken = curChar
        eof = advance()  // bypass :
        // Information field
        let info =  jumpEOL()
        return buildABCToken(start:start,stop:curOffs,token:String(informationToken),type:.InformationField,content:ParsedData.StdContent(data:info), line: line , col: col)
      } else {
        if ( isEndOfLine(curChar) ) { // empty line case
          doEndOfLine()
          return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.EmptyLine,content:ParsedData.StdContent(data: curToken), line: line , col: col)
        } else {
          // Something else
          if ( context == .Score ) {
            // ignore $ ignore endOfLine
            if ( curChar == "$") {
              eof = advance()  // bypass :
            }
            // Check Notes first
            if ( curChar == "!" ) {
              // capture empty decorators
              if ( isEndOfLine(nextChar) ) {
                eof = advance() // bypass decorator
                eof = advance() // bypas EOL
                doEndOfLine()
              }
            }
            if ( isNoteSet(char: curChar) ||
                 isDecoratorSet(char: curChar) ||
                 ( curChar == "!" ) || // Note decoration
                 isAccidentalSet(char: curChar) ) {
              let scoreNote: ABCNote? = parseNote()
              if ( scoreNote == nil ) {
                return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.InvalidToken,content:ParsedData.StdContent(data: curToken), line: line , col: col)
              }
              return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.Note,content:ParsedData.Note(note: scoreNote!),  line: line , col: col)
            } else {
              if ( isScoreNotationSet(char: curChar)) {
                switch(curChar) {
                case "|" :
                  if ( isNumerical(char: nextChar)) {
                    eof = advance()
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.RepeatBarEnd,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                  }
                  if ( nextChar == ":" ) {
                    eof = advance()
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.RepeatBarStart,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                  }
                  if ( nextChar != "|" ) {
                    return ABCToken(start:start,stop:curOffs,token:String(curChar),type:.Bar,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                  }
                  eof = advance()
                  if ( nextChar == ":" ) {
                    eof = advance()
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.RepeatBarStart,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                  } else {
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.DoubleBar,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                  }
                case "(" : // start tie
                  return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.StartTie,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                case ")" : // end Tie
                  return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.EndTie,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                case ":" :
                  if ( nextChar == "|" ) {
                    eof = advance()
                    if ( nextChar == "|") {
                      eof = advance()
                      if ( nextChar == ":") {
                        eof = advance()
                      }
                    }
                    if ( isNumerical(char:nextChar)) {
                      eof = advance()
                    }
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.RepeatBarEnd,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                  }
                  return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.InvalidToken,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                case "]" :
                  if ( chordStarted ) {
                    chordStarted = false
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.ChordEnded,content:ParsedData.StdContent(data: curToken),line: line , col: col)

                  } else {
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.FinalEnded,content:ParsedData.StdContent(data: curToken), line: line , col: col)

                  }
                case "[" :
                  eof = advance()
                  if ( isNumerical(char: curChar)) {
                    // Final [1 ... [2
                    return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.Final,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                  }
                  if ( isInformationSet(char: curChar) && nextChar == ":") {
                    let infoChar = curChar
                    eof = advance()
                    var vId : String = ""
                    while ( nextChar != "]" && !eof ) {
                      eof = advance()
                      vId.append(curChar)
                    }
                    eof = advance() // bypass ]
                    if ( eof ) {
                      return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.EndOfSource,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                    }
                    return ABCToken(start:start,stop:curOffs,token:String(infoChar),type:.InformationField,content:ParsedData.StdContent(data: vId), line: line , col: col)
                  } else {
                    // check for chords
                    while ( curChar == " ") && ( !eof ) {
                      eof = advance()
                    }
                    if ( isNoteSet(char: curChar) ||
                         isDecoratorSet(char: curChar) ||
                        ( curChar == "!" ) || // Note/chord decoration
                        isAccidentalSet(char: curChar) ) {
                      self.chordStarted = true
                      let scoreNote: ABCNote? = parseNote()
                      if ( scoreNote == nil ) {
                        return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.InvalidToken,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                      }
                      return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.Note,content:ParsedData.Note(note: scoreNote!),  line: line , col: col)
                    }
                  }
                  return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.InvalidToken,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                default :
                  return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.InvalidToken,content:ParsedData.StdContent(data: curToken), line: line , col: col)
                }
              } else {
                // Here we assume a free text comment line if we're not in score context
                curToken =  String(curChar) + jumpEOL()
                return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.FreeText,content:ParsedData.StdContent(data: curToken), line: line , col: col)
              }
            }
          } else {
            // Here we assume a free text comment line if we're not in score context
            curToken =  String(curChar) + jumpEOL()
            return buildABCToken(start:start,stop:curOffs,token:String(curChar),type:.FreeText,content:ParsedData.StdContent(data: curToken), line: line , col: col)
          }
        }
      }
    }
  }
  
  /**
   fully parse absSrc returning token array images Image
  */
  func parse() throws  -> ABCIR {
    let returned = ABCIR(parser:self)
    returned.parse()
    debugLinesOffset()
    self.completed = true
    return returned
  }
}
