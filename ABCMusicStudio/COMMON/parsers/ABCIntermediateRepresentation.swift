//
//  ABCIntermediateRepresentation.swift
//  ABCStudio
//
//  Created by jymengant on 11/6/18 week 45.
//  Copyright © 2018 jymengant. All rights reserved.
//
//  Intermediate parsed ABC representation data
//

import Foundation
import CocoaLumberjack


struct SyntaxError {
  let message : String
  let token : ABCToken
  let severe : Bool
}

private func syntaxError(_ token : ABCToken , msg: String , severe : Bool = true) -> SyntaxError {
  var err : String
  if ( severe ) {
    err =  loc.tr("SEVERE at LINE:% COL:% ABC Parsing problem:%",token.line+1,token.col,msg)
  } else {
    err = loc.tr( "Warning at LINE:% COL% ABC Parsing problem:%",token.line+1,token.col,msg)
  }
  return SyntaxError(message: err ,token:token,severe:severe )
}

struct ABCStyleSheet {
}

struct ABCFreeText {
  let line : Int
  let text : String
}

struct ABCTypesetText {
  let id : String
  let value : String
  let line : Int
}

enum InformationFieldType :String {
  case Area = "A"
  case Book = "B"
  case Composer = "C"
  case Discography = "D"
  case FileUrl = "F"
  case Group = "G"
  case History = "H"
  case Instruction = "I"
  case Key = "K"
  case UnitNoteLength = "L"
  case Meter = "M"
  case Macro = "m"
  case Notes = "N"
  case Origin = "O"
  case Parts = "P"
  case Tempo  = "Q"
  case Rythm = "R"
  case Remarks = "r"
  case Source = "S"
  case Voice = "V"
  case SymbolLine = "s"
  case TuneTitle = "T"
  case UserDefined = "U"
  case Words = "W"
  case TunesWords = "w"
  case ReferenceNumber = "X"
  case Transcription = "Z"
}



struct Ratio {
  let type : InformationFieldType
  let left : Int
  let right : Int
  let speed : Int?
}

public enum KeyMode:String {
  case Minor = "m"
  case Major = "M"
  case Ionian = "Ionian"
  case Aeolian = "Aeolian"
  case Mixolydian = "Mixolydian"
  case Dorian = "Dorian"
  case Phrygian = "Phrygian"
  case Lydian = "Lydian"
  case Locrian = "Locrian"
  case Unknown = "Unknown"
}

enum Alteration: String {
  case Flat = "b"
  case Sharp = "#"
  case Natural = ""
}

class ABCInformationField {
  let type : InformationFieldType
  let content : String
  
  init( type: InformationFieldType , content: String ) {
    self.type = type
    self.content = content
  }
}


struct ABCStylesheetDirective {
  let directive : String
  let content : String?
}


enum ColoringType : Int , Codable {
  case Comment = 0
  case Directive
  case InformationField
  case Lyrics
  case Text
  case PlaceHolder
  case Error
  case Warning
  case InfoMsg
  case Title
  case X
  case Note
  case Unknown // keep unknown in last enum position for GUI table presentation order reason
  // folowing case are used in tree only
  case DirtyScoreFlag
  case FolderTreeItem
  case ScoreTreeItem
  case TuneTreeItem
}

struct ABCColorElement {
  private let _DEBUG_ : Bool = false

  let type : ColoringType
  let offset : Int
  var length : Int 
  
  func debug(src: String) {
    if ( _DEBUG_ ) {
      let colored = src[offset...offset+length-1]
      DDLogDebug("ColorElement : type:\(type) , offset: \(offset) , length: \(length) , content:\(colored)" )
    }
  }
}

enum ABCTie {
  case Start
  case End
}

enum ScoreElement {
  case InfoField(field : ABCInformationField)
  case Note( note: ABCNote)
  case Notation( type: ABCTokenType )
  case Final( number: Int )
  case Key( key: TuneKey )
  case Tie( tie : ABCTie )
  case NoteDim( dim : Ratio)
  case Annotation( text: String)
  case Rythm( text: String)
  case Remark( text: String)
  case Part( text: String)
  case LineBreak
  
}

struct ABCScorePart {
  let name: String
  let line : Int
}




class ABCScore {
  var elements : [ScoreElement] = [ScoreElement]()
  var lyrics : [String] = [String]()
  var titles : [String] = [String]()
  var symbols : [String] = [String]()
  var parts : [ABCScorePart] = [ABCScorePart]()
  
  func add( element: ScoreElement) {
    elements.append(element)
  }
  
  func addTitle( text: String ) {
    titles.append(text)
  }
  
  func addNotation(_ type: ABCTokenType) {
    elements.append(ScoreElement.Notation(type:type))
  }
  
  func add(lyrics : String ) {
    let lyricsArr : [String] = lyrics.components(separatedBy: " ")
    for element in lyricsArr {
      self.lyrics.append(element)
    }
  }
  
  func add(symbols : String ) {
    let lyricsArr : [String] = symbols.components(separatedBy: " ")
    for element in lyricsArr {
      self.symbols.append(element)
    }
  }
  
  func addPart(_ part: ABCScorePart) {
    parts.append(part)
  }
}

enum AbcStem  {
  case Up
  case Down
  
  init( withString : String ) {
    let upper = withString.uppercased()
    if ( upper == "UP" ) {
      self = .Up
    } else {
      self = .Down
    }
  }
}


class ScoreContainer {
  var score: ABCScore?

  func checkScore() {
    if ( score == nil ) {
      score = ABCScore()
    }
  }

  func addScoreLyrics(line: String){
    checkScore()
    score!.add(lyrics: line)
  }
  
  func addSymbols(line: String){
    checkScore()
    score!.add(symbols: line)
  }
  
  func addNotation( type: ABCTokenType) {
    checkScore()
    score!.addNotation(type)
  }
  
  func addFinal(num : Int) {
    checkScore()
    score!.add(element: ScoreElement.Final(number: num))
  }
  
  func add( element: ScoreElement) {
    checkScore()
    score!.add(element:element)
  }
  
  func add( title: String) {
    checkScore()
    score!.addTitle(text: title)
  }
  
}

class Voice : ScoreContainer {
  var id : String
  var name : String? // This sets the long version of the voice name, to be written before the staves at the start of a piece
  var subName : String? // Short version of the name, written before subsequent staves
  var stem : AbcStem? //This says that stems for this voice's notes should all go in the same direction, which may be "up" or "down"
  var clef : String? // Chooses the clef (treble, alto, or bass). It can also be bass+8 and so on.
  var middle : String?
  var staves : Int? // This is the number of staves (starting from the current one) to connect by tall vertical bar lines.
  var brace : Int? // This is the number of staves (starting from the current one) to be grouped together with a brace.
  var bracket: Int? // The number of staves to be grouped together by a bracket.
  var space: Int? // This defines or modifies the vertical space between this staff and the one below it.
  var gchords: Bool // This controls whether any guitar chords embedded in the current voice are actually written.
  var octave: Int?
  var transpose: Int?
  
  init( id: String ) {
    self.id = id
    self.gchords = false
  }
}

class FieldContainer {
  var informationFields : [ABCInformationField] = [ABCInformationField]()
  
  func addInformationField(_ field : ABCInformationField ) {
    informationFields.append(field)
  }
}

struct TuneKey {
  let key : String
  var alteration : Alteration = .Natural
  let keyMode : KeyMode
  var minorKey : Bool = false
}

class CommonHeader {
  var meter : Ratio? = nil
  var length : Ratio? = nil
  var tempoBase : Ratio? = nil
  var notes : [String] = [String]()
  var origin : String?
  var rythm : String? = nil
  var remark : String? = nil
  var history : String? = nil


  func parseLength( ir : ABCIR ) -> SyntaxError? {
    let ret = ir.parseLength(toParse: ir.lastToken)
    if let length = ret.1  {
      self.length = length
    }
    return ret.0
  }
  
  func parseMeter( ir : ABCIR ) -> SyntaxError? {
    let ret = ir.parseMeter(toParse: ir.lastToken)
    if let meter = ret.1  {
      self.meter = meter
    }
    return ret.0
  }
  
  func parseTempo( ir : ABCIR ) -> SyntaxError? {
    let ret = ir.parseTempo(toParse: ir.lastToken)
    if let tempo = ret.1  {
      self.tempoBase = tempo
    }
    return ret.0
  }
  
  func add( history : String ) {
    if ( self.history == nil ) {
      self.history = history
    } else {
      self.history?.append(" ")
      self.history?.append(history)
    }
  }
  
  func add( notes: String ) {
    self.notes.append(notes)
  }
}

class ABCTuneHeader : FieldContainer {
  let tuneNumber : Int?
  var tittle : String?
  var key : TuneKey?
  var infoFields = [ABCInformationField]()
  var part : String? = nil
  var voices : [String:Voice] = [String:Voice]()
  var common : CommonHeader = CommonHeader()
  
  init ( _ number: Int? ) {
    tuneNumber = number
  }
  
  func addVoice(_ voice : Voice) {
    voices[voice.id] = voice
  }
  
  func getVoice( id : String ) -> Voice? {
    return voices[id]
  }
  
}



class ABCTune : ScoreContainer {
  let header : ABCTuneHeader
  var typesetText : [String: ABCTypesetText] = [String: ABCTypesetText]()
  var lyrics : String? = nil
  var curVoice : Voice? = nil
  var defVoice : Voice? = nil


  init( head : ABCTuneHeader ) {
    header = head
  }
  
  func addLyric(line: String) {
    if ( lyrics == nil ) {
      lyrics = line
    } else {
      lyrics! += "\n"
      lyrics! += line
    }
  }
  
  override func add( element: ScoreElement) {
    checkScore()
    if let curVoice = curVoice  {
      curVoice.add(element:element)
    } else {
      score!.add(element:element)
    }
  }
  
  func addVoice(_ voice : Voice) {
    checkScore()
    header.addVoice(voice)
  }
  
  func enterVoice( voiceID : String ) -> Voice? {
    checkScore()
    curVoice = header.voices[voiceID]
    return curVoice
  }
}

class ABCHeader : FieldContainer {
  var version : String? = nil
  var directives : [String:ABCStylesheetDirective] = [String:ABCStylesheetDirective]()
  var freeText : [ABCFreeText] = [ABCFreeText]()
  var defVoice : Voice? = nil
  var common : CommonHeader = CommonHeader()

  func addDirective( _ token : ABCToken ) {
    let tkStr = token.content!.getStd()!
    let result = tkStr.split(separator: " ")
    if ( result.count > 0 ) {
      let name = String(result[0])
      var content : String? = nil
      if ( result.count > 1 ) {
        content = String(result[1])
      }
      directives[name] = ABCStylesheetDirective(directive:name, content: content)
    }
  }
  
}

struct Property {
  let key : String
  let value : String
}


class ABCIR {
  var lastToken : ABCToken = ABCToken(token:"",type:.NotStarted, line: 0 , col: 0)
  var fileHeader : ABCHeader
  var abcTunes = [ABCTune]()
  var tuneList = [TuneDescription]()
  var curDescription : TuneDescription? = nil
  var notes : [ABCNote] = [ABCNote]()

  
  var countTunes : Int  {
    get {
      return tuneList.count
    }
  }
  
  var colors = [ABCColorElement]()
  let lex : ABCParser
  var encoding : Unicode? = nil
  var storedError : SyntaxError? = nil
  var curTune : ABCTune? = nil
  var errors : SyntaxError? {  // Parsing error if not nil
    set(newError) {
      if ( storedError == nil ) {
       storedError = newError
      }
    }
    get {
      return storedError
    }
  }
  
  var tunePos : TuneContext = .FileHeader // initially in file Header
  var lastEmpty : Int = -1

  init( parser : ABCParser ) {
    fileHeader = ABCHeader()
    self.lex = parser
  }
  
  private func addColoring( color : ABCColorElement) {
    colors.append(color)
  }
  
  private func addTune( _ number : Int ) -> ABCTune {
    let tuneHeader = ABCTuneHeader(number)
    let newTune = ABCTune(head:tuneHeader)
    curDescription = TuneDescription(number:number ,
                                     line: lastToken.line ,
                                     offset : lastToken.start
                                    )
    abcTunes.append(newTune)
    tuneList.append(curDescription!)
    return newTune
  }

  private func checkCurTune( number : Int ) -> ABCTune {
    if ( curTune == nil ) {
      curTune = addTune(number)
    }
    return curTune! ;
  }
  
  private func checkVoice( number : Int ) -> ABCTune {
    if ( curTune == nil ) {
      curTune = addTune(number)
    }
    return curTune! ;
  }
  
  private func getContent( content: String ) -> String? {
    var space = content.firstIndex(of: " ")
    if ( space == nil ) {
      return nil
    }
    space = content.index(after: space!) //bypass space
    return String(_:content.suffix(from: space!))
  }
  
  private func add(note: ABCNote ) {
    notes.append(note)
  }
  
  func getNote(at: Int , ending: Int) -> ABCNote?{
    for note in notes {
      if (
        ((at >= note.startOffset) && ( ending <= note.endOffset )) ||
        ((at <= note.startOffset) && ( ending >= note.endOffset))
      ) {
        return note
      }
    }
    return nil
  }
  
  func getTuneAt( offset : Int ) -> Int {
    var curTune : Int = 0
    for ii in 0...tuneList.count  {
      if ( tuneList[ii].offset! > offset ) {
        return curTune
      }
      curTune += 1
    }
    return curTune
  }

  func handleCharset( _ token: ABCToken , encoding: String ) -> SyntaxError? {
    if ( self.encoding == nil )  {
      self.encoding = Unicode(encoding:encoding)
      let newSrc = self.encoding!.decode(str:lex.abcSrc)
      lex.replaceSource(src: newSrc!)
      return nil
    } else {
      return syntaxError(token, msg:"Multiple encoding prohibited : \(encoding) ignored",severe : false)
    }
  }
  
  private func textOffset(_ ofs: Int ) -> Int {
    return ofs+1
  }
  
  private func parseProperty (token : String , separator : String) -> Property  {
    let pieces = token.components(separatedBy: separator)
    var value = pieces[1]
    if ( value.first == "\"") {
      value = value.dropFirst().string
    }
    if ( value.last == "\"") {
      value = value.dropLast().string
    }
    return Property(key:pieces[0] , value: value)
  }
  
  private func parseHeaderVoice( toParse: ABCToken ) -> (Voice? , SyntaxError?) {
    DDLogDebug("Entering parseVoice")
    var voice : Voice? = nil
    var error : SyntaxError? = nil
    let voiceData = toParse.content!.getStd()!
    //let idEnd = voiceData.indexOf(" ")
    //let voiceId = voiceData[voiceData.startIndex ..< find(voiceData, " ")!]
    let pieces = voiceData.components(separatedBy: " ")
    for piece in pieces {
      if ( piece.indexOf("=") != nil ) {
        let prop = parseProperty(token:piece,separator: "=")
        if ( prop.key == "name" || prop.key == "nm" ) {
          voice?.name = prop.value
        } else if ( prop.key == "clef") {
          voice?.clef = prop.value
        } else if ( prop.key == "middle") {
          voice?.middle = prop.value
        } else if (prop.key == "subname" || prop.key == "snm" ) {
          voice?.subName = prop.value
        } else if ( prop.key == "stem" ) {
          voice?.stem = AbcStem(withString: prop.value)
        } else if ( prop.key == "staves" ) {
          voice?.staves = Int(prop.value) ?? 2
        } else if ( prop.key == "brace" ) {
          voice?.brace = Int(prop.value) ?? 2
        } else if ( prop.key == "bracket" ) {
          voice?.bracket = Int(prop.value) ?? 4
        } else if ( prop.key == "space" ) {
          voice?.space = Int(prop.value) ?? 40
        } else if ( prop.key == "octave" ) {
          voice?.octave = Int(prop.value) ?? 0
        } else if ( prop.key == "transpose" ) {
          voice?.transpose = Int(prop.value) ?? 0
        } else if ( prop.key == "gchords" ) {
          voice?.gchords = Bool(prop.value) ?? false
        } else {
          error = syntaxError(toParse,msg:"invalid voice property field  : \(piece) ")
        }
      } else {
        if ( voice == nil ) {
          voice = Voice(id: piece)
        } else {
          if ( piece == "treble" ) {
            voice?.clef = piece
          }
        }
      }
    }
    return (voice , error)
  }
  
  private func checkKeyMode( _  mode : String , token: ABCToken ) -> KeyMode {
    if ( mode.lowercased().hasPrefix("Ionian") ) {
      return .Ionian
    }
    if ( mode.lowercased().hasPrefix("min") ||
         mode.lowercased().hasPrefix("m")
      ) {
      return .Minor
    }
    if ( mode.lowercased().hasPrefix("major") ) {
      return .Major
    }
    if ( mode.lowercased().hasPrefix("ionian") ) {
      return .Ionian
    }
    if ( mode.lowercased().hasPrefix("aeolian") ) {
      return .Aeolian
    }
    if ( mode.lowercased().hasPrefix("mixolydian") ) {
      return .Mixolydian
    }
    if ( mode.lowercased().hasPrefix("locrian") ) {
      return .Locrian
    }
    if ( mode.lowercased().hasPrefix("lydian") ) {
      return .Lydian
    }
    if ( mode.lowercased().hasPrefix("dorian") ||
         mode.lowercased().hasPrefix("dor")
      ) {
      return .Dorian
    }
    return .Unknown
  }
  
  private func parseKey( toParse: ABCToken ) -> ( SyntaxError? , TuneKey? ) {
    // A-G[#|b][m]
    var alteration : Alteration = .Natural
    var keyMode : KeyMode = .Unknown
    let minorkey : Bool = false
    let keyString = toParse.content!.getStd()!
    if ( keyString.count == 0 ) {
      return ( syntaxError(toParse, msg: loc.tr("undefined empty Key")), nil )
    }
    let keyChars = CharacterSet(charactersIn: "ABCDEFG")
    let first = toParse.content!.getStd()!.startIndex
    if (  ParseUtils.belongs(char: keyString[first] , to: keyChars ) ) {
      if ( first == keyString.endIndex) {
        return ( nil , TuneKey(key:keyString, alteration:alteration ,keyMode: .Unknown, minorKey: minorkey) )
      }
      var next =  keyString.index(after:first)
      if ( next == keyString.endIndex) {
        return ( nil , TuneKey(key:keyString, alteration:alteration, keyMode: .Unknown, minorKey: minorkey) )
      }
      let alter = String(keyString[next])
      if ( alter == "b" || alter == "#") {
        alteration = Alteration(rawValue: String(keyString[next]))!
        next =  keyString.index(after:next)
      }
      if ( next == keyString.endIndex) {
        return ( nil , TuneKey(key:keyString, alteration:alteration,keyMode: .Unknown,minorKey: minorkey) )
      }
      let reminder = keyString[next ..< keyString.endIndex]
      keyMode = checkKeyMode(String(reminder), token: toParse)
      if ( keyMode == .Unknown ) {
        // extraneous like clef= ... accepted here
        return ( nil , TuneKey(key:keyString, alteration:alteration,keyMode: .Unknown,minorKey: minorkey) )
      } else {
        return ( nil , TuneKey(key:keyString, alteration:alteration,keyMode: keyMode,minorKey: minorkey) )
      }
    } else {
      // check for accptable values : treble,bass,baritone,tenor,alto,mezzosoprano,soprano
      if ( keyString == "treble" ||
        keyString == "bass" ||
        keyString == "baritone" ||
        keyString == "tenor" ||
        keyString == "alto" ||
        keyString == "soprano" ||
        keyString == "mezzosopranotreble" ) {
        return ( nil , TuneKey(key:keyString, alteration:alteration,keyMode: .Unknown,minorKey: minorkey) )
      }
    }
    return (syntaxError(toParse , msg:"Invalid score key : \(keyString)"),nil)
  }

  private func parseRatio(type:  InformationFieldType , toParse: ABCToken ) -> Ratio? {
    let ratio = toParse.content!.getStd()!
    let slash = ratio.firstIndex(of: "/")
    let equal = ratio.firstIndex(of: "=")
    var nRatio : String.Index? = slash
    var speed : Int? = nil
    if ( nRatio != nil ) {
      let left = ratio[ratio.startIndex ..< nRatio! ]
      let leftInt = Int(left)
      let next = ratio.index(after:nRatio!)
      if ( next != ratio.endIndex ) {
        if let equal = equal {
          nRatio = equal
        } else {
          nRatio = ratio.endIndex
        }
        let right = ratio[next ..< nRatio! ]
        let rightInt = Int(right)
        if let equal = equal {
          let next = ratio.index(after:equal)
          let speedStr = ratio[next ..< ratio.endIndex ]
          speed = Int(speedStr)
        }
        if ( leftInt != nil && rightInt != nil ) {
          return Ratio(type:type ,left:leftInt!,right:rightInt!,speed: speed)
        }
      }
    }
    return nil
  }
  
  func parseTempo( toParse: ABCToken ) -> ( SyntaxError? , Ratio? , Int?) {
    let tempo = toParse.content!.getStd()!
    let tempoInt = Int(tempo)
    if let tempoInt = tempoInt {
      // simplest patern
      return (nil,nil , tempoInt) // good shape
    }
    var tempoBeat : Int? = nil
    let tempoBase = parseRatio(type: .Tempo, toParse: toParse)
    if ( tempoBase == nil ) {
      return (syntaxError(toParse , msg:"Invalid score key : \(toParse.content!)"),nil,nil)
    }
    let equal = tempo.firstIndex(of: "=")
    if ( equal != nil ) {
      let next = tempo.index(after:equal!)
      if ( next != tempo.endIndex) {
        let beat = tempo[next ..< tempo.endIndex ]
        tempoBeat = Int(beat)
        if ( tempoBeat != nil ) {
          return (nil,tempoBase , tempoBeat) // good shape
        }
      }
    }
    return (syntaxError(toParse , msg:"Invalid tempo : \(tempo)"),nil,nil)
  }
  
  func parseMeter( toParse: ABCToken ) -> ( SyntaxError?, Ratio?) {
    let strContent = toParse.content!.getStd()!
    if ( strContent == "C" ) { // -> 4/4
      return (nil, Ratio(type: .Meter, left: 4, right: 4, speed: nil))
    } else if ( strContent == "C|") {
      return (nil, Ratio(type: .Meter, left: 2, right: 2, speed: nil))
    } else {
      let meter = parseRatio(type: .Meter,toParse: toParse)
      if ( meter == nil ) {
        return (syntaxError(toParse, msg: "Invalid non numerical ratio : \(toParse.content!)"),nil)
      }
      return (nil,meter)
    }
  }
  
  func parseLength( toParse: ABCToken ) -> (SyntaxError?, Ratio?) {
    let length = parseRatio(type: .UnitNoteLength ,toParse: toParse)
    if ( length == nil ) {
      return ( syntaxError(toParse, msg: "Invalid non numerical duration : \(toParse.content!)"),nil)
    }
    return (nil , length)
  }

  
  func newTune( numStr : String) -> SyntaxError? {
    tunePos = .Header
    let tuneNumber : Int? = Int(numStr)
    if ( tuneNumber == nil) {
      return syntaxError(lastToken,msg:"Invalid tune Number: \(numStr) ")
    }
    if let number = tuneNumber {
      curTune = addTune(number)
    }
    return nil
  }
  
  func addHeaderVoice() -> String? {
    let ( voice , error ) = parseHeaderVoice(toParse: lastToken)
    if let error = error {
      errors = error
      return nil
    } else {
      // V:id score in header
      // check def voice
      if ( voice!.id == "*") {
        if let curtune = self.curTune {
          curtune.defVoice = voice
        }
      } else  {
        self.curTune?.addVoice(voice!)
      }
      return voice?.id
    }
  }
    
  func addInformationField( token : ABCToken ) {
    let type : InformationFieldType = InformationFieldType(rawValue: token.token!)!
    let strContent = token.content!.getStd()!
    let field = ABCInformationField(type:type ,content: strContent)
    
    // Handle Coloring
    switch( type ) {
    case .Words , .TunesWords:
      addColoring(color: ABCColorElement(type:.Lyrics,offset:lastToken.start,length:lastToken.length))
      break
    case .ReferenceNumber :
      addColoring(color: ABCColorElement(type:.X,offset:lastToken.start,length:lastToken.length))
      
      break
    case .TuneTitle :
      addColoring(color: ABCColorElement(type:.Title,offset:lastToken.start,length:lastToken.length))
      break
    default :
      addColoring(color: ABCColorElement(type:.InformationField,offset:lastToken.start,length:lastToken.length))
    }
    
    switch ( tunePos ) {
    case .FileHeader :
      let header = self.fileHeader
      switch ( type ) {
      case .Instruction :
        if ( token.content!.getStd()!.starts(with: "abc-charset")) {
          let charset = getContent(content:lastToken.content!.getStd()!)
          errors = handleCharset(lastToken, encoding: charset!)
        } else {
          header.addInformationField(field)
        }
      case .History :
        header.common.add(history: field.content)
        break
      case .Area , .Book , .Composer , .Discography , .FileUrl , .Group , .Transcription , .Source:
        header.addInformationField(field)
        break
      case .UnitNoteLength :
        errors =  header.common.parseLength(ir: self)
        break
      case .Meter :
        errors =  header.common.parseMeter(ir:self)
        break
      case .Tempo :
        errors = header.common.parseTempo(ir:self)
        break
      case .Notes :
        header.common.add(notes: strContent)
        break
      case .Origin :
        header.common.origin = strContent
        break
      case .Rythm :
        header.common.rythm = strContent
        break
      case .Remarks :
        header.common.remark = strContent
        break
      case .Voice :
        let ( voice , error ) = parseHeaderVoice(toParse: lastToken)
        if let error = error {
          errors = error
        } else {
          if ( voice!.id == "*") {
            fileHeader.defVoice = voice
          } else  {
            errors = syntaxError(lastToken,msg:"Non default voice in file header : \(lastToken.token!) ")
          }
        }
        break
      case .ReferenceNumber :
        errors = newTune(numStr: strContent)
        break
      default :
        errors =  syntaxError(lastToken,msg:"MisPlaced \(type.rawValue) information field in file header : \(lastToken.token!) ")
        break
      }
      
    case .Header :
      let header = checkCurTune(number: 1).header

      switch ( type ) {
        
      case .ReferenceNumber :
        errors = newTune(numStr: strContent)
        break
      case .TuneTitle :
        header.tittle = strContent
        if ( curDescription?.title == nil  ) {
          // we take the first title when multiple titles are provided
          curDescription?.title = strContent
        }
        break
      case .History :
        header.common.add(history: field.content)
        break
      case .Area , .Book , .Composer , .Discography , .FileUrl , .Group , .Instruction ,.Transcription , .Source:
        header.addInformationField(field)
        break
      case .UnitNoteLength :
        errors = header.common.parseLength(ir:self)
        break
      case .Meter :
        errors = header.common.parseMeter(ir:self)
        break
      case .Tempo :
        errors = header.common.parseTempo(ir:self)
        break
      case .Notes :
        header.common.add(notes: strContent)
        break
      case .Origin :
        header.common.origin = strContent
        break
      case .Rythm :
        header.common.rythm = strContent
        break
      case .Remarks :
        header.common.remark = strContent
        break
      case .Parts :
        header.part = strContent
        break
      case .Voice :
        _ = addHeaderVoice()
        break
      case .Words :
        addColoring(color: ABCColorElement(type:.Lyrics,offset:lastToken.start,length:lastToken.length))
        checkCurTune(number: 1).addLyric(line:strContent)
        break
      case .Key :
        let ret = parseKey(toParse: lastToken)
        tunePos = .Score // end of tune Header
        if let key = ret.1  {
          checkCurTune(number: 1).header.key = key
        }
        errors = ret.0
        break
      default :
        errors = syntaxError(lastToken,msg:"MisPlaced \(type.rawValue) information field in score header : \(lastToken.token!) ")
        break
      }
    case .Score :
      let header = checkCurTune(number: 1).header // some header may appear inside score like history
      let curTune = checkCurTune(number: 1)
      let elem : ScoreElement = ScoreElement.InfoField(field: field)
      switch ( type ) {
      case .History : // History also acceptable in score
        header.common.add(history: field.content)
      break
      case .ReferenceNumber :
        // New Tune header context in Multiple tune context
        errors = newTune(numStr: strContent)
        break
      case .Instruction :
        curTune.add(element: elem)
        break
      case .UnitNoteLength :
        let ret = parseLength(toParse: lastToken)
        if let length = ret.1  {
          checkCurTune(number: 1).add(element: ScoreElement.NoteDim(dim: length))
        }
        errors = ret.0
        break
      case .Meter :
        let ret = parseMeter(toParse: lastToken)
        if let meter = ret.1  {
          checkCurTune(number: 1).add(element: ScoreElement.NoteDim(dim: meter))
        }
        errors = ret.0
        break
      case .Tempo :
        let ret = parseTempo(toParse: lastToken)
        if let tempo = ret.1  {
          checkCurTune(number: 1).add(element: ScoreElement.NoteDim(dim: tempo))
        }
        errors = ret.0
        break
      case .Notes :
        checkCurTune(number: 1).add(element: ScoreElement.Annotation(text: strContent))
        break
      case .Rythm :
        checkCurTune(number: 1).add(element: ScoreElement.Rythm(text: strContent))
        break
      case .Parts :
        checkCurTune(number: 1).add(element: ScoreElement.Part(text: strContent))
        break
      case .Remarks :
        checkCurTune(number: 1).add(element: ScoreElement.Remark(text: strContent))
        break
      case .SymbolLine :
        checkCurTune(number: 1).addSymbols(line: strContent)
        break
      case .TuneTitle :
        checkCurTune(number: 1).add(title: strContent)
        break
      case .Voice:
        // V:id in score [V:id]
        DDLogDebug("ScoreVoice")
        var voiceId : String? = strContent
        if ( token.type != .BracketedInformationField ) {
          voiceId  = addHeaderVoice()
        }
        let curVoice = checkCurTune(number: 1).enterVoice(voiceID:(voiceId)!)
        if ( curVoice == nil ) {
          errors = syntaxError(lastToken, msg: loc.tr("Undefined Voice ID : %",voiceId!))
        }
        break
      case .Key :
        let ret = parseKey(toParse: lastToken)
        if let key = ret.1  {
          checkCurTune(number: 1).add(element: ScoreElement.Key(key: key))
        }
        errors = ret.0
        break
      case .Words :
        checkCurTune(number: 1).addLyric(line:lastToken.content!.getStd()!)
        break
      case .TunesWords :
        checkCurTune(number: 1).addScoreLyrics(line:lastToken.content!.getStd()!)
        break
      default :
        errors = syntaxError(lastToken,msg:"MisPlaced \(type.rawValue) information field in score : \(lastToken.token!) ")
        break
      }
    }
  }
  
  func parse() {
    lastToken = lex.nextToken(context: tunePos)
    var parsed = false
    while ( !parsed ) {
      switch lastToken.type {
      case .EmptyLine :
        lastEmpty = lastToken.line
        break
      case .ChordEnded ,
           .FinalEnded :
        // nothing to do
        break
      case .ABCVersion :
        fileHeader.version = lastToken.content!.getStd()
        break
      case .StyleSheetDirective :
        addColoring(color: ABCColorElement(type:.Directive,offset:lastToken.start,length:lastToken.length))
        fileHeader.addDirective(lastToken)
        break
      case .Comment :
        // Just handle coloring
        addColoring(color: ABCColorElement(type:.Comment,offset:lastToken.start,length:lastToken.length))
        break
      case .FreeText :
        // Just handle coloring
        addColoring(color: ABCColorElement(type:.Comment,offset:lastToken.start,length:lastToken.length))
        fileHeader.freeText.append(ABCFreeText(line:lastToken.line , text: lastToken.content!.getStd()!))
        break
      case .InformationField , .BracketedInformationField:
        addInformationField(token: lastToken)
        break
      case .Bar :
        checkCurTune(number: 1).addNotation(type:.Bar)
        break;
      case .Final :
        let finalNum : Int? = Int(lastToken.token!)
        if let finalNum = finalNum {
          checkCurTune(number: 1).addFinal(num: finalNum)
        } else {
          errors = syntaxError(lastToken, msg: loc.tr("Non numerical finalization score  : %",lastToken.token!))
        }
        break
      case .DoubleBar :
        checkCurTune(number: 1).addNotation(type:.DoubleBar)
        break;
      case .RepeatBarEnd :
        checkCurTune(number: 1).addNotation(type:.RepeatBarEnd)
        break;
      case .RepeatBarStart :
        checkCurTune(number: 1).addNotation(type:.RepeatBarStart)
        break;
      case .Note :
        addColoring(color: ABCColorElement(type:.Note,offset:lastToken.start,length:lastToken.length))
        let curNote = lastToken.content!.getNote()!
        self.add(note:curNote)
        checkCurTune(number: 1).add(element: ScoreElement.Note(note: curNote))
        break
      case .StartTie :
        addColoring(color: ABCColorElement(type:.Note,offset:lastToken.start,length:lastToken.length))
        checkCurTune(number: 1).add(element:ScoreElement.Tie(tie: ABCTie.Start))
        break
      case .EndTie :
        addColoring(color: ABCColorElement(type:.Note,offset:lastToken.start,length:lastToken.length))
        checkCurTune(number: 1).add(element:ScoreElement.Tie(tie: ABCTie.End))
        break
      case .NoteSeparator :
        break
      case .EndOfSource :
        parsed = true
        break
      default :
        errors = syntaxError(lastToken, msg: loc.tr("Unexpected token type : %",lastToken.token!))
        parsed = true
        break
      }
      lastToken = lex.nextToken(context:tunePos)
      // DDLogDebug("Ofs:\(lastToken.start) len:\(lastToken.length) lastToken content: \(lastToken.toString(src:lex.abcSrc))")
    }
  }
  
  
}
