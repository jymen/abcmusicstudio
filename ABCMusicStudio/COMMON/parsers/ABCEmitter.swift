//
//  ABCEmitter.swift
//  ABCMusicStudio
//
//  Created by jymengant on 12/6/20 week 49.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation


class Emitter {
  
  var buffer : String = ""
  
  func eol() {
    buffer += "\n"
  }
  
  func space() {
    buffer += " "
  }
  
  func append( _ candidate : String? ) {
    if let candidate = candidate {
      buffer += candidate
    }
  }
  
  func reset() {
    buffer = ""
  }
  
  func emitCommentLine(_ str : String) {
    buffer += "% \(str)"
    eol()
  }
  
}

enum ABCScoreClef : String  {
  case G
  case F
  case C
  
  func emit() -> String {
    switch self {
    case .G :
      return "treble"
    case .F :
      return "bass"
    case .C :
      return "alto"
    }
  }
}

struct ABCScoreKey {
  var key : Int
  
  static func !=(lhs: ABCScoreKey, rhs: ABCScoreKey) -> Bool {
    if ( lhs.key != rhs.key ) {
      return true
    }
    return false
  }
  
  func emit() throws -> String {
    switch key {
    case 1:
      return "G"
    case 2:
      return "D"
    case 3:
      return "A"
    case 4:
      return "E"
    case 5:
      return "B"
    case 6:
      return "F#"
    case 7:
      return "C#"
    case 0 :
      return "C"
    case -1:
      return "F"
    case -2:
      return "Bb"
    case -3:
      return "Eb"
    case -4:
      return "Ab"
    case -5:
      return "Db"
    case -6:
      return"Gb"
    case -7:
      return"Cb"
    default :
      throw ABCFailure(message:"ABCScoreKey : \(key) Invalid key",kind:.ABCParsing)
    }
  }
}

//
// Multi voice usage
//
struct ScoreVoice {
  let id : Int
  let clef: ABCScoreClef?
  let name : String?
  let snm: String?
  let nicName : String 
  
  func emitHeader(emitter: ABCEmitter) {
    emitter.append("V:\(nicName) ")
    if let name = name {
      emitter.append("name=\"\(name)\" ")
    }
    if let snm = snm {
      emitter.append("snm=\"\(snm)\" ")
    }
    if let clef = clef {
      emitter.append("clef=\"\(clef.emit())\" ")
    }
    emitter.eol()
  }
  
  func emitLine(emitter:ABCEmitter) {
    emitter.append("[V:\(nicName)] ")
  }
}


struct ScoreInstrument  {
  let longName : String
  let shortName : String
  let trackName : String
}

enum ScoreABCDuration : String {
  case dwhole
  case dmeasure
  case dquarter
  case dhalf
  case deighth
  case d16th
  case d32nd
  case d64th
  case d128th

  func getAbcDuration(base: Int , dots: Int) -> String {
    var dur : Int = 0
    switch self {
    case .dwhole :
      dur = 8+dots
      break
    case .dmeasure :
      dur = 8+dots
      break
    case .dhalf :
      dur = 4+dots
      break
    case .dquarter :
      dur = 2+dots
      break
    case .deighth :
      if ( dots == 0 ) {
        return ""
      }
    case .d16th :
      return "/"
    case .d32nd :
      return "//"
    case .d64th :
      return "///"
    case .d128th :
      return "////"
    }
    return "\(dur)"
  }
  
  static func toABC( timeSig : ScoreABCTimeSig , duration : Int ) -> ScoreABCDuration? {
    let sigD = timeSig.sigD
    if ( (sigD != 4) && (sigD != 8) ) {
      return nil
    }
    switch duration {
    case 72 :
      if ( sigD == 4 ) {
        return .dmeasure
      }
      return .dhalf
    case 48 :
      if ( sigD == 4 ) {
        return .dhalf
      }
      return .dquarter
    case 24 :
      if ( sigD == 4 ) {
        return .dquarter
      }
      return .deighth
    case 8 :
      if ( sigD == 4 ) {
        return .deighth
      }
      return .d16th
    case 6 :
      if ( sigD == 4 ) {
        return .d16th
      }
      return .d32nd
    case 0 :
      if ( sigD == 4 ) {
        return .d32nd
      }
      return .d64th
    default :
    return nil
    }
  }
}

struct ScoreABCTimeSig : Equatable {
  let sigN : Int
  let sigD : Int
  
  static func == (lhs: ScoreABCTimeSig, rhs: ScoreABCTimeSig) -> Bool {
    if ( (lhs.sigD == rhs.sigD) && ( lhs.sigN == rhs.sigN )) {
      return true
    }
    return false
  }
  
  func getDefaultBeams( lastDuration : ScoreABCDuration? ) -> Int {
    guard let lastDuration = lastDuration else {
      return 0
    }
    var maxBeams : Int = 0
    switch sigN {
    case 2 :
      if ( sigD == 4 ) {
        maxBeams = 2
      }
    case 4 :
      if ( sigD == 4 ) {
        maxBeams = 4
      }
    case 6 :
      if ( sigD == 8 ) {
        maxBeams = 3
      }
    default :
      break
    }
    switch lastDuration {
    case .d16th :
      maxBeams *= 2
      break
    case .d32nd ,
         .d64th ,
         .d128th :
      maxBeams *= 4
      break
    default :
      break
    }
    return maxBeams
  }
}

enum ABCWedge : String {
  case crescendo
  case diminuendo
  case stop
}

enum ABCTied : String {
  case start
  case stop
}



enum ABCAlteration {
  case Flat
  case Sharp
  case Natural
}

enum ABCBeamType : String {
  case Begin
  case End
  case Continue
}

struct ABCBeam {
  let type : ABCBeamType?
  let number : Int
  let tuplet : Bool 
}

class ABCWordLine {
  var words : String = "w: "
  
  func add( word : String ) {
    words += word + " "
  }
  
  func emit(emitter:ABCEmitter) {
    emitter.append(words)
    emitter.eol()
  }

}


struct ScoreABCNote {
  let duration : ScoreABCDuration
  let note : String?   // nil assume rest
  let dots : Int
  let alteration : ABCAlteration?
  let tied : ABCTied?
  var isRest : Bool {
    get {
      if let _ = note {
        return false
      }
      return true
    }
  }
  
  /*
  func canBeam( previous : ScoreABCNote ) -> Bool {
    switch duration {
    case .deighth ,
         .d16th ,
         .d32nd ,
         .d64th ,
         .d128th :
      if ( previous.duration == duration ) {
        return true
      }
    default:
      break
    }
    return false
  }
  */
}


/*
 
 Emit ABC Score stuff
 
 */
class ABCEmitter : Emitter {
  let MaxMesureForLine = 6
  
  var currentClef : ABCScoreClef = .G
  var curKSignature = ABCScoreKey(key:0)
  var curTimeSig : ScoreABCTimeSig? = nil
  var curVoiceNumber : Int = 0
  var partChanged : Bool = false
  var curInstrument : ScoreInstrument? = nil
  var isMultiPart : Bool = false
  var curNote : ScoreABCNote? = nil
  var lastNote : ScoreABCNote? = nil
  var nbBeams : Int = 0
  var nbMesuresInLine : Int = 0
  var insideChord: Bool = false
  var wordLine : ABCWordLine? = nil
  var isWedgeCrescendo : Bool = false
  var inBeam : Bool = false

  func time( beats : Int , beatsType : Int){
    buffer += "M:\(beats)/\(beatsType)"
    eol()
    self.curTimeSig = ScoreABCTimeSig(sigN: beats, sigD: beatsType)
  }
  

  func emitHeader( number: Int ) {
    append("X:\(number)")
    eol()
  }
  
  func emitTitle( name: String ) {
    append("T:\(name)")
    eol()
  }
  
  func emitNote( text: String ) {
    append("N:\(text)")
    eol()
  }

  func emitWord(_ w: String ) {
    if ( wordLine == nil ) {
      wordLine = ABCWordLine()
    }
    wordLine!.add(word: w)
  }
  
  func emitNoteLength( l: Int ) {
    append("L:1/\(l)")
    eol()
  }

  func emitComposer( name: String) {
    append("C:\(name)")
    eol()
  }
  
  func emitVoiceHead( name: String ) {
    append("(\(name)) ")
  }
  
  func emitHeader(voice: ScoreVoice ) {
    voice.emitHeader(emitter: self)
  }
  
  func emitClef() throws {
    append("K:\(try curKSignature.emit()) ")
    switch currentClef {
    case .G :
      append(" clef=treble ")
    case .C :
      append(" clef=alto ")
    case .F :
      append(" clef=bass middle=d transpose=-24")
    }
  }
  
  func checkVoiceChanges( clef : ABCScoreClef , signature : ABCScoreKey ) throws {
    if ( (clef != currentClef) || (signature != curKSignature) ) {
      currentClef = clef
      curKSignature = signature
      append("[")
      try emitClef()
      append("]")
    }
  }
  
  func emitRepeat( start: Bool) {
    if ( start ) {
      append("|:")
    } else {
      append(" :|")
    }
  }
  
  func emitWords() {
    if let wordLine = wordLine {
      wordLine.emit(emitter:self)
      self.wordLine = nil
    }
  }
  
  func emitWedge ( _ wedge : ABCWedge ) {
    switch wedge {
    case .crescendo :
      append(" !<(! ")
      isWedgeCrescendo = true
    case .diminuendo :
      append(" !>(! ")
      isWedgeCrescendo = false
    case .stop :
      if ( isWedgeCrescendo ) {
        append(" !<)! ")
      } else {
        append(" !>)! ")
      }
    }
  }
  
  func emitPp( count: Int ) {
    append("!")
    for _ in 1...count {
      append("p")
    }
    append("!")
  }
  
  func emitBeam( _ beam : ABCBeam ) {
    if ( beam.tuplet ) {
      append("(\(beam.number)")
    }
    inBeam = true
  }
  
  func emitDirection( _ dir : String ) {
    append("\"^\(dir)\"")
  }
  
  func emitMeasure( bold : Bool = false ) {
    if ( bold ) {
      append("|| ")
    } else {
      append("| ")
    }
    if ( !isMultiPart ) {
      nbMesuresInLine += 1
      if ( nbMesuresInLine > MaxMesureForLine ) {
        eol()
        nbMesuresInLine = 0
        emitWords() // w: emit if any 
      }
    }
  }
  
  private func checkPrevNote( prev: ScoreABCNote?) {
    guard let prev = prev else {
      return
    }
    if prev.isRest {
      append(" ")
      return
    }
    /*
    switch prev.duration {
    case .dwhole,
         .dmeasure,
         .dquarter,
         .dhalf :
      append(" ")
      break
    default :
      break
    }
    */
  }
  
  func emitTimeSig() {
    if let curTimeSig = curTimeSig {
      append("M:\(curTimeSig.sigN)/\(curTimeSig.sigD)")
    }
  }
  
  
  func emitChord(start: Bool) {
    if start {
      insideChord = true
      append(" [")
    } else {
      insideChord = false
      append("] ")
    }
  }
  
  func emit( note: ScoreABCNote ) {
    lastNote = curNote
    if ( note.isRest ) {
      append(" ")
      append("z\(note.duration.getAbcDuration(base: 8, dots: note.dots))")
    } else {
      switch note.alteration {
      case .Sharp :
        append("^")
        break
      case .Flat :
        append("_")
        break
      case .Natural :
        append("=")
        break
      case .none:
        break
      }
      if let tied = note.tied {
        if tied == .start {
          append("(")
        }
      }
      if ( !insideChord ) {
        // handle beams
        if ( !inBeam ) {
          append(" ")
          nbBeams = 0
        } else {
          checkPrevNote(prev:lastNote)
          nbBeams += 1
        }
      }
      append("\(note.note!)\(note.duration.getAbcDuration(base: 8, dots: note.dots))")
      if let tied = note.tied {
        if tied == .stop {
          append(")")
        }
      }
    }
    curNote = note
  }
  
}

protocol ABCEmittingActor {
  func emitABC( _ emitter : ABCEmitter ) throws ;
}

