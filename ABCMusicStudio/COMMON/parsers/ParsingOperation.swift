//
//  ParsingOperation.swift
//  ABCStudio
//
//  Created by jymengant on 7/21/19 week 29.
//  Copyright © 2019 jymengant. All rights reserved.
//

import CocoaLumberjack
import PromiseKit


class ParsingOperation : ABCOperation {
  
  var parser : ABCParser
  var ir : ABCIR? = nil
  
  init( source : String) {
    parser = ABCParser(src: source )
    self.parser.abcSrc = source
    super.init()
    ready(true)
  }
  
  override func main() {
    guard isCancelled == false else {
      finish(true)
      return
    }

    do {
      executing(true)
      DDLogDebug("before parsing ...")
      self.ir = try parser.parse()
      DDLogDebug("after parsing ...")
      finish(true)
      return
    } catch {
      // SEVERE parsing ERRORS (Not syntax ...)
      if ModalAlert().show(question: "ABC Parsing UNEXPECTED ERROR", text:"ABC unexpected error in parser\(String(describing: error))", style: .Critical){}
      finish(true)
      return
    }
  }
}

class ParsingOperationStub {
  
  private let operationQueue: OperationQueue = OperationQueue()
  private let toParse : String
  
  init(src : String)  {
    self.operationQueue.maxConcurrentOperationCount = 1
    toParse = src
  }
  
  func buildIR(onCompleted: @escaping ((ABCIR) -> ())) {
    operationQueue.cancelAllOperations()
    operationQueue.isSuspended = true
    let parsingOperation = ParsingOperation(source: toParse)
    
    parsingOperation.completionBlock = {
      if let ir = parsingOperation.ir {
        DispatchQueue.main.async(execute: {
          DDLogDebug("returning from parsing ...")
          onCompleted(ir)
        })
      }
    }

    operationQueue.addOperation(parsingOperation)
    operationQueue.isSuspended = false

  }
  
  func getIr() -> Promise<ABCIR> {
    return Promise { seal in
      self.buildIR { (ir) in
        DDLogDebug("returning from parsing")
        seal.fulfill(ir)
      }
    }
  }
}
