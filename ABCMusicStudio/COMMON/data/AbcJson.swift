//
//  AbcJson.swift
//  ABCMusicStudio
//
//  Created by jymengant on 3/21/20 week 12.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation

/**
 Generic encoder / decoder
 */
class AbcJson {
  
  static func encode<T: Codable>(source: T ,encoding: String.Encoding) throws -> String? {
    let jsonEncoder = JSONEncoder()
    do {
      var jsonData : Data? = nil
      jsonData = try jsonEncoder.encode(source)
      return String(data: jsonData!, encoding: encoding)
    } catch {
      let decodeError = error.localizedDescription
      throw ABCFailure( message:decodeError , kind: ABCFailure.FailureKind.JSONParsing)
    }
  }
  
  /**
    decode Generic Decodable
  */
  static func decode<T: Decodable>( jsonSource : String , encoding: String.Encoding) throws -> T {
    let jsonDecoder = JSONDecoder()
    do {
      let data = jsonSource.data(using: encoding)!
      let returned = try jsonDecoder.decode(T.self, from:data)
      return returned // success
    } catch {
      let errorStr = "DECODE ERROR : \(error)"
      throw ABCFailure( message:errorStr , kind: ABCFailure.FailureKind.JSONParsing)
    }
  }
  
}
