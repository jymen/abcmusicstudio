//
//  AbcTreeFeeder.swift
//  ABCStudio
//
//  Created by jymengant on 6/8/18 week 23.
//  Copyright © 2018 jymengant. All rights reserved.
//

import CocoaLumberjack

class ABCTreeData : NSObject {
  
  private var store : ABCTreeJsonDataStore? = nil
  private var root : Folder? = nil
  
  override init() {
    super.init()
    store = ABCTreeJsonDataStore()
    root = store?.getABCFolderContent(path: nil)
  }

  func getRootFolder() -> Folder? {
    return root
  }
  
  func setRoot( folder : Folder ) {
    root = folder
    // populate to store for later serialization
    store?.data = ABCFolder(root:root!)
  }
  
  /**
   return current node size
   */
  func getSize( folder : Folder? ) -> Int {
    var curSize : Int = 0
    if ( folder != nil) {
      if ( folder!.files != nil ) {
        curSize +=  folder!.files!.count
      }
      if ( folder!.folders != nil ) {
        curSize += folder!.folders!.count
      }
    }
    return curSize
  }
  
  //
  // Find folder from Root
  //
  func find( item : Folder , parent : Folder? = nil ) -> Folder? {
    var start = parent
    if ( start == nil ) {
      if ( root == nil ) {
        return nil
      }
      start = root
    }
    if ( start!.getLeaf() == item.getLeaf() ) {
      return start
    }
    if let folders = start?.folders {
      for f in folders {
        let found = self.find(item:item , parent: f )
        if ( found != nil ) {
          return found
        }
      }
    }
    return nil
  }
  
  //
  // Find file from Root
  //
  func find( item : File , parent : Folder? = nil , recurse: Bool = false) -> File? {
    var start = parent
    if ( start == nil ) {
      if ( root == nil ) {
        return nil
      }
      start = root
    }
    if let files = start?.files {
      for f in files {
        if ( f == item) {
          return f
        }
      }
    }
    if ( recurse ) {
      if let folders = start?.folders {
        for f in folders {
          let found = self.find(item:item , parent: f , recurse: recurse)
          if ( found != nil ) {
            return found
          }
        }
      }
    }
    return nil
  }
  
  func setSelected( folder: Folder? , parent: Folder?)  {
    store!.setSelected(folder: folder,parent: parent)
  }
  
  func setSelected( file: File? , parent: Folder? ) {
    store!.setSelected(file: file, parent: parent)
  }
  
  func updateSelected( infos : Leaf ) {
    store!.updateSelected(infos: infos)
  }
  
  func getSelectedFolder() -> Folder? {
    return store!.getSelectedFolder()
  }
  
  func getSelectedFile() -> File? {
    return store!.getSelectedFile()
  }
  
  func getIndexedItem ( folder : Folder , index: Int ) -> Any? {
    guard let store = store else {
      return nil
    }
    return store.getIndexedItem(folder:folder,index:index)
  }
  
  func getIndexedItem ( file : File , index: Int ) -> Any? {
    guard let store = store else {
      return nil
    }
    return store.getMultipleTunes(file:file, index:index)
  }
  
  /**
   add item in tree
  */
  func addItem( data : ABCResourcePanelData , content : String? , before: ABCTreeElement? = nil , atIndex: Int = -1) -> (Any?,Folder?) {
    do {
      return (try store?.addItem(data: data , content: content , before: before , atIndex: atIndex))!
    }  catch let error as ABCFailure {
      if ModalAlert().show(question: "AddItem FATAL ERROR", text: "\(String(describing: error.message))", style: .Critical){}
    } catch {}
    return (nil,nil)
  }
  
  /*
    proceed with drag n drop item move
  */
  func move ( source : ABCTreeElement? , dest: Folder , at: Int) {
    if let s = source  {
      store?.move(candidate: s, to: dest , at: at)
    }
  }
  
  func move ( file : File , before: File , at: Int) {
    store?.move(file:file , before:before , at : at )
  }
  
  func removeSelected() {
    if let file = getSelectedFile() {
      // a file is selected for removal
      store?.remove(score: file)
    } else if let folder = getSelectedFolder() {
      // a folder is selected
      store?.remove( folder: folder)
    }
  }
  
  func encode<T:Codable>(source: T) throws -> String? {
    return try ABCTreeJsonDataStore.encode(source: source, encoding:.utf8)
  }
  
  func decodeTreeElement( jsonSource : String ) throws -> ABCTreeElement? {
    return try ABCTreeJsonDataStore.decodeTreeElement(jsonSource:jsonSource)
  }
  
  func getSerialized() throws -> Data? {
    return try store?.serialize()
  }
  
  func deserialize(_ data: Data ) throws {
    try (store?.deserialize(data))!
    // refresh root after deserialization
    root = store?.getABCFolderContent(path: nil)
  }

}

