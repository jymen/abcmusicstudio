//
//  ABCFileTreeDataStore.swift
//  ABCStudio
//
//  Created by jymengant on 7/4/18 week 27.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack

struct ExportOptions {
  let exportExtra : Bool
  let exportSound : Bool
  let exportIcon  : Bool
  
  init() {
    exportExtra = false
    exportSound = false
    exportIcon = false
  }
  
  init( extra : Bool , sound : Bool , icon : Bool ) {
    exportExtra = extra
    exportSound = sound
    exportIcon = icon
  }
}



enum ABCResType : String , Codable {
  case ABCScore
  case Group
  case GroupWithFolder
  
  func getFileSuffix() -> String? {
    if ( self == .ABCScore ) {
      return ".abc"
    }
    return nil 
  }
}

/*============================================*/
/* Codable AbcFileTree serializable JSON DATA */
/* used by abcmusicstudio file storage        */
/* and JSON export file storage format        */
/* see ABCDatabaseModel for dbStorage         */
/*============================================*/

//
// exported stuff
//
class DataContent : Codable {
  var oid : ABCOid? // Used for DB storage unicity
  var encoded : Bool?
  var abc : String?
  var imageB64 : String?
  var image : Data?
  var iconB64 : String?
  var icon : Data?
  var soundB64 : String?
  var sound : Data?
  
  init() {
    encoded = false
  }
  
  var empty : Bool {
    get{
      if ( abc == nil && imageB64 == nil && iconB64 == nil && soundB64 == nil ){
        return true
      }
      return false
    }
  }
  
  private func encode( _ from : URL? ) -> String? {
    encoded = true
    if let url = from {
      let encoder = Base64Encoder()
      return encoder.encode(url: url)
    }
    return nil
  }
  
  func setAbc( from : URL? ) {
    self.abc = self.encode(from)
  }
  
  func setImage( from : URL? ) {
    self.imageB64 = self.encode(from)
  }
  
  func setIcon( from : URL? ) {
    self.iconB64 = self.encode(from)
  }
  
  func setSound( from : URL? ) {
    self.soundB64 = self.encode(from)
  }
}

class Leaf : Codable {
  var oid : ABCOid? // Used for DB storage unicity
  var title: String
  var tags : String? // for filtering purposes
  var creation: Date?
  var storageURL : URL?
  var iconURL : URL?
  var imageURL : URL?
  var imageZoom : CGFloat?
  var imageBgColor : CodableColor?
  var soundURL : URL?
  var comments: String?
  var contents : DataContent? // used on import export only
  var encoding : UInt?
  var type : ABCResType?
  
  init ( title : String ,
         creation: Date? = nil ,
         id : ABCOid? = nil ,
         storageURL: URL? = nil ,
         iconURL: URL? = nil ,
         imageURL: URL? = nil ,
         zoom: CGFloat? = nil ,
         imageBgColor: CodableColor? = nil ,
         soundURL: URL? = nil ,
         comments: String? = nil ,
         encoding: String.Encoding ,
         type : ABCResType? ) {
    self.title = title
    self.creation = creation
    self.encoding = encoding.rawValue
    self.type = type
    self.oid = id
    if let url = storageURL {
      self.storageURL = url
    }
    if let url = iconURL {
      self.iconURL = url
    }
    if let url = imageURL {
      self.imageURL = url
    }
    if let url = soundURL {
      self.soundURL = url
    }
    if let cmt = comments {
      self.comments = cmt
    }
    if  let zoom = zoom {
      self.imageZoom = zoom
    }
    if  let imageBgColor = imageBgColor {
      self.imageBgColor = imageBgColor
    }
  }
  
  var zoomImage : CGFloat  {
    get {
      if let zoom = self.imageZoom {
        return zoom
      } else {
        return 100.0
      }
    }
    set {
      self.imageZoom = newValue
    }
  }
  
  // Equal operator for Leaf struct
  static func ==(left: Leaf , right:Leaf ) -> Bool {
    if ( left.title == right.title ) {
      return true
    }
    return false
  }
  
  // Not Equal operator for Leaf struct
  static func !=(left: Leaf , right:Leaf ) -> Bool {
    if ( left.title != right.title ) {
      return true
    }
    return false
  }
  
  func makeExportable( exportType : ExportOptions) {
    if contents == nil {
      contents = DataContent()
    }
    
    if let content = contents {
      content.setAbc(from: storageURL)
      
      if ( exportType.exportIcon ) {
        content.setIcon(from: iconURL)
      }
      if ( exportType.exportSound ) {
        content.setSound(from: soundURL)
      }
      if (exportType.exportExtra ) {
        content.setImage(from: imageURL)
      }
      if ( content.empty ) {
        self.contents = nil
      }
    }
    
  }
  
  func cleanup() {
    if let _ = contents {
      contents = nil // just cleanup to free space
    }
  }
  

  
}

/**
 make tree operations generic
 */
protocol ABCTreeElement : Codable {
  func addTo( folder : Folder , before: Leaf? , at : Int)
  func removeFrom( folder : Folder )
  func getLeaf() -> Leaf

}

// add some generic methods here
extension ABCTreeElement {
  func move(from: Folder, to: Folder , before: Leaf? = nil , at: Int = -1) {
    self.removeFrom(folder: from)
    self.addTo(folder:to, before:before, at:at)
  }
}

class TuneDescription : Codable , Equatable {
  let number : Int
  var title : String? = nil
  let line : Int
  var offset : Int? = nil
  
  init( number: Int , line : Int , offset : Int) {
    self.line = line
    self.number = number
    self.offset = offset
  }
  
  static func ==(lhs: TuneDescription, rhs: TuneDescription) -> Bool {
      return lhs.title == rhs.title && lhs.number == rhs.number
  }
}

class File : ABCTreeElement {
  
  var oid : ABCOid? // Used for DB storage unicity
  var infos: Leaf
  var tunes : [TuneDescription]? = nil
  var state : TreeState?

  init( infos: Leaf) {
    self.infos = infos
    self.state = .FolderCLosed
  }
  
  // Equal operator for File struct
  static func ==(left: File , right:File ) -> Bool {
    if ( left.infos == right.infos ) {
      return true
    }
    return false
  }
  
  // Equal operator for File struct
  static func !=(left: File , right:File ) -> Bool {
    if ( left.infos != right.infos ) {
      return true
    }
    return false
  }
  
  //
  // will return 0 as first and single tune when not available
  //
  func indexOf(tune: TuneDescription?) -> Int {
    guard let tune = tune else {
      return 0 
    }
    guard let tunes = self.tunes else {
      return 0
    }
    var pos : Int = 0
    for curtune in tunes {
      // title is unsafe since same title may happen twice inside given score
      if ( curtune.line == tune.line ) {
        return pos
      }
      pos += 1
    }
    return 0
  }

  func addTo(folder: Folder , before: Leaf? = nil , at : Int = -1) {
    if ( folder.files == nil ) {
      folder.files = [File]()
    }
    if ( before == nil ) {
      if ( (at != -1) && ( at < folder.files!.count ) ) {
        folder.files!.insert(self, at: at)
      } else {
        folder.files!.append(self)
      }
    } else {
      var pos = 0
      for curFile in folder.files! {
        if ( curFile.infos == before! ) {
          folder.files!.insert(self, at: pos)
          break
        }
        pos += 1
      }
    }
  }
  
  func removeFrom(folder: Folder) {
    folder.files = folder.files!.filter { !($0 == self) }
  }
  
  func getLeaf() -> Leaf {
    return infos
  }
  
}

enum TreeState : String , Codable {
  case FolderOpened
  case FolderCLosed
}

class Folder : ABCTreeElement {
  
  var oid : ABCOid? // Used for DB storage unicity
  var infos: Leaf
  var folders : [Folder]?
  var files : [File]?
  var state : TreeState?

  init( infos : Leaf ) {
    self.infos = infos
  }
  
  // Equal operator for Folder struct
  static func ==(left: Folder , right:Folder ) -> Bool {
    if ( left.infos == right.infos ) {
      return true
    }
    return false
  }
  
  
  /**
    adding new file or folder item
  */
  func addItem( data: ABCResourcePanelData ,
                content : String? ,
                before: Leaf? = nil ,
                at : Int = -1
  ) -> ABCTreeElement? {
    DDLogDebug("Folder : adding 1 item under current folder")
    if ( !data.existing && ( data.type != .Group )) {
      do {
        // create empty file
        let fu = FileUtils()
        if ( data.type == .GroupWithFolder ) {
          if ( !fu.existFile(url: data.url!, directory: true)) {
            try fu.createLocalFolder(url: data.url!)
          }
        } else {
          try fu.storeTextUrl(url: data.url!, content: content ?? "" , encoding: data.encoding)
        }
      } catch let error as ABCFailure {
        let msg = ModalAlert()
        _ = msg.show(question: "File ", text: "\(data.url!) initial creation failed : \(error.message!)", style: .Critical)
        return nil
      }   catch {}
    }
    let leaf = Leaf(title: data.title! ,
                    creation: Date() ,
                    storageURL : data.url ,
                    imageURL : data.imageURL ,
                    soundURL: data.soundURL ,
                    comments: data.comments ,
                    encoding: data.encoding,
                    type: data.type)
    
    if ( data.isScore() ) {
      let file = File(infos: leaf)
      file.addTo(folder: self,before: before)
      return file
    } else {
      let folder = Folder(infos:leaf)
      folder.addTo(folder: self)
      return folder
    }
  }
  
  /**
    return back item parent or nil
  */
  func lookForParent( candidate : Leaf , asFile : Bool = true ) -> Folder? {
    if ( asFile ) {
      if let curFiles = files {
        for curFile in curFiles {
          if ( curFile.infos == candidate ) {
            return self
          }
        }
      }
    }
    // either a folder search or file not found yet
    if let curFolders = folders {
      for curFolder in curFolders {
        if ( !asFile ) {
          if ( curFolder.infos == candidate ) {
            return self
          }
        }
        // NOT FOULD => recurse from curFolder for file or folder
        let parent = curFolder.lookForParent(candidate: candidate,asFile:asFile)
        if ( parent != nil ) {
          return parent
        }
      }
    }
    return nil // NOT FOUND here finally 
  }
  
  //
  // look for file parent of given TuneDescription
  //
  func lookForParent( candidate : TuneDescription ) -> File?  {
    guard let files = self.files else {
      return nil
    }
    for file in files {
      if let tunes = file.tunes {
        for tune in tunes {
          if ( tune.title == candidate.title ) {
            return file
          }
        }
      }
    }
    return nil
  }
  
  private func delete( path : String ) {
    let msg = ModalAlert()
    do {
      let fu : FileUtils = FileUtils()
      try fu.deleteOnDisk(path: path)
    } catch let error as ABCFailure {
      // We assume that removing on disk is a minor failure
      _ = msg.show(question: "File Deletion Failure", text: "\(path) deletion failed : \(error.message!)", style: .Warning)
    } catch {
    }
    // EventManager.shared.emit(event:ABCEvent(type:.TreeElementDeleted,msg:self))
  }
  
  private func treeRemoval( score : File?) {
    score!.removeFrom(folder: self)
    EventManager.shared.emit(event:ABCEvent(type:.TreeElementRemoved,msg:self))
  }
  
  func remove( score: File?  ) {
    // ask for confirmation
    if ( score != nil ) {
      if self.files != nil {
        // ask for confirmation
        let question = ModalAlert()
        question.confirmTreeRemoval(leaf: score!.infos).done {
          response in
          switch response {
          case .StopRemoval :
            return // DO NOTHING
          case .Delete :
            // proceed with deletion on disk
            let fPath = score!.infos.storageURL!.path
            self.treeRemoval(score: score)
            self.delete(path: fPath)
            break
          case .Remove :
            self.treeRemoval(score: score)
            break
          }
        }.cauterize()
      }
    }
  }
  
  private func treeRemoval(folder: Folder ) {
    folder.removeFrom(folder: self)
    EventManager.shared.emit(event:ABCEvent(type:.TreeElementRemoved,msg:"Removed"))
  }
  
  func remove( folder: Folder?  ) {
    if let folder = folder  {
      // ask for confirmation
      if self.folders != nil {
        // ask for confirmation
        let question = ModalAlert()
        question.confirmTreeRemoval(leaf: folder.infos).done {
            response in
          switch response {
          case .StopRemoval :
          return // DO NOTHING
          case .Delete :
            let fu = FileUtils()
            // proceed with deletion
            if ( fu.existFile(url: folder.infos.storageURL!, directory: false)) {
              let fPath = folder.infos.storageURL!.path
              self.delete(path: fPath)
            }
            self.treeRemoval(folder: folder)
            break
          case .Remove :
            self.treeRemoval(folder: folder)
            break;
          }
        }.cauterize()
      }
    }
  }
  
  //
  // ABC TreeElement protocol
  //
  
  func addTo(folder: Folder , before: Leaf? = nil , at: Int = -1 ) {
    if ( folder.folders == nil ) {
      folder.folders = [Folder]()
    }
    if ( at != -1 ) {
      folder.folders!.insert(self, at: at)
    } else {
      folder.folders!.append(self)
    }
  }
  
  func removeFrom(folder: Folder) {
    folder.folders = folder.folders!.filter { !($0 == self) }
  }

  func getLeaf() -> Leaf {
    return infos
  }

}

struct ABCFolder : Codable {
  var root : Folder
}


