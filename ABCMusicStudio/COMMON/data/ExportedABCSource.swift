//
//  ExportedABCFolder.swift
//  ABCMusicStudio
//
//  Created by jymengant on 3/18/20 week 12.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation

struct ExportedABCSource {

  var source : Folder
  
  init( source : Folder ) {
    self.source = source
  }
  
  
  /**
   Build dest for external exporting
   */
  private func doExportable( curNode : Folder? , exportType : ExportOptions ) {
    if let curFolder = curNode {
      if let folders = curFolder.folders {
        for childFolder in folders {
          self.doExportable(curNode: childFolder , exportType: exportType)
        }
      }
      if let files = curFolder.files {
        for file in files {
          // only files needs to populate content
          file.infos.makeExportable( exportType: exportType)
        }
      }
    }
  }
  
  private func undoExportable( curNode : Folder? ) {
    if let curFolder = curNode {
      curFolder.infos.cleanup()
      if let folders = curFolder.folders {
        for childFolder in folders {
          self.undoExportable(curNode: childFolder)
        }
      }
      if let files = curFolder.files {
        for file in files {
          file.infos.cleanup()
        }
      }
    }
  }
  
  //
  // for document storage or device transfert
  //
  private func serialize() -> String?  {
    let jsonEncoder = JSONEncoder()
    do {
      let data = try jsonEncoder.encode(source)
      return String(data: data, encoding: .utf8)
    } catch {
      let decodeError = error.localizedDescription
      if ModalAlert().show(question: "Error Serializing Exported JSON ", text: "\(String(describing: decodeError))", style: .Critical){}
    }
    return nil
  }
  
  private func deserialize( json : String ) -> ABCFolder? {
    let jsonDecoder = JSONDecoder()
    do {
      let data = json.data(using: .utf8)!
      let decoded  = try jsonDecoder.decode(ABCFolder.self, from:data)
      return  decoded
    } catch {
      let errorStr = "DECODE ERROR : \(error)"
      if ModalAlert().show(question: "Error decodig JSON archive", text: "\(errorStr)", style: .Critical){}
    }
    return nil
  }
  
  func prepareForExport( exportType: ExportOptions ) -> String? {
    let root = self.source
    doExportable(curNode: root ,  exportType: exportType )
    return serialize()
  }
  
  func exportArchive( dest : URL , exportType: ExportOptions) {
    if let data = prepareForExport( exportType: exportType ) {
      do {
        let fUtils = FileUtils()
        try fUtils.storeTextUrl(url: dest, content: data, encoding: .utf8)
        undoExportable(curNode: self.source) // cleanup
      } catch {
        let decodeError = error.localizedDescription
        if ModalAlert().show(question: "JSON export write error ", text: "\(String(describing: decodeError))", style: .Critical){}
      }
    }
  }
  
  func importArchive( source : URL , overrideExisting : Bool) -> ABCFolder? {
    let fUtils = FileUtils()
    if fUtils.existFile(url: source, directory: false) {
      do {
        let unarchive =  try fUtils.read(url: source, backupEncoding: .utf8)
        return deserialize(json: unarchive.0)
      } catch {
        let decodeError = error.localizedDescription
        if ModalAlert().show(question: "Error reading JSON archive", text: "\(String(describing: decodeError))", style: .Critical){}
      }
    }
    return nil
  }

}

