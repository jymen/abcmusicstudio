//
//  AttributedStringDataStore.swift
//  ABCStudio
//
//  Created by jymengant on 2/14/19 week 7.
//  Copyright © 2019 jymengant. All rights reserved.
//

import CocoaLumberjack

struct StyleElement {
  let key : NSAttributedString.Key
  let value : Any
}

//
// made it Codable for preferences serialization
//
extension StyleElement : Codable {
  enum CodingKeys: String, CodingKey {
    case key
    case value
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    key = NSAttributedString.Key(rawValue: try container.decode(String.self, forKey: .key))
    switch key  {
    case .foregroundColor :
      let colorData = try container.decode(Data.self, forKey: .value)
      value = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(colorData) as? NSColor ?? NSColor.black
      break
    case .font :
      let fontData = try container.decode(Data.self, forKey: .value)
      value = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(fontData) as? NSFont ?? NSFont.userFont(ofSize: 12)! 
      break
    default:
      throw ABCFailure(message: "implementation missing", kind: .EncodingFailure) // NOT IMPLEMENTED YET
    }
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(key.rawValue, forKey: .key)
    switch key  {
    case .foregroundColor :
      let colorData = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false)
      try container.encode(colorData, forKey: .value)
      break
    case .font :
      let fontData = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false)
      try container.encode(fontData, forKey: .value)
      break
    default:
      throw ABCFailure(message: "implementation missing", kind: .EncodingFailure) // NOT IMPLEMENTED YET
    }
  }
}

class Style : Codable {
  var info : String
  var styles : [StyleElement]
  
  init( inf: String , _ values : [StyleElement]) {
    info = inf
    styles = values
  }
  
  init( attributed : NSAttributedString ) {
    info = attributed.string
    self.styles = [StyleElement]()
    let range = NSRange(0..<attributed.length)
    attributed.enumerateAttributes(in: range) { (value , range , nil) in
      DDLogDebug("enumerationg attribs in range : \(value.count)")
      for attrib in value {
        self.styles.append(StyleElement(key:attrib.key , value:attrib.value))
      }
    }
  }
  
  //
  // just populating attributes
  //
  func addAttributes( string : NSMutableAttributedString , range: NSRange) {
    for style in styles {
      string.addAttribute(style.key,value: style.value , range: range )
    }
  }
  
  //
  // get requested attribute back
  //
  func getAttribute( key : NSAttributedString.Key ) -> Any? {
    for style in styles {
      if ( style.key == key ) {
        return style.value
      }
    }
    return nil
  }
  
  //
  // build sample attributed from Style and info
  //
  func toAttributedString() -> NSMutableAttributedString {
    let returned = NSMutableAttributedString(string: info)
    let range = NSRange(location: 0 , length: info.count)
    addAttributes(string: returned, range: range)
    return returned
  }
  
  func toAttributes() -> [NSAttributedString.Key:Any]? {
    var returned :  [NSAttributedString.Key:Any]? =  [NSAttributedString.Key:Any]()
    for style in styles {
      returned![style.key] = style.value
    }
    return returned
  }
  
}





