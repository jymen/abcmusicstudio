//
//  ABCAbout.swift
//  ABCStudio
//
//  Created by jymengant on 1/12/20 week 2.
//  Copyright © 2020 jymengant. All rights reserved.
//

import CocoaLumberjack


struct AboutContent {
  
  // TO be changed for each stable build
  var version : String {
    get {
      let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"]!
      let build = Bundle.main.infoDictionary!["CFBundleVersion"]!
      return "ABCMusicStudio V \(version) (Build \(build) )"
    }
  }

  let description : String
  let copyright : String
  
  init( resource: String ) {
    let l = Localizer(resource: resource)
    description = l.tr("details")
    copyright = l.tr("CopyRights")
  }
}

class ABCAbout {
  
  let about : AboutContent
  
  init() {
    about = AboutContent (resource: "About")
  }
}

