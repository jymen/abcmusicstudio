//
//  ABCFileTreeDataStore.swift
//  ABCStudio
//
//  Created by jymengant on 6/1/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import CocoaLumberjack

/**
 JSON Based DataStore for ABCFileTree
 */

/** Public DataStore Class */
class ABCTreeJsonDataStore {
  
  var data : ABCFolder? = nil
  var selectedFolder : Folder? = nil // Currently selected folder
  var selectedFile : File? = nil // Currently selected file
  var selectedParent : Folder? = nil // parent Folder of current selection
  var decodeError : String? = nil
  
  
  func setSelected(folder: Folder? , parent: Folder?) {
    self.selectedFolder = folder
    self.selectedParent = parent
    self.selectedFile = nil
  }
  
  func setSelected(file: File? , parent: Folder?) {
    self.selectedFile = file
    self.selectedParent = parent
    self.selectedFolder = nil
  }
  
  func getSelectedFolder() -> Folder? {
    return self.selectedFolder
  }
  
  func getSelectedFile() -> File? {
    return self.selectedFile
  }
  
  
  func updateSelected( infos: Leaf ) {
    if let fi = selectedFile {
      fi.infos = infos
    } else if let fo = selectedFolder {
      fo.infos = infos
    }
  }
  
  func checkResource ( url: URL? , folder: Bool) throws {
    if let myUrl = url {
      let fUtils : FileUtils = FileUtils()
      switch ( fUtils.getResourceType(url: myUrl)) {
      case .Directory :
        if ( !folder ) {
          throw ABCFailure(message: "\(myUrl.absoluteString) is a Directory where file is expected !!",kind:.IOError)
        }
        // All is OK just return back
        return
      case .File :
        if ( folder ) {
          throw ABCFailure(message: "\(myUrl.absoluteString) is a file where directory is expected !!",kind:.IOError)
        }
        return
      case .Inexisting :
        // create local folder
        if ( folder ) {
          try fUtils.createLocalFolder(url: myUrl)
        }
        break
      case .NotALocalFile :
        // TODO : handle non file system URLS
        break
      }
    }
  }
  
  /**
     add new element
  */
  func addItem ( data: ABCResourcePanelData , content: String? , before: ABCTreeElement? = nil , atIndex: Int = -1 ) throws -> (Any?,Folder?)  {
    var curContent : String? = content
    var beforeFileLeaf : Leaf? = nil
    if let before = before {
      if let beforeFolder = before as? Folder {
        selectedFolder = beforeFolder
      } else {
        let root = self.data!.root
        let beforeFile = before as! File
        beforeFileLeaf = beforeFile.infos
        selectedFolder = root.lookForParent(candidate: beforeFileLeaf!)
      }
    } else {
      if ( selectedFolder == nil ) {
        selectedFolder = self.data!.root
      }
    }
    if ( data.isScore() ) {
      if let url = data.url {
        let fUtils : FileUtils = FileUtils()

        // ABC existing Score or created with choosen storage place
        if ( fUtils.existFile(url: url, directory: false)) {
          data.existing = true
        } else {
          data.existing = false
          // Check for template inclusion
          if curContent == nil {
            let template = TemplateHeaderFile()
            curContent = template.importTemplate(inSource: "")
            curContent?.append("X:1\n")
            curContent?.append("T:\(data.title ?? loc.tr("No Title"))\n")
          }
        }
      } else {
        // create in default storage place from preferences singleton
        data.url = preferences.defStorage
      }
    } else {
      if ( data.type ==  ABCResType.GroupWithFolder ) {
        // Storage group
        try checkResource(url : data.url, folder: true)
      }
    }
    // finally populate to tree ( if nothing has been thrown ... )
    let newItem = selectedFolder!.addItem(data: data , content: curContent , before: beforeFileLeaf , at: atIndex)
    return (newItem , selectedFolder)
  }

  
  /*
   move ABCTreeElement (Folder or File)  in tree
   */
  func move(candidate: ABCTreeElement?, to: Folder , at: Int = -1) {
    let rootFolder = data?.root
    let leaf = candidate!.getLeaf()
    let isFile = candidate is File
    let sourceParent = rootFolder?.lookForParent(candidate: leaf, asFile: isFile)
    candidate!.removeFrom(folder: sourceParent!)
    candidate!.addTo(folder: to,before:nil, at:at)
  }
  
  func move ( file : File , before: File , at: Int = -1) {
    let rootFolder = data?.root
    let sourceLeaf = file.infos
    let destLeaf = before.infos
    let sourceParent = rootFolder?.lookForParent(candidate: sourceLeaf)
    let destParent = rootFolder?.lookForParent(candidate: destLeaf)
    file.removeFrom(folder: sourceParent!)
    file.addTo(folder: destParent!, before: before.infos , at:at)
  }

  func remove( folder: Folder?) {
    if ( folder != nil ) {
      self.selectedParent?.remove(folder:folder)
    }
  }
  
  func remove( score: File?) {
    if ( score != nil ) {
      self.selectedParent?.remove(score:score)
    }
  }

  
  func getABCFolderContent(path: String?) -> Folder? {
    if ( path == nil ) {
      if ( data != nil ) {
        return data?.root
      }
    }
    return nil
  }
  
  func rootFolder() -> Folder? {
    if ( data != nil ) {
      return data?.root
    }
    return nil
  }
  
  /**
   returns Folder or File item at given position
   NB : Folder array comes first
  */
  func getIndexedItem ( folder : Folder? , index: Int ) -> Any? {
    guard let folder = folder else {
      return 0
    }
    var size = 0
    if let folders = folder.folders {
      if ( index < folders.count ) {
        return folders[index]
      }
      size += folders.count
    }
    if let files = folder.files {
      if ( index < (size + files.count) ) {
        return files[index-size]
      }
    }
    return 0
  }
  
  func getMultipleTunes( file : File? , index: Int) -> Any? {
    guard let file = file else {
      return 0
    }
    guard let tunes = file.tunes else {
      return 0
    }
    
    if ( tunes.count > 1) {
      return tunes[index]
    }
    return 0
  }
  
  static func encode<T:Codable>(source: T , encoding:String.Encoding) throws -> String? {
    return try AbcJson.encode(source: source, encoding:encoding )
  }
  
  /**
    decode Generic Decodable
  */
  static func decode<T: Decodable>( jsonSource : String , encoding : String.Encoding ) throws -> T {
    return try AbcJson.decode( jsonSource: jsonSource, encoding: encoding)
  }

  static func decodeTreeElement( jsonSource : String ) throws -> ABCTreeElement? {
    do {
      let folder : Folder = try Self.decode( jsonSource: jsonSource, encoding:.utf8)
      if ( folder.infos.type == .ABCScore ) {
        let file : File  = try Self.decode(jsonSource: jsonSource, encoding:.utf8)
        return file
      } else {
        return folder
      }
    } catch {
      let file : File = try Self.decode(jsonSource: jsonSource,encoding:.utf8)
      return file
    }
  }
  
  private func treeChecker( data : ABCFolder ) {
    let root = data.root
    var nbFiles = 0
    var nbFolders = 0
    if let files = root.files {
      nbFiles = files.count
    }
    if let folders = root.folders {
      nbFolders = folders.count
    }
    DDLogDebug("*-------------------------------------------------------------------------*")
    DDLogDebug("initial TREE CONTENT at Root is : files : \(nbFiles) folders:\(nbFolders)")
    DDLogDebug("*-------------------------------------------------------------------------*")
  }

  /**
    Decode root ABCFolder using Generic
  */
  func decode( jsonSource : String ) throws -> Any? {
    // DDLogDebug("TO BE DECODED JsonSource : \(jsonSource)")
    let data : ABCFolder? = try Self.decode(jsonSource: jsonSource, encoding:.utf8)
    assert(data != nil)
    treeChecker(data: data!)
    return data
  }

  //
  // for document storage
  //
  func serialize() throws -> Data?  {
    if let data = self.data {
      return try Self.encode(source:data, encoding:.utf8)?.data(using: .utf8)
    }
    return nil
  }
  
  
  //
  // out of document storage
  //
  func deserialize(_ data : Data ) throws {
    let strJSon = String(data: data, encoding: .utf8)
    self.data = try self.decode(jsonSource:strJSon!) as? ABCFolder
    if ( self.data == nil ) {
      throw ABCFailure( message:"ABCTreeJsonDataStore unexpected data type when deserializing" ,
                        kind: ABCFailure.FailureKind.JSONParsing)
    }
  }
  
}

