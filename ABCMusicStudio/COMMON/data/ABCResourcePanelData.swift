//
//  ABCResourcePanelData.swift
//  ABCStudio
//
//  Created by jymengant on 8/6/18 week 32.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation

// Data for ABCResourcePanelController
class ABCResourcePanelData {
  var title : String? = nil
  var url : URL? = nil
  var creationDate : Date? = nil
  var icon : ABCImage? = nil
  var base64Icon : String? = nil
  var iconURL : URL? = nil
  var imageURL : URL? = nil
  var soundURL : URL? = nil
  var base64Image : String? = nil

  var comments : String? = nil
  var type : ABCResType = .ABCScore
  var existing : Bool = false
  var encoding : String.Encoding = .utf8
  
  // Basic init with values
  init( type : ABCResType ,
        url: URL? = nil ,
        iconUrl : URL? = nil ,
        imageUrl : URL? = nil ,
        soundUrl : URL? = nil ,
        title : String? = nil ,
        creation : Date? = nil,
        encoding: String.Encoding = .utf8 ,
        existing : Bool = false) {
    self.type = type
    self.url = url
    self.iconURL = iconUrl
    self.imageURL = imageUrl
    self.soundURL = soundUrl
    self.title = title
    self.creationDate = creation
    self.encoding = encoding
  }
  
  func buildLeaf() -> Leaf {
    return Leaf(title: title!, creation: creationDate!, storageURL: url, comments: comments, encoding: encoding, type: type)
  }
  
  // Constructor with existing data
  init( from: Leaf , existing: Bool) {
    self.title = from.title
    self.comments = from.comments
    self.creationDate = from.creation
    self.existing = existing
    self.url = from.storageURL
    self.iconURL = from.iconURL
    self.imageURL = from.imageURL
    self.soundURL = from.soundURL
    self.encoding = String.Encoding(rawValue: from.encoding ??
      UInt(Unicode.encodingIndex(encoding: .utf8)) )
  }
  
  // Init with Folder
  convenience init( from: Folder ) {
    let leaf : Leaf = from.infos
    self.init(from:leaf,existing:true)
    self.url = leaf.storageURL
    if let _ = self.url {
      self.type = .GroupWithFolder
    } else {
      self.type = .Group
    }
  }
  
  // init with File
  convenience init( from: File ) {
    let leaf : Leaf = from.infos
    var existing = true
    if ( leaf.storageURL == nil ) {
      existing = false
    }
    self.init(from: leaf, existing: existing)
    // self.url = leaf.storageURL
    if ( leaf.type == nil ) {
      self.type = ABCResType.ABCScore
    }
  }
  
  func isScore() -> Bool {
    if ( self.type == .ABCScore ) {
      return true
    } 
    return false
  }
}

