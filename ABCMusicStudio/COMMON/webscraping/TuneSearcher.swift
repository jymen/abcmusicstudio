//
//  TuneSearcher.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/31/20 week 22.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation

struct ABCSearchedScore {
  let name : String
  let href : String
}

protocol TuneSearcher  {
  
  func search( url : String )

  
  func getResultList( html: String )
  
  
  func getABC( html: String )
  
}
