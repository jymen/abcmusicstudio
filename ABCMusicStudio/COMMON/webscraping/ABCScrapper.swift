//
//  ABCScrapper.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/30/20 week 22.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import Kanna
import Alamofire
import CocoaLumberjack

enum ABCSites : String {
  case TheSession
}

class ABCSCrapper {
  
  var html : String?
  let site : ABCSites
  var error : String?
  
  init( site : ABCSites ) {
    self.site = site
  }
  
  func load( webUrl : String ,
             parser : @escaping ( String ) -> Void
  ) throws {
    AF.request(webUrl)
      .validate(statusCode: 200..<300)
      .responseString { response in
        switch response.result {
        case .success:
          print("Validation Successful")
          self.html = response.value
          if let html = self.html  {
            parser(html)
          }
        case let .failure(error):
          self.error = error.errorDescription
        }
    }
  }
  
  func parse( html: String ) {
  }
  
}
