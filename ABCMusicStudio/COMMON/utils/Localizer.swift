//
//  Localizer.swift
//  ABCStudio
//
//  Created by jymengant on 6/5/18 week 23.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack

let loc = Localizer(resource : "Localizable" )  // Singleton

private extension String {
  func replacingFirstOccurrence(of string: String, with replacement: String) -> String {
    guard let range = self.range(of: string) else { return self }
    return replacingCharacters(in: range, with: replacement)
  }
}

class Localizer {
  
  private let resource : String
  private var translator : [String:String] = [:]
  private let locale : Locale = Locale.current
  private var inited = false
  private var errorStr : String? = nil
  
  private func initWithBundle() -> Bool {
    if let filepath = Bundle.main.path(forResource : resource , ofType: "json"){
      do {
        let contents = try String(contentsOfFile: filepath)
        print(contents)
        let jsonDecoder = JSONDecoder()
        translator = try jsonDecoder.decode([String:String].self, from: contents.data(using:.utf8)!)
      } catch {
        errorStr = "Localizer Bundle or Decode error for \(locale.languageCode!) : \(error) !"
        return false
      }
    } else {
      errorStr = "Localizable.json not found for \(locale.languageCode!) !"
      return false
    }
    return true
  }
  
  private func convert( value : Any ) -> String {
    if let val=value as? Int {
      DDLogDebug("Int found")
      return String(val)
    } else if let val=value as? String {
      DDLogDebug("String found")
      return val
    } else if let val = value as? Float {
      return String(val)
    } else if let val = value as? Substring {
      return String(val)
    } else {
      DDLogDebug("unmanaged type in convert: \(String(describing: value))")
      assert(false)
    }
    return ""
  }

  private func substitute( _ src : String , _ args : [Any]) -> String {
    DDLogDebug("substitute for : \(src)")

    var ii : Int = 0
    var returned = src
    var finished = false
    while ( !finished ) {
      if ( src.range(of:"%") == nil ) {
        return src
      }
      if ( ii < args.count ) {
        returned  = returned.replacingFirstOccurrence(of: "%", with: convert(value:args[ii]))
        ii += 1
      } else {
        finished = true
      }
    }
    return returned
  }


  init( resource: String ) {
    self.resource = resource
  }
  
  func tr( _ src : String , _ args : Any...) -> String {
    if ( !inited ) {
      inited = initWithBundle()
      if ( !inited ) {
        return src
      }
    }
    var returned : String? = translator[src]
    
    if let ret = returned {
      returned = substitute(ret,args)
    } else {
      // no translation found return back original removing underscore
      returned  = src.replacingOccurrences(of: "_", with: " ")
      returned = substitute(returned!,args)
    }
    return returned!
  }
}

