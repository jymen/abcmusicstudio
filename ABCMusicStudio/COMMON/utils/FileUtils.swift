//
//  FileUtils.swift
//  ABCStudio
//
//  Created by jymengant on 9/2/18 week 35.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack
import PromiseKit


enum FileType {
  case Directory
  case File
  case Inexisting
  case NotALocalFile
}

class FileUtils {
  
  // Just in case !!!
  #if os(iOS)
  let ios = true
  #elseif os(macOS)
  let ios = false
  #endif
  
  private let fm = FileManager.default
  
  var applicationDirectory : URL?  {
    do {
      return  try fm.url( for: .applicationSupportDirectory ,
                             in: .userDomainMask ,
                             appropriateFor : nil ,
                             create: true
                           )
    } catch {
      return nil
    }
  }
  
  func getResourceType ( url : URL ) -> FileType {
    var isDir : ObjCBool = false
    if ( url.isFileURL ) {
      if fm.fileExists(atPath: url.path, isDirectory:&isDir) {
        if ( isDir.boolValue ) {
          return .Directory
        } else {
          return .File
        }
      } else {
        return .Inexisting
      }
    } else {
      return .NotALocalFile
    }
  }
  
  private func buildLeaf(url : URL , type: ABCResType) -> Leaf {
    return  Leaf(title: url.deletingPathExtension().lastPathComponent,
                 creation: Date() ,
                 storageURL: url,
                 encoding: .utf8,
                 type: type)
  }
  
  func folderFromFileSystem( url: URL , filter: String? , recurse: Bool) throws -> Folder? {
    if ( getResourceType(url: url) == .Directory ) {
      let folder = Folder(infos: buildLeaf(url:url,type: .GroupWithFolder))
      let items = try fm.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
      for childURL in items {
        //let childURL : URL = url.appendingPathComponent(item)
        if ( getResourceType(url: childURL) == .Directory ) {
          if ( recurse ) {
            let childFolder = try folderFromFileSystem(url: childURL, filter: filter, recurse: recurse)
            if let childFolder = childFolder {
              childFolder.addTo(folder:folder)
            }
          }
        } else if ( getResourceType(url: childURL) == .File ) {
          var selected : Bool = true
          if let filter = filter {
            selected = childURL.absoluteString.hasSuffix(filter)
          }
          if ( selected ) {
            let f = File(infos: buildLeaf(url:childURL,type: .ABCScore))
            f.addTo(folder:folder)
          }
        }
      }
      return folder
    }
    return nil
  }
  
  
  func createLocalFolder( url: URL ) throws {
    do {
      try fm.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
    } catch let error as NSError {
      throw ABCFailure(message: "\(url.absoluteString) error creating folder : \(error)",kind:.IOError)
    }
  }
  
  func checkPath(parent : URL , name : String , directory: Bool) throws -> URL  {
    let returnedURL = parent.appendingPathComponent(name)
    var isDir : ObjCBool = false
    let exists : Bool = fm.fileExists(atPath: returnedURL.path,isDirectory: &isDir)
    if ( exists ) {
      if ( isDir.boolValue != directory ) {
        throw ABCFailure(message: "\(returnedURL.absoluteString) bad type : found directory instead of file or reverse",kind:.IOError)
      }
    } else {
      if ( directory ) {
        // does not exist => mkdir
        try createLocalFolder(url: returnedURL)
      } else {
        throw ABCFailure(message: "File not found: \(returnedURL.absoluteString)",kind:.NotFound)
      }
    }
    return returnedURL
  }
  
  func  changeFName(url: URL) -> URL {
    let fComps = url.pathComponents
    let fPath = fComps[0...fComps.count-2].joined(separator: "/")
    var newFName : String? = nil
    // append "1" for unicity automatically
    let fName = fComps[fComps.count-1] // last part is file
    var fNameComps = fName.components(separatedBy: ".")
    fNameComps[0] = fNameComps[0] + "1" // make unique
    newFName = fNameComps.joined(separator: ".")
    var returned : URL = URL(fileURLWithPath: fPath)
    returned = returned.appendingPathComponent(newFName!)
    return returned
  }
  
  func existFile( url: URL , directory: Bool) -> Bool {
    var isDir : ObjCBool = ObjCBool(false)
    if ( fm.fileExists(atPath: url.path , isDirectory : &isDir) ) {
      if ( isDir.boolValue && directory ) {
        return true
      } else {
        if ( !directory ) {
          return true
        }
      }
    }
    return false
  }
  
  func existDatabase( url : URL ) -> Bool {
    return existFile(url: url, directory: false)
  }
  
  func deleteDatabase( url : URL ) throws {
    if ( existDatabase(url: url)) {
      try deleteOnDisk(path: url.path)
    }
  }
  
  func listDatabases() throws -> [String]? {
    let dbFolder = preferences.databasePath
    let list = try fm.contentsOfDirectory(at: dbFolder!, includingPropertiesForKeys: nil )
    var returned = [String]()
    for element in list {
      returned.append(element.lastPathComponent)
    }
    return returned
  }
  
  /**
   Check drirPath can be written
   */
  func canWriteAt(dirPath : String) -> Bool {
    return fm.isWritableFile(atPath: dirPath)
  }
  
  /**
   delete file or folder on disk
   */
  func deleteOnDisk(path: String) throws {
    do {
      try fm.removeItem(atPath:path)
      DDLogDebug("file : \(path) has been deleted")
    }
    catch let error as NSError {
      throw ABCFailure(message: "\(path) error deleting item : \(error)",kind:.IOError)
    }
  }
  
  
  func read( url: URL ) throws -> Data {
    return try Data(contentsOf:url)
  }
  
  func read( absolute: String ) throws -> Data {
    return try self.read(url: URL(fileURLWithPath: absolute))
  }
  
  func read( url: URL , backupEncoding: String.Encoding) throws -> ( String , String.Encoding )   {
    do {
      //
      // first let the system guess for encoding when reading
      //
      let data = try read(url: url)
      var content1 : NSString?
      let encoding = NSString.stringEncoding(for: data, encodingOptions: nil, convertedString: &content1 , usedLossyConversion: nil)
      var strEncoding = String.Encoding(rawValue: encoding)
      if ( encoding == 0 ) {
        // let use the one provided as backup
        strEncoding = backupEncoding
      }
      
      DDLogDebug("reading \(url.absoluteString) with encoding : \(strEncoding.description)")
      let content = try String(contentsOf: url, encoding: strEncoding)
      DDLogDebug("content: \(content)")
      return ( content , strEncoding )
    }
    catch let error as NSError {
      let desc = error.localizedDescription
      throw ABCFailure(message: "\(url.absoluteString) error reading  : \(desc)",kind:.IOError)
    }
  }
  
  func asyncRead( url: URL ,
                  backupEncoding: String.Encoding )
                  -> Promise<( String , String.Encoding )> {
    return Promise { seal in
      do {
        let result = try read(url: url, backupEncoding: backupEncoding)
        seal.fulfill(result)
      } catch let error as ABCFailure {
        seal.reject(error)
      } catch {
      }
    }
  }
  
  func write( url: URL , content: String , encoding: String.Encoding ) throws {
    do {
      var curEncoding = encoding
      if let bad = Unicode.checkEncoding(forString: content, using: encoding) {
        let backupEncodingIndex  = Unicode.encodingIndex(strEncoding: bad[0])
        curEncoding = Unicode.encoding(atIndex: backupEncodingIndex)
      }
      let data = content.data(using: curEncoding)
      try data!.write(to: url)
      // try content.write(to: url, atomically: false, encoding: encoding)
    }
    catch {
      dump(error)
      throw ABCFailure(message: "WRITE ERROR on: \(url.absoluteString) : "+error.localizedDescription ,kind:.IOError)
    }
  }
  
  //
  // load from bundle
  //
  func loadTextFile( bundle : Bundle ,
                     name : String ,
                     ofType : String ,
                     directory : String ,
                     encoding : String.Encoding = .utf8 ) throws -> ( String , String.Encoding ) {
    let path = bundle.path(forResource: name, ofType: ofType , inDirectory: directory)
    if ( path == nil ) {
      throw ABCFailure(message: "Not found in bundle \(bundle.bundlePath) :\(name) of type \(ofType) " ,kind:.IOError)
    }
    let url = URL(fileURLWithPath: path!)
    return try read(url:url, backupEncoding : encoding)
  }
  
  func loadTextFile( parent : URL , name: String , encoding: String.Encoding ) throws -> (String,String.Encoding)  {
    let fileURL : URL = try checkPath(parent:parent ,name:name,directory:false)
    //reading
    return try read(url: fileURL, backupEncoding: encoding)
  }
  
  func storeTextUrl( url : URL , content: String , encoding: String.Encoding ) throws  {
    try write( url : url , content: content , encoding: encoding)
  }
  
  func storeTextFile(parent : URL , name: String , content: String , encoding: String.Encoding ) throws {
    let fileURL : URL = parent.appendingPathComponent(name)
    //writing
    try storeTextUrl(url: fileURL, content: content, encoding: encoding)
  }
  
}
