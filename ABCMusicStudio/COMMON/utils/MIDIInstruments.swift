//
//  MIDIInstruments.swift
//  ABCStudio
//
//  Created by jymengant on 7/2/19 week 27.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa

class MIDIInstruments {
  
  // Standard mid instrument classification
  static let instruments : [String] = [
    loc.tr("Acoustic Grand Piano") ,
    loc.tr("Bright Acoustic Piano") ,
    loc.tr("Electric Grand Piano") ,
    loc.tr("Honky-tonk Piano") ,
    loc.tr("Electric piano 1") ,
    loc.tr("Electric piano 2") ,
    loc.tr("Harpsichord") ,
    loc.tr("Clavinet") ,
    loc.tr("Celesta") ,
    loc.tr("Glockenspiel") ,
    loc.tr("Music Box") ,
    loc.tr("Vibraphone") ,
    loc.tr("Marimba") ,
    loc.tr("Xylophone") ,
    loc.tr("Tubular bells") ,
    loc.tr("Dulcimer") ,
    loc.tr("Drawbar organ") ,
    loc.tr("Percusive organ") ,
    loc.tr("Rock organ") ,
    loc.tr("Church organ") ,
    loc.tr("Reed organ") ,
    loc.tr("Accordion") ,
    loc.tr("Harmonica") ,
    loc.tr("Tango accordion") ,
    loc.tr("Acoustic guitar(nylon)") ,
    loc.tr("Acoustic guitar(steel)") ,
    loc.tr("Electric guitar(jazz)") ,
    loc.tr("Electric guitar(clean)"),
    loc.tr("Electric guitar(muted)"),
    loc.tr("Overdriven Guitar") ,
    loc.tr("Distorsion Guitar"),
    loc.tr("Guitar harmonics" ),
    loc.tr("Acoustic bass") ,
    loc.tr("Electric bass(finger)") ,
    loc.tr("Electric bass(pick)") ,
    loc.tr("Fretless bass") ,
    loc.tr("Slap bass 1") ,
    loc.tr("Slap bass 2") ,
    loc.tr("Synth bass 1") ,
    loc.tr("Synth bass 2") ,
    loc.tr("Violin") ,
    loc.tr("Viola") ,
    loc.tr("Cello"),
    loc.tr("Contrabass") ,
    loc.tr("Tremolo strings") ,
    loc.tr("Pizzicato strings") ,
    loc.tr("Orchestral harp") ,
    loc.tr("Timpani") ,
    loc.tr("String ensemble 1") ,
    loc.tr("String ensemble 2") ,
    loc.tr("Choir Aahs") ,
    loc.tr("Voice Oohs") ,
    loc.tr("Synth Choir") ,
    loc.tr("Orchestral Hit") ,
    loc.tr("Trumpet") ,
    loc.tr("Trombone" ),
    loc.tr("Muted trumpet") ,
    loc.tr("French horn") ,
    loc.tr("Brass section") ,
    loc.tr("Synth brass 1") ,
    loc.tr("Synth brass 2") ,
    loc.tr("Soprano sax") ,
    loc.tr("Alto sax") ,
    loc.tr("Tenor sax") ,
    loc.tr("Baritone sax") ,
    loc.tr("Oboe") ,
    loc.tr("English horn") ,
    loc.tr("Bassoon") ,
    loc.tr("Clarinet") ,
    loc.tr("Piccolo") ,
    loc.tr("Flute") ,
    loc.tr("Recorder") ,
    loc.tr("Pan flute") ,
    loc.tr("Blown bottle") ,
    loc.tr("Shakuhachi") ,
    loc.tr("Whistle"),
    loc.tr("Ocarina") ,
    loc.tr("Synth lead 1") ,
    loc.tr("Synth lead 2"),
    loc.tr("Synth lead 3") ,
    loc.tr("Synth lead 4") ,
    loc.tr("Synth lead 5") ,
    loc.tr("Synth lead 6") ,
    loc.tr("Synth lead 7") ,
    loc.tr("Synth lead 8")
  ]
  
  func toCombo( combo : NSComboBox , selected : String ) {
    for instrument in Self.instruments {
      combo.addItem(withObjectValue: instrument)
      if ( selected == instrument ) {
        combo.selectItem(withObjectValue: selected)
      }
    }
    combo.numberOfVisibleItems = 5
    combo.hasVerticalScroller = true
    combo.usesDataSource = false
  }

  
}
