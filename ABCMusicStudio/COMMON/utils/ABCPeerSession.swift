//
//  ABCPeerSession.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/16/20 week 20.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import MultipeerConnectivity
import CocoaLumberjack

protocol PeerSessionController {
  func present( sessionController : MCBrowserViewController )
  func dismiss()
}

enum PeerSessionState : String {
  case Dormant
  case Started
  case Connected
  case Disconnected

}

protocol PeerSessionObserver {
  
  func sessionChanges( state: PeerSessionState )
  func newMessageReceived( args: EntryPointArgs )
  
}

// Service type must be a unique string, at most 15 characters long
// and can contain only ASCII lowercase letters, numbers and hyphens.
private let serviceType = "abcmstdoservice"


class ABCScoreService : NSObject,
  MCNearbyServiceAdvertiserDelegate,
  MCNearbyServiceBrowserDelegate,
  MCSessionDelegate {
  
  // Service type must be a unique string, at most 15 characters long
  // and can contain only ASCII lowercase letters, numbers and hyphens.
  private let serviceType = "abcmstdoservice"
  private let peerID : MCPeerID
  
  private var serviceAdvertiser : MCNearbyServiceAdvertiser?
  private var serviceBrowser : MCNearbyServiceBrowser?
  
  var currentPeerName : String?
  var observer : PeerSessionObserver?
  var state: PeerSessionState = .Dormant
  
  lazy var session : MCSession = {
        let session = MCSession(peer: self.peerID)
        session.delegate = self
        return session
  }()
  
  init(deviceName: String , observer: PeerSessionObserver? = nil) {
    self.peerID = MCPeerID(displayName: deviceName )
    super.init()
    self.observer = observer
  }
  
  func startHosting() {
    self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: peerID,
                                                       discoveryInfo: nil,
                                                       serviceType: serviceType)
    self.serviceAdvertiser!.delegate = self
    self.serviceAdvertiser!.startAdvertisingPeer()
    self.state = .Started
    notify()
  }
  
  func joinSession() {
    self.serviceBrowser = MCNearbyServiceBrowser(peer: peerID,
                                                 serviceType: serviceType)

    self.serviceBrowser!.delegate = self
    self.serviceBrowser!.startBrowsingForPeers()
  }

  func stop() {
    self.serviceAdvertiser?.stopAdvertisingPeer()
    self.serviceBrowser?.stopBrowsingForPeers()
    self.state = .Dormant
    notify()
    DDLogDebug("ABCScoreService has been cleaned up")
  }
  
  private func errorHandler(err: String?) {
    if let err = err {
      if ModalAlert().show(question: "ABCScoreService error :", text: err, style: .Critical){}
    }
  }
  
  private func notify() {
    if let observer = observer {
      observer.sessionChanges(state: self.state)
    }
  }
  
  func sendMessage( args : EntryPointArgs ) {
    var err : String? = nil
    if session.connectedPeers.count > 0 {
      let encoder = JSArgsEncoderDecoder()
      let strJs = encoder.encode(args: args)
      if let strJs = strJs {
        let data = strJs.data(using: .utf8)
        if let data = data {
          do {
            try session.send(data, toPeers: session.connectedPeers, with: .reliable)
          } catch let error as NSError {
            err = "PeerSession send critical error : \(error.localizedDescription)"
          }
        } else {
          err = "PeerSession: args utf8 convertion error"
        }
      } else {
        err = "PeerSession: unable to encode args"
      }
      errorHandler(err: err)
    }
  }
  
  func sendFile( url : URL ) {
    
  }

  //
  // MCNearbyServiceAdvertiserDelegate
  //
  func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
    DDLogDebug("Invitation received from : \(peerID.displayName)")
    self.currentPeerName = peerID.displayName
    invitationHandler(true, self.session)
    self.state = .Connected
    notify()
    
  }
  
  func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
    DDLogDebug("didNotStartAdvertisingPeer ERROR: : \(error)")
    self.state = .Disconnected
    notify()
  }

  //
  // MCNearbyServiceBrowserDelegate
  //
  func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
    DDLogDebug("didNotStartBrowsingForPeers: \(error)")
  }

  func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
    DDLogDebug("foundPeer: \(peerID)")
    DDLogDebug("invitePeer: \(peerID)")
    browser.invitePeer(peerID, to: self.session, withContext: nil, timeout: 30)
    // stop browsing when found
    browser.stopBrowsingForPeers()
  }

  func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
    DDLogDebug("lostPeer: \(peerID)")
    self.state = .Disconnected
    notify()
  }

  //
  // MCSessionDelegate
  //

  func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
    switch state {
    case MCSessionState.connected:
      DDLogDebug("ABCPeerSession Connected: \(peerID.displayName)")
      self.currentPeerName = peerID.displayName
      self.state = .Connected
      notify()

      
    case MCSessionState.connecting:
      DDLogDebug("ABCPeerSession Connecting: \(peerID.displayName)")
      
    case MCSessionState.notConnected:
      DDLogDebug("ABCPeerSession Not Connected: \(peerID.displayName)")
      self.currentPeerName = nil
      self.state = .Disconnected
      notify()

    @unknown default:
      DDLogDebug("Unknown ABCPeerSession state")
    }
  }
  
  func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
    var err : String? = nil
    let args = String(data: data, encoding: .utf8)
    if let args = args {
      let encoder = JSArgsEncoderDecoder()
      do {
        let decoded = try encoder.decode(json: args)
        if let observer = observer {
          observer.newMessageReceived(args: decoded)
        }
      } catch let error as NSError {
        err = "JSON decoding error : \(error.localizedDescription)"
      }
    } else {
      err = "PeerSession: data to String conversion error"
    }
    errorHandler(err: err)
  }
  
  func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    DDLogDebug("receiving STREAM : \(streamName)")
  }
  
  func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    DDLogDebug("receiving RESOURCE : \(resourceName)")
  }
  
  func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    DDLogDebug("receiving RESOURCE HAS ENDED: \(resourceName)")
  }

  
}
