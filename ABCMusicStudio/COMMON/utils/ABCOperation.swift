//
//  ABCOperation.swift
//  ABCStudio
//
//  Created by jymengant on 8/10/19 week 32.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation

class ABCOperation : Operation {

  private var _ready : Bool = true
  private var _executing : Bool = false
  private var _finished : Bool = false
  
  override var isExecuting: Bool {
    return _executing
  }
  
  override var isReady: Bool {
    return _ready
  }
  
  override var isFinished: Bool {
    return _finished
  }
  
  func executing(_ executing: Bool) {
    _executing = executing
  }
  
  func finish(_ finished: Bool) {
    _finished = finished
  }
  
  func ready(_ ready: Bool) {
    _ready = ready
  }
  
}
