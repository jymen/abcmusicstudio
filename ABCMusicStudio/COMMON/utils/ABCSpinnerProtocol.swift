//
//  ABCSpinnerProtocol.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/14/20 week 20.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation

protocol ABCSpinnerProtocol {
  
  // start spinner 
  func start() -> Bool ;

  // stop it
  func stop() ;

}
