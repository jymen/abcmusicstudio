//
//  ABCFailure.swift
//  ABCStudio
//
//  Created by jymengant on 6/12/18 week 24.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation

/**
 Handle ABC project exceptions
 */
struct  ABCFailure : Error {
  enum FailureKind{
    case initFailure
    case generalFailure
    case IOError
    case NotFound
    case ABCParsing
    case JSONParsing
    case JSCALLFailure
    case EncodingFailure
    case DatabaseFailure
    case AUDIOFailure
    case XMLError
    case XMLMusicError
  }
  let token: ABCToken?
  let message : String?
  let kind : FailureKind
  
  init( message: String? = nil , kind : FailureKind ,token: ABCToken? = nil ) {
    self.token = token
    self.kind = kind
    self.message = message
  }
}

