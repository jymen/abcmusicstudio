//
//  Base64Encoder.swift
//  ABCMusicStudio
//
//  Created by jymengant on 3/15/20 week 11.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation

enum Base64EncodingType {
  case Audio
  case Image
}

/**
 Base64 Encoder Decoder
 */
class Base64Encoder {
  
  
  private func getUrlData(url : URL) -> Data? {
    if FileManager.default.fileExists(atPath: url.path){
      if let data = NSData(contentsOfFile: url.path) {
        return data as Data
      }
    }
    return nil
  }
  
  private func getImageData() -> Data? {
    return nil
  }
  

  func encode(url : URL) -> String? {
    if let data = getUrlData(url:url) {
      let imageStr = data.base64EncodedString()
      return imageStr
    }
    return nil
  }
  
  func decode( str64 : String ) -> Data? {
    return Data(base64Encoded: str64, options: .ignoreUnknownCharacters)
  }
}
