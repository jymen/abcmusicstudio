//
//  Portability.swift
//  ABCStudio
//
//  Created by jymengant on 8/16/19 week 33.
//  Copyright © 2019 jymengant. All rights reserved.
//

//
//  some dedicated classes for cross platfor portability
//

import Foundation

//
// Alert style portability
//
enum ABCAlertStyle {
  case Critical
  case Informational
  case Warning
}
