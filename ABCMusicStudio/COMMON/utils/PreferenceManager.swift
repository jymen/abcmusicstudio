//
//  PreferenceManager.swift
//  ABCStudio
//
//  Created by jymengant on 6/9/18 week 23.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack

private let ABCHome = "ABCHome"
private let ABCStorage = "ABCStorage"
private let IncludeTemplate = "IncludeTemplate"

/* Handle ColorModes */
enum InterfaceStyle : String {
  case Dark, Light
  
  init() {
    let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
    self = InterfaceStyle(rawValue: type)!
  }
}

// init preferences
let preferences = PreferenceManager()

/**
  Manages preferences for ABCStudio
*/
class PreferenceManager {
  
  private let userDefaults = UserDefaults.standard
  private let repoName : String = "abcrepository"
  private let prefPathName : String = "preferences"
  private let printPathName : String = "printing"
  private let defStorageName : String = "abcdata"
  private let databasePathName : String = "databases"
  private let currentStyle = InterfaceStyle()
  private let fUtils = FileUtils()
  var homePathURL : URL? = nil
  var defStorage : URL? = nil
  var userPath : URL? = nil
  var prefsPath : URL? = nil
  var printPath : URL? = nil
  var databasePath : URL? = nil
  var repoURL : URL? = nil

  func isDarkMode() -> Bool {
    if ( currentStyle == .Dark ) {
      return true
    }
    return false
  }
  
  var includeTemplate : Bool {
    get {
      return userDefaults.bool(forKey: IncludeTemplate)
    }
    set {
      let defaults = UserDefaults.standard
      defaults.set(newValue, forKey: IncludeTemplate)
    }
  }
  
  func registerDefaultPreferences() {
    let defaults : [String : Any] =
    [
      ABCHome : "ABCMusicStudio" ,
      ABCStorage : ".json" ,
      IncludeTemplate : true
    ]
    UserDefaults.standard.register(defaults:defaults)
    DDLogDebug("UserDefaults preferences registered")
  }
    
  func getStorageType() -> String {
    return userDefaults.string(forKey: ABCStorage)!
  }
 
  func getRepoName() -> String {
    return repoName
  }
  
  
  
  /**
   
   check preferences (to be called at app init time )
   check .ABCFolder exists else create it
   
  */
  func checkPreferences() throws {
    let fu = FileUtils()
    self.registerDefaultPreferences()
    let homeDir = fu.applicationDirectory
    if let homeDir = homeDir {
      let productPath = userDefaults.string(forKey: ABCHome)
      let homePathURL = try fUtils.checkPath(parent: homeDir, name: productPath!,directory: true)
      defStorage =  try fUtils.checkPath(parent: homePathURL, name: defStorageName,directory: true)
      prefsPath = try fUtils.checkPath(parent: homePathURL, name : prefPathName,directory: true)
      printPath = try fUtils.checkPath(parent: homePathURL, name : printPathName,directory: true)
      databasePath = try fUtils.checkPath(parent: homePathURL, name : databasePathName,directory: true)
    
      // dir is available check build path repos
      // repo existence to be checked later
      let repName = self.repoName + self.getStorageType()
      repoURL = URL(fileURLWithPath: repName, relativeTo: homePathURL)
    } else {
      if ModalAlert().show(question: "application directory  ", text: "fail to get application directory : NIL ", style: .Critical){}

    }
  }
  
  
}
