//
//  JSArgEncoderDecoder.swift
//  ABCStudio
//
//  Created by jymengant on 8/20/19 week 34.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation

//
// JS communications structs and classes
//

struct JsReturned {
  let err: String?
  let response : Any?
  let str : String?
  
  func checkError() throws {
    // check application error if any
    if let err = self.err {
      throw ABCFailure(message: err, kind: .JSCALLFailure , token: nil)
    }
  }
  
  func isDone() -> Bool {
    if let str = response as? String {
      if ( str == "Done") {
        return true
      }
    }
    return false
  }
}

//
// Js function arguments encoder decoder
//
struct JSONArgs : Codable {
  var args : [String:String]? = nil
}


//
// Encode Decode JS arguments
//
class JSArgsEncoderDecoder {
  
  func encode( args : EntryPointArgs ) -> String? {
    let encoder = JSONEncoder()
    if let jsonData = try? encoder.encode(args) {
      if let jsonString = String(data: jsonData, encoding: .utf8) {
        return jsonString
      }
    }
    return nil
  }
  
  func decode( json: String ) throws -> EntryPointArgs  {
    let decoder = JSONDecoder()
    let returned : EntryPointArgs  = try decoder.decode(EntryPointArgs.self, from: json.data(using: .utf8)!)
    return returned
  }
}

