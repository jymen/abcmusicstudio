//
//  EventManager.swift
//  ABCStudio
//
//  Created by jymengant on 8/23/18 week 34.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation
import EmitterKit

enum ABCEventType {
  case TreeElementRemoved
  case TreeElementDeleted
  case TreeSelectionChanged
  case SyntaxColoringChanged
  case TuneNumbersChanged
}

struct ABCEvent {
  let type : ABCEventType
  let msg : Any?
}

class EventManager {
  
  // singleton(AB
  static let shared = EventManager()
  
  let events :  Event<ABCEvent> =  Event<ABCEvent>()
  
  func add( listener: @escaping (ABCEvent) -> Void  ) -> EventListener<ABCEvent> {
    return events.on( listener )
  }
  
  func addOnce( listener: @escaping (ABCEvent) -> Void  ) {
    events.once(handler: listener )
  }
  
  func emit( event: ABCEvent ) {
    events.emit(event)
  }
  
}

