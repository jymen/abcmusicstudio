//
//  ABCWebView.swift
//  ABCStudio
//
//  Created by jymengant on 8/20/19 week 34.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack
import WebKit
import PromiseKit

//
// IOS / Osx portable WkWebview 
//
class ABCWebView : NSObject ,
  WKUIDelegate ,
  WKNavigationDelegate ,
  WKScriptMessageHandler {
  
  let view : WKWebView
  var critical: Bool = false
  var pixSize : CGSize? = nil
  var scoreInWebView : Bool = false
  var webRect : CGRect? = nil
  var spinner : ABCSpinnerProtocol? = nil

  
  // callback portable IOS/OSX handler dedicated  protocol
  var portableHandler : ABCWebViewPortabilityAdapter
  
  init(view : WKWebView , webHandler : ABCWebViewPortabilityAdapter ) {
    self.view = view
    self.portableHandler = webHandler
    super.init()
    self.view.uiDelegate = self
    self.view.navigationDelegate = self
    let config = self.view.configuration
    config.userContentController.add(self, name: "jsCallBackHandler")
    config.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
    // no user gestures requested to play media
    let myAudiovisualMediaType: WKAudiovisualMediaTypes = []
    config.mediaTypesRequiringUserActionForPlayback = myAudiovisualMediaType
  }
  
  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    print("Provisional html navigation faillure ")
    DDLogError("Provisional html navigation faillure : \(error)")
  }
  
  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    print("html navigation faillure ")
    DDLogError("html navigation faillure : \(error)")
  }
  
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    print("Start loading html ...")
    #if os(OSX)
    spinner = ABCSpinner(view:self.view)
    #else
    spinner = IOSSpinner(view:self.view)
    #endif
    _ = spinner!.start()
  }
  
  func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String,
               initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
    if ModalAlert().show( question:"Javascript Alert" , text: message , style: .Informational ){}
    completionHandler()
  }
  
  
  func load( name : String , type : String) {
    let filePath = Bundle.main.path(forResource: name, ofType: type, inDirectory: "webstuff")!
    do {
      let html = try String(contentsOfFile: filePath, encoding: .utf8)
      view.loadHTMLString(html , baseURL: Bundle.main.resourceURL?.appendingPathComponent("webstuff"))
    } catch {
      let myError : ABCFailure = error as! ABCFailure
      _ = ModalAlert().show(question: "ERROR Loading index.html", text: myError.message! , style: .Critical )
      critical = true
    }
  }
  
  private func getWebPageDimensions() {
    self.view.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
      guard let contentHeight = height as? CGFloat else { print("Content height could not be obtained"); return }
      self.view.evaluateJavaScript("document.body.scrollWidth", completionHandler: { (width, error) in
        let contentWidth = width as! CGFloat
        self.webRect = CGRect(x: 0, y: 0, width: contentWidth, height: contentHeight)
      })
    })
  }
  
  private func jsMainPageIniter( entryPoint : String ) -> Promise<Any> {
    return Promise {
      seal in
      let callString = "\(entryPoint)()"
      DDLogDebug("call jsIniter")
      // DDLogDebug("call evaluateJavaScript using : \(callString)")
      view.evaluateJavaScript( callString,
                               completionHandler: { (object, error) in
                                DDLogDebug("evaluateJavaScript returning")
                                // this one will just return "OK"
                                if let retStr = object as? String {
                                  if retStr != "OK" {
                                    _ = ModalAlert().show(question: "ERROR in JS Initer", text: retStr , style: .Critical )
                                    seal.fulfill(retStr)
                                  } else {
                                    seal.fulfill(retStr)
                                  }
                                } else {
                                  dump(error) // Better dump everything here to get all JS error details
                                  seal.reject(error!)
                                }
        })
    }
  }

  private func basicJsCall( entryPoint : String , args : String?  ) -> Promise<Any> {
    return Promise {
      seal in
      var callString : String
      if let args = args {
        callString = "\(entryPoint)(\(args) )"
      } else {
        callString = "\(entryPoint)()"
      }
      DDLogDebug("call evaluateJavaScript")
      // DDLogDebug("call evaluateJavaScript using : \(callString)")
      view.evaluateJavaScript( callString,
                               completionHandler: { (object, error) in
                                DDLogDebug("evaluateJavaScript returning")
                                
                                if let retStr = object as? NSDictionary {
                                  if let returned = retStr["fxRet"] as? NSDictionary {
                                    if let error = returned["error"] as? String {
                                      _ = ModalAlert().show(question: "ERROR in JS call", text: error , style: .Critical )
                                    } else {
                                      seal.fulfill(returned["data"]! )
                                    }
                                  }
                                } else {
                                  dump(error) // Better dump everything here to get all JS error details
                                  seal.reject(error!)
                                }
      })
    }
  }
    
  func swiftEntryPoint( className : String , fx : String , args : [Any]? , completion: @escaping (JsReturned)->() ){
    let fxArgs = EntryPointArgs(className : className, fx: fx , args: args, fxRet: nil)
    let encoder = JSArgsEncoderDecoder()
    let strJs = encoder.encode(args: fxArgs)
    basicJsCall(entryPoint: "window.abcStudioAPI.externalEntryPoint", args: strJs )
      .done {
        response in
        DDLogDebug("swiftEntryPoint JS OK returned : \(response)")
        completion(JsReturned(err: nil, response: response, str: nil))
      }.catch {
        error in
        DDLogDebug("swiftEntryPoint JS in error \(error.localizedDescription)")
        completion( JsReturned(err: error.localizedDescription, response: nil, str: nil))
    }
  }
  
  func setParam( params: Dictionary<String,AnyCodable> ,  completion: @escaping (JsReturned)->() ) {
    DDLogDebug("Entering setParams")
    swiftEntryPoint(className:"abcjs", fx: "setParams", args:[params], completion: completion )
  }
  
  private func mainPageIniter() {
    #if os(OSX)
    let initer = "abcStudioAPI.scoreAppView"
    #elseif os(iOS)
    let initer = "abcStudioAPI.mainAppView"
    #endif
    jsMainPageIniter(entryPoint: initer)
      .done {
        response in
        DDLogDebug("JS initer returned : \(response)")

    } .catch {
       error in
       DDLogDebug("JS initer in error \(error.localizedDescription)")
    }
  }
  
  private func setScoreParams() {
    // set abc score parameters
    if let pixSize = pixSize {
      let abcParams = [
        "staffWidth" : AnyCodable("\(pixSize.width)")
      ]
      setParam( params: abcParams, completion: {
        result in
        assert( result.isDone() )
        DDLogDebug("setParams : Done")
      })
    }
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    DDLogDebug("html page has been loaded")
  }
  
  //
  // sample minimal test function
  //
  func sampleJS( completion: @escaping (JsReturned)->() ) {
    DDLogDebug("Entering sampleJS")
    basicJsCall(entryPoint: "sampleTest", args: nil)
      .done {
        response in
        DDLogDebug("sample JS OK returned : \(response)")
        completion(JsReturned(err: nil, response: nil, str: (response as! String) ))
      }.catch {
        error in
        DDLogDebug("sample JS in error \(error.localizedDescription)")
        completion( JsReturned(err: error.localizedDescription, response: nil, str: nil))
    }
  }
  
  func ping( msg : String , completion: @escaping (JsReturned)->()  ) {
    DDLogDebug("Entering ping")
    swiftEntryPoint(className: "abcjs",
                    fx: "ping",
                    args: [msg],
                    completion: completion )
  }
  
  func cleanDiv( tag : String , completion: @escaping (JsReturned)->()  ) {
    DDLogDebug("Entering tag cleanup")
    swiftEntryPoint( className: "externalAPI" ,
                     fx: "cleanupChildren",
                     args: [tag],
                     completion: completion )
  }
  
  func showAbcScore( id : String , score: String , tuneNumber : Int , instrument : Int , bpm : Int , completion: @escaping (JsReturned)->() ) {
    DDLogDebug("Entering show ABC Score")
    swiftEntryPoint(className:"abcjs",
                    fx: "showScore",
                    args: [id,score,tuneNumber,instrument,bpm ],
                    completion: completion )
    getWebPageDimensions()
    scoreInWebView = true
  }
    
  func colorAbcNote( start : Int , end : Int , htmlColor : String ,completion: @escaping (JsReturned)->() ) {
    swiftEntryPoint(className:"abcjs",
                    fx: "highlightNote",
                    args: [start,end,htmlColor,true ],
                    completion: completion )
  }

  func uncolorAbcNote( start : Int , end : Int ,completion: @escaping (JsReturned)->()) {
    swiftEntryPoint(className:"abcjs",
                    fx: "highlightNote",
                    args: [start,end,"",false ],
                    completion: completion )
  }


  //
  // JS Callback handler
  //
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    if message.name == "jsCallBackHandler" {
      guard let jsonMsg = message.body as? String else {
        return
      }
      print(message.body)
      // check for sample html javascript init phase completed first
      if jsonMsg == "HtmlViewInited" {
        // just proceed with main page display
        DDLogDebug("Javascript init phase completed ... dispyaing main html view")
        mainPageIniter()
        setScoreParams()
        self.spinner?.stop()
      } else {
        // standard callback
        let jsoner = JSArgsEncoderDecoder()
        do {
          let decoded : EntryPointArgs = try jsoner.decode(json: jsonMsg)
          portableHandler.handleJSCallbackReturn(content: decoded)
        } catch {
          portableHandler.handleJSCallbackError(title: "JS Callback decoding", message: "JSON decoding message error:\(String(describing: error))")
        }
      }
    }
  }
  
}
