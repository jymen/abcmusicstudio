//
//  StringProtocol.swift
//  ABCStudio
//
//  Simplifying string substring
//
//  Created by jymengant on 1/29/19 week 5.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation

extension String {
  func indexOf(_ input: String,
               options: String.CompareOptions = .literal) -> String.Index? {
    return self.range(of: input, options: options)?.lowerBound
  }
  
  func substring(with nsrange: NSRange) -> Substring? {
    guard let range = Range(nsrange, in: self) else { return nil }
    return self[range]
  }

  func capitalizingFirstLetter() -> String {
      return prefix(1).capitalized + dropFirst()
  }

  mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
  }

}

extension StringProtocol {
  
  
  subscript(offset: Int) -> Element {
    return self[index(startIndex, offsetBy: offset)]
  }
  
  subscript(_ range: CountableRange<Int>) -> SubSequence {
    return prefix(range.lowerBound + range.count)
      .suffix(range.count)
  }
  subscript(range: CountableClosedRange<Int>) -> SubSequence {
    return prefix(range.lowerBound + range.count)
      .suffix(range.count)
  }
  
  subscript(range: PartialRangeThrough<Int>) -> SubSequence {
    return prefix(range.upperBound.advanced(by: 1))
  }
  subscript(range: PartialRangeUpTo<Int>) -> SubSequence {
    return prefix(range.upperBound)
  }
  subscript(range: PartialRangeFrom<Int>) -> SubSequence {
    return suffix(Swift.max(0, count - range.lowerBound))
  }
}

extension Substring {
  var string: String { return String(self) }
}

extension BidirectionalCollection {
  subscript(safe offset: Int) -> Element? {
    guard !isEmpty, let i = index(startIndex, offsetBy: offset, limitedBy: index(before: endIndex)) else { return nil }
    return self[i]
  }
}
