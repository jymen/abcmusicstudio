//
//  DirScannerOperation.swift
//  ABCStudio
//
//  Created by jymengant on 8/10/19 week 32.
//  Copyright © 2019 jymengant. All rights reserved.
//

import CocoaLumberjack
import PromiseKit


class DirScannerOperation : ABCOperation {
  
  let url : URL
  let filter : String?
  var result : Folder?
  var recurse : Bool
  
  init( url : URL , filter : String? = nil  , recurse : Bool = false) {
    self.url = url
    self.filter = filter
    self.recurse = recurse
    super.init()
    self.ready(true)
  }
  
  override func main() {
    guard isCancelled == false else {
      finish(true)
      return
    }
    do {
      executing(true)
      DDLogDebug("before scanning directory ...")
      //self.ir = try parser.parse()
      let fu = FileUtils()
      self.result = try fu.folderFromFileSystem(url: self.url, filter: self.filter, recurse: self.recurse)
      DDLogDebug("after scanning ...")
      finish(true)
      return
    } catch {
      // SEVERE parsing ERRORS (Not syntax ...)
      if ModalAlert().show(question: "Dir Scanning UNEXPECTED ERROR", text:"dir scan error :\(String(describing: error))", style: .Critical){}
      finish(true)
      return
    }
  }
}

class DirScannerOperationStub {
  
  private let operationQueue: OperationQueue = OperationQueue()
  private let url : URL
  private let filter: String?
  private let recurse : Bool
  
  init(url : URL , filter: String? , recurse: Bool)  {
    self.operationQueue.maxConcurrentOperationCount = 1
    self.url = url
    self.filter = filter
    self.recurse = recurse
  }
  
  func scanDir(onCompleted: @escaping ((Folder?) -> ())) {
    operationQueue.cancelAllOperations()
    operationQueue.isSuspended = true
    let dirScanOperation = DirScannerOperation(url: url,filter:filter,recurse:recurse)
    
    dirScanOperation.completionBlock = {
      if let result = dirScanOperation.result {
        DispatchQueue.main.async(execute: {
          DDLogDebug("returning from dir scanning ...")
          onCompleted(result)
        })
      }
    }
    
    operationQueue.addOperation(dirScanOperation)
    operationQueue.isSuspended = false
    
  }
  
  func getScanFolder() -> Promise<Folder?> {
    return Promise { seal in
      self.scanDir { (scanResult) in
        DDLogDebug("returning from scanning")
        seal.fulfill(scanResult)
      }
    }
  }
}

