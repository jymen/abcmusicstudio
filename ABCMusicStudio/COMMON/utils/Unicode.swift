//
//  Unicode.swift
//  ABCStudio
//
//  Created by jymengant on 12/15/18 week 50.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack


class Unicode {
  
  
  
  static let encodings : [ String : String.Encoding ] = [
    "ascii" : String.Encoding.ascii ,
    "iso2022JP" : String.Encoding.iso2022JP ,
    "isoLatin1" : String.Encoding.isoLatin1 ,
    "isoLatin2" : String.Encoding.isoLatin2 ,
    "japaneseEUC" : String.Encoding.japaneseEUC ,
    "macOSRoman" : String.Encoding.macOSRoman ,
    "nextstep" : String.Encoding.nextstep ,
    "nonLossyASCII" : String.Encoding.nonLossyASCII ,
    "shiftJIS" : String.Encoding.shiftJIS ,
    "symbol" : String.Encoding.symbol ,
    "unicode" : String.Encoding.unicode ,
    "utf16" : String.Encoding.utf16 ,
    "utf16BigEndian" : String.Encoding.utf16BigEndian ,
    "utf16LittleEndian" : String.Encoding.utf16LittleEndian ,
    "utf32" : String.Encoding.utf32 ,
    "utf32BigEndian" : String.Encoding.utf32BigEndian ,
    "utf32LittleEndian" : String.Encoding.utf32LittleEndian ,
    "utf8" : String.Encoding.utf8 ,
    "windowsCP1250" : String.Encoding.windowsCP1250 ,
    "windowsCP1251" : String.Encoding.windowsCP1251 ,
    "windowsCP1252" : String.Encoding.windowsCP1252 ,
    "windowsCP1253" : String.Encoding.windowsCP1253 ,
    "windowsCP1254" : String.Encoding.windowsCP1254 ,
  ]
  
  static let sortedEncodingsKeys = encodings.keys.sorted() { $0 > $1 }
  
  
  var encoding : String.Encoding?
  
  init( encoding : String ) {
    self.encoding = Unicode.encodings[encoding]
    if ( self.encoding == nil ) {
      // default to utf-8 if unknown
      self.encoding = Unicode.encodings["utf8"]
    }
  }
  
  static func encodingIndex( strEncoding : String ) -> Int {
    for index in 0..<sortedEncodingsKeys.count {
      if ( sortedEncodingsKeys[index] == strEncoding ) {
        return index
      }
    }
    return -1
  }

  static func encoding( atIndex : Int ) -> String.Encoding {
    if ( atIndex < 0 || atIndex > sortedEncodingsKeys.count ) {
      // default to utf8 if bad
      return .utf8
    }
    let strEncoding = sortedEncodingsKeys[atIndex]
    return encodings[strEncoding]!
  }
  
  static func encodingIndex( encoding : String.Encoding ) -> Int {
    return encodingIndex( strEncoding : fromEncoding(encoding: encoding)!)
  }
  
  static func fromEncoding( encoding: String.Encoding ) -> String? {
    for strEncoding in encodings.keys {
      if ( encodings[strEncoding] == encoding ) {
        return strEncoding
      }
    }
    return nil
  }
  
  /**
   returns nil if current encoding is acceptable else
   return list of valid encoding for given String
   */
  static func checkEncoding( forString :String , using : String.Encoding ) -> [String]? {
    DDLogDebug("checked encoding = \(String(describing: fromEncoding(encoding: using)))")
    if let _ = forString.data(using: using ) {
      DDLogDebug("Encoding is OK")
      return nil //
    }
    DDLogDebug("Encoding is BAD")
    var returned = [String]()
    for key in encodings.keys {
      if let _ =  forString.data(using: encodings[key]!) {
        returned.append(key)
      }
    }
    return returned
  }

  
  
  /**
   String to unicode
  */
  func encode ( str: String ) -> String? {
    if ( encoding != nil ) {
      let dataEnc = str.data(using: encoding!)
      if ( dataEnc != nil ) {
        return String(data: dataEnc! , encoding: encoding!)
      }
    }
    return nil
  }
  
  /**
   unicode to String
   */
  func decode ( str: String ) -> String? {
    if ( encoding != nil ) {
      let datadec  = str.data(using: encoding!)
      if ( datadec != nil ) {
        return String( data: datadec! , encoding: encoding!)
      }
    }
    return nil
  }
}
