//
//  AnyCodable.swift
//  ABCStudio
//
//  Created by jymengant on 4/13/19 week 15.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation

public struct AnyCodable: Codable , Hashable
{
  public var value: Any
  
  init( _ val : Any ) {
    value = val
  }
  
  public init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer()
    if let intVal = try? container.decode(Int.self) {
      value = intVal
    } else if let doubleVal = try? container.decode(Double.self) {
      value = doubleVal
    } else if let boolVal = try? container.decode(Bool.self) {
      value = boolVal
    } else if let stringVal = try? container.decode(String.self) {
      value = stringVal
    } else if let dictVal = try? container.decode(Dictionary<String,AnyCodable>.self) {
      value = dictVal
    } else {
      throw DecodingError.dataCorruptedError(in: container, debugDescription: "ABCStudio failure :the container contains nothing serialisable")
    }
  }
  
  public func encode(to encoder: Encoder) throws {
    var container = encoder.singleValueContainer()
    if let intVal = value as? Int {
      try container.encode(intVal)
    } else if let doubleVal = value as? Double {
      try container.encode(doubleVal)
    } else if let boolVal = value as? Bool {
      try container.encode(boolVal)
    } else if let stringVal = value as? String {
      try container.encode(stringVal)
    } else if let dictVal = value as? Dictionary<String,AnyCodable> {
      try container.encode(dictVal)
    } else {
      throw EncodingError.invalidValue(value, EncodingError.Context.init(codingPath: [], debugDescription: "ABCStudio failure : The value is not encodable"))
    }
  }
  
  public static func == (lhs: AnyCodable, rhs: AnyCodable) -> Bool {
    if let val = lhs.value as? Int {
      return val == rhs.value as! Int
    } else if let val = lhs.value as? Double {
      return val == rhs.value as! Double
    } else if let val = lhs.value as? Bool {
      return val == rhs.value as! Bool
    } else if let val = lhs.value as? String {
      return val == rhs.value as! String
    }
    assert(false)
    return false
  }
  
  public func hash(into hasher: inout Hasher) {
    if let intVal = self.value as? Int {
      hasher.combine(intVal)
    } else if let intVal = self.value as? Double {
      hasher.combine(intVal)
    } else if let intVal = self.value as? Bool {
      hasher.combine(intVal)
    } else if let intVal = self.value as? Bool {
      hasher.combine(intVal)
    } else {
      assert(false)
    }
  }
}

struct FxRet : Codable {
  let error : String?
  var data :  [String: AnyCodable]
  
  enum ArgsKeys: String, CodingKey
  {
    case error
  }

  
  init ( data : [String:AnyHashable] ) {
    error = nil
    self.data = [String:AnyCodable]()
    for ( key , value ) in data {
      let codKey = key
      let codValue = AnyCodable(value.base)
      self.data[codKey] = codValue
    }
  }
}

struct EntryPointArgs : Codable {
  var className : String? 
  let fx : String
  var fxArgs :  [AnyCodable]?
  var fxRet : FxRet?
  
  enum ArgsKeys: String, CodingKey
  {
    case className, fx, fxArgs, fxRet
  }
  
  init( className : String? , fx: String , args:  [Any]? , fxRet : FxRet?  ) {
    self.className = className
    self.fx = fx
    self.fxRet = fxRet
    if let args = args {
      self.fxArgs = []
      for arg in args {
        if let arg = arg as? String {
          fxArgs!.append(AnyCodable(arg))
        } else if let arg = arg as? Int {
          fxArgs!.append(AnyCodable(arg))
        } else if let arg = arg as? Double {
          fxArgs!.append(AnyCodable(arg))
        } else if let arg = arg as? Bool {
          fxArgs!.append(AnyCodable(arg))
        } else if let arg = arg as? Dictionary<String, AnyCodable> {
          fxArgs!.append(AnyCodable(arg))
        } else if let arg = arg  as? AnyCodable {
          fxArgs!.append(arg)
        } else {
          print("invalid arg: \(arg)")
          assert(false) // Unsuported Codable type
        }
      }
    }
  }
  
  init (from decoder: Decoder) throws {
    let container =  try decoder.container (keyedBy: ArgsKeys.self)
    className = try container.decode(String.self, forKey: .className)
    fx = try container.decode(String.self, forKey: .fx)
    fxArgs = try container.decodeIfPresent( [AnyCodable].self, forKey: .fxArgs)
    fxRet = try container.decodeIfPresent(FxRet.self, forKey: .fxRet)
  }
  
  func encode (to encoder: Encoder) throws  {
    var container = encoder.container (keyedBy: ArgsKeys.self)
    try container.encode (className, forKey: .className)
    try container.encode (fx, forKey: .fx)
    if ( fxArgs != nil ) {
      try container.encode(fxArgs, forKey: .fxArgs)
    }
    if ( fxRet != nil) {
      try container.encode(fxRet, forKey: .fxRet)
    }
  }
}


