//
//  ABCWebViewPortability.swift
//  ABCStudio
//
//  Created by jymengant on 8/25/19 week 34.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation

protocol ABCWebViewPortabilityAdapter  {
  
  //
  // Implement dedicated UI callBack to handle IOS/OSX idiosynchrasies
  //
  func handleJSCallbackReturn( content : EntryPointArgs )
  
  func handleJSCallbackError( title:  String , message: String )
  
}
