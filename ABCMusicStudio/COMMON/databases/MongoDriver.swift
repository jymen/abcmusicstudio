//
//  MongoDriver.swift
//  ABCMusicStudio
//
//  Created by jymengant on 3/22/20 week 12.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import ABCMongoC.Mongo
import PMJSON

class MongoDriver : DbProtocol {
  
  private static let mongo = AbcMongo()
  private var name : String
  static var hasMongocInited : Bool = false

  var connected : Bool = false
  var dbError : String? {
    get {
      return Self.mongo.errMessage
    }
  }
  
  init(name : String) {
    self.name = name
    if Self.hasMongocInited {
      return
    }
    Self.mongo.initMongo()
  }
  
  //
  // call when ending program
  //
  func terminate() {
    Self.mongo.terminateMongo() 
  }
  
  func oidGenerate() -> Int64 {
    return Self.mongo.oidGenerate()
  }
  
  //
  // server or system connections
  // to be use for checking environment availability
  //
  func connect( to: String ) -> Bool {
    connected =  Self.mongo.connect(to)
    return connected
  }
  
  func createDatabase() throws {
    let created = Self.mongo.createDataBase(name)
    if (!created) {
      throw ABCFailure(message: Self.mongo.errMessage, kind: .DatabaseFailure )
    }
  }
  
  func dropDatabase() throws {
    let dropped = Self.mongo.dropDataBase(name)
    if (!dropped) {
      throw ABCFailure(message: Self.mongo.errMessage, kind: .DatabaseFailure )
    }
  }
  
  func begin( collection: String  ) throws  {
    if ( Self.mongo.getCollection(name,collection) ) {
      return
    }
     throw ABCFailure(message: Self.mongo.errMessage, kind: .DatabaseFailure )
  }
  
  func end( db: String , collection: String ) throws {
    if ( Self.mongo.releaseCollection(db,collection) ) {
      return
    }
     throw ABCFailure(message: Self.mongo.errMessage, kind: .DatabaseFailure )
  }
  
  func insert( db: String , collection: String , jsonStr : String ) throws -> String {
    let inserted = Self.mongo.insert(db,collection,jsonStr,nil)
    if let inserted = inserted {
      return inserted
    } else {
      throw ABCFailure(message: Self.mongo.errMessage, kind: .DatabaseFailure )
    }
  }
  
  func select( db: String , collection: String , selection : String? = nil ) throws -> Int {
    let cursor = Self.mongo.select(db, collection, selection)
    if ( cursor == -1 ) {
      throw ABCFailure(message: Self.mongo.errMessage, kind: .DatabaseFailure )
    } else {
      return cursor
    }
  }
  
  func fetch( cursor: Int  ) throws -> String? {
    let strJson = Self.mongo.fetch(cursor)
    if let error = Self.mongo.errMessage {
      throw ABCFailure(message: error, kind: .DatabaseFailure )
    }
    return strJson
  }
    
  func close( cursor : Int) throws {
    let closed = Self.mongo.closeCursor(cursor)
    if ( closed ) {
      return
    }
    throw ABCFailure(message: Self.mongo.errMessage, kind: .DatabaseFailure )
  }
  
  func isConnected() -> Bool {
    return connected
  }
  
  func openDatabase() throws {
    if( !connect(to: name) ) {
      throw ABCFailure(message: "failed to connect to \(name) mongo db server", kind: .DatabaseFailure )
    }
  }
  
  func closeDatabase() {
  }
  
  func checkCatalog() throws -> Bool  {
    return true
  }
  
  func add( candidate : ABCDbCandidate , parent: ABCOid?) throws -> String {
    return "{}"
  }
  
  func delete( candidate : ABCDbCandidate ) throws {
    
  }
  
  func update( candidate : ABCDbCandidate , parent: ABCOid?) throws {
    
  }
  
  func load( query : ABCQuery ) throws -> String {
    return "{}"
  }
  
  func existsDatabase() -> Bool {
    return true
  }

  func deleteDatabase() throws {
    try self.dropDatabase()
  }

  
}
