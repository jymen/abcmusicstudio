//
//  RealmDriver.swift
//  ABCMusicStudio
//
//  Created by jymengant on 4/18/20 week 16.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import RealmSwift

/**
 Driver for RealmIo
 */
class RealmDriver : DbProtocol {
  
  private var realm : Realm?
  private var name : String
  private var dbUrl :  URL {
    get {
      let dbUrl = preferences.databasePath
      return dbUrl!.appendingPathComponent("\(name).realm.abcmusicstudio")
    }
  }
  
  init( name: String ) {
    self.name = name
  }
  
  func createDatabase() throws {
    var config = Realm.Configuration()
    // Use the default directory, but replace the filename with the username
    config.fileURL = dbUrl // Open the Realm with the configuration
    realm = try! Realm(configuration: config)
  }

  func deleteDatabase() throws {
    let fu = FileUtils()
    try fu.deleteDatabase(url: dbUrl)
  }
  
  func openDatabase() throws {
    try createDatabase()
  }
  
  func existsDatabase() -> Bool {
    let fu = FileUtils()
    return fu.existDatabase(url: dbUrl)
  }

  
  func closeDatabase() {
  }
  
  func terminate() {
  }
  
  func checkCatalog() throws -> Bool {
    return true 
  }

  func add( candidate : ABCDbCandidate , parent: ABCOid?) throws -> String {
    return "{}"
  }
  
  func delete( candidate : ABCDbCandidate ) throws {
  }
  
  func update( candidate : ABCDbCandidate , parent: ABCOid?) throws {
  }
  
  func load( query : ABCQuery ) throws -> String {
    return "{}"
  }

  
}
