//
//  DbProtocol.swift
//  ABCMusicStudio
//
//  Created by jymengant on 3/22/20 week 12.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation

enum SqlType {
  case Text
  case Text64
  case Integer32
  case UInteger64
  case Integer64
  case Real
  case Blob
}

/**
 Database Access common protocol
 */
protocol  DbProtocol {
  
  func createDatabase() throws
  func deleteDatabase() throws
  
  func openDatabase() throws
  func existsDatabase() -> Bool
  
  func closeDatabase() 
  // do any cleanup requested
  func terminate()
  
  func checkCatalog() throws -> Bool ;

  func add( candidate : ABCDbCandidate , parent: ABCOid?) throws -> String 
  func delete( candidate : ABCDbCandidate ) throws 
  func update( candidate : ABCDbCandidate, parent: ABCOid? ) throws
  func load( query : ABCQuery ) throws -> String 

}

protocol SqlStatement {
  
  func prepare( statement : String ) throws ;

  func exec() throws
  func close() throws
  func reset() throws
  
  func bind( position : Int32 , type : SqlType , data : Any? ) throws
  func fetch() throws -> Int ;
  func getCol( number : Int , type : SqlType ) throws -> Any?
}

//
// Relational db protocol
//
protocol SQLDbProtocol : DbProtocol {
  
  func createDataModel() throws
  func dropDataModel() throws
  
  
  func newStatement() throws -> SqlStatement
  
  func updateForeignKey( table: String ,
                         col: String ,
                         pKey: ABCOid ,
                         fKey: ABCOid) throws


  func delete( table : String , key: String , id: ABCOid ) throws 

}
