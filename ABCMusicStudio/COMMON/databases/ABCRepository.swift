//
//  IosRepository.swift
//  ABCMusicStudio-IOS
//
//  Created by jymengant on 5/29/20 week 22.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation

import CocoaLumberjack

enum ABCDbType : String , Codable {
  case SqlLite
  case Realm 
}

enum DatabaseAction : String {
  case CreateDatabase
  case DeleteDatabase
  case ListDatabase
  case StorageOperation
}

enum DbOperation : Int , Codable {
  case Add = 0
  case Update = 1
  case Delete = 2
  case Load = 3
}

struct ABCQuery : Codable {
  let isFolder : Bool
  let maxItems : Int?
  let tags : String?
  let oid : ABCOid?
  // var result: Folder?
  
  init( isFolder : Bool , maxItems :Int? = nil , tags : String? = nil , oid : ABCOid? = nil  ) {
    self.isFolder = isFolder
    self.maxItems = maxItems
    self.tags = tags
    self.oid = oid
  }
  
  func select () -> Folder? {
    
    return nil
  }
}

struct ABCDbCandidate : Codable {
  let isFolder : Bool
  let oid : ABCOid?
  // let infos : Leaf?
  let file : File?
  let folder : Folder?
}

class StorageOperation : Codable {
  let op : DbOperation
  let parent : ABCOid?
  var candidate : ABCDbCandidate?  // File or Folder
  let query : ABCQuery?
  var completion : String?
  var driver : DbProtocol!
  
  enum CodingKeys: String, CodingKey {
    case op
    case parent
    case candidate
    case query
    case completion
  }
  
  init( op: DbOperation , parent: ABCOid? , candidate: ABCDbCandidate? = nil , query: ABCQuery? = nil) {
    self.op = op
    self.parent = parent
    self.candidate = candidate
    self.query = query
  }
  
  required init( from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    op = try values.decode(DbOperation.self, forKey: .op)
    query = try values.decodeIfPresent(ABCQuery.self, forKey: .query)
    parent = try values.decodeIfPresent(ABCOid.self, forKey: .parent)
    // candidates are file or folders
    candidate = try values.decodeIfPresent(ABCDbCandidate.self, forKey: .candidate)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(op, forKey: .op)
    try container.encode(parent, forKey: .parent)
    try container.encode(query, forKey: .query)
    try container.encode(completion, forKey: .completion)
    try container.encode(candidate, forKey: .candidate)
  }

  
  func proceed( driver: DbProtocol) throws -> String {
    self.driver = driver
    switch self.op {
    case .Add :
      // return added json OID
      if let candidate = candidate {
        return try driver.add(candidate: candidate, parent: self.parent)
      }
    case .Delete:
      if let candidate = candidate {
        try driver.delete(candidate: candidate)
      }
      break
    case .Update:
      if let candidate = candidate {
        try driver.update(candidate: candidate, parent: self.parent)
      }
      break
    case .Load:
      // return loaded object
      if let query = self.query {
        if candidate != nil {
          return try driver.load(query: query)
        }
      } else {
        throw ABCFailure(message: "query argument missing", kind: .DatabaseFailure)
      }
      break
    }
    return "{}" // defaulted  empty json result return
  }
}

struct DBCore : Codable {
  let name : String
  let type : ABCDbType
}

struct ABCDb : Codable {
  let core : DBCore
  var operation : StorageOperation?
  
  
  init ( name: String , type : ABCDbType , operation : StorageOperation? = nil ) {
    self.core = DBCore(name:name,type:type)
    self.operation = operation
  }
  
  var fSuffix : String {
    get {
      switch core.type {
      case .SqlLite :
        return ".sqlite3.abcmusicstudio"
      case .Realm :
        return ".realm.abcmusicstudio"
      }
    }
  }
  
  var dbFName : String {
    get {
      return core.name + fSuffix
    }
  }
}


class ABCRepository  {
  
  let db : ABCDb
  var d : DbProtocol?
  
  init( db : ABCDb ) {
    self.db = db
  }
  
  var driver : DbProtocol? {
    get {
      if ( d != nil ) {
        return d
      }
      switch db.core.type {
      case .SqlLite :
        d = SqliteDriver(dbPath: preferences.databasePath! , name: db.core.name)
        return d
      default :
        // NOT IMPLEMENTED
        assert(false)
      }
      return nil
    }
  }
  
  
  var mounted : Bool {
    get {
      if let _ = driver {
        return true
      }
      return false
    }
  }
  
  /**
   Load data
   */
  func load() throws -> String {
    switch db.core.type {
    case .SqlLite :
      break
    default:
      throw ABCFailure(message: "NOT IMPLEMENTED YET", kind: .DatabaseFailure )
    }
    return "{}"
  }
  
  func storageOperation() throws -> String {
    // get requested operation
    if let op = db.operation {
      if let driver = driver {
        return try op.proceed(driver: driver)
      } else {
        throw ABCFailure(message: "DB operation : DB not mounted", kind: .DatabaseFailure )
      }
    } else {
      throw ABCFailure(message: "DB operation is missing", kind: .DatabaseFailure )
    }
  }
  
  func importOperation() throws -> String {
    return "{}"
  }
  
  func create() throws {
    if ( !driver!.existsDatabase() ) {
     try driver?.createDatabase()
    }
  }
  
  func delete() throws {
    try driver?.deleteDatabase()
  }
  
  
  func mount() throws {
    if ( !driver!.existsDatabase() ) {
      try create()
    } else {
      try driver!.openDatabase()
      _ = try driver!.checkCatalog()
    }
  }
  
  func unmount() {
    if ( mounted ) {
      driver!.closeDatabase()
      driver!.terminate()
    }
  }
  
  static func list() throws -> [ABCDb]? {
    let fu = FileUtils()
    let files = try fu.listDatabases()
    
    if let files = files {
      if files.count == 0 { // empty db case
        return nil
      }
      var returned =  [ABCDb]()
      for file in files {
        let fileNameArr = file.components(separatedBy:".")
        let fName = fileNameArr[0]
        let fExtension = fileNameArr[1]
        var type : ABCDbType = .SqlLite
        if fExtension.contains("realm") {
          type = .Realm
        }
        returned.append(ABCDb(name: fName, type: type))
      }
      return returned
    }
    return nil
  }
}

class ABCDbJsonRequest {
  
  private func listRepo() throws -> String? {
    let repoList = try ABCRepository.list()
    if let repoList = repoList {
      return try  AbcJson.encode(source: repoList,encoding:.utf8)!
    } else {
      return nil
    }
  }
  
  func doRequest( fx : DatabaseAction ,
                       // Default value just used when no db arguments are
    // requested
    jsonRepo : String =  """
                            {
                            "core" :
                              {
                                "name" : "defaultRepository" ,
                                "type" : "SqlLite"
                              }
                            }
                            """ ) throws -> String{
    let decoder  = JSONDecoder()
    var jsonDest : String  = jsonRepo
    let db = try decoder.decode(ABCDb.self, from: jsonRepo.data(using: .utf8)!)
    let repository = ABCRepository(db: db)
    switch fx {
    case .StorageOperation :
      try repository.mount()
      jsonDest = try repository.storageOperation()
      repository.unmount()
    case .CreateDatabase :
      try repository.create()
    case .DeleteDatabase :
      try repository.delete()

    case .ListDatabase :
      let repoList = try listRepo()
      if let repoList = repoList {
        jsonDest = repoList
      } else {
        //
        // create a default sqllite instance when repolist is empty
        //
        try repository.create()
        jsonDest = try listRepo()!
      }
    }
    return jsonDest
  }
  
}

class AbcDbRequestFactory {
  
  private var db : ABCDb
  
  var parent : ABCOid? = nil
  var file : File? = nil
  var folder : Folder? = nil
  var query : ABCQuery? = nil
  
  var isFolder : Bool {
    get {
      if ( folder == nil ) {
        return false
      }
      return true
    }
  }
  
  var oid : ABCOid? {
    get {
      if let file = file {
        return file.oid
      }
      if let folder = folder {
        return folder.oid
      }
      return nil
    }
  }
  
  init( dbName : String , dbType : ABCDbType ) {
    self.db = ABCDb(name: dbName, type: dbType)
  }
  
  func build( operation: DbOperation ) {
    let candidate = ABCDbCandidate(isFolder: isFolder, oid: oid, file: file, folder: folder)
    let oper = StorageOperation(op: operation, parent: parent,query: self.query)
    oper.candidate = candidate
    db.operation = oper
  }
  
  func emit() throws -> String {
    return try AbcJson.encode(source: db, encoding: .utf8)!
  }
  
  func exec() throws -> String {
    let jsonRequest = try self.emit()
    let request = ABCDbJsonRequest()
    let result = try request.doRequest(fx: .StorageOperation, jsonRepo: jsonRequest)
    return result
  }
  
  func deleteDb() throws {
    let jsonRequest = try self.emit()
    let request = ABCDbJsonRequest()
    _ = try request.doRequest(fx: .DeleteDatabase , jsonRepo: jsonRequest)
  }
  
  func createDb() throws {
    let jsonRequest = try self.emit()
    let request = ABCDbJsonRequest()
    _ = try request.doRequest(fx: .CreateDatabase , jsonRepo: jsonRequest)
  }
  
  //
  // simple add wrapper for tree importing 
  //
  func dbImport(root : Folder) throws  {
    self.folder = root
    self.build(operation: .Add)
    _ = try self.exec()
  }
}

