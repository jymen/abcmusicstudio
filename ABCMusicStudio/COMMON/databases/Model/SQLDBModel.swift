//
//  ABCTags.swift
//  ABCMusicStudio
//
//  Created by jymengant on 4/2/20 week 14.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack
import MiniFlake

extension Date {
  var millisecondsSince1970:Int64 {
    return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
  }
  
  init(milliseconds:Int64) {
    self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
  }
}



enum SQLDbType {
  case Sqlite
  case MySql // Not implemented Yet
  case PostGreSql // not implemented Yet
}

protocol DatabaseCollection {
  func store( dbDriver : SQLDbProtocol ) throws
  func load( dbDriver : SQLDbProtocol , id: ABCOid , recursive: Bool ) throws
  func update( dbDriver : SQLDbProtocol ) throws
  func delete( dbDriver : SQLDbProtocol ) throws
  
  // provide DDL SQL
  func createSqlDDl( dbType : SQLDbType) -> String
  func dropSqlDDl( dbType : SQLDbType) -> String
}

//
// delete func is optional (due to cascading rules)
//
extension DatabaseCollection {
  func delete( dbDriver : SQLDbProtocol ) throws {}
}


struct ABCOid : Codable {
  var oid : Int64
  
  init( oid: Int64 ) {
    self.oid = oid
  }
  
  init() {
    self.oid = Thread.current.nextFlakeID()
    if ( self.oid == 0 ) {
      self.oid = Thread.current.nextFlakeID()
      assert(self.oid != 0)
    }
    DDLogDebug("oid generated = \(self.toString())")
  }
  
  func toString() -> String {
    return String(format:"%016X", oid)
  }
  
}


class ABCSqlFolder : DatabaseCollection {
  
  var oid : ABCOid?
  var leaf : ABCOid?
  var folder : Folder?
  var parent : ABCOid?
  
  
  init( folder : Folder? = nil ,
        parent : ABCOid? = nil ) {
    self.folder = folder
    self.parent = parent
  }
  
  // provide DDL SQL
  func createSqlDDl( dbType : SQLDbType) -> String {
    return """
    CREATE TABLE ABCFolder
    (
    oid INTEGER PRIMARY KEY ,
    leaf INTEGER NULL ,
    parent INTEGER ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (parent)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE
    ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (leaf)
    REFERENCES ABCLeaf(oid)
    )
    """
  }
  
  func dropSqlDDl (dbType : SQLDbType) -> String {
    return """
    DROP TABLE IF EXISTS ABCFolder
    """
  }
  
  private func rootExists(dbDriver : SQLDbProtocol ) throws -> Bool {
    let stmtStr = "SELECT * FROM ABCFolder WHERE oid=0"
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    var exists : Bool = false
    if ( try stmt.fetch() > 0 ) {
      exists = true
    }
    try stmt.close()
    return exists
  }
  
  
  func store( dbDriver : SQLDbProtocol ) throws {
    guard let folder = self.folder else {
      throw ABCFailure(message:"Nil folder instance" ,  kind: .DatabaseFailure)
    }
    let stmtStr = """
      INSERT INTO ABCFolder
      (
        oid, leaf ,  parent
      )
      VALUES (?, ?  ,?)
     """
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    self.oid = ABCOid()
    if self.parent == nil {
      // Root special key to avoid level indexing
      if (try rootExists(dbDriver: dbDriver) ) {
        self.parent = ABCOid(oid: 0)
      } else {
        self.oid!.oid = 0
      }
    }
    folder.oid = self.oid
    try stmt.bind(position: 1, type: .Integer64, data: folder.oid!.oid )
    // set leaf foreign key to nil
    try stmt.bind(position: 2, type: .Integer64, data: nil)
    try stmt.bind(position: 3, type: .Integer64, data: parent?.oid)
    // insert current
    try stmt.exec()
    try stmt.close()
    
    // store leaf first
    let leaf = folder.getLeaf()
    let dbLeaf = ABCSqlLeaf(leaf: leaf , folderOwner: self )
    try dbLeaf.store(dbDriver: dbDriver) // store leaf
    // fkey update
    try dbDriver.updateForeignKey(table: "ABCFolder",
                                  col: "leaf",
                                  pKey: folder.oid!,
                                  fKey: leaf.oid!)
    
    // handle chidren
    if let childFolders = folder.folders {
      for curFolder in childFolders {
        let curDbFolder = ABCSqlFolder(folder: curFolder, parent: folder.oid)
        try curDbFolder.store(dbDriver: dbDriver)
      }
    }
    if let childFiles = folder.files {
      for curFile in childFiles {
        let curDbFile = ABCSqlFile(file: curFile, parent: folder.oid)
        try curDbFile.store(dbDriver: dbDriver)
      }
    }
  }
  
  func load( dbDriver : SQLDbProtocol , id: ABCOid , recursive : Bool = false ) throws {
    let stmtStr = "SELECT * FROM ABCFolder WHERE oid=\(id.oid)"
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    if ( try stmt.fetch() > 0 ) {
      let leafOid = try stmt.getCol(number: 1, type: .Integer64) as? Int64
      self.leaf = ABCOid(oid:leafOid!)
      let dbLeaf = ABCSqlLeaf()
      try dbLeaf.load(dbDriver: dbDriver, id: self.leaf!)
      self.folder = Folder(infos: dbLeaf.leaf!)
      self.folder?.oid = id
      // load children folders
      var stmtStr = "SELECT * FROM ABCFolder WHERE parent=\(id.oid)"
      var stmt1 = try dbDriver.newStatement()
      try stmt1.prepare(statement: stmtStr)
      var childrenFolders : [Folder]?
      while ( try stmt1.fetch() > 0 ) {
        let fOid = try stmt1.getCol(number: 0, type: .Integer64) as? Int64
        let dbId = ABCOid(oid: fOid!)
        if childrenFolders == nil {
          childrenFolders = [Folder]()
        }
        let f = ABCSqlFolder()
        try f.load(dbDriver: dbDriver, id: dbId)
        childrenFolders?.append(f.folder!)
      }
      self.folder!.folders = childrenFolders
      try stmt1.close()
      // load children files
      stmtStr = "SELECT * FROM ABCFile WHERE parent=\(id.oid)"
      stmt1 = try dbDriver.newStatement()
      try stmt1.prepare(statement: stmtStr)
      var childrenFiles : [File]?
      while ( try stmt1.fetch() > 0 ) {
        let fOid = try stmt1.getCol(number: 0, type: .Integer64) as? Int64
        let dbOid = ABCOid(oid: fOid!)
        if childrenFiles == nil {
          childrenFiles = [File]()
        }
        let f = ABCSqlFile()
        try f.load(dbDriver: dbDriver, id: dbOid)
        childrenFiles?.append(f.file!)
      }
      self.folder!.files = childrenFiles
      try stmt1.close()
    }
    try stmt.close()
  }
  
  //
  //  updating folder infos
  //
  func update( dbDriver : SQLDbProtocol ) throws {
    guard let folder = self.folder else {
      throw ABCFailure(message:"Nil folder instance" ,  kind: .DatabaseFailure)
    }
    let dbLeaf = ABCSqlLeaf()
    dbLeaf.leaf = folder.infos
    try dbLeaf.update(dbDriver: dbDriver)
  }
  
  func delete(dbDriver : SQLDbProtocol ) throws {
    var deloid : ABCOid? = nil
    if let oid = self.oid {
      deloid = oid
    } else {
      if let folder = self.folder {
        deloid = folder.oid
      }
    }
    guard let oid = deloid else {
      throw ABCFailure(message:"Nil folder instance" ,  kind: .DatabaseFailure)
    }
    if ( oid.oid == 0 ) {
      throw ABCFailure(message: "Root folder deletion is forbidden", kind: .DatabaseFailure )
    }
    try dbDriver.delete(table: "ABCFolder", key:"oid" , id: oid)
  }
  
}


class ABCSqlFile : DatabaseCollection {
  var oid : ABCOid?
  var leaf : ABCOid?
  var file : File?
  let parent : ABCOid?
  
  init( file: File? = nil , parent : ABCOid? = nil) {
    self.file = file
    self.parent = parent
  }
  
  // provide DDL SQL
  func createSqlDDl( dbType : SQLDbType) -> String {
    return """
    CREATE TABLE ABCFile
    (
    oid INTEGER PRIMARY KEY ,
    leaf INTEGER ,
    parent INTEGER ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (parent)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (leaf)
    REFERENCES ABCLeaf(oid)
    )
    """
  }
  
  func dropSqlDDl (dbType : SQLDbType) -> String {
    return """
    DROP TABLE IF EXISTS ABCFile
    """
  }
  
  private func checkABCData( infos : Leaf ) {
    if infos.contents != nil  {
      return // Data content is filled
    }
    infos.makeExportable(exportType: .init(extra: true, sound: true, icon: true))
  }
  
  func store(dbDriver : SQLDbProtocol ) throws {
    guard let file = self.file else {
      throw ABCFailure(message:"Nil file instance" ,  kind: .DatabaseFailure)
    }
    checkABCData(infos: file.infos) // import local file storage if needed

    let stmtStr = """
      INSERT INTO ABCFile
      (
        oid, leaf , parent
      )
      VALUES (?, ? , ?);
     """
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    self.oid = ABCOid()
    file.oid = self.oid
    try stmt.bind(position: 1, type: .Integer64, data: file.oid!.oid)
    // insert current
    try stmt.exec()
    try stmt.close()
    
    let leaf = file.getLeaf()
    let dbLeaf = ABCSqlLeaf(leaf: leaf, fileOwner: self, folderOwner: nil)
    try dbLeaf.store(dbDriver: dbDriver)
    
    // fkeys update
    try dbDriver.updateForeignKey(table: "ABCFile",
                                  col: "leaf",
                                  pKey: file.oid!,
                                  fKey: leaf.oid!)
    
    try dbDriver.updateForeignKey(table: "ABCFile",
                                  col: "parent",
                                  pKey: file.oid!,
                                  fKey: parent!)
  }
  
  func load(dbDriver : SQLDbProtocol ,id : ABCOid , recursive: Bool = false) throws {
    let stmtStr = "SELECT * FROM ABCfile WHERE oid=\(id.oid)"
    let stmt = try dbDriver.newStatement()
    
    try stmt.prepare(statement: stmtStr)
    if ( try stmt.fetch() > 0 ) {
      let leafOid = try stmt.getCol(number: 1, type: .Integer64) as? Int64
      let leafDbOid = ABCOid(oid: leafOid!)
      self.leaf = leafDbOid
      let dbLeaf = ABCSqlLeaf()
      try dbLeaf.load(dbDriver: dbDriver, id: leafDbOid)
      self.file = File(infos: dbLeaf.leaf!)
      self.file?.oid = id
    }
    try stmt.close()
  }
  
  //
  //  updating file infos or file contents
  //
  func update( dbDriver : SQLDbProtocol) throws {
    guard let file = self.file else {
      throw ABCFailure(message:"Nil file instance" ,  kind: .DatabaseFailure)
    }

    let dbLeaf = ABCSqlLeaf()
    dbLeaf.leaf = file.getLeaf()
    try dbLeaf.update(dbDriver: dbDriver)
  }
  
  func delete( dbDriver : SQLDbProtocol) throws {
    try dbDriver.delete( table: "ABCFile", key : "oid" , id: self.oid!)
  }
  
}

class ABCSqlDataContent : DatabaseCollection {
  var oid : ABCOid?
  var owner : ABCOid?
  var content: DataContent?
  var parent: ABCSqlLeaf?
  
  init( owner : ABCOid  , content: DataContent , parent: ABCSqlLeaf) {
    self.owner = owner
    self.content = content
    self.parent = parent
  }
  
  init() {}
  
  func createSqlDDl(dbType: SQLDbType) -> String {
    return """
    CREATE TABLE ABCDataContent
    (
    oid INTEGER PRIMARY KEY ,
    owner INTEGER ,
    abc BLOB ,
    image  BLOB ,
    icon  BLOB ,
    sound BLOB ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (owner)
    REFERENCES ABCLeaf(oid)
    ON DELETE CASCADE
    )
    """
  }
  
  func dropSqlDDl(dbType: SQLDbType) -> String {
    return """
    DROP TABLE IF EXISTS ABCDataContent
    """
  }
  
  func fromBase64(str : String? ) -> Data? {
    if let str = str {
      let encoder = Base64Encoder()
      return encoder.decode(str64: str)
    }
    return nil
  }
  
  private func bindValues( statement : SqlStatement , content : DataContent) throws {
    // parent Fkey
    try statement.bind(position: 2, type: .Integer64, data: parent?.oid?.oid)
    try statement.bind(position: 3, type: .Text64, data: content.abc)
    try statement.bind(position: 4, type: .Text64, data: content.imageB64)
    try statement.bind(position: 5, type: .Text64, data: content.iconB64)
    try statement.bind(position: 6, type: .Text64, data: content.soundB64)
  }
  
  func store(dbDriver: SQLDbProtocol) throws {
    guard let content = self.content else {
      throw ABCFailure(message:"Nil content instance" ,  kind: .DatabaseFailure)
    }
    
    let stmtStr = """
      INSERT INTO ABCDataContent
      (
        oid, owner , abc, image , icon , sound
      )
      VALUES (?, ? , ? ,? , ? , ?);
     """
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    content.oid = ABCOid()
    try stmt.bind(position: 1, type: .Integer64, data: content.oid!.oid)
    try self.bindValues(statement: stmt, content: content)
    // insert current
    try stmt.exec()
    try stmt.close()

    // Fkey update
    try dbDriver.updateForeignKey(table: "ABCLeaf", col: "contents",
                                  pKey: parent!.oid!, fKey: content.oid!)
  }
  
  func load(dbDriver: SQLDbProtocol, id: ABCOid , recursive: Bool = false ) throws {
    let stmtStr = "SELECT * FROM ABCDataContent WHERE oid=\(id.oid)"
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    if ( try stmt.fetch() > 0 ) {
      let datacontent = DataContent()
      datacontent.oid = id
      datacontent.abc = try stmt.getCol(number: 2, type: .Text64) as? String
      datacontent.iconB64 = try stmt.getCol(number: 3, type: .Text64) as? String
      datacontent.imageB64 = try stmt.getCol(number: 4, type: .Text64) as? String
      datacontent.soundB64 = try stmt.getCol(number: 5, type: .Text64) as? String
    }
    try stmt.close()
  }
  
  func update(dbDriver: SQLDbProtocol) throws {
    let stmtStr = """
      UPDATE ABCDataContent
      SET VALUE
        owner = ? ,
        abc = ?,
        image = ?,
        icon = ?,
        sound = ?
      WHERE oid = \(self.content!.oid!.oid)
     """
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    try self.bindValues(statement: stmt, content: self.content!)
    try stmt.exec()
    try stmt.close()
  }
  
  func delete( dbDriver : SQLDbProtocol) throws {
    try dbDriver.delete( table: "ABCDataContent", key : "oid" , id: self.oid!)
  }
  

}

class ABCSqlLeaf : DatabaseCollection {
  var oid : ABCOid?
  var container : ABCOid?
  var leaf : Leaf?
  var fileOwner : ABCSqlFile?
  var folderOwner : ABCSqlFolder?
  
  init(){
  }
  
  // init from storage leaf
  init( leaf: Leaf? = nil , fileOwner : ABCSqlFile? = nil , folderOwner : ABCSqlFolder? = nil) {
    self.leaf = leaf
    self.fileOwner = fileOwner
    self.folderOwner = folderOwner
  }
  
  // provide DDL SQL
  func createSqlDDl( dbType : SQLDbType) -> String {
    return """
    CREATE TABLE ABCLeaf
    (
    oid INTEGER PRIMARY KEY ,
    fileOwner INTEGER ,
    folderOwner INTEGER ,
    title  TEXT ,
    creation  INTEGER ,
    storageURL TEXT ,
    iconURL TEXT ,
    imageURL TEXT ,
    soundURL TEXT ,
    comments TEXT ,
    contents INTEGER ,
    encoding INTEGER ,
    type TEXT ,
    CONSTRAINT fk_ABCFile
    FOREIGN KEY (fileOwner)
    REFERENCES ABCFile(oid)
    ON DELETE CASCADE ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (folderOwner)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE ,
    CONSTRAINT fk_ABCContent
    FOREIGN KEY (contents)
    REFERENCES ABCDataContent(oid)
    )
    
    """
  }
  
  func dropSqlDDl (dbType : SQLDbType) -> String {
    return """
    DROP TABLE IF EXISTS ABCLeaf
    """
  }
  
  private func bindValues( statement : SqlStatement ,
                           leaf : Leaf) throws {
    let fileOwnerOid = self.fileOwner?.oid?.oid
    try statement.bind(position: 2, type: .Integer64, data: fileOwnerOid)
    let folderOwner = self.folderOwner?.oid?.oid
    try statement.bind(position: 3, type: .Integer64, data: folderOwner)
    try statement.bind(position: 4, type: .Text, data: leaf.title)
    var created : Int64
    if let creation = leaf.creation {
      created = creation.millisecondsSince1970
    } else {
      created = Date().millisecondsSince1970
    }
    try statement.bind(position: 5, type: .Integer64, data: created)
    let storageUrl = leaf.storageURL
    try statement.bind(position: 6, type: .Text, data: storageUrl?.absoluteString)
    let iconUrl = leaf.iconURL
    try statement.bind(position: 7, type: .Text, data: iconUrl?.absoluteString)
    let imageUrl = leaf.imageURL
    try statement.bind(position: 8, type: .Text, data: imageUrl?.absoluteString)
    let soundUrl = leaf.soundURL
    try statement.bind(position: 9, type: .Text, data: soundUrl?.absoluteString)
    let comments = leaf.comments
    try statement.bind(position: 10, type: .Text, data: comments)
    try statement.bind(position: 11, type: .Text, data: nil) // to be set later
    if ( leaf.encoding == nil ) {
      leaf.encoding = UInt(Unicode.encodingIndex(encoding: .utf8))
    }
    try statement.bind(position: 12, type: .Integer64, data: Int64(leaf.encoding!))
    try statement.bind(position: 13, type: .Text, data: leaf.type?.rawValue)
  }
  

  func store(dbDriver : SQLDbProtocol ) throws {
    guard let leaf = self.leaf else {
      throw ABCFailure(message:"Nil leaf instance" ,  kind: .DatabaseFailure)
    }
    
    let stmtStr = """
      INSERT INTO ABCLeaf
      (
        oid, fileOwner , folderOwner, title , creation , storageURL ,iconURL ,
        imageURL ,soundURL , comments ,contents , encoding ,type
      )
      VALUES (?, ? , ? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? );
     """
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    self.oid = ABCOid()
    leaf.oid = self.oid
    try stmt.bind(position: 1, type: .Integer64, data: leaf.oid!.oid)
    try self.bindValues(statement: stmt, leaf: leaf)
    // insert current
    try stmt.exec()
    try stmt.close()
    
    if let contents = leaf.contents {
      let dbContent = ABCSqlDataContent(owner : leaf.oid! , content: contents, parent: self )
      try dbContent.store(dbDriver: dbDriver)
    }
    
    // handle tags
    if let _ = leaf.tags {
      let dbTags = ABCSqlTags(leaf: self)
      try dbTags.store(dbDriver: dbDriver)
    }
    
    // foreign keys update
    if let folder = self.folderOwner {
      try dbDriver.updateForeignKey(table: "ABCfolder", col: "leaf",
                                    pKey: folder.oid!, fKey: leaf.oid!)
    }
    if let file = self.fileOwner {
      try dbDriver.updateForeignKey(table: "ABCfile", col: "leaf",
                                    pKey: file.oid!, fKey: leaf.oid!)
    }
  }
  
  private func buildUrl( from: String? ) -> URL?{
    if let from = from {
      return URL(fileURLWithPath: from)
    }
    return nil
  }
  
  func load(dbDriver : SQLDbProtocol ,id: ABCOid , recursive: Bool = false) throws {
    let stmtStr = "SELECT * FROM ABCLeaf WHERE oid=\(id.oid)"
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    if ( try stmt.fetch() > 0) {
      let title  = try stmt.getCol(number: 3, type: .Text) as? String
      var creationDate : Date?
      if let creation = try stmt.getCol(number: 4, type: .Integer64) as? Int64 {
        creationDate = Date(milliseconds: creation)
      }
      let storageUrl = try stmt.getCol(number: 5, type: .Text) as? String
      let iconUrl = try stmt.getCol(number: 6, type: .Text) as? String
      let imageUrl = try stmt.getCol(number: 7, type: .Text) as? String
      let soundUrl = try stmt.getCol(number: 8, type: .Text) as? String
      let comments = try stmt.getCol(number: 9, type: .Text) as? String
      let contentId = try stmt.getCol(number: 10, type: .Integer64) as? Int64
      let encoding = UInt(try stmt.getCol(number: 11, type: .Integer64) as! Int64)
      var abcType : ABCResType?
      if let type = try stmt.getCol(number: 12, type: .Text) as? String {
        abcType = ABCResType(rawValue: type)
      } 
      self.leaf = Leaf(title: title!,
                       creation: creationDate!,
                       id: id,
                       storageURL: buildUrl(from:storageUrl),
                       iconURL: buildUrl(from:iconUrl),
                       imageURL: buildUrl(from:imageUrl),
                       soundURL: buildUrl(from:soundUrl),
                       comments: comments,
                       encoding: String.Encoding(rawValue: encoding),
                       type: abcType)
      // get stored content next and populate it
      if let contentId = contentId {
        let contentOid = ABCOid(oid:contentId)
        let content = ABCSqlDataContent()
        try content.load(dbDriver: dbDriver , id : contentOid)
        self.leaf?.contents = content.content
      }
    }
  }
  
  func update(dbDriver : SQLDbProtocol) throws {
    guard let leaf = self.leaf else {
      throw ABCFailure(message:"Nil leaf instance" ,  kind: .DatabaseFailure)
    }
    let stmtStr = """
      UPDATE ABCLeaf
      SET
        oid = ?,
        fileOwner = ? ,
        folderOwner = ? ,
        title = ? ,
        creation = ?,
        storageURL = ? ,
        iconURL = ? ,
        imageURL = ? ,
        soundURL = ? ,
        comments = ?,
        contents = ?,
        encoding = ?,
        type = ?
      WHERE oid=\(leaf.oid!.oid)
      """
      let stmt = try dbDriver.newStatement()
      try stmt.prepare(statement: stmtStr)
      try stmt.bind(position: 1, type: .Integer64, data: leaf.oid!.oid)
      try self.bindValues(statement: stmt, leaf: leaf)
      // update current
      try stmt.exec()
      try stmt.close()
      if let contents = leaf.contents {
        let dbContent = ABCSqlDataContent(owner : leaf.oid! , content: contents, parent: self )
        try dbContent.update(dbDriver: dbDriver)
      }
      //  handle tags
      if let _ = leaf.tags {
        let dbTags = ABCSqlTags(leaf: self)
        try dbTags.update(dbDriver: dbDriver)
      }
  }
}

class ABCSqlTags : DatabaseCollection {
  
  var oid : ABCOid?   // tag ID
  var container : ABCSqlLeaf? // leaf container ID
  
  init() {}
  
  init( leaf : ABCSqlLeaf ) {
    self.container = leaf
  }
  
  // provide DDL SQL
  func createSqlDDl( dbType : SQLDbType) -> String {
    return """
    CREATE TABLE ABCTags
    (
    oid  INTEGER PRIMARY KEY ,
    container  INTEGER ,
    sequence  INTEGER ,
    value  TEXT ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (container)
    REFERENCES ABCLeaf(oid)
    ON DELETE CASCADE
    )
    """
  }
  
  func createIndex( dbType : SQLDbType ) -> String {
    return """
      CREATE INDEX TagValueIndex
      ON ABCTags ( value )
    """
  }
  
  func dropSqlDDl (dbType : SQLDbType) -> String {
    return """
    DROP TABLE IF EXISTS ABCTags
    """
  }
  
  private func parseTags( tagList: String? ) -> [String]? {
    if let tagList = tagList {
      if ( tagList.count > 0 ) {
        return tagList.components(separatedBy: ",")
      }
    }
    return nil
  }

  func store(dbDriver : SQLDbProtocol) throws {
    guard let leaf = self.container else {
      throw ABCFailure(message:"Nil leaf instance" ,  kind: .DatabaseFailure)
    }
    
    let tagsStr = leaf.leaf?.tags
    var tagPos = 0
    if let tags = parseTags(tagList: tagsStr) {
      let stmtStr = """
          INSERT INTO ABCTags
          (
            oid, container , value , sequence
          )
          VALUES (?, ? , ? ,? );
         """
      let stmt = try dbDriver.newStatement()
      try stmt.prepare(statement: stmtStr)
      for tag in tags {
        self.oid = ABCOid()
        try stmt.bind(position: 1, type: .UInteger64, data: self.oid!.oid)
        try stmt.bind(position: 2, type: .UInteger64, data: container!.oid?.oid)
        try stmt.bind(position: 3, type: .UInteger64, data: tag)
        try stmt.bind(position: 4, type: .UInteger64, data: tagPos)
        tagPos += 1
        // insert current
        try stmt.exec()
        try stmt.reset()
      }
      try stmt.close()
    }
  }
  
  //
  // query on given tag value
  //
  func query(  dbDriver : SQLDbProtocol , tag : String ) throws  -> [ABCTreeElement]? {
    let stmtStr = "SELECT * FROM ABCTags WHERE value=\"\(tag)\""
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    var returned : [ABCTreeElement]? = nil
    while ( try stmt.fetch() > 0 ) {
      if returned == nil {
        returned = [ABCTreeElement]()
      }
      let leafParent = try stmt.getCol(number: 2, type: .UInteger64) as? Int64
      let dbLeaf = ABCSqlLeaf()
      dbLeaf.oid = ABCOid(oid: leafParent!)
      try dbLeaf.load(dbDriver: dbDriver, id: dbLeaf.oid!)
      if let fileParent = dbLeaf.fileOwner {
        returned!.append(fileParent.file!)
      }
      if let folderParent = dbLeaf.folderOwner {
        returned!.append(folderParent.folder!)
      }
    }
    try stmt.close()
    return returned
  }
  
  func load( dbDriver : SQLDbProtocol,id: ABCOid , recursive: Bool = false) throws {
    let stmtStr = "SELECT * FROM ABCTags WHERE container=\(id.oid)"
    let stmt = try dbDriver.newStatement()
    try stmt.prepare(statement: stmtStr)
    var tagArray : [String] = [String]()
    while ( try stmt.fetch() > 0 ) {
      let sequence = Int(try stmt.getCol(number: 3, type: .Text) as! Int64)
      let tag = try stmt.getCol(number: 4, type: .Text) as? String
      tagArray[sequence] = tag!
    }
    self.container?.leaf?.tags = tagArray.joined(separator: ",")
  }
  
  func delete( dbDriver : SQLDbProtocol) throws {
    try dbDriver.delete( table: "ABCTags", key : "container" , id: self.container!.leaf!.oid!)
  }
  

  func update( dbDriver : SQLDbProtocol ) throws {
    try self.delete(dbDriver: dbDriver)
    try self.store(dbDriver: dbDriver)
  }
  
}

