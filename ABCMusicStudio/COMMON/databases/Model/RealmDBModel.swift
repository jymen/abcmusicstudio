//
//  RealmDBModel.swift
//  ABCMusicStudio
//
//  Created by jymengant on 4/18/20 week 16.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import RealmSwift

//
// Realm model starts
//
class ABCDbObject: Object {
  @objc dynamic var oid : Int64 = -1
  
  override static func primaryKey() -> String? {
      return "oid"
  }

}

// Folder model
class ABCDbFolder: ABCDbObject {
  @objc var leaf : ABCDbLeaf?
  @objc var treelevel : Int = 0
  @objc var parent : ABCDbFolder? = nil
}

// Folder model
class ABCDbFile: ABCDbObject {
  @objc var leaf : ABCDbLeaf?
  @objc var parent : ABCDbFolder? = nil
}


class ABCDbLeaf : ABCDbObject {
  @objc var title : String?
  var tags = List<String>()
  @objc var creation : Date?
  @objc var storageURL : String?
  @objc var imageURL : String?
  @objc var soundURL : String?
  @objc var comments : String?
  var contents : ABCDbContent?
  @objc var encoding : Int = 0
}

class ABCDbContent : ABCDbObject {
  @objc var owner : ABCDbLeaf?
  @objc var abc : String?
  @objc var image : Data?
  @objc var icon : Data?
  @objc var sound : Data?
}

//
// Realm model ends
//
