//
//  SqliteDriver.swift
//  ABCMusicStudio
//
//  Created by jymengant on 4/2/20 week 14.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import SQLite3
import CocoaLumberjack

class SqliteStatement : SqlStatement {
  
  let driver : SqliteDriver
  var curStatement: OpaquePointer?
  var lastChanges : Int = 0
  private var db : OpaquePointer? {
    get {
      return driver.db
    }
  }

  init( driver: SqliteDriver ) {
    self.driver = driver
  }
  
  private func reportError(msg : String , code : Int32) throws {
    throw ABCFailure(message: "ERROR(retcode:\(code))" + msg, kind: .DatabaseFailure )
  }

  
  func prepare( statement : String ) throws {
    let retCode = sqlite3_prepare_v2(db, statement, -1, &self.curStatement, nil)
    if ( retCode != SQLITE_OK ) {
      try reportError(msg :"prepare error \(statement)" , code: retCode)
    }
  }

  func close() throws {
    if let statement = curStatement {
      sqlite3_finalize(statement)
      curStatement = nil
    } else {
      try reportError(msg :"exec SQL No statement prepared  " , code: -1)
    }
  }
  
  func exec() throws  {
    if let statement = curStatement {
      let retCode = sqlite3_step(statement)
      lastChanges = Int(sqlite3_changes(db))
      if ( retCode != SQLITE_DONE ) {
        try reportError(msg :"exec SQL error " , code: retCode)
      }
    } else {
      try reportError(msg :"exec SQL No statement prepared  " , code: -1)
    }
  }

  func reset() throws {
    if let statement = curStatement {
      let retCode = sqlite3_reset(statement)
      if ( retCode != SQLITE_DONE ) {
        try reportError(msg :"reset SQL error " , code: retCode)
      }
    } else {
      try reportError(msg :"exec SQL No statement prepared  " , code: -1)
    }
  }
  

  private func nullBind( position : Int32 ) throws {
    if self.curStatement == nil  {
      try reportError(msg :"statement has not been prepared " , code: -1)
    }
    let retCode : Int32 = sqlite3_bind_null(curStatement, position)
    if (retCode != SQLITE_OK)  {
      try reportError(msg :"bind data failure at \(position) position " , code: retCode)
    }
  }
  
  //
  // data value binder
  //
  func bind(position: Int32, type: SqlType, data: Any?) throws {
    let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
    if self.curStatement == nil  {
      try reportError(msg :"statement has not been prepared " , code: -1)
    }
    var retCode : Int32
    if ( data == nil ) {
      try nullBind(position: position)
    } else {
      switch type {
      case .Text :
        let strData : NSString = data as! NSString
        retCode = sqlite3_bind_text(curStatement, position , strData.utf8String, -1, nil)
        break
      case .Text64 :
        let strData : NSString = data as! NSString
        retCode = sqlite3_bind_text64(curStatement, position , strData.utf8String, sqlite3_uint64(strData.length), nil , UInt8(SQLITE_UTF8))
        break
      case .Integer32 :
        let intData : Int32 = data as! Int32
        retCode = sqlite3_bind_int(curStatement, position , intData)
        break
      case .Real :
        let realData : Double = data as! Double
        retCode = sqlite3_bind_double(curStatement, position , realData)
        break
      case .UInteger64 :
        let intData : UInt64 = data as! UInt64
        retCode = sqlite3_bind_int64(curStatement, position , sqlite3_int64(intData))
        break
      case .Integer64 :
        let intData : Int64 = data as! Int64
        retCode = sqlite3_bind_int64(curStatement, position , sqlite3_int64(intData))
        break
      case .Blob :
        var blobData = data as! Data
        let count = UInt64(blobData.count) as UInt64
        retCode = sqlite3_bind_blob64(curStatement,position,&blobData,count,SQLITE_STATIC)
      }
      if ( retCode != SQLITE_OK ) {
        try reportError(msg :"Binding Error " , code: retCode)
      }
    }
  }
  
  func fetch() throws -> Int {
    if let statement = curStatement {
      let retCode = sqlite3_step(statement)
      if ( retCode == SQLITE_ROW ) {
        return Int(sqlite3_column_count(statement))
      }
    }
    return 0
  }
  
  private func isNULL(statement: OpaquePointer, col : Int32 ) -> Bool {
    if sqlite3_column_type(statement,col) == SQLITE_NULL {
      return true
    }
    return false
  }


  func getCol( number : Int , type : SqlType ) throws -> Any? {
    if let statement = curStatement {
      let colNumber = Int32(number)
      if isNULL(statement: statement , col: colNumber) {
        return nil
      }
      switch type {
      case .Text ,
           .Text64 :
        let txtValue = sqlite3_column_text(statement, colNumber)
        if let txtValue = txtValue {
          return String( cString: txtValue)
        }
        return nil
      case .Integer32 :
        return sqlite3_column_int(statement, colNumber)
      case .Real :
        return sqlite3_column_double(statement, colNumber)
      case .UInteger64 ,
           .Integer64 :
        return sqlite3_column_int64(statement, colNumber)
      case .Blob :
        let nbBytes = sqlite3_column_bytes(statement,colNumber)
        let ptr = sqlite3_column_blob(statement,colNumber)
        return NSData(bytes: ptr , length: Int(nbBytes) )
      }
    }
    return nil
  }
  
  deinit {
      // perform the deinitialization
    do {
      if let _ = curStatement {
        try self.close()
      }
    } catch {
      DDLogError("error on sqlite statement deinit : \(error)")
    }
  }
}

class SqliteDriver : SQLDbProtocol {
  
  static var hasSqliteInited : Bool = false
  var db: OpaquePointer?
  var fkeyEnabled : Bool = false
  
  var dbPath : URL
  var name : String
  
  var dbUrl : URL {
    get {
      return dbPath.appendingPathComponent(name+fSuffix)
    }
  }
  
  var fSuffix : String {
    get {
      return ".sqlite3.abcmusicstudio"
    }
  }
  
  var isOpened : Bool {
    get {
      if ( db == nil ) {
        return false
      } else {
        return true
      }
    }
  }
  
  
  init( dbPath : URL  , name : String ) {
    self.dbPath = dbPath
    self.name = name
    if (!Self.hasSqliteInited) {
      sqlite3_initialize()
      Self.hasSqliteInited = true
    }
  }
  
  private func reportError(msg : String , code : Int32) throws {
    throw ABCFailure(message: "ERROR(retcode:\(code))" + msg, kind: .DatabaseFailure )
  }
  
  private func foreignKey( enable : Bool ) throws {
    if ( enable == self.fkeyEnabled ) {
      return
    }
    self.fkeyEnabled = enable
    var strEnable = "OFF"
    if enable {
      strEnable = "ON"
    }
    let stmt = "PRAGMA foreign_keys = \(strEnable)"
    if sqlite3_exec(db, stmt, nil, nil, nil) != SQLITE_OK {
      let err = String(cString: sqlite3_errmsg(db))
      try reportError(msg :"error enabling foreign keys : \(err)" , code: -1)
    }
  }
  
  func openDatabase() throws {
    let retcode = sqlite3_open(dbUrl.absoluteString, &db)
    if ( retcode == SQLITE_OK ) {
      try foreignKey(enable: true)
      return
    }
    try reportError(msg :"cannot open or create Sqlite db : \(dbUrl.absoluteString)" , code: retcode)
  }
  
  func createDatabase() throws {
    if ( !isOpened ) {
      try self.openDatabase()
    }
    if ( try !checkCatalog() ) {
      try self.createDataModel()
    }
  }
  
  func existsDatabase() -> Bool {
    let fu = FileUtils()
    return fu.existDatabase(url: dbUrl)
  }

  
  func deleteDatabase() throws {
    if ( isOpened ) {
      self.closeDatabase()
    }
    let fu = FileUtils()
    try fu.deleteDatabase(url: self.dbUrl)
  }
  
  //
  // DDL exec
  //
  private func DDLExec( stmt : String ) throws {
    var ddlStatement: OpaquePointer?
    var retcode = sqlite3_prepare_v2(self.db, stmt, -1, &ddlStatement, nil)
    if retcode  == SQLITE_OK {
      retcode = sqlite3_step(ddlStatement)
      if retcode != SQLITE_DONE {
        try reportError(msg :"DDL statement failed : \(stmt)" , code: retcode)
      }
    } else {
      try reportError(msg :"DDL statement prepare failed : \(stmt)" , code: retcode)
    }
    sqlite3_finalize(ddlStatement)
  }
  
  
  //
  // check Db catalog semantic
  //
  func checkCatalog() throws -> Bool  {
    let stmtStr = """
    SELECT
        name
    FROM
        sqlite_master
    WHERE
        type ='table' AND
        name NOT LIKE 'sqlite_%'
    """
    let stmt = self.newStatement()
    try stmt.prepare(statement: stmtStr)
    var fTableFound = false
    while ( try stmt.fetch() > 0 ) {
      let tbName = try stmt.getCol(number: 0, type: .Text) as? String
      if ( tbName == "ABCFolder") {
        fTableFound = true
        break
      }
    }
    if  ( fTableFound ) {
      return true
    }
    return false
  }
  
  
  //
  // initial datamodel creation
  //
  func createDataModel() throws {
    try DDLExec(stmt: ABCSqlFolder().createSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlFile().createSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlLeaf().createSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlDataContent().createSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlTags().createSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlTags().createIndex(dbType: .Sqlite))
  }
  
  //
  // dataModel cleanup
  //
  func dropDataModel() throws {
    try foreignKey(enable: false)
    try DDLExec(stmt: ABCSqlFile().dropSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlFolder().dropSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlLeaf().dropSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlDataContent().dropSqlDDl(dbType: .Sqlite))
    try DDLExec(stmt: ABCSqlTags().dropSqlDDl(dbType: .Sqlite))
    try foreignKey(enable: true)
  }

  func delete( table : String , key: String , id: ABCOid ) throws {
    let stmtStr = "DELETE FROM \(table) WHERE \(key)=\(id.oid)"
    let stmt = self.newStatement()
    try stmt.prepare(statement: stmtStr)
    try stmt.exec()
    try stmt.close()
  }
  
  
  func closeDatabase() {
    if let db = self.db {
      sqlite3_close(db)
      self.db = nil
      self.fkeyEnabled = false
    }
  }
  
  // do any cleanup requested
  func terminate() {
    sqlite3_shutdown()
  }
  
  func updateForeignKey( table: String ,
                         col: String ,
                         pKey: ABCOid ,
                         fKey: ABCOid ) throws {
    let stmt = SqliteStatement(driver:self)
    let stmtString = "UPDATE " + table + " set " + col + "=? WHERE oid=?"
    try stmt.prepare(statement: stmtString)
    try stmt.bind(position: 1, type: .Integer64, data: fKey.oid)
    try stmt.bind(position: 2, type: .Integer64, data: pKey.oid)
    try stmt.exec()
    try stmt.close()
  }

  //
  // associated statement semantics
  //
  func newStatement() -> SqlStatement {
    return SqliteStatement(driver: self)
  }
  
  
  private func newLeaf( candidate : ABCDbCandidate ) throws -> Leaf {
    var leaf : Leaf? = nil
    if let folder = candidate.folder {
      leaf = folder.infos
    }
    if  let file = candidate.file {
      leaf = file.infos
    }
    if let leaf = leaf {
      if ( leaf.creation == nil ) {
        leaf.creation = Date()
      }
      return leaf
    }
    throw ABCFailure(message:"ABCDbCandidate leaf information missing" , kind: .generalFailure)
  }
  

  private func newFolder( candidate : ABCDbCandidate , parent : ABCOid?) throws -> String {
    
    if let folder = candidate.folder {
      let sqlFolder = ABCSqlFolder(folder: folder, parent: parent)
      try sqlFolder.store(dbDriver: self)
      if let encoded = try AbcJson.encode(source: folder.oid, encoding: .utf8) {
        return encoded
      }
      throw ABCFailure(message: "newFolder Bad oid encoding ", kind: .DatabaseFailure)
    } else {
      throw ABCFailure(message: "addind NIL undefined folder", kind: .DatabaseFailure)
    }
  }
  
  private func newFile( candidate : ABCDbCandidate , parent: ABCOid? ) throws -> String {
    if let file = candidate.file {
      let sqlFile = ABCSqlFile(file: file, parent: parent)
      try sqlFile.store(dbDriver: self)
      if let encoded =  try AbcJson.encode(source: file.oid, encoding: .utf8) {
        return encoded
      }
      throw ABCFailure(message: "newFile Bad oid encoding ", kind: .DatabaseFailure)
    } else {
      throw ABCFailure(message: "adding NIL undefined file", kind: .DatabaseFailure)
    }
  }
  
  
  //
  // add candidate to database (file or folder)
  //
  func add( candidate : ABCDbCandidate , parent:ABCOid?) throws -> String {
    if ( candidate .isFolder ) {
      return try newFolder(candidate: candidate, parent: parent)
    } else {
      return try  newFile(candidate: candidate, parent: parent)
    }
  }
  
  func delete( candidate : ABCDbCandidate ) throws  {
    if  let oid = candidate.oid {
      if ( candidate.isFolder ) {
        let dbFolder = ABCSqlFolder()
        dbFolder.oid = oid
        try dbFolder.delete(dbDriver: self)
      } else { // File assumed
        let dbFile = ABCSqlFile()
        dbFile.oid = oid
        try dbFile.delete(dbDriver: self)
      }
    }
  }
  
  func update( candidate : ABCDbCandidate , parent: ABCOid?) throws {
    if ( candidate.isFolder ) {
      if let folder = candidate.folder {
        let dbFolder = ABCSqlFolder(folder: folder)
        try dbFolder.update(dbDriver: self)
      }
    } else {
      if let file = candidate.file {
        let dbFile = ABCSqlFile(file: file)
        try dbFile.update(dbDriver: self)
      }
    }
  }
  
  func load( query : ABCQuery ) throws -> String {
    if  let oid = query.oid {
      // direct query on oid
      if ( query.isFolder ) {
        let dbFolder = ABCSqlFolder()
        try dbFolder.load(dbDriver: self, id: oid)
        if let folder = dbFolder.folder {
          if let encoded =  try AbcJson.encode(source: folder, encoding: .utf8) {
            return encoded
          } else {
            throw ABCFailure(message: "load folder Bad oid encoding ", kind: .generalFailure)
          }
        }
      } else {
        let dbFile = ABCSqlFile()
        try dbFile.load(dbDriver: self, id: oid)
        if let file = dbFile.file {
          if let encoded =  try AbcJson.encode(source: file, encoding: .utf8) {
            return encoded
          } else {
            throw ABCFailure(message: "load file Bad oid encoding ", kind: .generalFailure)
          }
        }
      }
    } else if let tag = query.tags {
      let dbTag = ABCSqlTags()
      let result = try dbTag.query(dbDriver: self, tag: tag)
      // TODO : missing serialization on result 
    }
    return "{}"
  }

  deinit {
    // cleanup sqlite stuff
    if ( isOpened ) {
      closeDatabase()
    }
  }
  
}

