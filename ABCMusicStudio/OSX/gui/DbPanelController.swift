//
//  DbPanelController.swift
//  ABCMusicStudio
//
//  Created by jymengant on 4/5/20 week 14.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

struct DatabaseUsageInfo : Codable  {
  let documentName : String
  let dbModel : Int
  let dbUrl : String
  
}

class DbPanelController : NSViewController {
  
  @IBOutlet weak var topInfo: NSTextField!
  @IBOutlet weak var useDBButton: NSButton!
  @IBOutlet weak var dbSelection: NSComboBox!
  @IBOutlet weak var storageUrl: NSTextField!
  @IBOutlet weak var okButton: NSButton!
  @IBOutlet weak var checkButton: NSButton!
  
  var documentName : String? = nil
  var ftController: ABCFileTreeController? = nil
  
  override var nibName: NSNib.Name? {
    return "DatabasePanel"
  }
  
  private func setOn() {
    useDBButton.state = .on
    useDBButton.isEnabled = false
    dbSelection.isEnabled = true
    storageUrl.isEnabled = true
    checkButton.isEnabled = true
    okButton.isEnabled = false
  }
  
  private func setOff() {
    useDBButton.state = .off
    useDBButton.isEnabled = true
    dbSelection.isEnabled = false
    storageUrl.isEnabled = false
    checkButton.isEnabled = false
    okButton.isEnabled = false
  }
  
  private func buildDbUrl( docName : String ) -> URL {
    let path = preferences.databasePath
    let dbName = path?.appendingPathComponent(documentName!)
    let dbFinal = dbName?.appendingPathComponent("dbconfig.json")
    return dbFinal!
  }
  
  private func dataIn() {
    let fu = FileUtils()
    let path = preferences.databasePath
    let dbName = path?.appendingPathComponent(documentName!)
    let dbFinal = dbName?.appendingPathComponent("dbconfig.json")
    if ( fu.existFile(url: dbFinal!, directory: false) ) {
      do {
        let source = try fu.read(url: dbFinal!, backupEncoding: .utf8)
        let dbInfo : DatabaseUsageInfo = try AbcJson.decode( jsonSource: source.0, encoding:.utf8)
        self.setOn()
        dbSelection.selectItem(at: dbInfo.dbModel)
        storageUrl.stringValue = dbInfo.dbUrl
      } catch {
        _ = ModalAlert().show(question:"Database config FATAL read error" ,
                          text:"error \(error)" ,style: .Critical)
      }
    } else {
      self.setOff()
    }
  }
  
  private func dataOut() {
    if ( useDBButton.state == .on ) {
      let dbIndex = dbSelection.indexOfSelectedItem
      let url = storageUrl.stringValue
      let dbInfo = DatabaseUsageInfo(documentName: documentName!, dbModel: dbIndex, dbUrl: url)
      do {
        let encoded = try AbcJson.encode(source: dbInfo , encoding: .utf8 )
        let destUrl = buildDbUrl(docName: documentName!)
        let fu = FileUtils()
        try fu.write(url: destUrl, content: encoded!, encoding: .utf8)
      } catch {
        _ = ModalAlert().show(question:"Database config FATAL write/encoding error" ,
                            text:"error \(error)" ,style: .Critical)
      }
    }
  }
  
  override func viewDidLoad() {
    DDLogDebug("DbPanelController entering viewDidLoad")
    dbSelection.selectItem(at: 0)
    topInfo.stringValue = loc.tr("Database Selection for %" , documentName!)
    super.viewDidLoad()
  }

  @IBAction func useDB(_ sender: Any) {
    DDLogDebug("use db clicked")
    if ( useDBButton.state == .on ) {
      setOn()
    } else {
      // TODO : Ponder about this case
    }
  }
  @IBAction func checkDatabase
    (_ sender: Any) {
    DDLogDebug("db Check")
  }
  
  @IBAction func cancel(_ sender: Any) {
    DDLogDebug("cancel entered")
    ftController?.dismiss(self)
  }

  @IBAction func Ok(_ sender: Any) {
    DDLogDebug("cancel entered")
  }
}
