//
//  DynamicViewController.swift
//  ABCStudio
//
//  Created by jymengant on 6/7/18 week 23.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

class AnchorViewController : NSViewController {
    
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
  
  convenience init() {
    self.init(view: nil)
  }
  
  init(view : NSView? ) {
    //self._view = view
    super.init(nibName: nil, bundle: nil)
    self.view = view!
  }
  
  func addSubView( controller : NSViewController ) {
    self.view.addSubview(controller.view)
  }

  func getBounds() -> NSRect {
    return self.view.bounds 
  }
  
  
}
