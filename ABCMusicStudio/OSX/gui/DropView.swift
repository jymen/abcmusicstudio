//
//  DropView.swift
//  ABCMusicStudio
//
//  Created by jymengant on 2/15/20 week 7.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Cocoa

class DropView : NSView {
  
  var highlightForDragging : Bool = false {
    didSet {
      needsDisplay = true
    }
  }

  private func commonInit() {
    // Register accepted dragged stuff
    let types = [NSPasteboard.PasteboardType.fileURL]
    self.registerForDraggedTypes(types)
  }
  
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    commonInit()
  }

  //
  // Drag n drop stuff follow
  // Drag n drop is used for Editor / Images and sound
  //
  private func getDraggedUrl( _ draggingInfo: NSDraggingInfo ) -> URL? {
    //2.Got a reference to the dragging pasteboard from the dragging session info.
    let pasteBoard = draggingInfo.draggingPasteboard
    
    //3.Asked pasteboard if it has any URLs
    if pasteBoard.canReadObject(forClasses: [NSURL.self], options: nil) {
      return  NSURL(from: pasteBoard) as URL?
    }
    return nil
  }

  /**
   returns true if URL drop is accepted
   */
  func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
    var canAccept = false
    if let _ = getDraggedUrl(draggingInfo) {
      canAccept = true
    }
    return canAccept
  }

  override func draw(_ rect: NSRect) {
    super.draw(rect)
    // dropper?.colorBorder(highlight: highlightForDragging , stdWidth: 3.0, highlightWidth: 10.0)
  }
  
  override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
    print("ABCFileTreeView draggingEntered entered")
    let allow = shouldAllowDrag(sender)
    highlightForDragging = allow
    return allow ? .copy : NSDragOperation()
  }

  override func draggingExited(_ sender: NSDraggingInfo?) {
    highlightForDragging = false
    print("ABCFileTreeView drop exit")
  }
  
  override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
    let allow = shouldAllowDrag(sender)
    return allow
  }

  override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
    let pasteboard = sender.draggingPasteboard
    if let urls = pasteboard.readObjects(forClasses: [NSURL.self], options:nil) as? [URL], urls.count > 0 {
      // ftController?.processTxtAreaDropURLs(url: urls[0])
      print ("Drop accepted for : \(urls[0].absoluteString)")
      return true
    }
    return false
  }
}
