//
//  ABCGlobalMessage.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/13/21 week 19.
//  Copyright © 2021 jymengant. All rights reserved.
//

import Cocoa

class ABCGlobalMessage {
  
  private let msgTextArea : NSTextField
  private let syntaxColor = ColoringRules()

  
  init( txtField : NSTextField ) {
    msgTextArea = txtField
  }
  
  func information( txt : String ) {
    msgTextArea.textColor = syntaxColor.getForegroundColor(tkType: .InfoMsg)
    msgTextArea.stringValue = txt
  }
  
  func error( txt : String ) {
    msgTextArea.textColor = syntaxColor.getForegroundColor(tkType: .Error)
    msgTextArea.stringValue = txt
  }
  
  func warning( txt : String ) {
    msgTextArea.textColor = syntaxColor.getForegroundColor(tkType: .Warning)
    msgTextArea.stringValue = txt
  }
}
