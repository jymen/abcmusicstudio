//
//  MainWindowController.swift
//  ABCStudio
//
//  Created by jymengant on 5/31/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Preferences
import SwiftUI
import Combine


/**
  controller for main application window
*/
class MainWindowController : NSWindowController {
  @IBOutlet var mainMenu: NSMenu!
  private var aboutController : NSHostingController<AboutAbcStudio>?
  private var aboutW : NSWindow!
  private var aboutWController : NSWindowController!
  
  let preferencesWindowController = PreferencesWindowController(
    viewControllers: [
      ColorPreferenceTabViewController() ,
      HeaderTemplatePrefTabViewController()
    ]
  )
  
  override func windowDidLoad() {
    super.windowDidLoad()
    preferencesWindowController.window!.title = "ABC Studio preferences"
  }

  override var windowNibName: NSNib.Name? {
    return "MainMenu"
  }
  @IBAction func ShowAbout(_ sender: Any) {
    DDLogDebug("Show about panel")
    // About stuff
    let aboutView = AboutAbcStudio(aboutData: ABCAbout().about )
    aboutController = NSHostingController(rootView: aboutView )
    aboutW = NSWindow(contentRect: NSMakeRect(0, 0, 1024, 900),
    styleMask: [.closable, .titled],
    backing: .buffered,
    defer: true)
    aboutWController = NSWindowController(window: aboutW)
    aboutW.contentViewController = aboutController
    aboutW.center()
    aboutWController.showWindow(self)

  }
  
  @IBAction func newScoreAction(_ sender: Any) {
    DDLogDebug("newScoreAction entered")
    let document = NSDocumentController.shared.currentDocument
    if let document = document as? ABCMusicDocument {
      DDLogDebug("parent document linked")
      document.editor!.newEmptyScore()
    }
  }
  @IBAction func importMusicXmlAction(_ sender: Any) {
    DDLogDebug("importMusicXmlAction entered")
  }
  
  @IBAction func newGroupAction(_ sender: Any) {
    DDLogDebug("newGroupAction entered")
    let document = NSDocumentController.shared.currentDocument
    if let document = document as? ABCMusicDocument {
      DDLogDebug("parent document linked")
      document.editor!.newEmptyGroup()
    }
  }
  
  @IBAction func newGroupWithFolderAction(_ sender: Any) {
    DDLogDebug("newGroupWithFolderAction entered")
    let document = NSDocumentController.shared.currentDocument
    if let document = document as? ABCMusicDocument {
      DDLogDebug("parent document linked")
      document.editor!.newEmptyGroupWithFolder()
    }
  }
  
  @IBAction func newABCDocumentStore(_ sender: Any) {
    DDLogDebug("newAbcDocumentStore entered")
    let controller = NSDocumentController.shared
    do {
      _ = try controller.openUntitledDocumentAndDisplay(true)
      DDLogDebug("openUntitledAnd Display is OK")
    } catch  {
      if ModalAlert().show(question: "Cannot create new Document", text: "\(String(describing: error.localizedDescription))", style: .Critical){}
    }
  }
  
  func getMainMenu() -> NSMenu {
    return mainMenu
  }

  @IBAction func testMenuAction(_ sender: NSMenuItem) {
    DDLogDebug("testMenuAction entered")
  }
  
  @IBAction func showPreferences(_ sender: NSMenuItem) {
    DDLogDebug("show preferences entered")
    preferencesWindowController.showWindow()
  }
  @IBAction func printPreviewDocument(_ sender: Any) {
    DDLogDebug("print preview ABC Score entered")
    let document = NSDocumentController.shared.currentDocument
    if let document = document as? ABCMusicDocument {
      DDLogDebug("parent document linked")
      document.editor!.printSelectedABC(preview: true)
    }
  }
  
  @IBAction func importFromInternalJson(_ sender: Any) {
    DDLogDebug("import from internalJSon entered")
  }
  
  private func doExport( type : ExportOptions ) {
    DDLogDebug("exportAsInternalJson Open file dialog requested")
    let dialog = NSSavePanel()
    dialog.title                   = loc.tr("Export to external Json");
    dialog.showsResizeIndicator    = true;
    dialog.showsHiddenFiles        = false;
    dialog.canCreateDirectories    = true;
    dialog.allowedFileTypes        = ["json"]
      
    if (dialog.runModal() == NSApplication.ModalResponse.OK) {
      let result = dialog.url // Pathname of the file
        
      if let result = result {
        if let document = document as? ABCMusicDocument {
          document.treeController?.exportTree(to: result, exportType: type)
        }
      }
    } else {
      // User clicked on "Cancel"
      return
    }
  }
  
  @IBAction func exportAsInternalJSON(_ sender: Any) {
    let optionsPanel = ExportOptionsController()
    let document = NSDocumentController.shared.currentDocument
    if let document = document as? ABCMusicDocument {
      if let parent = document.treeController {
        optionsPanel.ftController = parent
        parent.presentAsSheet(optionsPanel)
      }
    }
  }
}

