//
//  XmlMusicEditorContent.swift
//  ABCStudio
//
//  Created by jymengant on 3/26/19 week 13.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Highlightr

//
// An XmlMusic file to be edited in EditorTextArea
//
class XmlMusicEditorContent {
  
  /**
     proceed with xml syntax coloring on XMLMusic stuff
  */
  func coloring( xmlCode : String ) -> NSAttributedString? {
    let highlightr = Highlightr()
    highlightr?.setTheme(to: "paraiso-dark")
    // You can omit the second parameter to use automatic language detection.
    let highlightedCode = highlightr?.highlight(xmlCode, as: "xml")
    return highlightedCode
  }
  
}
