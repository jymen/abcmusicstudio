//
//  EditorWindowControler.swift
//  ABCStudio
//
//  Created by jymengant on 5/29/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import WebKit
import SwiftUI
import Combine

/**
 
 Dedicated controller for ABC music Editor
 
*/

class EditorWindowController : NSWindowController {
  
  @IBOutlet var mainWindow: NSWindow!
  @IBOutlet weak var viewScoreButton: NSToolbarItem!
  @IBOutlet weak var webButton: NSButton!
  @IBOutlet weak var msgArea: NSTextField!
  
  // @IBOutlet var abcTxtArea: ABCEditorTextArea!
  
  
  @IBOutlet weak var viewFTreeButton: NSButton!
  @IBOutlet weak var leftAnchorView: NSView!
  @IBOutlet weak var hSplit: NSSplitView!
  @IBOutlet weak var botMenuBox: NSBox!
  @IBOutlet weak var scoreTabs: ABCTabView!
  
  @IBOutlet weak var mainTopView: NSView!
  @IBOutlet weak var mainBottomView: NSView!
  
  @IBOutlet weak var topTabView: ABCTabView!
  @IBOutlet weak var peerConnect: PeerConnectorToolbaItem!
  
  var peerConnectorController : PeerConnector?
  var topTab : ABCPanelTabController?
  var bottomTab : ABCPanelTabController?
  
  @IBOutlet weak var zoomIn: CustomizedButton!
  @IBOutlet weak var zoomOut: CustomizedButton!
  
  
  private var musicBarController : NSHostingController<ScoreBarView>?
  private var dbPanelController : NSHostingController<TestUIView>?
  
  var ftController : ABCFileTreeController?
  var userMessage : ABCGlobalMessage!

  var sharedSound : Sound?
  private var subscriber : AnyCancellable?

  // Singleton dispatcher used by tab panels children
  var dispatcher : EditorEventDispatcher!
  
  private var msgWrapper : ABCMessageArea? = nil
  private var loadedData : Data? = nil
  var txtAreaController : ABCEditorController? {
    get {
      if let dispatcher = dispatcher {
       return  dispatcher.focusedEditorCtl
      }
      return nil
    }
  }

  var curSelectedFile : ABCEditedFile? {
    get {
      if let dispatcher = dispatcher {
        if let editor = dispatcher.focusedEditorCtl {
          return editor.curEditedFile
        }
      }
      return nil
    }
  }
  
  
  var midiBpm : Int = 100
  var documentName : String? = nil
  
  var selectedEncoding : ComboSelection = ComboSelection(index: 0)
  var encodingSubscriber : AnyCancellable?
  var selectedInstrument : ComboSelection = ComboSelection(index: 0)
  var instrumentSubscriber : AnyCancellable?
  
  override var windowNibName: NSNib.Name? {
     // Returns the nib file name of the document
     return "MainSplitWindow"
   }
  
  func validateToolbarItem(_ item: NSToolbarItem) -> Bool {
    return true ;
  }
  
  func selectScoreTabs(at : Int ) {
    scoreTabs.selectTabViewItem(at: at)
  }
  
  //
  // ensure we have both editor and score visible
  //
  func checkScoreViewVisible()   {
    if ( topTab!.isScoreSelected ) {
      return
    }
    if ( !bottomTab!.isScoreSelected ) {
      bottomTab!.selectScore()
    }
  }
  
  
  //
  // request current serialized tree back
  //
  func serializeCurrentDocument() throws -> Data? {
    // save editor content
    if let ctrl = txtAreaController {
      ctrl.saveCurrent()
    }
    // serialize json structure then
    return try ftController?.getSerialized()
  }
  
  func svgCleanup() {
    if let topTab = self.topTab {
      topTab.svgCleanup()
    }
    if let bottomTab = self.bottomTab {
      bottomTab.svgCleanup()
    }
  }
  
  func writeDocument( to url: URL ) throws {
    if let myData = try self.serializeCurrentDocument() {
      try myData.write(to: url)
      return
    }
    throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
  }
  
  private func switcher (old: ABCPanelTabController?) {
    if let old = old  {
      if ( old.isEditorSelected ) {
        // save current editor
        old.saveEdited()
        // swith to score
        old.selectScore()
      }
    }
  }
  
  func getTwinTabView( current : ABCTabView ) -> ABCTabView {
    if ( current == topTab!.tbView ) {
      return bottomTab!.tbView
    }
    return topTab!.tbView
  }
  
  func lookForScoreTabController() -> ABCPanelTabController? {
    guard let topTab = self.topTab ,
      let bottomTab = self.bottomTab else {
        return nil
    }
    if ( topTab.isEditorSelected ) {
      return bottomTab
    }
    return topTab
  }
  
  func checkForSwitchRequested( from: ABCPanelTabController ) {
    guard let topTab = self.topTab ,
      let bottomTab = self.bottomTab else {
        return
    }
    var switchTop : Bool = false
    var switchBot : Bool = false
    if ( ( from == bottomTab ) && ( from.isEditorSelected ) ){
      switchTop = topTab.isEditorSelected
    }
    if ( ( from == topTab ) && ( from.isEditorSelected ) ) {
      switchBot = bottomTab.isEditorSelected
    }
    if ( switchTop ) {
      switcher(old: self.topTab )
      bottomTab.reloadEdited()
    }
    if ( switchBot ) {
      switcher(old: self.bottomTab)
      topTab.reloadEdited()
    }
  }
  
  func deserializeCurrentDocument(_ data: Data ) throws {
    assert(loadedData == nil) // avoid erasing data
    loadedData = data // lazily store it for later usage
  }
  
  func initSerializedDocument(withData : Data) {
    do {
      try ftController?.deserialize(withData)
    } catch  {
      if ModalAlert().show(question: "load serialized document failed ", text: "\(String(describing: error.localizedDescription))", style: .Critical){}
    }
  }
  
  private func subscribeTreeSelectionChanges( dataSource: ABCFileTreeDataSource ) {
    subscriber = dataSource.selectionChangePublisher.sink(
      receiveCompletion: {completion in
        print("receive completion(sink) :" , completion)
      } ,
      receiveValue: {
        value in
        print("receive value (sink)" , value)
        
        if let selectedFile = value.candidate as? File {
          // check existingsound
          if let soundUrl = selectedFile.infos.soundURL {
            self.sharedSound?.url = soundUrl
            self.sharedSound?.title = selectedFile.infos.title

          } else {
            self.sharedSound?.url = nil
            self.sharedSound?.title = ""
          }
        }
      }
    )
  }
  
  override func windowDidLoad() {
    DDLogDebug("EditorWindowController window did load")
    super.windowDidLoad()
    // dispatcher allocation
    dispatcher = EditorEventDispatcher()
    // messenger allocation
    userMessage = ABCGlobalMessage(txtField: msgArea)
    // editor controller
    // txtAreaController = ABCEditorController(id: "master TOP" , parentController : self)
    // ftree
    ftController = ABCFileTreeController()
    ftController?.parentController = self
    let ftView = (ftController?.view)!
    // load deserialized data if any
    if let data = loadedData {
      initSerializedDocument(withData: data)
    }
    leftAnchorView.addSubview(ftView)
    // change initial window size
    self.mainWindow.setFrame(NSRect(x:0,y:0,width: 1440,height: 790), display: true)
    self.mainWindow.center()
    self.mainWindow.delegate = self
    // init sound stuff as shared between tab and soundbar
    // init sound stuff for later usage
    self.sharedSound = Sound( url: nil ,
                            image: nil,
                            title: "",
                            description: ""
    )
    sharedSound!.initAudioPlayer()
    // be notified upon sound Url changes on tree
    subscribeTreeSelectionChanges(dataSource: ftController!.dataSource)
    // spit positioning
    hSplit.setPosition(250, ofDividerAt: 0)
    // populate encoding combo
    selectedEncoding.newSelection(
      newIndex: Unicode.encodingIndex(encoding: .utf8))
    // Midi bpm
    updateBpmFields()
    // Tabs controllers
    topTab = ABCPanelTabController(topTabView,id :"TOP Tab" , parent: self)
    bottomTab = ABCPanelTabController(scoreTabs,id: "BOTTOM tab" , parent: self)
    // Music Navigation Bar
    let scoreBarView = ScoreBarView(
    sound: sharedSound! ,
    selectedEncoding: selectedEncoding,
    selectedInstrument: selectedInstrument)
    musicBarController = NSHostingController(rootView: scoreBarView )
    botMenuBox.contentView = musicBarController!.view

    // init tabs
    if ( topTab!.isEditorSelected ) {
      bottomTab?.selectScore()
    } else {
      topTab?.selectScore()
      bottomTab?.selectEditor()
    }
  }
 
  
  override func showWindow(_ sender: Any?) {
    DDLogDebug("EditorWindowController showing")
    super.showWindow(self) ;
  }
  
  
  func getCurrentEncoding() -> String.Encoding? {
    return  Unicode.encoding(atIndex: selectedEncoding.index)
  }
  
  func updateEncoding(newValue : String.Encoding ) {
    selectedEncoding.newSelection(
      newIndex: Unicode.encodingIndex(encoding: newValue)
    )
  }
  
  func updateCurrentEncoding() {
    if let selected = curSelectedFile {
      let curEncoding = selected.updateCurrentEncoding()
      // Populate to New SwiftUI component
      updateEncoding(newValue: curEncoding)
    }
  }
  
  func treeSelectionChanged( to: ABCEditedFile) {
    assert(false) // case to be pondered
  }
    
  //
  // create plain new empty score
  //
  func newEmptyScore() {
    ftController?.showABCElementSheet(url: nil,type: .ABCScore)
  }
  
  func newEmptyGroup() {
    ftController?.showABCElementSheet(url: nil,type: .Group)
  }
  
  func newEmptyGroupWithFolder() {
    ftController?.showABCElementSheet(url: nil,type: .GroupWithFolder)
  }
  
  @IBAction func newScore(_ sender: NSToolbarItem) {
    newEmptyScore()
  }
  
  @IBAction func databases(_ sender: Any) {
    DDLogDebug("databases pushed")
    let dbCtrl = DbPanelController()
    dbCtrl.documentName = self.documentName
    dbCtrl.ftController = ftController
    // let dbForm = TestUIView()
    // dbPanelController = NSHostingController(rootView: dbForm )
    ftController!.presentAsSheet(dbCtrl)
    //let treeController = ABCResourcePanelController()
    //treeController.ftController = ftController
    //ftController!.presentAsSheet(treeController)
  }
  
  @IBAction func peerConnectClicked(_ sender: Any) {
    DDLogDebug("peerConnect pushed")
    if let peerConnectorController = peerConnectorController {
      peerConnectorController.stop()
      self.peerConnectorController = nil // cleanup request
    } else {
      peerConnectorController = PeerConnector( item:self.peerConnect,
        deviceName: Host.current().localizedName ?? "UNKNOWN"
                                            )
      peerConnectorController?.start()
    }
  }
  
  func updateBpmFields() {
    // stepper.integerValue = midiBpm
    // bpmField.integerValue = midiBpm
  }
  
  @IBAction func bpmDidChange(_ sender: NSTextField) {
    midiBpm = sender.integerValue
    updateBpmFields()
  }
  
  @IBAction func stepperDidChange(_ sender: NSStepper) {
    midiBpm = sender.integerValue
    updateBpmFields()
  }
  
  @IBAction func viewFileTree(_ sender: NSButton) {
  }
  
  @IBAction func switchDetailsViews(_ sender: Any) {
    DDLogDebug("switching detail view action")
  }
  
  @IBAction func testAction(_ sender: Any) {
    DDLogDebug("entering test action")
    let url = URL(string: "file:///users/jymen/development/xcodeprojects/ABCStudio/abcstudio/webstuff/caden001.html")!
    if NSWorkspace.shared.open(url) {
      print("default browser was successfully opened")
      
    }
  }
  
  @IBAction func printAction(_ sender: Any) {
    DDLogDebug("entering print request")
  }
  
  private func buildPrintWorkFile() -> URL? {
    return nil
  }
  
  
  @IBAction func zoomExtraIn(_ sender: Any) {
    DDLogDebug("Zoom extra In")
    topTab?.zoomImage(action: .IN)
    bottomTab?.zoomImage(action: .IN)
  }
  
  @IBAction func zoomExtraOut(_ sender: Any) {
    DDLogDebug("Zoom extra Out")
    topTab?.zoomImage(action: .OUT)
    bottomTab?.zoomImage(action: .OUT)
  }
  
  /**
    printing selected ABC through EPS / SVG and/Or  default browser
  */
  func printSelectedABC( preview : Bool = false)  {
    if let selectedFile =  curSelectedFile {
      var image : NSImage? = nil
      if ( selectedFile.isEmpty ) {
        // No Abc Score
        if ( selectedFile.hasExtraImage ) {
          // print extra image
          image = NSImage(contentsOf: selectedFile.file.infos.imageURL!)
          image?.size = NSSize(width: 595, height: 842)
        } else {
          return // nothing to print yet
        }
      } else {
        let file = selectedFile.file.infos.storageURL!
        if preview {
          // preview SVG in default browser
          let prtEngine = ABC2CModules(candidate : file)
          let result = prtEngine.emitForPrinting(type: .SVG)
          if ( result.0 == 0) {
            if NSWorkspace.shared.open(prtEngine.outUrl!) {
              print("default browser was successfully opened")
            }
          }
          return
        } else {
          let prtEngine = ABC2CModules(candidate : file)
          let result  = prtEngine.emitForPrinting(type: .EPS)
          if ( result.0 == 0) {
            image = NSImage.init(contentsOf: prtEngine.outUrl!)
          }
        }
      }
      if let prtImage = image {
        let printView = NSImageView(image: prtImage)
        printView.frame = NSMakeRect(0, 0, prtImage.size.width, prtImage.size.height)
        let printInfo = NSPrintInfo()
        
        printInfo.bottomMargin = 0
        printInfo.topMargin = 0
        printInfo.leftMargin = 0
        printInfo.rightMargin = 0
        
        let printOp = NSPrintOperation(view: printView, printInfo: printInfo)
        printOp.run()
      }
    }
  }
  
  func saveCatalog() {
    DDLogDebug("Saving catalog by serializing File tree")
    if  let url = self.document?.fileURL! {
      do {
        try writeDocument(to: url)
      } catch  {
        DDLogError("Final serialization failure : \(error)")
      }
    }
  }
}

extension EditorWindowController : NSWindowDelegate {
  
  func windowWillClose(_ notification: Notification) {
    DDLogDebug("entering windowWillClose in EditorWindowClontroller => Saving work before leaving")
    saveCatalog()
  }
}


