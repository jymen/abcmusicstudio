//
//  ExportOptionsController.swift
//  ABCMusicStudio
//
//  Created by jymengant on 6/11/20 week 24.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack


class ExportOptionsController : NSViewController {
  
  @IBOutlet weak var exportExtra: NSButton!
  @IBOutlet weak var exportSound: NSButton!
  @IBOutlet weak var exportIcon: NSButton!
  
  var ftController: ABCFileTreeController? = nil

  var options : ExportOptions {
    get {
      var exportExtra = false
      if ( self.exportExtra.state == .on ) {
        exportExtra = true
      }
      var exportSound = false
      if ( self.exportSound.state == .on ) {
        exportSound = true
      }
      
      var exportIcon = false
      if ( self.exportIcon.state == .on ) {
        exportIcon = true
      }

      return ExportOptions( extra: exportExtra, sound: exportSound, icon: exportIcon )
    }
  }
  
  override var nibName: NSNib.Name? {
    return "ExportPanel"
  }
  
  private func doExport() {
    DDLogDebug("exportAsInternalJson Open file dialog requested")
    let dialog = NSSavePanel()
    dialog.title                   = loc.tr("Export to external Json");
    dialog.showsResizeIndicator    = true;
    dialog.showsHiddenFiles        = false;
    dialog.canCreateDirectories    = true;
    dialog.allowedFileTypes        = ["json"]
      
    if (dialog.runModal() == NSApplication.ModalResponse.OK) {
      let result = dialog.url // Pathname of the file
        
      if let result = result {
        ftController?.exportTree(to: result, exportType: options)
      }
    } else {
      // User clicked on "Cancel"
      return
    }
  }


  
  @IBAction func okPressed(_ sender: Any) {
    doExport()
    ftController?.dismiss(self)
  }
  
  @IBAction func cancelPressed(_ sender: Any) {
    DDLogDebug("cancel entered")
    ftController?.dismiss(self)
  }
}
