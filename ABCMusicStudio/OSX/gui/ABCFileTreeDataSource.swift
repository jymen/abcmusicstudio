//
//  ABCFileTreeDataSource.swift
//  ABCStudio
//
//  Created by jymengant on 6/1/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Combine

let DRAGDROP_PASTEBOARD_TYPE = "org.ABCStudio.outline.item"


//
// This class is used to keep open tree nodes position
//
class TreePosition : Codable {
  var files = [File]()
  var folders = [Folder]()
}

struct TreeSelection {
  let candidate : ABCTreeElement?
  let tune : TuneDescription?
  let tunePos : Int
}

class TreePositions : Codable {
  var positions : [ String : TreePosition ] =  [String : TreePosition ]()
  
  private func get(_ fName : String ) -> TreePosition {
    if let position = positions[fName]  {
      return position
    } else {
      self.positions[fName] = TreePosition()
      return self.positions[fName]!
    }
  }
  
  func add( fName: String , file : File ) {
    let pos = get(fName)
    pos.files.append(file)
  }
  
  func add(fName: String , folder : Folder ) {
    let pos = get(fName)
    pos.folders.append(folder)
  }
}

/**
 
 DataSource for ABCFileTree view
 
 */
class ABCFileTreeDataSource : NSObject ,
  NSOutlineViewDataSource ,
  NSTextFieldDelegate 
{
  private let syntaxColor = ColoringRules()

  var dataStore : ABCTreeData? = nil
  var critical : Bool = false // true when something critical has happened
  var data : Folder? = nil
  
  var txtController : ABCEditorController? = nil
  var controller : ABCFileTreeController? = nil
  var currentFolder : Folder? = nil  // selected folder or parent folder of selected file
  var selectedRow : Int? = nil
  var tuneSelected : Bool = false
  var documentName : String? = nil
  // singleton
  var treePositions = TreePositions()
  
  //
  // communication upon selection changes
  //
  private let subject = PassthroughSubject<TreeSelection,Never>()
  let selectionChangePublisher : AnyPublisher<TreeSelection,Never>
  var selectedElement : TreeSelection? {
    didSet {
      if let selected = selectedElement {
        subject.send(selected)
      }
    }
  }
  
  
  override init() {
    dataStore = ABCTreeData()
    data = dataStore!.getRootFolder()
    selectionChangePublisher = subject.eraseToAnyPublisher()
  }
  
  
  //
  // receive current tree export request point request handled here
  //
  func exportTree(to: URL , exportType: ExportOptions) {
    print("receive export request to: \(to.absoluteString)")
    if let root = self.data {
      let exporter = ExportedABCSource(source:root)
      exporter.exportArchive(dest: to , exportType: exportType)
    }
  }
  
  /**
   returns how many items to show
   */
  func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
    if ( item == nil ) {
      // initial root case ALLWAYS RETURNS 1 (the main root folder)
      return 1
    }
    if let folder = item as? Folder {
      return dataStore!.getSize(folder: folder)
    }
    if let file = item as? File {
      if ( file.infos.type == .ABCScore ) {
        if let tunes = file.tunes {
          if ( tunes.count > 1 )  {
            return tunes.count
          }
        }
      }
    }
    return 0
  }
  
  private enum TreeTextFieldType {
    case Folder
    case File
    case Tune
  }
  
  private func setColorFor( textField : NSTextField ,
                            type : TreeTextFieldType = .Folder ,
                            isDirty : Bool = false  ) {
    var curColor = syntaxColor.getForegroundColor(tkType: .FolderTreeItem)
    switch type {
    case .File :
      if ( isDirty ) {
        curColor = syntaxColor.getForegroundColor(tkType: .DirtyScoreFlag)
      } else {
        curColor = syntaxColor.getForegroundColor(tkType: .ScoreTreeItem)
      }
      break
    case .Tune :
      curColor = syntaxColor.getForegroundColor(tkType: .TuneTreeItem)
      break
    default :
      break
    }
    textField.textColor = curColor
  }
  
  func controlTextDidChange(_ obj: Notification) {
    let textField = obj.object as! NSTextField
    DDLogDebug("new textfield value for cell :\(textField.stringValue)")
    guard let  selectedfile = txtController!.curEditedFile else {
      return
    }
    selectedfile.fileTitleChanged(newTitle: textField.stringValue)
  }
  
  /**
   when tree is initially emptuy create root folder automatocally
  */
  private func createInitialRoot() -> Folder {
    let infos = Leaf(
      title : loc.tr("initial root Folder") ,
      creation : Date() ,
      encoding : String.Encoding.utf8,
      type: .Group
    )
    return  Folder(infos: infos)
  }
  
  private func cellviewFor( folder : Folder ,
                            cellView: NSTableCellView? ,
                            dateColumn: Bool ,
                            dateFormatter : DateFormatter ) {
    if let textField = cellView?.textField {
      if ( dateColumn ) {
        textField.stringValue = dateFormatter.string(from:folder.infos.creation!)
      } else {
        textField.isEditable = true
        textField.delegate = self
        setColorFor(textField: textField,type: .Folder)
        textField.stringValue = folder.infos.title
        cellView!.imageView?.image = NSImage( named: "folder")
        textField.sizeToFit()
      }
    }
  }
  
  /**
    Column view formatting
  */
  private func buildCellViewFor(view: NSOutlineView, item: Any? , tableColumn: NSTableColumn?) -> NSTableCellView {
    var cellView: NSTableCellView?
    var dateColumn : Bool = false
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy/MM/dd"
    if ( (tableColumn?.identifier)!.rawValue == "DateColumn" ) {
      dateColumn = true
      cellView = view.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DateCell"), owner: self) as? NSTableCellView
    } else {
      cellView = view.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "FolderCell"), owner: self) as? NSTableCellView
    }
  
    if let score = item as? File {
      if let textField = cellView?.textField {
        if ( dateColumn ) {
          textField.stringValue = dateFormatter.string(from:score.infos.creation!)
        } else {
          cellView?.imageView?.image = NSImage(named: "note")
          textField.isEditable = true
          textField.delegate = self
          setColorFor(textField: textField,type: .File, isDirty: scoreBuffer.isDirty(file: score))
          if ( score.infos.type == .ABCScore) {
            if let tunes = score.tunes {
              if ( tunes.count > 1) {
                cellView?.imageView?.image = NSImage(named: "notes")
              }
            }
          }
          textField.stringValue = score.infos.title
          textField.sizeToFit()
        }
      }
      return cellView!
    }
    if let folder = item as? Folder {
      cellviewFor(folder: folder, cellView: cellView, dateColumn: dateColumn, dateFormatter: dateFormatter)
      return cellView!
    }
    if let tune = item as? TuneDescription {
      // multiple tunes score case => expand tunes
      if let textField = cellView?.textField {
        if ( dateColumn ) {
          textField.stringValue = ""
        } else {
          cellView?.imageView?.image = NSImage(named: "note")
          setColorFor(textField: textField,type: .Tune)
          textField.isEditable = false
          textField.stringValue = tune.title!
          textField.sizeToFit()
        }
      }
      // if file of tune has been modified : show it although not selected yet
      let fileParent = view.parent(forItem: tune) as? File
      let parentRow = view.row(forItem: fileParent)
      let rowView = view.rowView(atRow: parentRow, makeIfNecessary: false)
      let cell = rowView?.view(atColumn: 0) as? NSTableCellView
      if let fileTextField = cell?.textField {
        setColorFor(textField: fileTextField,type: .File, isDirty: scoreBuffer.isDirty(file: fileParent!))
      }
      return cellView!

    }
    //
    // Initial empty root case follows
    //
    dataStore!.setRoot(folder: createInitialRoot())
    cellviewFor(folder: dataStore!.getRootFolder()!, cellView: cellView, dateColumn: dateColumn, dateFormatter: dateFormatter)
    return cellView!
  }
  
  //
  // File or folder naming changed
  //
  func control(_: NSControl, textShouldEndEditing: NSText) -> Bool {
    DDLogDebug("tree node field modified ")
    let f = dataStore?.getSelectedFile()
    if let f = f {
      f.infos.title = textShouldEndEditing.string
    } else {
      let d = dataStore?.getSelectedFolder()
      if let d = d {
        d.getLeaf().title = textShouldEndEditing.string
      }
    }
    return true
  }
  
  /**
     get back folder index element => may be File or Folder
   */
  func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
    if ( item == nil ) {
      return dataStore!.getRootFolder() as Any
    }
    if let folder = item as? Folder {
      let returned : Any? = dataStore!.getIndexedItem(folder: folder, index: index)
      if ( returned != nil ) {
        return returned!
      }
    }
    if let file = item as? File {
      let returned : Any? = dataStore!.getIndexedItem(file: file, index:index)
      if ( returned != nil ) {
        return returned!
      }
    }
    return -1
  }
  
  /**
   wether given item is expandable or not
   */
  func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
    if let _ = item as? Folder {
      return true
    }
    if let file = item as? File {
      guard let tunes = file.tunes else {
        return false
      }
      if  ( tunes.count > 1) {
        return true
      }
    }
    return false
  }
  
  func outlineView(_: NSOutlineView, didRemove: NSTableRowView, forRow: Int) {
    DDLogDebug("an item has been removed at row:\(forRow) ")
  }
  
  //
  // populate selection change to datasource
  //
  func proceedWithSelectionChange(abcTree: NSOutlineView) {
    guard abcTree.selectedRow >= 0 else {
      DDLogDebug("double click on no selected => NO ACTION ")
      return
    }
    let selected : Int = abcTree.selectedRow
    let selectedItem = abcTree.item(atRow: selected)
    let parent = abcTree.parent(forItem: selectedItem) as? Folder
    if let file = selectedItem as? File {
      self.setSelected(item: file, parent: parent)
      self.currentFolder = parent
      self.dataStore!.setSelected(file: file, parent: parent)
    } else if let folder = selectedItem as? Folder {
      self.setSelected(item: folder, parent: parent)
    }
  }
  
  func recoverCurrenSelection( abcTree: NSOutlineView) {
    if  let _ = getSelectedFile() {
      return // in place
    }
    // else try to recover
    proceedWithSelectionChange(abcTree: abcTree)
  }
    
  func outlineViewSelectionDidChange(_ notification: Notification) {
    DDLogDebug("tree selection changed ")
    if ( !self.tuneSelected ) {
      controller?.abcTree.allowsMultipleSelection = false
    }

    guard let outlineView = notification.object as? NSOutlineView else {
      return
    }
    self.selectedRow = outlineView.selectedRow
    guard let selectedIndex = self.selectedRow else {
      return
    }
    self.proceedWithSelectionChange(abcTree: outlineView)
    
    self.tuneSelected = false
    if let _ = outlineView.item(atRow: selectedIndex) as? Folder {
      // Cleanup previously selected file if any
      self.txtController?.cleanupEditor()
      return  // nothing else to be done
    }
    // It's a file ??
    if let feedItem = outlineView.item(atRow: selectedIndex) as? File {
      // activate notification change
      selectedElement = TreeSelection( candidate: feedItem ,
                                       tune : nil ,
                                       tunePos : 0 )
    } else {
      // may also be a tune in a multitunes score
      // => Check score is loaded
      // => position text on selected tune
      // => show selected score
      if let feedItem = outlineView.item(atRow: selectedIndex) as? TuneDescription {
        self.tuneSelected = true
        controller?.abcTree.allowsMultipleSelection = true
        let fileParent = outlineView.parent(forItem: feedItem) as? File
        self.currentFolder = outlineView.parent(forItem: fileParent) as? Folder
        // activate notification change
        selectedElement = TreeSelection( candidate: fileParent ,
                                         tune : feedItem ,
                                         tunePos : fileParent!.indexOf(tune: feedItem))
      }
    }
  }
  
  //
  // reload serialized tree opened positions
  //
  func outlineView(_ outlineView: NSOutlineView, itemForPersistentObject object: Any) -> Any? {
    DDLogDebug("itemForPersistentObject entered")
    if let toDecode = object as? String {
      DDLogDebug("is expanded JSONED: \(toDecode)")
      let jsonDecoder = JSONDecoder()
      do {
        let folder = try jsonDecoder.decode(Folder.self, from: toDecode.data(using: .utf8)!)
        // find corresponding  decoded Item value in Datastore
        let returned = dataStore?.find(item: folder)
        return returned // return datastore instance
      }  catch {
        do {
          let file = try jsonDecoder.decode(File.self, from: toDecode.data(using: .utf8)!)
          // find corresponding  decoded Item value in Datastore
          let returned = dataStore?.find(item: file, recurse: true)
          return returned // return datastore instance
        }  catch {
          print("unknown tree positions have not been reloaded ... : \(error.localizedDescription)")
        }
      }
    }
    return nil
  }
  //
  // Keep track of tree opened positions
  //
  func outlineView(_ outlineView: NSOutlineView, persistentObjectForItem item: Any?) -> Any? {
    DDLogDebug("persistentObjectForItem entered")
    let jsonEncoder = JSONEncoder()
    if let curItem = item as? Folder {
      DDLogDebug("A Folder : \(curItem.infos.title)")
      curItem.state = TreeState.FolderOpened
      do {
        let data = try jsonEncoder.encode(curItem)
        let jsoned : String? = String(data: data, encoding: .utf8)
        return jsoned
      }  catch {
        print("error : \(error.localizedDescription)")
        assert(false) // TO BE IMPROVED later
      }
    } else if let curItem = item as? File {
      DDLogDebug("A File : \(curItem.infos.title)")
      curItem.state = TreeState.FolderOpened
      do {
        let data = try jsonEncoder.encode(curItem)
        let jsoned : String? = String(data: data, encoding: .utf8)
        return jsoned
      }  catch {
        print("error : \(error.localizedDescription)")
        assert(false) // TO BE IMPROVED later
      }

    }
    return nil
  }
  
  //
  // Handle score containing multiple tunes
  //
  func handleMultipleTunes( tunes : [TuneDescription] ) {
    guard let selected = getSelectedFile() else {
      return
    }
  
    if ( tunes.count > 1 ) {
      selected.tunes = tunes  // populate info to tree data source
    } else {
      selected.tunes = nil
    }
    
  }
  
  func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
    // return view
    return buildCellViewFor(view: outlineView, item: item,tableColumn: tableColumn )
  }

  func addNew( treeData : ABCResourcePanelData , content: String? , before: ABCTreeElement? = nil , atIndex : Int = -1) -> (Any?,Folder?) {
    return  (dataStore?.addItem(data: treeData , content: content , before: before , atIndex: atIndex))!
  }
  
  func updateSelected( infos : Leaf ) {
    dataStore?.updateSelected(infos: infos)
  }
  
  func removeSelected() {
    dataStore?.removeSelected()
  }
  
  func getSelectedFile() -> File? {
    return dataStore?.getSelectedFile()
  }
  
  func getSelectedFolder() -> Folder? {
    return dataStore?.getSelectedFolder()
  }
  
  func setSelected( item: Any? , parent: Folder?) {
    if let file = item as? File {
      DDLogDebug("\(file.infos.title) selected")
      dataStore?.setSelected(file: file, parent: parent)
    } else {
      if let folder = item as? Folder {
        DDLogDebug("\(folder.infos.title) selected")
        dataStore?.setSelected(folder: folder, parent: parent)
      }
    }
  }
  
  func getSerialized() throws -> Data? {
    return try dataStore?.getSerialized()
  }
  
  func deserialize( _ data: Data ) throws {
    try dataStore!.deserialize(data)
    // refresh local data after deserialization
    self.data = dataStore!.getRootFolder()
  }

}


extension ABCFileTreeDataSource :   NSOutlineViewDelegate {
 
  
  private func getInnerPasteBoard( info: NSDraggingInfo ) -> String? {
    let pasteboard = info.draggingPasteboard
    return  pasteboard.string(forType: NSPasteboard.PasteboardType(rawValue: DRAGDROP_PASTEBOARD_TYPE))
  }

  private func isOuterDrop( _ outlineView: NSOutlineView ,info: NSDraggingInfo , item: Any? , atIndex : Int) -> Bool {
    let pasteboard = info.draggingPasteboard
    if let urls = pasteboard.readObjects(forClasses: [NSURL.self], options:nil) as? [URL], urls.count > 0 {
      let element : ABCTreeElement? = item as? ABCTreeElement
      let kind = ScoreUrlKind.getKindOf(url: urls[0])
      controller?.processURLs(urls, inItem: element ,urlKind: kind , atIndex: atIndex)
      return true
    }
    return false
  }
  
  private func isInnerDrop( _ outlineView: NSOutlineView ,info: NSDraggingInfo , item: Any? , atIndex : Int ) -> Bool {
    let  serializedStr = getInnerPasteBoard(info: info)
    if let serializedStr = serializedStr {
      if let dest = item as? ABCTreeElement {
        do {
          let decoded = try dataStore!.decodeTreeElement(jsonSource: serializedStr)
          if let destFolder = dest as? Folder {
            // move to folder
            dataStore!.move(source: decoded , dest : destFolder , at: atIndex )
          }
          else if let destFile = dest as? File {
            if let file = decoded as? File {
              // move in same folder before selected destfile
              dataStore!.move(file:file, before:destFile , at: atIndex)
            }
          }
          outlineView.reloadData() // refresh after move
          return true
        } catch {
          assert(false) ;  // TO BE IMROVED
        }
      }
      return true
    }
    return false
  }
  
  /**
   exportDrop =   transfert to attached device
   innerDrop =    move to another place
   outerDrop =    add to tree
   */
  func outlineView(_ outlineView: NSOutlineView, validateDrop info: NSDraggingInfo, proposedItem item: Any?, proposedChildIndex index: Int) -> NSDragOperation {
    let pasteboard = info.draggingPasteboard
    let  serializedStr = pasteboard.string(forType: NSPasteboard.PasteboardType(rawValue: DRAGDROP_PASTEBOARD_TYPE))
    if let _ = serializedStr {
      return NSDragOperation.move // inner drag/drop operation
    } else {
      return NSDragOperation.generic // outer drag/drop
    }
  }
  
  func outlineView(_ outlineView: NSOutlineView, acceptDrop info: NSDraggingInfo, item: Any?, childIndex index: Int) -> Bool {
    DDLogDebug("NSOutlineView drop accepted for item \(String(describing: item)) ")
    if ( item == nil ) {
      return false
    }
    if ( isInnerDrop(outlineView , info: info , item: item , atIndex: index) ) {
      return true
    } else if ( isOuterDrop (outlineView , info: info , item: item , atIndex: index) ) {
      return true
    }
    return false // invalid drop position
  }

  func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forItems draggedItems: [Any]) {
    DDLogDebug("Drag session is beginning ")
    controller?.dragStarted()
  }

  func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, operation: NSDragOperation) {
    DDLogDebug("Drag session has ended ")
    controller?.dragCompleted()
  }
}


//
// Internal drag n drop of cells
//
extension ABCFileTreeDataSource : NSPasteboardItemDataProvider {
  
  
  func pasteboard(_ pasteboard: NSPasteboard?, item: NSPasteboardItem, provideDataForType type: NSPasteboard.PasteboardType) {
    let s = "Outline Pasteboard Item"
    item.setString(s, forType: type)
  }
  
  //
  // Handle dragged value
  //
  func outlineView(_ outlineView: NSOutlineView, pasteboardWriterForItem item: Any) -> NSPasteboardWriting? {
    let pbItem = NSPasteboardItem()
    // cleanly accept Leaf or Folders as drag sources
    var dragData : String? = nil
    do {
      if let l = item  as? File {
        dragData = try dataStore!.encode(source: l)
      } else if let f = item as? Folder {
        dragData = try dataStore!.encode(source: f)
      } else {
        return nil
      }
    } catch {
      
    }
    pbItem.setString(dragData!,forType: NSPasteboard.PasteboardType(rawValue: DRAGDROP_PASTEBOARD_TYPE))
    return pbItem
  }
  
  
  
}

