//
//  ABCPanelTabController.swift
//  ABCStudio
//
//  Created by jymengant on 12/29/19 week 52.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Combine
import WebKit

enum Zoom {
  case IN
  case OUT
}

enum ABCItemOffset : Int {
  case EditorOffset = 0
  case ScoreOffset = 1
  case ExtraOffset = 2
  case SoundOffset = 3
}

private let EDITOR = "ABCEditor"
private let SCORE = "ABCScore"
private let EXTRA = "ABCExtra"
private let SOUND = "ABCSound"

private let ids : [String] = [
  EDITOR , SCORE , EXTRA , SOUND
]


class ABCTabView : NSTabView  {
  
  
  var editorItem = ABCPanelTabViewItem(id: ids[0] , label: "Editor" )
  var scoreItem = ABCPanelTabViewItem(id: ids[1] , label: "Score" )
  var extraItem = ABCPanelTabViewItem(id: ids[2] , label: "Extra" )
  var soundItem = ABCPanelTabViewItem(id: ids[3] , label: "Recording" )
  
  func populateItems() {
    self.insertTabViewItem(editorItem, at: ABCItemOffset.EditorOffset.rawValue)
    self.insertTabViewItem(scoreItem, at: ABCItemOffset.ScoreOffset.rawValue)
    scoreItem.toolTip = loc.tr("show computed abc score")
    self.insertTabViewItem(extraItem, at: ABCItemOffset.ExtraOffset.rawValue)
    self.insertTabViewItem(soundItem, at: ABCItemOffset.SoundOffset.rawValue)
  }
  
}


class ABCPanelTabViewItem : NSTabViewItem {
  
  init(id : String , label : String ) {
    super.init(identifier: id)
    super.label = label
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}


class ABCPanelTabController : NSViewController ,
                              NSTabViewDelegate {
  
  var tbView : ABCTabView
  var abcParent : EditorWindowController
  
  private var txtAreaCtlr : ABCEditorController
  private var svgController : SVGViewController
  private var imageDropController = ImageExtraInfoController(color: CGColor(red: 0.294, green: 0.278, blue: 0.286, alpha: 1))
  private var imageScoreController : ScoreImageScrollController?
  private let soundTabController : SoundExtraInfoController
  
  
  private var htmlView : ABCWebView? = nil
  
  var curEditedFile : ABCEditedFile?
  private var curEditedTune : TuneDescription?
  
  private var treeSubscriber : AnyCancellable?
  private var txtAreaSubscriber : AnyCancellable?
  
  private var dispatcher : EditorEventDispatcher
  private let notificationCenter: NotificationCenter = .default
  
  var isEditorSelected : Bool {
    get {
      if ( self.tbView.selectedTabViewItem == self.tbView.editorItem ) {
        return true
      }
      return false
    }
  }
  
  var isExtraSelected : Bool {
    get {
      if ( self.tbView.selectedTabViewItem == self.tbView.extraItem ) {
        return true
      }
      return false
    }
  }
  
  var isScoreSelected : Bool {
    get {
      if ( self.tbView.selectedTabViewItem == self.tbView.scoreItem ) {
        return true
      }
      return false
    }
  }
  

  init(_ view : ABCTabView , id : String , parent : EditorWindowController ) {
    tbView = view
    self.abcParent = parent
    self.dispatcher = parent.dispatcher
    soundTabController = SoundExtraInfoController(
      item: tbView.soundItem ,
      data: parent.sharedSound! ,
      color: CGColor(red: 0.522, green: 0.716, blue: 0.328, alpha: 1),
      label : loc.tr("Drop any Sound file here")
    )
    txtAreaCtlr = ABCEditorController(id: id , topController: parent)
    svgController = SVGViewController(id: id)
    super.init(nibName:nil,bundle:nil )
    super.view = view
    tbView.delegate = self
    tbView.populateItems()
    // populate controllers
    add(controller: txtAreaCtlr, to: ABCItemOffset.EditorOffset.rawValue)
    add(controller: svgController, to: ABCItemOffset.ScoreOffset.rawValue )
    add(controller: imageDropController, to: ABCItemOffset.ExtraOffset.rawValue )
    add(controller: soundTabController, to: ABCItemOffset.SoundOffset.rawValue )
    
    imageDropController.parentController = self
    soundTabController.parentController = self
    txtAreaCtlr.parentController = self
    
    // get notified on tree selection changes
    subscribeTreeSelectionChanges(dataSource:  parent.ftController!.dataSource)
    // image removal observers
    notificationCenter.addObserver(self,
                                   selector: #selector(imageRemoved),
                                   name: .extraImageRemove,
                                   object: nil
    )
    notificationCenter.addObserver(self,
                                   selector: #selector(imageBackgroundColorSet),
                                   name: .imageBgColorChanged,
                                   object: nil
    )
  }
  
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  private func updateScore() {
    if let selected = curEditedFile {
      // selected.loadScore()
      //let instrument : Int = midiInstrumenCombo.indexOfSelectedItem
      let instrument : Int = 0
      //let bpm :Int = bpmField.integerValue
      let bpm :Int = 120
      var tuneNumber = 0
      if let tune = curEditedTune {
        tuneNumber = selected.file.indexOf(tune: tune)
      }
      
      svgController.display(file: selected, tune: tuneNumber, instrument: instrument, bpm: bpm)
      // sync editor position
      if let currentEditor = dispatcher.focusedEditorCtl {
        currentEditor.jumpTo(tune: curEditedTune)
      }
    } else {
      svgController.initialHtml()
    }
  }
  
  private var hasImage : Bool {
    get {
      if let selected = curEditedFile {
        let details = selected.file.infos
        guard let _ = details.imageURL else {
          return false
        }
        return true
      }
      return false
    }
  }
  
  
  
  func getAdjustedImage( leaf : Leaf ) -> NSImage? {
    var image : NSImage? = nil
    if let url = leaf.imageURL {
      image = NSImage.init(contentsOf: url)
      if let img = image {
        let width = img.width * ( leaf.zoomImage / 100 )
        let height = img.height * ( leaf.zoomImage / 100 )
        image = img.resize(withSize: NSSize(width: width, height: height))
      }
    }
    return image
  }
  
  func zoomImage ( action: Zoom ) {
    if let selected = curEditedFile {
      let details = selected.file.infos
      var curZoom = details.zoomImage
      switch action {
      case .IN :
        curZoom += 10
        break
      case .OUT :
        curZoom -= 10
        break
      }
      details.zoomImage = curZoom
    }
    if ( isExtraSelected ) {
      updateExtra()
    }
  }
  
  private func noImage() {
    imageDropController.display(image: nil)
    add(controller: imageDropController, to: ABCItemOffset.ExtraOffset.rawValue )
  }
  
  private func updateExtra() {
    if let selected = curEditedFile {
      let details = selected.file.infos
      guard let imageURL = details.imageURL else {
        noImage()
        return
      }
      let futils = FileUtils()
      if ( futils.existFile(url: imageURL, directory: false) ) {
        let image = getAdjustedImage(leaf: details)
        if let image = image {
          imageScoreController = ScoreImageScrollController( image: image,
                                                             bounds: imageDropController.pView.bounds,
                                                             background: details.imageBgColor?.cgColor
          )
          add(controller: imageScoreController!, to: ABCItemOffset.ExtraOffset.rawValue )
        }
      } else {
        noImage()
        abcParent.userMessage.error(txt: loc.tr("Extra URL not found : %" , imageURL.absoluteString))
      }
    }
  }
  
  @objc private func imageBackgroundColorSet(_ notification: Notification) {
    DDLogDebug("image Background color changed")
    if let selected = curEditedFile {
      let details = selected.file.infos
      if let color = notification.object as! CGColor? {
        details.imageBgColor = CodableColor(cgColor: color)
      } else {
        details.imageBgColor = nil
      }
      updateExtra() // populate changes
    }
  }
  
  @objc private func imageRemoved(_ notification: Notification) {
    if let selected = curEditedFile {
      DDLogDebug("extra score image is removing ...")
      let details = selected.file.infos
      details.imageURL = nil
      details.imageZoom = 100.0
      updateExtra()
      DDLogDebug("extra score image has been removed")
    }
  }
  
  func svgCleanup() {
    svgController.cleanup()
  }
  
  func showExtraSound() {
    guard let file = curEditedFile?.file else {
      return
    }
    let details = file.infos
    guard let soundURL = details.soundURL else {
      // cleanup any previous content
      // soundTabController?.reset()
      return
    }
    let futils = FileUtils()
    if ( futils.existFile(url: soundURL, directory: false) ) {
      let sound = soundTabController.soundData
      sound.title = details.title
      sound.url = details.soundURL
    } else {
      abcParent.userMessage.error(txt: loc.tr("Sound URL not found : %" , soundURL.absoluteString))
    }
  }
  
  private func newSelected(userClick : Bool = false ) {
    //
    // refresh current Selected Tab content
    //
    let selectedId = tbView.selectedTabViewItem!.identifier as! String
    // in any cases update sound to populate associated event
    switch selectedId {
    case EDITOR:
      // update editor
      if let selected = curEditedFile {
        txtAreaCtlr.display(file: selected , tune: curEditedTune )
      }
      break
    case SCORE:
      // no score edited yet but some image there
      // => display image if not a user click action
      if ( !userClick  ) {
        if ( txtAreaCtlr.isEmpty && hasImage ) {
          selectExtra()
          updateExtra()
        } else {
          let twin = abcParent.getTwinTabView(current: tbView)
          let twinId =  twin.selectedTabViewItem!.identifier as! String
          // avoid showing score both on top and bot
          if ( twinId != SCORE ) {
            updateScore()
          }
        }
      } else {
        updateScore()
      }
      break
    case EXTRA:
      // enable zoom
      abcParent.zoomIn.isEnabled = false
      abcParent.zoomOut.isEnabled = false
      updateExtra()
      break
    case SOUND:
      showExtraSound()
      break
    default:
      assert(false)
    }
    if ( isExtraSelected ) {
      abcParent.zoomIn.isEnabled = true
      abcParent.zoomOut.isEnabled = true
    } else {
      abcParent.zoomIn.isEnabled = false
      abcParent.zoomOut.isEnabled = false
    }
    
  }
  
  
  private func subscribeTreeSelectionChanges( dataSource: ABCFileTreeDataSource ) {
    treeSubscriber = dataSource.selectionChangePublisher.sink(
      receiveCompletion: {completion in
        print("receive subscribeTreeSelectionChanges completion(sink) :" , completion)
      } ,
      receiveValue: {
        value in
        print("receive subscribeTreeSelectionChanges value (sink)" , value)
        dataSource.txtController = self.txtAreaCtlr // just refresh
        if let selectedFile = value.candidate as? File {
          // file selection change in tree
          let fEncoding = String.Encoding(rawValue: selectedFile.infos.encoding!)
          self.curEditedFile = ABCEditedFile( parent: self.txtAreaCtlr,
                                              file: selectedFile ,
                                              encoding: fEncoding)
          DDLogDebug("selected file encoding is \(fEncoding)")
          self.abcParent.updateEncoding(newValue: fEncoding)
          self.curEditedTune = value.tune
          self.newSelected()
        }
      }
    )
  }
  
  
  func removeExtra( kind: ScoreUrlKind ) {
    if let current = curEditedFile {
      let leaf = current.file.infos
      switch kind {
      case .ImageScore :
        // remove image
        leaf.imageURL = nil
      case .SoundScore :
        // remove score
        leaf.soundURL = nil
      default :
        assert(false)
      }
    }
  }
  
  func hasDropped(kind: ScoreUrlKind ,  url : URL? ) {
    if let current = curEditedFile  {
      let leaf = current.file.infos
      switch kind {
      case .ImageScore :
        // add image to selected file
        leaf.imageURL = url
        updateExtra()

      case .SoundScore :
        // add image to selected file
        leaf.soundURL = url
        showExtraSound()
      default :
        assert(false)
      }
    } else {
      // new empty score instance
      let ftController = abcParent.ftController
      ftController?.showABCElementSheet(url: url, urlKind: kind)
    }
  }
  
  func selectScore()  {
    select(at : ABCItemOffset.ScoreOffset.rawValue )
  }
  
  func selectExtra()  {
    select(at : ABCItemOffset.ExtraOffset.rawValue )
  }
  
  func selectEditor()  {
    select(at : ABCItemOffset.EditorOffset.rawValue )
  }
  
  func saveEdited() {
    guard let _ = curEditedFile else {
      return
    }
    self.txtAreaCtlr.saveCurrent()
  }
  
  func reloadEdited() {
    guard let _ = curEditedFile else {
      return
    }
    self.txtAreaCtlr.reload()
  }
  
  private func add( controller: NSViewController , to : Int ) {
    tbView.tabViewItems[to].viewController = controller
  }
  
  func select(at : Int) {
    tbView.selectTabViewItem(at: at)
  }
  
  //
  // MARK:  delegate
  //
  
  func tabView(_ tabView: NSTabView, shouldSelect tabViewItem: NSTabViewItem?) -> Bool {
    DDLogDebug("entering tabView should select")
    // checkCurSelection(selected: tabViewItem!)
    if let _ = tabViewItem!.viewController {
      return true
    }
    return false
  }
  
  func tabView(_ tabView: NSTabView, didSelect tabViewItem: NSTabViewItem?) {
    DDLogDebug("entering tabView did select")
    if ( tabViewItem == self.tbView.editorItem ) {
      // update parent focused editor
      dispatcher.focusedEditorCtl = txtAreaCtlr
    } else if ( tabViewItem == self.tbView.scoreItem ) {
      // update parent focused editor
      dispatcher.focusedSvgCtl = svgController
    }
    // check for requested tab switching
    abcParent.checkForSwitchRequested(from: self)
    // populate new tab selection accordingly
    newSelected(userClick: true)
  }
  
  
}
