//
//  EditorEventDispatcher.swift
//  ABCStudio
//
//  Created by jymengant on 1/13/20 week 3.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Combine

private struct LastPosition {
  let start : Int
  let end : Int
}

class EditorEventDispatcher {
  
   private var txtAreaSubscriber : AnyCancellable?
   private var scoreSelectionSubscriber : AnyCancellable?
   private var lastPosition : LastPosition? = nil
   private var curTune : TuneDescription? = nil

  
   // Txt Editor with current focus
   var focusedEditorCtl : ABCEditorController? {
     didSet {
       // get notified on textAreaChanges
      if let focused = focusedEditorCtl {
         DDLogDebug("focused Editor is now : \(focused.id)")
        subscribeTextAreaChanges(focused : focused)
      }
     }
   }
  
   var focusedSvgCtl : SVGViewController? {
     didSet {
      if let focused = focusedSvgCtl {
        DDLogDebug("focused SVGScore is now : \(focused.id)")
        subscribeScoreSelection(focused : focused)
      }
     }
   }
  
  func isFocused(_ editor : ABCEditorController) -> Bool{
    if let focused = focusedEditorCtl {
      if ( focused == editor ) {
        return true
      }
    }
    return false
  }
  
  func resetToEmpty() {
    focusedSvgCtl?.initialHtml()
    focusedEditorCtl?.resetToEmpty()
  }
  
  private func setCurTune( _ position: ScoreTextContext ) {
    self.focusedSvgCtl?.showCurrent(content: position.content!, tune: position.curTuneIndex)
    curTune = position.curTune
  }
  
  private func textPositionChanged( _ position: ScoreTextContext  ) {
    if let current = position.curTune {
      // show current postion's tune if not yet in place
      if let shown = curTune {
        if ( shown != current ) {
          setCurTune(position)
        }
      } else {
        setCurTune(position)
      }
    }
    
    if let last = self.lastPosition {
      self.focusedSvgCtl!.unhighlightScoreNote(start: last.start, end: last.end)
    }
    self.lastPosition = LastPosition(start: position.startPosition ,
                                         end: position.endPosition )
    if ( position.isNote ) {
      self.focusedSvgCtl!.highlightScoreNote(start: position.startPosition,
                                            end: position.endPosition)
    }
  }
  
  private func textChanged( source: String , isXml : Bool , tune: Int) {
    self.focusedSvgCtl!.showAbcScore(source, tune: tune)
  }
  
  private func subscribeScoreSelection(focused: SVGViewController) {
    scoreSelectionSubscriber = focused.scoreSelectionPublisher.sink(
      receiveCompletion: {completion in
        print("receive scoreSelection completion(sink) :" , completion)
    } ,
      receiveValue: {
        value in
        print("receive scoreSelection value (sink)" , value)
        self.focusedEditorCtl?.jumpTo(position: value)
      }
    )
  }
  
  private func subscribeTextAreaChanges(focused : ABCEditorController) {
    txtAreaSubscriber = focused.caretChangePublisher!.sink(
      receiveCompletion: {completion in
        print("receive caretChangePusblisher completion(sink) :" , completion)
    } ,
      receiveValue: {
        value in
        print("receive subscribeTextAreaChanges value (sink)" , value)
        if let position = value.position {
          self.textPositionChanged(position)
        }
        if let source = value.content {
          self.textChanged(source: source, isXml: value.isXml , tune: value.tunePosition)
        }
      }
    )
  }
  
  
}
