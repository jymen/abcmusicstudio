//
//  XmlMusicColorsPrefsController.swift
//  ABCStudio
//
//  Created by jymengant on 5/22/19 week 21.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

class TemplateHeaderPrefsController : NSViewController ,
                                      NSComboBoxDelegate ,
                                      NSTextViewDelegate {
  
  @IBOutlet var abcSourceTextView: ABCEditorTextArea!
  @IBOutlet weak var includeInNewScore: NSButton!
  
  private let _HEADER_FNAME_ = "HeaderTemplate"
  private let templateFile = TemplateHeaderFile()
  private var timer: Timer?

  // var textColoring : TextColoring?
  
  override var nibName: NSNib.Name? {
    return "TemplateHeaderPreferences"
  }
  
  var currentCarretPosition : Int {
    get {
      (abcSourceTextView.selectedRanges.first?.rangeValue.location)!
    }
  }
  
  var doInclusion : Bool {
    get {
      if ( includeInNewScore.state == .on) {
        return true
      }
      return false 
    }
  }
  
  private func loadInclusion() {
    let prefInclusion = preferences.includeTemplate
    if ( prefInclusion ) {
      includeInNewScore.state = .on
    } else {
      includeInNewScore.state = .off
    }
  }
  
  private func storeInclusion() {
    preferences.includeTemplate = doInclusion
  }

  //
  // cleanup all content
  //
  func cleanup() {
    let fullRange = NSRange(location: 0, length: abcSourceTextView.attributedString().length)
    // let newStorege = NSTextStorage(NSAttributedString(string: ""))
    abcSourceTextView.textStorage?.deleteCharacters(in: fullRange)
  }
  
  func coloringChanged( _ data: ABCEvent ) {
    DDLogDebug("coloring change callback entered")
    // Filter events
    if ( data.type == .SyntaxColoringChanged )  {
      if let ir = abcSourceTextView.abcIR {
        abcSourceTextView.coloring!.abcColoring(ir: ir)
      }
    }
  }

  override func viewWillAppear() {
    super.viewWillAppear()
    abcSourceTextView.isTemplate = true // Notify abc Editor to be in template mode
    abcSourceTextView.placeHolderTitleString = nil 
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    templateFile.setup()
    abcSourceTextView.startUp(delegate : self)
    abcSourceTextView.showTemplate(source : templateFile.content )
    loadInclusion()
  }
  
  @objc private func updateTimer() {
    DDLogDebug("TemplateHeaderPrefsController IS NOW OVER => UPDATE BUFFER/GUI")
    abcSourceTextView.showTemplate()
    self.timer = nil
    templateFile.content = abcSourceTextView.string
    templateFile.write()
  }
  
  //
  // abcTxtArea delegate methods
  //
  func textDidChange(_ notification: Notification) {
    guard (notification.object as? NSTextView) != nil else { return }
    // DDLogDebug(textView.string)
    DDLogDebug("TEXT CONTENT has been changed in TemplateHeaderPrefsController")
    DDLogDebug("carret at : \(currentCarretPosition)")
    // keep track of current carret position
    abcSourceTextView.templateCarretPosition = currentCarretPosition
    // trigger 2 second timer update
    if ( timer == nil ) {
      timer = Timer.scheduledTimer(timeInterval: 2.0,
                                   target: self,
                                   selector: #selector(updateTimer),
                                   userInfo: nil,
                                   repeats: false)
    }

  }

  @IBAction func checkIncludeChanged(_ sender: Any) {
    DDLogDebug("check includeTemplate has been changed" )
    storeInclusion()
  }
  
  
}
