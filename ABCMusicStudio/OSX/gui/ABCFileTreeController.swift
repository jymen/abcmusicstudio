//
//  AbcFileTreeController.swift
//  ABCStudio
//
//  Created by jymengant on 6/1/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import CocoaLumberjack
import Cocoa
import EmitterKit

enum ScoreUrlKind {
  case SourceScore
  case ImageScore
  case SoundScore
  case Unknown
  
  static func getKindOf( url : URL ) -> ScoreUrlKind {
    let imageFormats = ["jpg", "png", "gif" , "pdf" , "jpeg"]
    let sourceFormats = ["abc", "xml", "xmlmusic" , "musicxml" ]
    let extensi = url.pathExtension.lowercased()
    if ( imageFormats.contains(extensi)) {
      return ImageScore
    } else if ( sourceFormats.contains(extensi) ) {
      return .SourceScore
    }
    return Unknown
  }
}

class ABCFileTreeController : NSViewController  {
  
  @IBOutlet weak var abcTree: ABCFileTreeView!
  @IBOutlet weak var addButton: NSButton!
  @IBOutlet weak var removeButton: NSButton!
  
  let dataSource : ABCFileTreeDataSource = ABCFileTreeDataSource()
  var txtController : ABCEditorController? {
    get {
      if let pController = parentController {
        return pController.dispatcher.focusedEditorCtl
      }
      return nil
    }
  }
  var parentController : EditorWindowController? = nil
  private var eventListener : EventListener<ABCEvent>?
  private var dropItemPosition : ABCTreeElement? = nil
  private var dropIndexPosition : Int = -1
  private var dropper : URLViewDropper?


  
  override var nibName: NSNib.Name? {
    return "ABCMusicFileTree"
  }
  
  override func viewDidLoad() {
    DDLogDebug("ABCFileTreeController entering viewDidLoad")
    super.viewDidLoad()
    txtController?.treeController = self // link with textArea
    dataSource.txtController = txtController
    self.abcTree.controller = self
    self.abcTree.columnAutoresizingStyle = NSTableView.ColumnAutoresizingStyle.firstColumnOnlyAutoresizingStyle
    self.abcTree.delegate = dataSource
    self.abcTree.dataSource = dataSource
    dataSource.controller = self
    dataSource.documentName = parentController?.documentName
    // Register for the dropped object types we can accept.
    self.abcTree.registerForDraggedTypes(([
      NSPasteboard.PasteboardType(rawValue: DRAGDROP_PASTEBOARD_TYPE),
      NSPasteboard.PasteboardType.fileURL
                                        ]))

    // Disable dragging items from our view to other applications.
    self.abcTree.setDraggingSourceOperationMask(NSDragOperation(), forLocal: true)
    
    // Enable dragging items within and into our view.
    self.abcTree.setDraggingSourceOperationMask(NSDragOperation.every, forLocal: true)
    // Listen for coloring changes
    eventListener = EventManager.shared.add(listener:coloringChanged)
    // allocate dropper for treeView
    dropper = URLViewDropper(view: abcTree)
    abcTree.dropper = dropper
  }
  
  override func viewWillAppear() {
    DDLogDebug("ABCFileTreeController entering viewWillAppear")
    super.viewWillAppear()
  }
  
  func reloadData() {
    abcTree.reloadSelectedRow()
  }

  private func coloringChanged( _ data: ABCEvent ) {
    DDLogDebug("coloring change callback entered for tree")
    if ( data.type == ABCEventType.SyntaxColoringChanged ) {
      abcTree.reloadData() // reload data to apply new syntax coloring scheme
    }
  }
  
  override func viewDidAppear() {
    DDLogDebug("ABCFileTreeController entering viewDidAppear")
    super.viewDidAppear()
    // save tree opened / closed positions
    // This MUST BE DONE after the datastore has been populated
    self.abcTree.autosaveExpandedItems = true
    self.abcTree.autosaveName = "ABCStudio.tableautosave"
    // initially set selected element to root
    self.abcTree.selectRow(at: 0)
  }
  
  /**
   show ABCResourcePanel Modal window
   */
  func showABCElementSheet( url: URL? , urlKind : ScoreUrlKind = .SourceScore ,  type : ABCResType = ABCResType.ABCScore ) {
    let treeController = ABCResourcePanelController()
    // Check for dropped folder
    var panelData : ABCResourcePanelData?
    if ( urlKind == .SourceScore ) {
      panelData = ABCResourcePanelData(type: type , url: url)
    } else {
      panelData = ABCResourcePanelData(type: type , imageUrl: url)
    }
    treeController.setRessourceData(resource: panelData!)
    // let treeController = TestController()
    treeController.ftController = self
    self.presentAsSheet(treeController)
  }
  
  func selectionDidChange( newSelection: ABCEditedFile ) {
    parentController?.treeSelectionChanged(to: newSelection )
  }
  
  func exportTree( to : URL , exportType : ExportOptions ) {
    dataSource.exportTree(to : to , exportType: exportType)
  }
  
  /**
   add new element in tree
  */
  func addTreeElement( data : ABCResourcePanelData? , content: String? = nil ) {
    DDLogDebug("addTreeElement adding element to tree")
    if let data = data {
      let (item ,parentFolder)  = dataSource.addNew(treeData: data , content: content,before: self.dropItemPosition , atIndex : self.dropIndexPosition )
      // expand current parent
      abcTree.expandItem(parentFolder)
      abcTree.reloadData() // refresh screen
      // select current added item
      select(item:item)
    }
  }
  
  /**
   populate scanned tunes under selected Folder
  */
  func populateSelected( scoreList :  [SelectedScore] , parent : Folder) {
    for currentScore in scoreList {
      if ( currentScore.selected ) {
        let data = ABCResourcePanelData(from: currentScore.file.file)
        _  = dataSource.addNew(treeData: data , content: nil)
      }
    }
    abcTree.expandItem(parent)
    abcTree.reloadData() // refresh screen
    // select current added item
    select(item:parent)
  }
  
  /**
  * update selected element with new data
  */
  func updateTreeElement( data: ABCResourcePanelData? ) {
    DDLogDebug("updateTreeElement adding element to tree")
    if let data = data {
      dataSource.updateSelected(infos: data.buildLeaf())
    }
    parentController?.updateCurrentEncoding()
    abcTree.reloadData() // refresh screen
  }
  
  func showScore( file: File , forTune : TuneDescription? ) {
    self.txtController?.txtArea!.showScoreSource(file:file,forTune:forTune)
  }
  
  /* select item programmatically */
  private func select( item : Any?  ) {
    let index = abcTree.row(forItem: item)
    let indexSet = IndexSet(integer:index)
    abcTree.selectRowIndexes(indexSet, byExtendingSelection: false)
    if let file = item as? File {
      showScore( file: file , forTune: nil)
    }
  }
  
  func getCurrentFolder() -> Folder? {
    return dataSource.currentFolder
  }
  
  func getSerialized() throws -> Data? {
    return try dataSource.getSerialized()
  }
  
  func deserialize(_ data: Data ) throws {
    try dataSource.deserialize(data)
    abcTree.reloadData() // refresh screen
  }
  
  //
  // File has been dropped in TxtArea => Accept it in selected folder
  //
  func processTxtAreaDropURLs( url: URL ) {
    DDLogDebug("TxtArea DROP accepted")
    let importer = MusicUrlImportExport(url: url, parent: self)
    do {
      try importer.abcImport()
    } catch let error as ABCFailure {
      if ModalAlert().show(question: "ABCMusicStudio load error ", text: "\(String(describing: error.message))", style: .Critical){}
    } catch  {
      if ModalAlert().show(question: "MusicFile load unknown error", text: "UNKNOWN ERROR", style: .Critical){}
    }
  }
  
  /**
    show ABCResourcePanel Modal window
  */
  @IBAction func addAction(_ sender: NSButton) {
    self.showABCElementSheet(url: nil)
  }
  
  
  @IBAction func removeAction(_ sender: Any) {
    DDLogDebug("removeAction ")
    abcTree.delete()
  }
  
  
  @IBAction func cellDoubleClick(_ sender: ABCFileTreeView) {
    dataSource.proceedWithSelectionChange(abcTree : self.abcTree)
    var data : ABCResourcePanelData? = nil
    if let file = dataSource.getSelectedFile() {
      data = ABCResourcePanelData(from: file)
    } else if let folder = dataSource.getSelectedFolder() {
      data = ABCResourcePanelData(from: folder)
    } else  {
      return // NO ACTION
    }
    let treeController = ABCResourcePanelController()
    treeController.setRessourceData(resource: data!)
    treeController.ftController = self
    self.presentAsSheet(treeController)
  }
  
}

// To handle dropped data in controller
protocol ABCFileTreeViewDelegate {
  func processURLs(_ urls: [URL], inItem : ABCTreeElement? , urlKind : ScoreUrlKind , atIndex: Int )
  func dropCleanup()
  func dragStarted()
  func dragCompleted()
}

extension ABCFileTreeController: ABCFileTreeViewDelegate {
  
  /**
   Handle FileTree object drop data
   */
  func processURLs(_ urls: [URL], inItem : ABCTreeElement? , urlKind : ScoreUrlKind = .SourceScore , atIndex: Int) {
    DDLogDebug("Process Dropped URL data")
    self.dropItemPosition = inItem
    self.dropIndexPosition = atIndex
    
    // self.showABCElementSheet(url: urls[0],urlKind: urlKind) ;
    let importer = MusicUrlImportExport(url: urls[0], parent: self)
    do {
      try importer.abcImport()
    } catch {
      let err = error as! ABCFailure
      if ModalAlert().show(question: "ABCMusicStudio import error", text: "\(err.message!)", style: .Critical){}

    }
  }
  
  func dropCleanup() {
    self.dropItemPosition = nil
    self.dropIndexPosition = -1
  }

  func dragStarted() {
    abcTree.highlightForDragging = true
  }
  
  func dragCompleted() {
    abcTree.highlightForDragging = false
  }
}

