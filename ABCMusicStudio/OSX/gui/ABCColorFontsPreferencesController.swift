//
//  ColorFontsPreferenceController.swift
//  ABCStudio
//
//  Created by jymengant on 2/10/19 week 6.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Preferences

struct ColorData {
  let type : ColoringType
  let str : NSMutableAttributedString
}

final class ABCColorFontsPreferencesController: NSViewController , NSWindowDelegate {
  
  @IBOutlet weak var colorFontsTable: NSTableView!
  @IBOutlet weak var fontButton: NSButton!
  @IBOutlet weak var backgroundButton: NSButton!
  @IBOutlet weak var colorButton: NSButton!
  @IBOutlet weak var darkMode: NSButton!
  @IBOutlet weak var curPanelTitle: NSTextFieldCell!
  
  private let syntaxColor = ColoringRules()
  private var tableData : [ColorData] = [ColorData]()
  // current selections follow
  var selectedRow : Int = -1
  var selectedCell : NSTableCellView? = nil
 
  override var nibName: NSNib.Name? {
    return "ABCColorsAndFontsPreferences"
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Setup stuff here
    colorFontsTable.delegate = self
    self.reloadStylesTable()
    self.setButtons(enabled: false)
    // set preference mode accordingly to current thenme
    if ColoringTheme.currentTheme() == .dark {
      darkMode.state = .on
    } else {
      darkMode.state = .off
    }
    checkCurrentMode()
  }
  
  private func reloadStylesTable() {
    tableData = [ColorData]()
    // init tableData => convert Styles to attributed Strings
    tableData.reserveCapacity(syntaxColor.selectedStyles.abc.count)
    for element in syntaxColor.selectedStyles.abc {
      tableData.append(ColorData(type: element.key ,str: element.value.toAttributedString()))
    }
    colorFontsTable.reloadData() // force table refresh
  }
  
  @IBAction func backgroundAction(_ sender: Any) {
    DDLogDebug("backgoundAction entered")
    let colorPanel = NSColorPanel.shared
    colorPanel.setAction(#selector(backGroundColorChanged))
    showColors(panel: colorPanel)
  }
  
  @IBAction func ColorAction(_ sender: Any) {
    DDLogDebug("colorAction entered")
    let colorPanel = NSColorPanel.shared
    colorPanel.delegate = self
    // get current color to init color panel with
    let selectKeyColor = tableData[selectedRow].type
    if let curColor = syntaxColor.getForegroundColor(tkType: selectKeyColor) {
      colorPanel.color = curColor
    }
    colorPanel.setAction(#selector(colorChanged))
    showColors(panel: colorPanel)
  }
  
  //
  // Capture color panel closing to resign color action change
  // on shared object
  //
  func windowWillClose(_ notification: Notification) {
    DDLogDebug("ColorWindow Closing")
    let colorPanel = notification.object as! NSColorPanel
    colorPanel.setAction(nil) // resign color changed action
  }
  
  private func checkCurrentMode() {
    if ( darkMode.state == .on ) {
     syntaxColor.selectedTheme = .dark
     curPanelTitle.stringValue = loc.tr("Dark theme font & colors")
    } else {
     syntaxColor.selectedTheme = .light
     curPanelTitle.stringValue = loc.tr("Light theme font & colors")
    }
    self.reloadStylesTable()
  }
  
  @IBAction func onValueChanged(_ sender: Any) {
    DDLogDebug("Dark mode Clicked")
    checkCurrentMode()
  }
  
  private func setButtons( enabled: Bool ) {
    fontButton.isEnabled = enabled
    backgroundButton.isEnabled = enabled
    colorButton.isEnabled = enabled
  }
  
  private func showColors( panel: NSColorPanel) {
    panel.setTarget(self)
    panel.orderFront(self)
    panel.center()
  }
  
  private func saveToStyle( attStr : NSMutableAttributedString , key : ColoringType) {
    let style = Style(attributed: attStr)
    // store back to syntaxColor singleton
    syntaxColor.update(style: style, forKey: key)
    do {
      try syntaxColor.serialize()
      // Notify any listener
      EventManager.shared.emit(event:ABCEvent(type:.SyntaxColoringChanged,msg:"ABCColoringChanged"))
    } catch let error as ABCFailure {
      if ModalAlert().show(question: "Error Serializing Coloring ", text: "\(String(describing: error.message))", style: .Critical){}
    } catch {}
    DDLogDebug("stored back to syntaxColor prefs")
  }
  
  private func cellColorChanged (_ newColor: NSColor , isBackground : Bool ) {
    if ( selectedRow == -1 ) {
      return
    }
    let attrStr = tableData[selectedRow].str
    let type = tableData[selectedRow].type
    var attrKey :NSAttributedString.Key = .foregroundColor
    if ( isBackground ) {
      attrKey = .backgroundColor
    }
    let range = NSRange(location:0,length:attrStr.string.count )
    tableData[selectedRow].str.addAttribute(attrKey, value: newColor as Any, range: range)
    colorFontsTable.reloadData() // populate changes
    // keep selection
    let indexSet = IndexSet(integer: selectedRow)
    colorFontsTable.selectRowIndexes(indexSet, byExtendingSelection: false)
    saveToStyle(attStr: attrStr,key: type)
    DDLogDebug("keyStyle : \(type) color saved value = \(newColor.hexString)")
  }

  @objc func colorChanged( _ sender: NSColorPanel ) {
    let newColor = sender.color
    DDLogDebug("colorChanged entered with new color : \(newColor.hexString)")
    cellColorChanged(newColor, isBackground: false)
  }
  
  @objc func backGroundColorChanged( _ sender: NSColorPanel ) {
    let newColor = sender.color
    DDLogDebug("background color Changed entered with new color : \(newColor)")
    cellColorChanged(newColor, isBackground: true)
  }

  //
  // font dynamically changed
  //
  @objc func fontChanged( _ sender: NSFontManager ) {
    let newFont = sender.selectedFont
    let attrStr = tableData[selectedRow].str
    let type = tableData[selectedRow].type

    DDLogDebug("font changed to \(String(describing: newFont))")
    let range = NSRange(location:0,length:attrStr.string.count )
    tableData[selectedRow].str.addAttribute(.font, value: newFont as Any, range: range)
    colorFontsTable.reloadData()
    // keep selection
    let indexSet = IndexSet(integer: selectedRow)
    colorFontsTable.selectRowIndexes(indexSet, byExtendingSelection: false)
    // handle potential size changes
    selectedCell!.textField!.sizeToFit()
    colorFontsTable.noteHeightOfRows(withIndexesChanged: indexSet)
    saveToStyle(attStr: attrStr, key: type)
  }
  
  @IBAction func fontsAction(_ sender: Any) {
    DDLogDebug("fontAction entered")
    let fontManager = NSFontManager.shared
    let txtField = selectedCell!.textField
    let selectKeyColor = ColoringType(rawValue: selectedRow)
    let selStyle = syntaxColor.currentStyles.abc[selectKeyColor!]
    var curFont : NSFont? = selStyle?.getAttribute(key: .font) as? NSFont
    if ( curFont == nil ) {
      curFont = txtField?.font
    }
    fontManager.setSelectedFont(curFont!, isMultiple: false)
    fontManager.action = #selector(fontChanged)
    let panel = fontManager.fontPanel(true)
    panel?.center()
    fontManager.orderFrontFontPanel(self)
  }
  
  //
  // pref panel going away
  //
  override func viewWillDisappear() {
    DDLogDebug("color pref view will disapear => Saving prefs")
    super.viewWillDisappear()
  }
  

}


//
// complementary extensions For TableView
// dataSource populated in instance dataTableContent NSAttributedString array
// which is dynamically updated upon fonts and colors changes
//
extension ABCColorFontsPreferencesController : NSTableViewDataSource {
  
  func numberOfRows(in tableView: NSTableView) -> Int {
    return tableData.count
  }
  
  func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
    return tableData[row].str
  }
}

class CustomTableCellView: NSTableCellView {
  var count = 1
}



extension ABCColorFontsPreferencesController : NSTableViewDelegate {
  
  fileprivate enum CellIdentifiers {
    static let fontColorCell = "fontcolorCellId"
  }
  
  //
  // creating a custom cell in order to manage dynamic font sizes change
  // inside dedicated textfield
  // ( when using standard cell displaying big fonts is not correctly handled  )
  //
  private func createCell(_ id: NSUserInterfaceItemIdentifier) -> CustomTableCellView {
    // Create a text field for the cell
    let textField = NSTextField()
    textField.backgroundColor = NSColor.clear
    textField.translatesAutoresizingMaskIntoConstraints = false
    textField.isBordered = false
    textField.isEditable = false
    
    // Create a cell
    let cell = CustomTableCellView() // Doing this to see that cells get re-used
    cell.identifier = id
    cell.addSubview(textField)
    cell.textField = textField
    
    // Constrain the text field within the cell
    textField.widthAnchor.constraint(equalTo: cell.widthAnchor).isActive = true
    textField.heightAnchor.constraint(equalTo: cell.heightAnchor).isActive = true
    
    textField.bind(NSBindingName.value, to: cell, withKeyPath: "objectValue", options: nil)
    
    return cell
  }
  
  //
  // just create or reuse the dedicated Custom cell
  //
  func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
    let id = tableColumn!.identifier
    var view = tableView.makeView(withIdentifier: id, owner: nil) as? CustomTableCellView
    if view == nil {
      view = createCell(id)
    }
    view!.textField!.stringValue = "\(id.rawValue) \(row) \(view!.count)"
    view!.count += 1
    
    return view
  }

  //
  // due to change size of font the requested rect space may be
  // increased or decreased
  //
  func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
    let str = tableData[row].str
    let minSize = str.size()
    // DDLogDebug("row heigth has been changed to : \(minSize!.height)")
    return minSize.height
  }
  
  //
  // row selection change
  //
  func tableViewSelectionDidChange(_ notification: Notification) {
    DDLogDebug("selection changed")
    self.setButtons(enabled: true)
    selectedRow = colorFontsTable.selectedRow
    DDLogDebug("selection \(selectedRow)")
    selectedCell = (colorFontsTable.view(atColumn: 0, row: selectedRow, makeIfNecessary: true) as! NSTableCellView)
  }
}



