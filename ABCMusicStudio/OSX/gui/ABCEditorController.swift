//
//  ABCEditorController.swift
//  ABCStudio
//
//  Created by jymengant on 12/30/19 week 1.
//  Copyright © 2019 jymengant. All rights reserved.
//

import CocoaLumberjack
import Cocoa
import Combine

class ABCEditorController : NSViewController {
  
  /// Holds the attached line number gutter.
  private var lineNumberGutter: LineNumberGutter?
  private var gutterForegroundColor: NSColor? = NSColor.white
  private var gutterBackgroundColor: NSColor? = NSColor.lightGray

  var dropper : URLViewDropper?
  let id : String // for debugging purposes
  var parentView : NSScrollView
  var txtArea : ABCEditorTextArea?
  var parentController : ABCPanelTabController?
  var treeController : ABCFileTreeController?
  var loadedFile : File?
  
  var curEditedFile : ABCEditedFile? {
    get {
      return parentController?.curEditedFile
    }
  }
  
  var topController : EditorWindowController
    
  var msgWrapper : ABCMessageArea? {
    didSet {
      txtArea?.msgArea = msgWrapper
    }
  }
  
  var isEmpty : Bool {
    get {
      if ( content.count == 0 ) {
        return true
      }
      return false 
    }
  }
  
  var currentSelectedTune : TuneDescription? {
    get {
      if let txt = txtArea {
        return txt.currentSelectedTune
      }
      return nil
    }
  }
  
  // synch txtArea content
  var content : String {
    get {
      if let txt = txtArea {
        return txt.string
      }
      return ""
    }
    set {
      if let txt = txtArea {
        txt.string = newValue
      }
    }
  }
  
  var linesOffsets : [Int]? {
    set {
      lineNumberGutter?.linesOffsets = newValue
    }
    get {
      return lineNumberGutter?.linesOffsets
    }
  }
   
  var caretChangePublisher : AnyPublisher<ABCEditorChange,Never>? {
    get {
      return txtArea?.changePublisher
    }
  }
  
  init(id : String  , topController : EditorWindowController ) {
    self.id = id
    self.topController = topController
    let scrollViewFrame = topController.mainTopView.frame
    parentView = NSScrollView(frame: scrollViewFrame)
    parentView.hasVerticalScroller = true
    parentView.hasHorizontalScroller = false
    super.init(nibName:nil,bundle:nil )

    super.view =  parentView
    txtArea = ABCEditorTextArea(frame: parentView.bounds)
    if let txtArea = txtArea {
      txtArea.mainViewController = topController
      parentView.documentView = txtArea
      guard let _ = txtArea.enclosingScrollView else {
        fatalError("Unwrapping the text views scroll view failed!")
      }
      // enable drag and drop
      // Register for the dropped object types we can accept.
      // Dropped file here will follow the same semantics as the one
      // dropped inside tree
      //
      // allocate dropper for textArea
      dropper = URLViewDropper(view: txtArea)

      // init msgArea and populate it to abcTxtArea
      msgWrapper = ABCMessageArea(msgArea: topController.msgArea)
      
      parentView.hasHorizontalRuler = false
      parentView.hasVerticalRuler   = true
      parentView.rulersVisible      = true
      if let gutterBG = self.gutterBackgroundColor,
        let gutterFG = self.gutterForegroundColor {
        self.lineNumberGutter = LineNumberGutter(withTextView: txtArea, foregroundColor: gutterFG, backgroundColor: gutterBG)
      } else {
        self.lineNumberGutter = LineNumberGutter(withTextView: txtArea)
      }
      parentView.verticalRulerView  = self.lineNumberGutter
      
      txtArea.parentController = self      
      
      // finally populate view
      //self.view = testView
      self.view = parentView
      txtArea.startUp()
    }
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func drawLineGutter() {
    if let lineNumberGutter = self.lineNumberGutter {
      // DDLogDebug("DRAW GUTTER ENTERED")
      lineNumberGutter.needsDisplay = true
    }
  }
  
  func cleanupEditor() {
    if let txt = txtArea {
      txt.cleanup()
    }
  }
  
  func cleanupGutter() {
   lineNumberGutter?.linesOffsets = nil
  }

  func setGutterError(line : Int) {
    lineNumberGutter?.setError(line: line)
  }
  func clearGutterError() {
    lineNumberGutter?.clearError()
  }
  
  func repaintGutter() {
    lineNumberGutter?.needsDisplay = true
  }
  
  func saveCurrent() {
    if let txtArea = self.txtArea {
      do {
       try txtArea.saveEditorContent()
      } catch let error as ABCFailure {
        if ModalAlert().show(question: "saveCurrent FATAL ERROR", text: "\(String(describing: error.message))", style: .Critical){}
      } catch {}
    }
  }
  
  private func loadSelected( file: ABCEditedFile  , tune: TuneDescription?) {
    if let txtArea = self.txtArea {
      // check last loaded file
      if let loaded = loadedFile {
        let selected = file.file
        if ( selected == loaded ) {
          txtArea.showABC( forTune: tune)
          return
        }
      }
      // not yet loaded
      txtArea.cleanup() // remove previous text if any
      do {
        if ( file.exists ) {
          try file.loadScore()
          loadedFile = file.file
          if ( file.getScoreType() == ABCResType.ABCScore ) {
            txtArea.showABC( forTune: tune)
          } 
        } else {
          txtArea.cleanup() // remove previous text if any
        }
      } catch let error as ABCFailure{
        if ModalAlert().show(question: "Error loading file", text: "\(String(describing: error.message))", style: .Critical){}
      } catch {}
    }
  }
  
  func jumpTo( tune : TuneDescription? ) {
    if let tune = tune {
      txtArea?.jumpTo(tune: tune)
    }
  }
  
  func jumpTo( position : ScoreSelection ) {
    txtArea?.setCaretPositionAt(offset: position.offset,
                                length: position.length,
                                expected: position.content
                              )
  }
  
  func resetToEmpty() {
    parentController?.curEditedFile = nil
    self.content = ""
  }
  
  func reload() {
    if let txtArea = self.txtArea {
      if let editedFile = txtArea.currentEditedFile {
        loadSelected(file: editedFile , tune: editedFile.tuneNumber)
      }
    }
  }
  
  func display( file: ABCEditedFile , tune: TuneDescription? ) {
    loadSelected(file: file, tune: tune)
  }
  
}
