//
//  ABCResourcePanelControler.swift
//  ABCStudio
//
//  Created by jymengant on 6/23/18 week 25.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack


class ABCResourcePanelController : NSViewController {
  
  @IBOutlet weak var titleField: NSTextField!
  
  @IBOutlet weak var urlLocationField: NSTextField!
  @IBOutlet weak var dateField: NSDatePickerCell!
  
  @IBOutlet weak var soundFileUrl: NSTextField!
  @IBOutlet weak var scoreImageURL: NSTextField!
  @IBOutlet weak var type: NSPopUpButton!
  
  @IBOutlet weak var image: NSImageView!
  @IBOutlet var commentField: NSTextView!
  
  @IBOutlet weak var createB: NSButton!
  @IBOutlet weak var deleteB: NSButton!
  @IBOutlet weak var updateB: NSButton!
  @IBOutlet weak var scanB: NSButton!
  // @IBOutlet weak var encodingCombo: EncodingCombo!
  
  private var data : ABCResourcePanelData? = nil
  private var curType : ABCResType = ABCResType.ABCScore
  private var existing : Bool = false

  var ftController: ABCFileTreeController? = nil
  
  private func emptyURL() -> Bool {
    if ( urlLocationField.stringValue.count > 0) {
      return false
    }
    return true
  }
  private func emptyName() -> Bool {
    if ( titleField.stringValue.count > 0 ) {
      return false
    }
    return true
  }

  private func validateFields() {
    var valid : Bool = false
    switch ( curType ) {
    case .Group :
      valid =  !emptyName()
      urlLocationField.isEnabled = false
      soundFileUrl.isEnabled = false
      scoreImageURL.isEnabled = false
      break
    case .GroupWithFolder  :
      valid = !(emptyName() || emptyURL() )
      urlLocationField.isEnabled = true
      break
    case .ABCScore :
      valid = !emptyName()
      break
    }
    if ( existing ) {
      // prevent type change
      type.isEnabled = false
      dateField.isEnabled = false
      updateB.isEnabled = valid
      deleteB.isEnabled = true
      if ( curType == .GroupWithFolder ) {
        scanB.isEnabled = true
      }
    } else {
      createB.isEnabled = valid
      self.scanB.isEnabled = valid
    }
  }
  
  
  /**
    ABC Score type changed
  */
  @IBAction func typeChanged(_ sender: NSPopUpButton) {
    switch ( type.indexOfSelectedItem ) {
    case 0 :
      curType = ABCResType.ABCScore
      break
    case 1 :
      curType = ABCResType.Group
      break
    case 2 :
      curType = ABCResType.GroupWithFolder
      break
    default :
      break
    }
    self.validateFields()
  }
  
  /* populate data */
  
  func setRessourceData( resource : ABCResourcePanelData ) {
    data = resource
  }
  
  func setTitle( _ title: String? ) {
    titleField.stringValue = title ?? ""
  }
  
  func setLocationField( url: URL? ) {
    urlLocationField.stringValue = url?.absoluteString ?? ""
  }
  
  func setImageField( url: URL? ) {
    scoreImageURL.stringValue = url?.absoluteString ?? ""
  }
  
  func setSoundField( url: URL? ) {
    soundFileUrl.stringValue = url?.absoluteString ?? ""
  }
  
  func setDateField( creationDate: Date? ) {
    dateField.dateValue = creationDate ?? Date()
  }
  
  func setCommentField( _ comment: String? ) {
    commentField.string = comment ?? ""
  }

  /* end populate data */
    
  func setTypeField ( type: ABCResType ) {
    self.scanB.isEnabled = false // defaulted
    switch type {
    case .Group :
      self.type.selectItem(at: 1)
      break
    case .GroupWithFolder :
      self.type.selectItem(at: 2)
      break
    case .ABCScore :
      self.type.selectItem(at: 0)
      break
    }
    curType = type
  }

  // Populate data to interface fields
  private func populateDataIN() {
    if let data = data  {
      // Populate data
      setTitle(data.title)
      setLocationField(url: data.url)
      setImageField(url: data.imageURL)
      setSoundField(url: data.soundURL)
      setTypeField(type:data.type)
      // force .groupWithFolder for existing url directories
      if let url = data.url {
        let fu = FileUtils()
        if ( fu.existFile(url: url, directory: true ) ) {
          setTypeField(type: .GroupWithFolder)
        }
      }
      setDateField(creationDate: data.creationDate)
      setCommentField(data.comments)
      self.existing = data.existing
      // let strEncoding = Unicode.fromEncoding(encoding: data.encoding)
      //encodingCombo.selectItem(withObjectValue: strEncoding)
    }
    titleField.delegate = self // capture input data
  }
  
  //
  // Just assume that a local absolute path starts with /
  // everything else is assumed to be URL
  // (This may be imroved later if necessary ...)
  //
  private func isUrlFormat( string : String ) -> Bool {
    if ( string.starts(with: "/")) {
      return false
    }
    return true
  }
  
  private func validateURL( string : String ) -> URL? {
    let url = URL(string: string )
    return url
  }
  
  private func checkURL( field : NSTextField , createFile : Bool = false ) -> URL? {
    if ( self.curType == .ABCScore )  {
      if ( field.stringValue.count > 0 ) {
        if ( isUrlFormat(string: field.stringValue )) {
          return validateURL(string: field.stringValue)
        } else {
          return URL(fileURLWithPath: field.stringValue)
        }
      } else {
        if ( createFile ) {
          // New file created in defStorage using title name
          let fName = data!.title! + data!.type.getFileSuffix()!
          return preferences.defStorage!.appendingPathComponent(fName,isDirectory: false)
        }
      }
    }
    return nil
  }
  
  
  // populate interface fields to data
  private func populateDataOUT() {
    // let encodings = Unicode.encodings
    data!.type = self.curType
    data!.creationDate = dateField.dateValue
    data!.title = titleField.stringValue
    data!.url = checkURL(field: urlLocationField , createFile : true)
    data!.imageURL = checkURL(field: scoreImageURL)
    data!.soundURL = checkURL(field: soundFileUrl)
    data!.comments = commentField.string
    //data!.encoding = encodings[encodingCombo.objectValueOfSelectedItem as! String]!
  }
  
  override var nibName: NSNib.Name? {
    return "ABCResourcePanel"
  }
  
  override func viewDidLoad() {
    DDLogDebug("ABCResourcePanelController entering viewDidLoad")
    // populateEncoding()
    populateDataIN()
    validateFields()
    super.viewDidLoad()
  }
  
  @IBAction func updateAction(_ sender: NSButton) {
    DDLogDebug("ABCResourcePanelControler entering updateAction")
    populateDataOUT()
    ftController?.updateTreeElement(data: data)
    ftController?.dismiss(self)
  }
  
  @IBAction func deleteAction(_ sender: NSButton) {
    DDLogDebug("ABCResourcePanelControler entering deleteAction")
    ftController?.removeAction(self)
    ftController?.dismiss(self)
  }
  
  /**
    add new folder or ABC score
  */
  @IBAction func addAction(_ sender: NSButton) {
    DDLogDebug("ABCResourcePanelControler entering addAction")
    self.populateDataOUT()
    if let parent = ftController {
      parent.dismiss(self)
      parent.addTreeElement(data: data)
      parent.dropCleanup() // reset potential drop position
    }
  }
  
  @IBAction func scanAction(_ sender: Any) {
    DDLogDebug("ABCResourcePanelControler entering scanAction")
    populateDataOUT()
    ftController?.dismiss(self)
    // show scanning pannel
    let scannerController = ABCScoreChooserController()
    scannerController.folderData = self.data
    scannerController.parentController = ftController
    scannerController.existing = self.existing
    ftController?.presentAsSheet(scannerController)
  }
  
  @IBAction func closeAction(_ sender: NSButton) {
    DDLogDebug("ABCResourcePanelControler entering closeAction")
    DDLogDebug("TestController quit action")
    // parentController?.dismiss(self)
    NSApp.keyWindow?.close()
  }
  
  
  @IBAction func openFileDialog(_ sender: Any) {
    DDLogDebug("Open file dialog requested")
    let dialog = NSOpenPanel()
    dialog.title                   = loc.tr("Choose a abc xml folders");
    dialog.showsResizeIndicator    = true;
    dialog.showsHiddenFiles        = false;
    dialog.canChooseDirectories    = true;
    dialog.canCreateDirectories    = true;
    dialog.allowsMultipleSelection = false;
    dialog.allowedFileTypes        = ["abc" , "xml"]
    
    if (dialog.runModal() == NSApplication.ModalResponse.OK) {
      let result = dialog.url // Pathname of the file
      
      if (result != nil) {
        let path = result!.path
        urlLocationField.stringValue = path
      }
    } else {
      // User clicked on "Cancel"
      return
    }
  }
}

/* Capture TextFieldChange */


extension ABCResourcePanelController : NSTextFieldDelegate {
  func controlTextDidChange(_ obj: Notification)
  {
    // Just proceed to validationG
    self.validateFields()
  }
}



