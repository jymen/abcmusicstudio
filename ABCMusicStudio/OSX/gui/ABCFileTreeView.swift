//
//  ABCFileTreeView.swift
//  ABCStudio
//
//  Created by jymengant on 8/16/18 week 33.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import EmitterKit


class ABCFileTreeView : NSOutlineView {
  
  var curItem : Any? = nil
  var controller : ABCFileTreeController? = nil
  var dropper : URLViewDropper?


  var highlightForDragging : Bool = false {
    didSet {
      needsDisplay = true
    }
  }
  
  private func buildMenu( row : Int ) -> NSMenu? {
    if ( row < 0 ) {
      return nil
    }
    curItem = self.item(atRow: row)
    let menu = NSMenu()
    let deleteMenu : NSMenuItem = NSMenuItem(title:loc.tr("Delete") , action: #selector(deleteAction),keyEquivalent:"D")
    let nScoreMenu : NSMenuItem = NSMenuItem(title:loc.tr("New Score") , action: #selector(newScoreAction),keyEquivalent:"")
    let nGroupMenu : NSMenuItem = NSMenuItem(title:loc.tr("New Group") , action: #selector(newGroupAction),keyEquivalent:"")
    let nGroupeFolderMenu : NSMenuItem = NSMenuItem(title:loc.tr("New Group with folder") , action: #selector(newGroupFolderAction),keyEquivalent:"")

    menu.addItem(nScoreMenu)
    menu.addItem(nGroupMenu)
    menu.addItem(nGroupeFolderMenu)
    menu.addItem(NSMenuItem.separator())
    menu.addItem(deleteMenu)
    return menu
  }
  
  func delete() {
    DDLogDebug("Entering delete")
    let dataSource = self.delegate as! ABCFileTreeDataSource
    let callback : (_ data: ABCEvent) -> () = {
      // Asynch Promise deletion callback
      data in
      // Filter events
      if ( ( data.type == .TreeElementRemoved ) || ( data.type == .TreeElementDeleted ) ) {
        print( "entering delete callback" )
        // select parent folder
        if let parent = data.msg as? Folder {
          self.reloadItem(parent, reloadChildren: true)
        }
        // cleanup textArea for selected item
        self.controller?.txtController?.cleanupEditor()
        // cleanup assoc svg views
        self.controller?.parentController?.svgCleanup()
        // thow deleted file away from editor
        self.editorsCleanup()
        // save modified repository tree
        do {
          _ = try self.controller?.getSerialized()
        } catch {
          let err = error as! ABCFailure
          if ModalAlert().show(question: "ABCMusicStudio Repository save  error", text: "\(err.message!)", style: .Critical){}

        }
        print( "leaving delete callback" )
      }
      
    }
    EventManager.shared.addOnce(listener:callback)
    dataSource.removeSelected()
    
  }
  
  
  @objc func deleteAction( sender : Any? ) {
    delete()
  }
  
  @objc func newScoreAction( sender : Any? ) {
    controller?.showABCElementSheet(url: nil,type: .ABCScore)
  }
  
  @objc func newGroupAction( sender : Any? ) {
    controller?.showABCElementSheet(url: nil,type: .Group)
  }
  
  @objc func newGroupFolderAction( sender : Any? ) {
    controller?.showABCElementSheet(url: nil,type: .GroupWithFolder)
  }
  
  private func editorsCleanup() {
    if let controller = controller {
      if let mainCtrl = controller.parentController {
        if let dispatcher = mainCtrl.dispatcher {
          dispatcher.resetToEmpty()
        }
      }
    }
  }
  
  
  override func menu(for event: NSEvent) -> NSMenu? {
    let point = convert(event.locationInWindow, from: nil)
    let row = self.row(at: point)
    _ = self.item(atRow: row)
    
    // ...
    
    return self.buildMenu(row: row)
  }
  
  func selectRow(at index: Int) {
    selectRowIndexes(.init(integer: index), byExtendingSelection: false)
    if let action = action {
      perform(action)
    }
  }
  
  //
  // Safe data reloading on selected row keeping select position
  //
  func reloadSelectedRow() {
    let dataSource = self.delegate as! ABCFileTreeDataSource
    guard let selectedRow = dataSource.selectedRow else {
      return
    }
    let selectSet = IndexSet(integer:selectedRow)
    let columnSet = IndexSet(arrayLiteral: 0,1)
    let selectedItem = self.item(atRow: selectedRow)
    // force subtree reloading on selected item
    self.reloadItem(selectedItem, reloadChildren: true)
    self.reloadData(forRowIndexes: selectSet, columnIndexes: columnSet)
  }

  /**
   graphically notifying user of drag drop in progress
   */

  override func draw(_ dirtyRect: NSRect) {
    DDLogDebug("ENTERING DRAW")
    dropper?.colorBorder(highlight: highlightForDragging , stdWidth: 3.0, highlightWidth: 10.0)
  }

}


