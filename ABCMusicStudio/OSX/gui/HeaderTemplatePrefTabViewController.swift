//
//  HeaderTemplatePrefTabViewController.swift
//  ABCMusicStudio
//
//  Created by jymengant on 2/14/21 week 6.
//  Copyright © 2021 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Preferences
import EmitterKit



class HeaderTemplatePrefTabViewController : NSTabViewController ,
Preferenceable {
  
  let toolbarItemTitle = "ABC Header template"
  let toolbarItemIcon = NSImage(named: "notes")!
  private var eventListener : EventListener<ABCEvent>?
  private var prefsController : TemplateHeaderPrefsController!


  override var nibName: NSNib.Name? {
    return "TemplateHeaderPreferences"
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    let abcItem: NSTabViewItem = NSTabViewItem(identifier: "ABCColorsFonts")
    abcItem.label = "ABC Header template"
    self.prefsController = TemplateHeaderPrefsController()
    abcItem.viewController = self.prefsController
    
    addTabViewItem(abcItem)
    
    // Do any additional setup after loading the view.
    // Listen for coloring changes
    eventListener = EventManager.shared.add(listener:coloringChanged)
  }

  private func coloringChanged( _ data: ABCEvent ) {
    DDLogDebug("coloring change callback entered for Header template prefs")
    self.prefsController.coloringChanged(data)
  }
  

  
}
