//
//  ABCScoreBuffer.swift
//  ABCStudio
//
//  Created by jymengant on 8/5/19 week 32.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa

/// SINGLETON : dirty score buffer
var scoreBuffer = ABCScoreBuffer() 

class ABCScoreBuffer {
  var buffer : [URL : String] = [URL : String] ()
  
  func addScore( url : URL , content: String ) {
    buffer[url] = content
  }
  
  func removeScore( url : URL ) {
    buffer.removeValue(forKey: url)
  }
  
  func getScore( url : URL ) -> String? {
    return buffer[url]
  }
  
  func isBuffered( url : URL ) -> Bool {
    if (  buffer[url] != nil ) {
      return true
    }
    return false
  }

  func isDirty( file : File ) -> Bool {
    if let url = file.infos.storageURL {
      return isBuffered(url: url)
    }
    return false
  }
  
}




