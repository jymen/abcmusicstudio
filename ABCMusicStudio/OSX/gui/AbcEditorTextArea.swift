//
//  AbcEditorTextArea.swift
//  ABCStudio
//
//  Created by jymengant on 5/29/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import EmitterKit
import Combine


/**
 
 Abc source File Editor
 
 */

//
// TextArea text position communication structure
//
struct ScoreTextContext {
  let content: String?
  let isNote : Bool
  let startPosition : Int
  let endPosition : Int
  let curTune : TuneDescription?
  let curTuneIndex: Int
  let previousTuneIndex: Int
}

struct ABCEditorChange {
  let position : ScoreTextContext?
  let content: String?
  let isXml: Bool
  let tune:TuneDescription?
  let tunePosition: Int
}

class ABCEditorTextArea : NSTextView  ,
                          NSTextViewDelegate {
  
  
  // private let timer = AbcTimer(timeInterval:4) // wait 4 seconds before refreshing
  private var timer: Timer?
  private var eventListener : EventListener<ABCEvent>?
  var spinner : ABCSpinner?
  override var acceptsFirstResponder: Bool { return true }
  private var hasFocus : Bool = false
  private var programmaticCaretChange : Bool = false
  //
  // communication upon text caret position changes
  //
  private let subject = PassthroughSubject<ABCEditorChange,Never>()
  var changePublisher : AnyPublisher<ABCEditorChange,Never>?
  var currentPos : ScoreTextContext? {
    didSet {
      if let context = currentPos {
        subject.send(ABCEditorChange(position:context,content:nil,isXml:false,tune:nil, tunePosition: currentTunePos))
      }
    }
  }
  var templateCarretPosition : Int = 0
  
  var highlightForDragging : Bool = false {
    didSet {
      needsDisplay = true
    }
  }

  var dropper : URLViewDropper? {
    get {
      if let controller = parentController {
        return controller.dropper
      }
      return nil
    }
  }
  
  // populated by parent controller if any
  var msgArea : ABCMessageArea? = nil
  // text coloring class
  var coloring : TextColoring? = nil
  var abcIR : ABCIR? = nil // current abc intermediate representation if any
  // Lines / sourceOffsets start location
  var linesOffsets : [Int]? = nil
  // True => populate error on text
  struct ParsingError {
    let token : ABCToken
    let msg : String
  }
  var parsingErrors : [ParsingError]? = nil
  var errorPopover : ABCPopOver? = nil
  var mainViewController : EditorWindowController? = nil
  var ftController : ABCFileTreeController?  {
    get {
      if let mainController = mainViewController {
        return mainController.ftController
      }
      return nil
    }
  }
  var parentController : ABCEditorController? = nil
  var currentSelectedTune : TuneDescription? = nil {
    didSet {
      print("current selected changed")
    }
  }
  
  var currentEditedFile : ABCEditedFile? {
    get {
      if let controller = parentController {
        return controller.curEditedFile
      }
      return nil
    }
  }

  var currentTunePos : Int {
    get {
      if let tune = currentSelectedTune {
        if let file = currentEditedFile {
          return file.file.indexOf(tune: tune)
        }
      }
      return 0
    }
  }
  var isTemplate : Bool = false
  
  var  placeHolderTitleString: NSMutableAttributedString? = nil
  
  
  // Disable grammar and spell checking
  override var isContinuousSpellCheckingEnabled: Bool {
    get { return false }
    set {}
  }
  
  override var isGrammarCheckingEnabled: Bool {
    get { return false }
    set {}
  }
  
  override var isAutomaticQuoteSubstitutionEnabled: Bool {
    get { return false }
    set {}
  }
  
  func startUp( delegate : NSTextViewDelegate? = nil ) {
    self.addObservers()
    // init notifications
    changePublisher = subject.eraseToAnyPublisher()
    self.toolTip = nil
    if let delegate = delegate {
      self.delegate = delegate
    } else {
      self.delegate = self // not provided assume self
    }
    self.allowsUndo = true // enable default NstextView undo redo
    self.coloring = TextColoring(text:self)
    // Listen for coloring changes
    eventListener = EventManager.shared.add(listener:coloringChanged)
    // Init Spinner
    spinner = ABCSpinner(view:self)
  }
  
  private func coloringChanged( _ data: ABCEvent ) {
    DDLogDebug("coloring change callback entered")
    // Filter events
    if ( data.type == .SyntaxColoringChanged )  {
      DDLogDebug("Populating coloring change in current editor")
      if let curFile = self.currentEditedFile {
        if let _ = curFile.content {
          let msg : String = data.msg as! String
          if ( (msg == "ABCColoringChanged") && ( curFile.getScoreType() == .ABCScore )) {
            if let ir = self.abcIR {
              self.coloring!.abcColoring(ir: ir)
            }
          }
        }
      }
    }
  }
  
  func cleanupEditedFile() {
    self.currentSelectedTune = nil
    self.cleanup()
    mainViewController?.selectScoreTabs(at: ABCItemOffset.ScoreOffset.rawValue)
  }
  
  func setCaretPositionAt( offset : Int , length : Int = 0 , expected : String? = nil) {
    let curRange = NSMakeRange(offset, length)
    let curStr  = self.string.substring(with: curRange)
    if ( expected != nil ) {
      if ( expected != curStr?.string ) {
        DDLogError("Bad score location : expecting = \(String(describing: expected)) ; found = \(String(describing: curStr?.string))")
      }
    }
    if let window = self.window {
      if ( window.makeFirstResponder(self) ) {
        self.setSelectedRange(NSMakeRange(offset, length))
        self.scrollRangeToVisible(NSMakeRange(offset,length))
        programmaticCaretChange = true
      }
    }
  }
  
  func addTooltip( range : NSRange , content : String?  ) {
    self.textStorage!.addAttribute(NSAttributedString.Key.toolTip,value: content as Any, range:range)
  }
  
  /// Add observers to redraw the line number gutter, when necessary.
  internal func addObservers() {
    self.postsFrameChangedNotifications = true
    NotificationCenter.default.addObserver(self, selector: #selector(drawGutter), name: NSView.frameDidChangeNotification, object: self)
    // NotificationCenter.default.addObserver(self, selector: #selector(drawGutter), name: NSText.didChangeNotification, object: self)
  }
  
  /// Set needsDisplay of lineNumberGutter to true by notification.
  @objc internal func drawGutter(notification : NSNotification) {
    parentController?.drawLineGutter()
  }
  
  func saveEditorContent() throws {
    guard let editedFile = currentEditedFile else {
      return
    }
    try editedFile.saveContent()
    ftController?.reloadData() // reset dirty flag
  }
    
  private func resetTxtContent() {
    let fullRange = NSRange(location: 0, length: self.attributedString().length)
    self.textStorage?.deleteCharacters(in: fullRange)
  }
  
  //
  // cleanup all content
  //
  func cleanup() {
    DDLogDebug("TextArea is cleaning up ...")
    resetTxtContent()
    // cleanup lineNumber gutter as well
    parentController?.cleanupGutter()
    // cleanup popover parsing message if any
    resetParsing()
  }
  
  override func draw(_ rect: NSRect) {
    super.draw(rect)
    if ( isTemplate ) {
      return
    }
    if let editedFile = currentEditedFile {
      if ( !editedFile.isEmpty ) {
        return
      }
    }
    if let coloring = coloring {
      if ( placeHolderTitleString == nil ) {
        placeHolderTitleString = coloring.syntaxColor.color(string:  loc.tr("Manually type or drop here ABC,MusicXml or MuseScore source ..."), type: .PlaceHolder)
      }
    }
    if ( self.string.count == 0 ) {
      if let placeHolder = placeHolderTitleString {
        placeHolder.draw(at: NSMakePoint(2, 0))
        
      }
    }
  }
  
  private func showParsingErrors( token : ABCToken , msg : String ) -> ABCPopOver {
    let controller = MessagePopoverController(text: msg , tkType : .Error)
    let popOver = ABCPopOver(sender: self)
    let curRange = coloring!.colorAbcText(offset: token.start , length: token.length,  type : .Error)
    addTooltip(range: curRange, content: msg)
    self.layoutManager?.ensureLayout(forCharacterRange: curRange)
    self.scrollRangeToVisible(curRange)
    parentController?.setGutterError(line:  token.line+1 )
    popOver.show(content: controller, bounds: nil)
    return popOver
  }
  
  private func showFatals( error : String ) {
    msgArea?.displayError(error)
  }
  
  override func didChangeText() {
    super.didChangeText()
  }
  
  func showSelectedABC( forTune : TuneDescription? ) {
    if let source = self.currentEditedFile?.content {
      parseContentAsynch( src: source ,
                          forTune : forTune ,
                          endParsingHandler: parsingSelectionReturned )
    }
  }
  
  func showABC(forTune : TuneDescription? ) {
    guard let forSelected = self.currentEditedFile else {
      return
    }
    if let source = forSelected.content {
      parseContentAsynch( src: source ,
                          forTune : forTune ,
                          endParsingHandler: parsingSelectionReturned )
    }
  }

  private func showImageIfAny( withFile : ABCEditedFile) {
    if ( withFile.hasExtraImage ) {
      mainViewController?.selectScoreTabs(at: ABCItemOffset.ExtraOffset.rawValue)
    } else {
      mainViewController?.selectScoreTabs(at: ABCItemOffset.ScoreOffset.rawValue)
    }
  }
  
  private func templateParsingSelectionReturned( ir : ABCIR ) {
    DDLogDebug("returning from parsing for template")
    self.abcIR = ir
    let source =  ir.lex.abcSrc
    let text = coloring!.syntaxColor.color(string: source, type: .Text)
    // put attributed colored text for parsed score
    self.resetTxtContent() // cleanup old content
    self.textStorage!.append(text)
    self.coloring!.abcColoring(ir:ir)
    // handle errors if any
    checkParsingErrors(ir:ir)
    setCaretPositionAt(offset: templateCarretPosition)
  }

  func showTemplate( source : String? = nil  ) {
    var src = source
    if source == nil {
      src = self.string
    }
    self.resetParsing() // cleanup previous if any
    parseContentAsynch( src: src! ,
                        forTune : nil ,
                        endParsingHandler: templateParsingSelectionReturned )

  }
  
  func showScoreSource( file: File , forTune : TuneDescription? ) {
    self.cleanup() // remove previous text if any
    // self.currentEditedFile = file
    if let editedFile = self.currentEditedFile {
      do {
        if ( editedFile.exists ) {
          try editedFile.loadScore()
          if ( editedFile.isEmpty) {
            // Check for Image
            showImageIfAny(withFile: editedFile)
          } else {
            if ( editedFile.getScoreType() == ABCResType.ABCScore ) {
              showSelectedABC(forTune:forTune)
            }
            mainViewController?.checkScoreViewVisible()
          }
        } else {
          showImageIfAny(withFile: editedFile)
        }
      } catch let error as ABCFailure{
        if ModalAlert().show(question: "Error loading file", text: "\(String(describing: error.message))", style: .Critical){}
      } catch {}
    }
  }
  
  func jumpTo( tune : TuneDescription ) {
    guard let tunes = self.currentEditedFile?.file.tunes else {
      return
    }
    for curtune in tunes  {
      if curtune.title == tune.title {
        if let offset = curtune.offset {
          self.setCaretPositionAt(offset: offset)
        }
      }
    }
  }
  
  private func getTune( atOffset: Int ) -> TuneDescription? {
    guard let tunes = self.currentEditedFile?.file.tunes else {
      return nil
    }
    if ( tunes.count > 1) {
      for index in 0 ..< tunes.count  {
        if ( index+1 == tunes.count ) {
          return tunes[index]
        }
        let start = tunes[index].offset!
        let ends = tunes[index+1].offset!
        if ( atOffset >= start && atOffset < ends ) {
          return tunes[index]
        }
      }
    }
    return tunes[0]
  }
    
  private func parsingSelectionReturned( ir : ABCIR ) {
    DDLogDebug("returning from parsing for new selection")
    self.abcIR = ir
    let source =  ir.lex.abcSrc
    // refresh tunelist after parsing
    self.currentEditedFile?.file.tunes = ir.tuneList

    let text = coloring!.syntaxColor.color(string: source, type: .Text)
    // put attributed colored text for parsed score
    self.resetTxtContent() // cleanup old content
    self.textStorage!.append(text)
    
    self.coloring!.abcColoring(ir:ir)
    // update linesOffsets
    self.linesOffsets = ir.lex.linesOffsets
    parentController?.linesOffsets = self.linesOffsets
    parentController?.drawLineGutter()
    // update fileTree datasource
    ftController?.dataSource.handleMultipleTunes(tunes: ir.tuneList)
    ftController?.abcTree.reloadSelectedRow() // reload
    
    // handle errors if any
    checkParsingErrors(ir:ir)
    //
    if let curTune = self.currentSelectedTune {
      self.jumpTo(tune: curTune)
    }
  }
  
  //
  // for performance reason do ABC parsing asynchronously
  //
  private func parseContentAsynch( src : String ,
                           forTune : TuneDescription? ,
                           endParsingHandler : @escaping (ABCIR) -> Void = { _ in } ) {
    DDLogDebug("entering parseContent ")
    self.currentSelectedTune = forTune
    if (src.count ==  0 ) {
      return
    }
    if ( self.spinner!.start() ) { // Show spinner
      let opStub : ParsingOperationStub = ParsingOperationStub(src: src)
      opStub.getIr().done { (ir) in
        self.spinner!.stop() // release spinner
        // self.parsingSelectionReturned(ir: ir)
        endParsingHandler(ir)
      }.catch { (err) in
        DDLogDebug("NO errors are expected here")
      }
    }
  }
  
  private func checkParsingErrors( ir : ABCIR ) {
    guard let error = ir.errors else {
      return
    }
    // just syntax parsing ABC errors to handle
    let token = error.token
    DDLogDebug("text offsets : \(token.start) , length : \(token.length)   ")
    let content = self.string[token.start..<token.stop]
    DDLogDebug("text content : \(content)")
    errorPopover = showParsingErrors(token: token, msg: String(describing: error.message))
    //showFatals(error: "ABC Parsing error \(String(describing: error.message))" )
  }
  
  private func parsingUpdatesReturned(ir : ABCIR ) {
    DDLogDebug("Returning from parsing UPDATES")
    self.abcIR = ir
    // update text positions inside editor for accuracy
    textPositionChanged()
    //
    coloring!.abcColoring(ir:self.abcIR!)
    linesOffsets = ir.lex.linesOffsets // get lines/ source offsets
    parentController?.linesOffsets = linesOffsets
    parentController?.drawLineGutter() // refresh lines with new linesOffsets
    // update fileTree datasource
    if let controller = ftController {
      controller.dataSource.recoverCurrenSelection(abcTree: controller.abcTree)
      controller.dataSource.handleMultipleTunes(tunes: ir.tuneList)
      controller.abcTree.reloadSelectedRow() // reload
    }
    // handdle errors if any
    checkParsingErrors(ir:self.abcIR!)
    // refresh score
    refreshScoreAndDependencies()
    // Notify score change
    subject.send(ABCEditorChange(position:nil,content:self.string,isXml:false,tune:self.currentSelectedTune, tunePosition: currentTunePos))
    // reset asynch timer finally
    self.timer = nil
  }
  
  private func parseContent() {
    DDLogDebug("entering parseContent ")
    if ( self.string.count ==  0 ) {
      return
    }
    let parser : ABCParser = ABCParser(src: self.string )
    do {
      self.abcIR = try parser.parse()
      parsingUpdatesReturned(ir: self.abcIR!)
    } catch {
      // SEVERE parsing ERRORS (Not syntax ...)
      showFatals(error: "ABC unexpected error in parser\(String(describing: error))")
    }
  }
  
  //
  // cleanup last ABC parsing
  //
  private func resetParsing() {
    DDLogDebug("entering parsing cleanup ")
    parsingErrors = nil // cleanup errors
    if let pop = errorPopover {
      pop.dismiss()
    }
    parentController?.clearGutterError()
    errorPopover = nil
  }
  
  private func refreshScoreAndDependencies() {
    guard let curEdited = self.currentEditedFile else {
      return
    }
    parentController?.linesOffsets = self.linesOffsets
    // update dirty score buffer
    
    if let _ = curEdited.content {
      ftController?.reloadData() // refresh tree dirty file display
      //
      // var scoreType = ABCResType.ABCScore
      _ = curEdited.getScoreType()
    }
  }
  
  @objc private func updateTimer() {
    DDLogDebug("TextAreaActivity IS NOW OVER => UPDATE BUFFER/GUI")
    if let curEdited = self.currentEditedFile {
      curEdited.content = self.string
      if ( curEdited.needsParsing ) {
        self.resetParsing() // cleanup previous if any
        if ( curEdited.getScoreType() == .ABCScore ) {
          
          parseContentAsynch(src: curEdited.content!,
                             forTune: nil ,
                             endParsingHandler: parsingUpdatesReturned)
          
        }
      }
    }
  }
  
  private func textPositionChanged() {
    let currentPos = scoreContext(textView: self)
    self.currentSelectedTune = currentPos?.curTune
    self.currentPos = currentPos
  }
  
  private func newScore() {
    ftController?.showABCElementSheet(url: nil,type: .ABCScore)
  }
  
  //
  // abcTxtArea delegate methods
  //
  func textDidChange(_ notification: Notification) {
    guard (notification.object as? NSTextView) != nil else { return }
    // DDLogDebug(textView.string)
    DDLogDebug("TEXT CONTENT has been changed ")
    // deal with plain new score case
    if let curFile = self.currentEditedFile {
      if ( !curFile.isDefined ) {
        newScore()
      }
      // document is now requesting reparsing
      curFile.needsParsing = true // Force reparsing
      // update scoreBuffer
      // put in buffer to preseve any score switch
      if let curFileUrl =  curFile.file.infos.storageURL {
        scoreBuffer.addScore(url: curFileUrl, content: self.string)
        // reload selected tree element to display updated color on tree
        ftController?.reloadData()
      }
    } else {
      newScore()
    }
    // trigger 2 second timer update
    if ( timer == nil ) {
      timer = Timer.scheduledTimer(timeInterval: 2.0,
                                   target: self,
                                   selector: #selector(updateTimer),
                                   userInfo: nil,
                                   repeats: false)
    }
  }
  
  private func isNote(at: Int , ending : Int ) -> Bool {
    if let ir = abcIR {
      if let _ = ir.getNote(at: at, ending: ending) {
        return true
      }
    }
    return false
  }
  
  private func getIndex(ofTune : TuneDescription?) -> Int{
    guard let file = self.currentEditedFile?.file ,
          let tune = ofTune
    else {
      return 0
    }
    return file.indexOf(tune: tune)
  }

  private func scoreContext(textView : NSTextView ) -> ScoreTextContext? {
    guard let string = textView.textStorage?.string else {
        return nil
    }
    if let nsRange = textView.selectedRanges.first as? NSRange,
      let range = Range(nsRange, in: string) {
      let startPosition : Int = range.lowerBound.utf16Offset(in:string)
      let endPosition : Int = range.upperBound.utf16Offset(in:string)
      DDLogDebug("text position : \(startPosition)")
      let lastTune : Int = getIndex(ofTune: currentSelectedTune)
      currentSelectedTune = getTune(atOffset: startPosition)
      DDLogDebug("currentTune : \(currentSelectedTune?.number ?? 0)")
      let newTune : Int = getIndex(ofTune: currentSelectedTune)
      DDLogDebug("content: \(range.lowerBound.utf16Offset(in:string)) \(range.upperBound.utf16Offset(in:string))")

      return ScoreTextContext(content: string,
                              isNote: isNote(at:startPosition-1, ending: endPosition-1),
                              startPosition: startPosition,
                              endPosition: endPosition,
                              curTune : currentSelectedTune ,
                              curTuneIndex: newTune ,
                              previousTuneIndex: lastTune
                             )
    }
    return nil
  }
  
  override func becomeFirstResponder() -> Bool {
    hasFocus = true
    DDLogDebug("txtArea has the focus")
    return true
  }
  
  override func resignFirstResponder() -> Bool {
    DDLogDebug("txtArea lost the focus")
    hasFocus = false
    return true
  }

  //
  // Caret position has changed
  //
  public func textViewDidChangeSelection(_ notification: Notification) {
    if ( hasFocus ) {
      //
      // only when txtArea has the focus and carret changed by user
      // we then need to sync the displayed score accordingly
      //
      if ( programmaticCaretChange ) {
        programmaticCaretChange = false
      } else {
        textPositionChanged()
      }
    }
  }
  
  //
  // Drop file management
  // => Create a new score in tree's current folder location using
  // dropped URL ( same as drop in tree )
  //
  
  private func shouldAllowDrag(_ sender: NSDraggingInfo) -> Bool {
    // To be extended if needed
    return true
  }
  
  override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
    DDLogDebug("ABCFileTreeView draggingEntered entered")
    let allow = shouldAllowDrag(sender)
    highlightForDragging = allow
    return allow ? .copy : NSDragOperation()
  }

  override func draggingExited(_ sender: NSDraggingInfo?) {
    highlightForDragging = false
    DDLogDebug("ABCFileTreeView drop exit")
  }
  
  override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
    let allow = dropper!.shouldAllowDrag(sender)
    return allow
  }

  override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
    let pasteboard = sender.draggingPasteboard
    if let urls = pasteboard.readObjects(forClasses: [NSURL.self], options:nil) as? [URL], urls.count > 0 {
      ftController?.processTxtAreaDropURLs(url: urls[0])
      DDLogDebug("Url drop for : \(urls[0].absoluteString) has been accepted")
      // TODO : request here score creation for URL at current folder tree location
      return true
    }
    return false
  }

}
