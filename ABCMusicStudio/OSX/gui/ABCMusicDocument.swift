//
//  Document.swift
//  ABCStudio
//
//  Created by jymengant on 5/26/18 week 21.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

/**
 Define ABC music document Model
 */
class ABCMusicDocument: NSDocument {
  
  var editor : EditorWindowController? = nil
  var treeController : ABCFileTreeController? {
    get {
      if let editor =  editor  {
        return editor.ftController
      }
      return nil
    }
  }
  var printImage : NSImage? = nil
  
  override init() {
    super.init()
    // Add your subclass-specific initialization here.
    mainWindowController.document = self
  }
  
  // Create new document (only called for new documents)
  convenience init?(type typeName: String, error outError: NSErrorPointer) {
    self.init()
    fileType = typeName
    mainWindowController.document = self
    // add your own initialisation for new document here
  }
  
  override class var autosavesInPlace: Bool {
    return true
  }
  
  override func fileNameExtension(forType typeName: String,
                         saveOperation: NSDocument.SaveOperationType) -> String? {
    if ( typeName == "ABCMusicStudioDocument" ) {
      return "abcmusicstudio"
    }
    return nil
  }
  
  /**
    Handle ABCDesigner split window
  */
  override func makeWindowControllers() {
    // super.makeWindowControllers()
    // Dangerous Bug FIXED HERE : Check that editor has not been created before
    // else the second editor will be empty and will completely erase
    // data later when it will be saved
    if ( self.editor == nil ) {
      self.editor = EditorWindowController()
      self.addWindowController(self.editor!)
    }
  }
  
  override func writeSafely(to url: URL, ofType typeName: String, for saveOperation:
    NSDocument.SaveOperationType) throws {
    if ( typeName == "ABCMusicStudioDocument" ) {
      editor!.documentName = self.displayName // May have been changed by user in GUI (BUG #1)
      try editor!.writeDocument(to: url)
      DDLogDebug("ABCStudio catalog has been saved ...")
    }
  }
  
  /**
  */
  override func read(from data: Data, ofType typeName: String) throws {
    DDLogDebug("entering NSDocument read from Data")
    if ( typeName == "ABCMusicStudioDocument" ) {
      if ( editor == nil ) {
        editor = EditorWindowController()
        editor?.documentName = self.displayName
        addWindowController(editor!)
      }
      DDLogDebug("ABCStudio catalog Deserializing ...")
      try editor!.deserializeCurrentDocument(data)
    }
  }
  
  override func printDocument(_ sender: Any?) {
    DDLogDebug("entering print document")
    editor?.printSelectedABC()
  }
  
  //
  // ABC score document print entry point
  //
  override func printOperation(withSettings printSettings: [NSPrintInfo.AttributeKey : Any]) throws -> NSPrintOperation {
    let imageView : NSImageView = NSImageView(image: self.printImage!)
    let printInfo : NSPrintInfo = self.printInfo
    let printoperation = NSPrintOperation( view : imageView , printInfo: printInfo)
    return printoperation
  }
  
}

