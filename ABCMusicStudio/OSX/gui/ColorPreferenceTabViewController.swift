//
//  ColorPreferenceTab.swift
//  ABCStudio
//
//  Created by jymengant on 5/22/19 week 21.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Preferences


class ColorPreferenceTabViewController : NSTabViewController ,
Preferenceable {
  
  let toolbarItemTitle = "Color & Fonts"
  let toolbarItemIcon = NSImage(named: "FontsAndColors")!


  override var nibName: NSNib.Name? {
    return "TabViewPreferences"
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    let abcItem: NSTabViewItem = NSTabViewItem(identifier: "ABCColorsFonts")
    abcItem.label = "ABC Colors & Fonts"
    abcItem.viewController = ABCColorFontsPreferencesController()
    
    addTabViewItem(abcItem)
    
    // Do any additional setup after loading the view.
  }

}
