//
//  ScoreExtraInfosController.swift
//  ABCStudio
//
//  Created by jymengant on 9/5/19 week 36.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import SwiftUI
import Combine

class ScoreExtraInfoView : DragDropImageView {
  
  var controller : ScoreExtraInfoController? = nil
  var label : String = loc.tr("Drop any image or Pdf here")
  
  
  lazy var layoutManager: NSLayoutManager = {
    var layoutManager = NSLayoutManager()
    layoutManager.typesetterBehavior = .behavior_10_2_WithCompatibility
    return layoutManager
  }()
  
  func renderCenteredText(_ string:String, withColor color:NSColor , font : NSFont)  {
    let bounds = self.bounds
    let str : NSString = NSString(string: label)
    let textStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
    textStyle.alignment = NSTextAlignment.left
    
    let attributeDict: [NSAttributedString.Key : Any] = [
      .font: font,
      .foregroundColor: color,
      .paragraphStyle: textStyle,
    ]
    
    let size = str.size(withAttributes: attributeDict)
    let y = bounds.height / 2
    let x = (bounds.width / 2) - (size.width / 2 )
    let textRect = NSRect(x: x, y: y, width: size.width, height: size.height )
    
    str.draw(in: textRect, withAttributes: attributeDict)
  }
  
  //
  // override drag and drop completion
  //
  override func performDragOperation(_ draggingInfo: NSDraggingInfo) -> Bool {
    // Image has been displayed yet
    // get image url and update DataStore with it
    DDLogDebug("drag drop accepted")
    if let controller = controller {
      controller.hasDropped(url: super.dropper!.draggedUrl!)
    }
    return true
  }
  
  override func draw(_ dirtyRect : NSRect ) {
    if ( self.image == nil ) {
      self.layer!.backgroundColor = CGColor( red:0.278, green:0.278, blue:0.278, alpha:1) 
      let textColor = NSColor(calibratedRed: 0.812, green: 0.992, blue: 0.408, alpha: 0.5)
      let font  = NSFont(name: "Apple chancery", size: 20)
      renderCenteredText("Drop Image here",withColor: textColor, font: font!)
    } 
  }
  
}

extension Notification.Name {
  static var extraImageRemove: Notification.Name {
    return .init(rawValue: "ABCMusicStudio.extraImageRemoved")
  }
  static var imageBgColorChanged: Notification.Name {
    return .init(rawValue: "ABCMusicStudio.imageBgColorChanged")
  }
}

class ImageScroller : NSScrollView {
  
  private let notificationCenter: NotificationCenter
  
  override init( frame: NSRect ) {
    self.notificationCenter = .default
    super.init(frame: frame)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func buildMenu() -> NSMenu? {
    let menu = NSMenu()
    let deleteMenu : NSMenuItem = NSMenuItem(title:loc.tr("remove extra image") , action: #selector(removeAction),keyEquivalent:"")
    let colorMenu : NSMenuItem = NSMenuItem(title:loc.tr("change current image background color") , action: #selector(colorAction),keyEquivalent:"")
    let delcolorMenu : NSMenuItem = NSMenuItem(title:loc.tr("remove current image background color") , action: #selector(removeColorAction),keyEquivalent:"")
    menu.addItem(deleteMenu)
    menu.addItem(NSMenuItem.separator())
    menu.addItem(colorMenu)
    menu.addItem(delcolorMenu)
    return menu
  }
  
  private func showColors( panel: NSColorPanel) {
    panel.setTarget(self)
    panel.orderFront(self)
    panel.center()
  }


  @objc func removeAction( sender : Any? ) {
    DDLogDebug("image removal requested")
    notificationCenter.post(name: .extraImageRemove, object: nil)
  }

  @objc func backGroundColorChanged( _ sender: NSColorPanel ) {
    DDLogDebug("image background change requested")
    let newColor = sender.color
    notificationCenter.post(name: .imageBgColorChanged, object: newColor)
  }

  @objc func colorAction( sender : Any? ) {
    DDLogDebug("ichange image background")
    let colorPanel = NSColorPanel.shared
    colorPanel.setAction(#selector(backGroundColorChanged))
    showColors(panel: colorPanel)
  }

  @objc func removeColorAction( sender : Any? ) {
    DDLogDebug("reset image background color to nil")
    notificationCenter.post(name: .imageBgColorChanged, object: nil)
  }

  override func menu(for event: NSEvent) -> NSMenu? {
    return self.buildMenu()
  }
}

class ScoreImageScrollController : NSViewController {
  
  var scroller : ImageScroller
  
  init ( image : NSImage ,  bounds : NSRect , background : CGColor? ) {
    let imageRect = NSMakeRect(0.0, 0.0, image.size.width,image.size.height)
    let imageView = NSImageView(frame: imageRect)
    imageView.wantsLayer = true
    imageView.image = image
    if let bg = background {
      imageView.layer?.backgroundColor = bg
    }
    let scrollViewFrame = CGRect(origin: .zero,
                                 size: CGSize(width: bounds.width, height: bounds.height))
    scroller = ImageScroller(frame: scrollViewFrame)
    scroller.hasVerticalScroller = true
    scroller.hasHorizontalScroller = true
    scroller.documentView = imageView
    super.init(nibName: nil, bundle: nil)
    view = scroller
  }
  
  required init?(coder: NSCoder) {
    fatalError()
  }
  
}


class ScoreExtraInfoController : NSViewController{
  
  var curImage : NSImage? = nil
  
  var parentController: ABCPanelTabController? = nil
  var pView : ScoreExtraInfoView
  var scroller : NSScrollView?
  var kind : ScoreUrlKind
  
  init(color : CGColor ,  label : String? = nil , kind: ScoreUrlKind = .Unknown) {
    pView = ScoreExtraInfoView()
    if let label = label {
      pView.label = label
    }
    self.kind = kind
    super.init(nibName: nil, bundle: nil)
    view = pView
    view.wantsLayer = true
    view.layer!.backgroundColor = color
    pView.controller = self
  }
  
  required init?(coder: NSCoder) {
    fatalError()
  }
  
  override func viewDidLoad() {
    DDLogDebug("ScoreExtraInfoController entering viewDidLoad")
    super.viewDidLoad()
  }
  
  override func viewDidDisappear() {
    DDLogDebug("ScoreExtraInfoController entering viewDidDisapear")
    super.viewDidDisappear()
  }
  
  /*
  private func saveData( file : ABCEditedFile ) {
    do {
      try file.saveContent()
    } catch let error as ABCFailure{
      if ModalAlert().show(question: "extra image/sound drop save", text: "\(String(describing: error.message))", style: .Critical){}
    } catch {}
  }
  */
  
  internal func hideScrolledImage() {
    if let scroller = scroller {
      scroller.removeFromSuperview()
      view = pView
    }
  }
    
  func hasDropped( url : URL ) {
    if let parent = parentController {
      parent.hasDropped(kind: kind, url: pView.dropper!.draggedUrl)
    }
  }
  
  func removeExtra () {
    if let parent = parentController {
      parent.removeExtra(kind: kind)
      hideScrolledImage()
    }
  }
  
}


class ImageExtraInfoController : ScoreExtraInfoController {
  
  init(color : CGColor , image : NSImage? = nil , label : String? = nil) {
    super.init(color: color, label: label , kind : .ImageScore)
    curImage = image
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func display( image : NSImage? ) {
    curImage = image
    pView.image = curImage
    // super.showScrolledImage(image:image)
  }
  
}

class SoundExtraInfoController : ScoreExtraInfoController {
  
  var barData : [BarData]
  var soundPanelController : NSHostingController<MusicSoundPanel>?
  var soundData : Sound
  var urlChangeSubscriber : AnyCancellable?
  var soundDeletedSubscriber : AnyCancellable?
  var tvItem : ABCPanelTabViewItem
  
  init( item: ABCPanelTabViewItem ,
        data: Sound ,
        color : CGColor ,
        label : String? = nil ) {
    self.soundData = data
    self.barData = [BarData]()
    self.tvItem = item
    super.init(color: color , label: label , kind : .SoundScore)
    urlChangeSubscriber = soundData.objectdidChange.sink(
      receiveValue: { _ in
        let newValue = self.soundData.url
        print("soundata url has been changed : \(String(describing: newValue))")
        self.update()
    })
    soundDeletedSubscriber = soundData.soundDidDelete.sink(
      receiveValue: { _ in
        self.delete()
    })
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func reset() {
    self.view = self.pView
    soundPanelController = nil
    self.tvItem.viewController = self
  }

  private func delete() {
    if let curFile = parentController?.curEditedFile {
      let data = curFile.file.infos
      soundData.url = nil
      data.soundURL = nil
      update()
    }
  }
  
  private func update() {
    // Load sound panel from here
    // And populate it to Parente tabView
    if let _ = soundData.url {
      let musicPanel = MusicSoundPanel( sound: soundData, bars: self.barData)
      soundPanelController = NSHostingController(rootView: musicPanel)
      self.tvItem.viewController = soundPanelController
    } else {
      //
      // deleted reset to pView
      //
      reset()
    }
  }
  
}

