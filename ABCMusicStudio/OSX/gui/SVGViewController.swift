//
//  SVGViewController.swift
//  ABCStudio
//
//  Created by jymengant on 6/1/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import PromiseKit
import WebKit
import Combine

struct ScoreSelection {
  let offset : Int
  let length : Int
  let content : String?
}

typealias snapShotCB = (_ image : NSImage)  -> Void

class SVGViewController : NSViewController,
                          ABCWebViewPortabilityAdapter,
                          WKUIDelegate ,
                          WKNavigationDelegate ,
                          WKScriptMessageHandler  {
  
  let id : String // for debugging purpose
  var htmlView: ABCWebView?
  var visibleTune : Int? = nil

  var owningParent : EditorWindowController? = nil
  private var titleBarId : String? = nil

  
  //
  // communication upon note selection
  //
  private let subject = PassthroughSubject<ScoreSelection,Never>()
  let scoreSelectionPublisher : AnyPublisher<ScoreSelection,Never>

  
  required init?(coder: NSCoder) {
    scoreSelectionPublisher = subject.eraseToAnyPublisher()
    self.id = "Coder"
    super.init(coder: coder)
  }

  init(id : String) {
    scoreSelectionPublisher = subject.eraseToAnyPublisher()
    self.id = id
    super.init(nibName: nil, bundle: nil)

    self.view = WKWebView()
    self.view.autoresizingMask = [.width, .height]
    let htmlView = self.view as? WKWebView
    self.htmlView = ABCWebView(view: htmlView! , webHandler: self)
    self.htmlView?.pixSize = self.view.convertToBacking(self.view.visibleRect).size
    DDLogDebug("PIX width \(self.htmlView!.pixSize!.width)")
    //
    // Important : since tabView selection may be lazy , it's important to force
    // Javascript global definition initialisation before tab is selected else
    // RPC javacript call will fail ; this is done by calling initialHtml on next
    // statement initialHtml may be called multiple times; it just proceeds
    // with JS init
    initialHtml()
  }

  //
  // Portable WkWebView callback handler
  //
  func handleJSCallbackReturn(content: EntryPointArgs) {
      switch content.fx {
      case "ScoreSelection" :
        // Sync current score selection with abc score
        let offset = content.fxArgs![1].value as! Int
        let length = content.fxArgs![2].value as! Int
        let str = content.fxArgs![3].value as! String
        DDLogDebug("mouse pointed offset : \(offset) length:\(length)")
        subject.send(
          ScoreSelection( offset: offset,
                          length: length,
                          content: str)
        )
        break
      case "ScoreBarID" :
        self.titleBarId = content.fxArgs![0].value as? String
        break
      default:
        if ( ModalAlert().show(question: "JS Callback", text: "Unexpected callback function :\(content.fx)", style: .Critical)){}
        break
    }
  }
  
  func handleJSCallbackError( title:  String , message: String ) {
    if ModalAlert().show( question:title , text: message, style: .Critical ){}
  }
  
  //
  // JS Callback handler
  //
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    DDLogDebug("JS Callback entered :")
    if message.name == "jsCallBackHandler" {
      guard let jsonMsg = message.body as? String else {
        return
      }
      print(message.body)
      let jsoner = JSArgsEncoderDecoder()
      if let txtController = self.owningParent!.txtAreaController {
        do {
          let decoded : EntryPointArgs = try jsoner.decode(json: jsonMsg)
          switch decoded.fx {
          case "ScoreSelection" :
            // Sync current score selection with abc score
            let offset = decoded.fxArgs![1].value as! Int
            let length = decoded.fxArgs![2].value as! Int
            var tuneOffset = 0
            if let curTune = txtController.currentSelectedTune {
              tuneOffset = curTune.offset ?? 0
            }
            let position = ScoreSelection(offset: tuneOffset+offset, length: length,content:nil)
            txtController.jumpTo(position: position)
            break
          default:
            if ( ModalAlert().show(question: "JS Callback", text: "Unexpected callback function :\(decoded.fx)", style: .Critical)){}
          break
          }
        } catch {
          if ModalAlert().show( question:"JS Callback decoding" , text: "JSON decoding message error:\(String(describing: error))", style: .Critical ){}
        }
      }
    }
  }
  
  func initialHtml() {
    htmlView!.load(name: "index", type: "html")
  }
  
  func cleanup() {
    self.initialHtml()
  }
  
  func showCurrent( content: String , tune: Int ) {
    if let curTune = visibleTune {
      if ( curTune == tune ) {
        // in place
        return
      }
    }
    visibleTune = tune
    showAbcScore(content, tune: visibleTune!)
    
  }
  
  func highlightScoreNote( start : Int , end : Int ) {
    htmlView!.colorAbcNote(start: start ,
                           end: end,
                           htmlColor: "#007acc",
                           completion: {
                              result in
                              if ( result.isDone()) {
                                DDLogDebug("note has been shown")
                              } else {
                                DDLogDebug("javascript problem occured when showing note")
                              }
                            })
  }
  
  func unhighlightScoreNote( start : Int , end : Int ) {
    htmlView!.uncolorAbcNote(start: start ,
                             end: end,
                             completion: {
                                result in
                                // assert( result.isDone())
                                DDLogDebug("note has been hiddn")
                             })
  }
  
  func showAbcScore( _ content: String ,
                     tune : Int,
                     instrument: Int = 0 ,
                     bpm: Int = 0) {
    DDLogDebug("showing score number : \(tune)")
    htmlView?.showAbcScore( id : self.titleBarId!,
                            score: content ,
                            tuneNumber : tune,
                            instrument: instrument ,
                            bpm : bpm ,
                            completion: {
                               result in
                               assert( result.isDone())
                               DDLogDebug("showScore has been done")
                            })
  }
  
  //
  // display requested score
  //
  func display( file: ABCEditedFile ,
                tune: Int ,
                instrument: Int ,
                bpm : Int) {
    do {
      if ( file.exists ) {
        try file.loadScore()
        if let content = file.content {
          if ( content.count != 0 ) {
            if ( file.getScoreType() == .ABCScore ) {
              showAbcScore( content ,
                            tune : tune,
                            instrument: instrument ,
                            bpm : bpm )
            } else {
              // Xml here
              // showXmlScore( content)
            }
          } else {
            initialHtml()
          }
        } else {
          initialHtml()
        }
      } else {
        initialHtml()
      }
    } catch let error as ABCFailure{
      if ModalAlert().show(question: "Error loading file", text: "\(String(describing: error.message))", style: .Critical){}
    } catch {}

  }
  
}
