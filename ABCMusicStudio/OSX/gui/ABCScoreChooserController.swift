//
//  ABCScoreChooserController.swift
//  ABCStudio
//
//  Created by jymengant on 8/8/19 week 32.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

struct ScannedFile {
  let title : String
  let file  : File
  let parent : Folder
}

class SelectedScore{
  let file : ScannedFile
  var selected : Bool
  
  init( scanned : ScannedFile , selected : Bool = false ) {
    self.file = scanned
    self.selected = selected
  }
}


class ABCScoreChooserController : NSViewController
{
  
  @IBOutlet weak var scoreValidationTable: NSTableView!
  @IBOutlet weak var scoresValidatedB: NSButton!
  
  var dataSource = ABCScoreChooserDataSource()
  var parentController : ABCFileTreeController? = nil
  var spinner : ABCSpinner? = nil
  var folderData : ABCResourcePanelData? = nil
  var existing : Bool = false
  var scannedFolder : Folder? = nil
  
  override var nibName: NSNib.Name? {
    return "TuneChoicePanel"
  }
  
  override func viewDidLoad() {
    DDLogDebug("ABCScoreChooserController entering viewDidLoad")
    super.viewDidLoad()
    spinner = ABCSpinner(view:self.view)
    let url = folderData!.url
    if ( spinner!.start() ) {
      let opStub : DirScannerOperationStub = DirScannerOperationStub(url: url!,filter:".abc",recurse:true)
      opStub.getScanFolder().done { (folder) in
        self.spinner!.stop() // release spinner
        // self.parsingSelectionReturned(ir: ir)
        self.scanHasEnded(folder)
      }.catch { (err) in
        DDLogDebug("NO errors are expected here")
      }
    }
  }
  
  func scanHasEnded( _ folder : Folder? ) {
    DDLogDebug("scan has ended : populating table from datasource ...")
    if let folder = folder {
      dataSource.buildSelection(with: folder)
    }
    self.scannedFolder = folder
    scoreValidationTable.dataSource = dataSource
    scoreValidationTable.delegate = dataSource
  }
  
  func updateDataSource() {
    let nbRows = scoreValidationTable.numberOfRows
    for  i in 0...nbRows-1 {
      let cellView = scoreValidationTable.view(atColumn: 1, row: i, makeIfNecessary: false) as? NSButton
      var selected : Bool = false
      if (cellView!.state == .on) {
        selected = true
      }
      dataSource.set(selected: selected , at : i )
    }
  }
  
  @IBAction func scoresValidated(_ sender: Any) {
    DDLogDebug("scores has been validated")
    parentController?.dismiss(self)
    self.updateDataSource()
    if ( existing ) {
      parentController?.updateTreeElement(data: folderData)
    } else {
      parentController!.addTreeElement(data: folderData)
    }
    parentController!.populateSelected(scoreList: dataSource.selectedStuff , parent: self.scannedFolder! )
  }
  
  @IBAction func selectALL(_ sender: Any) {
    DDLogDebug("selecting all")
    dataSource.proceedWith(select: true)
    scoreValidationTable.reloadData()
  }
  
  @IBAction func deSelectAll(_ sender: Any) {
    DDLogDebug("unselecting all")
    dataSource.proceedWith(select: false)
    scoreValidationTable.reloadData()
  }
  
  @IBAction func cancel(_ sender: Any) {
    parentController?.dismiss(self)
  }
  
}

//
// DataSource / Delegate
//
class ABCScoreChooserDataSource : NSView ,
                                  NSTableViewDataSource ,
                                  NSTableViewDelegate
{
  
  var selectedStuff = [SelectedScore]()
  
  func buildSelection( with : Folder )  {
    if let files = with.files {
      for file in files {
        let scanned = ScannedFile(title:file.infos.title , file:file , parent: with)
        let selected = SelectedScore(scanned: scanned,selected: false)
        self.selectedStuff.append(selected)
      }
    }
    if let folders = with.folders {
      for folder in folders {
        self.buildSelection(with: folder)
      }
    }
  }
  
  func set(selected: Bool, at : Int) {
    selectedStuff[at].selected = selected
  }
  
  func proceedWith ( select: Bool ) {
    for cur in selectedStuff {
      cur.selected = select
    }
  }
  
  func numberOfRows(in tableView: NSTableView) -> Int {
    return selectedStuff.count
  }
  
  
  func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
    return buildCellViewFor(view: tableView, item: self.selectedStuff[row], tableColumn: tableColumn)
  }

  /**
   Column view formatting
   */
  private func buildCellViewFor(view: NSTableView, item: Any? , tableColumn: NSTableColumn?) -> NSView? {
    var checkView: NSButton
    var scoreView : NSTableCellView?
    if let curItem = item as? SelectedScore {
      if ( (tableColumn?.identifier)!.rawValue == "SelectColumn" ) {
        checkView = view.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "SelectCell"), owner: self) as! NSButton
        if ( curItem.selected ) {
          checkView.state = .on
        } else {
          checkView.state = .off
        }
        return checkView
      } else {
        // score cell
        scoreView = view.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ScoreCell"), owner: self) as? NSTableCellView
        if let textField = scoreView?.textField {
          textField.stringValue = curItem.file.title
        }
        return scoreView!
      }
    }
    return nil
  }
  
  
}
