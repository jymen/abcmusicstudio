//
//  ABCMessageArea.swift
//  ABCStudio
//
//  Created by jymengant on 1/12/19 week 2.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack


class ABCMessageArea  {
  
  private let msgArea : NSTextField
  private let syntaxColor = ColoringRules()
  
  init( msgArea : NSTextField) {
    self.msgArea = msgArea
  }
  
  func displayMessage(_ msg : String   ) {
    let attrStr = syntaxColor.color(string:msg, type: .InfoMsg)
    msgArea.attributedStringValue = attrStr
  }
  
  func displayWarning(_ msg : String  ) {
    let attrStr = syntaxColor.color(string:msg, type: .Warning)
    msgArea.attributedStringValue = attrStr
  }
  
  func displayError(_ msg : String  ) {
    let attrStr = syntaxColor.color(string:msg, type: .Error)
    msgArea.attributedStringValue = attrStr
  }
  
}
