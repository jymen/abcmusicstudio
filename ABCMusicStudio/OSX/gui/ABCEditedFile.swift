//
//  ABCEditedFile.swift
//  ABCStudio
//
//  Created by jymengant on 8/5/19 week 32.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

//
// current edited file in ABCEditorTextArea
//
class ABCEditedFile {
  let editor : ABCEditorController
  let file : File
  var needsParsing : Bool
  var loaded : Bool = false
  var modified : Bool {
    get {
      if let url = file.infos.storageURL {
        return scoreBuffer.isBuffered(url: url)
      }
      return false
    }
  }
  // update not saved content
  var buffer : String? {
    get {
      if let url = file.infos.storageURL {
        if ( modified ) {
          return scoreBuffer.buffer[url]
        }
      }
      return nil
    }
  }
  var content : String?
  var encoding : String.Encoding
  var untitled : Bool = false
  var tuneNumber: TuneDescription?
  
  // store lines offset for later NSView usage
  var exists : Bool  {
    get {
      guard let url = file.infos.storageURL else {
        return false
      }
      let fu = FileUtils()
      if (fu.existFile(url: url, directory: false)) {
        return true
      }
      return false
    }
  }
  
  var isEmpty : Bool {
    get {
      if let content = self.content {
        if ( content.count > 0 ) {
          return false
        }
      } 
      return true
    }
  }
  
  var hasExtraImage : Bool {
    get {
      if let _ = file.infos.imageURL {
        return true
      }
      return false
    }
  }
  
  var isDefined : Bool  {
    get {
      guard let _ = file.infos.storageURL else {
        return false
      }
      return true
    }
  }
  
  //
  // privide unique untitled name
  //
  private static func ckeckUntitle(suffix : String) -> (String , URL) {
    let notAvailable : Bool = true
    let fu = FileUtils()
    var unique : String = ""
    var num : Int = 0
    while ( notAvailable ) {
      let name = loc.tr("Untitled")
      let fName = name+unique+suffix
      let url = preferences.defStorage!.appendingPathComponent(fName,isDirectory: false)
      if (fu.existFile(url: url, directory: false)) {
        num += 1
        unique = String(num)
      } else {
        return (name,url)
      }
    }
  }
  
  init( parent : ABCEditorController , file: File? = nil , type : ABCResType = .ABCScore , encoding : String.Encoding) {
    self.editor = parent
    self.encoding = encoding
    self.needsParsing = true // Force initial parsing
    if let file = file  {
      self.file = file
    } else {
      // Build new untitled from scratch
      let fileSuffix = type.getFileSuffix()!
      let untitled = ABCEditedFile.ckeckUntitle(suffix: fileSuffix)
      let leaf = Leaf(title:untitled.0,creation:Date(),storageURL:untitled.1,encoding:String.Encoding.utf8,type:type)
      self.file = File(infos:leaf)
      self.untitled = true
    }
  }
  
  
  //
  // Keeping any \r\n is buggy in textArea Coloring parser
  // so here we replace it systematically with safe singe \n
  // upon loding file
  //
  private func eolCleanup( src : String ) -> String {
    var returned : String  = ""
    // Safely clean eol in source
    for char in src {
      if ( char == "\r\n" ) {
        returned.append("\n")
      } else {
        returned.append(char)
      }
    }
    return returned
  }
  
  func fileTitleChanged( newTitle : String ) {
    file.infos.title = newTitle
  }
  
  func loadScore() throws {
    let fu = FileUtils()
    let data = file.infos
    if ( file.infos.type == nil ) {
      file.infos.type = getScoreType() // if not yet set
    }
    
    // use dirtied buffer if score has been modified and not saved yet
    var encoding = String.Encoding(rawValue: data.encoding!)
    if let src = scoreBuffer.getScore(url: data.storageURL!) {
      content = src
      loaded = true 
    } else {
      let returned  = try fu.read(url: data.storageURL!, backupEncoding: encoding )
      content = eolCleanup(src :returned.0)
      encoding = returned.1
      // Update found encoding
      self.file.infos.encoding = encoding.rawValue
      editor.topController.updateCurrentEncoding()
    }
  }
  
  //
  // populate current combo selection value
  //
  private func getCurrentGUIEncoding() -> String.Encoding{
    let controller = editor.topController
      if let newEncoding = controller.getCurrentEncoding() {
        let str = editor.txtArea!.string
        if let bad = Unicode.checkEncoding(forString: str, using: newEncoding) {
          if ModalAlert().show(question: "bad encoding selected ", text: "use \(bad) as valid encodings ", style: .Critical){}
        } else {
          let infos = self.file.infos
          infos.encoding = newEncoding.rawValue
          DDLogDebug( "new GUI encoding populated : \(String(describing: Unicode.fromEncoding(encoding: newEncoding)))" )
        }
        return newEncoding
      }
    return .utf8 // just defaulting
  }
  
  //
  // from File to GUI Combo
  //
  func updateCurrentEncoding() -> String.Encoding{
    let curFile = self.file.infos
    var curEncoding = String.Encoding(rawValue: curFile.encoding!)
    if let strEncoding = Unicode.fromEncoding(encoding: curEncoding) {
      DDLogDebug("selected file encoding is : \(strEncoding)")
    } else {
      // bad encoding dynamic fix
      curEncoding = .utf8
      curFile.encoding = curEncoding.rawValue
    }
    return curEncoding
  }

  
  func saveContent() throws {
    do {
      let fu = FileUtils()
      let encoding = getCurrentGUIEncoding()
      self.content = editor.content
      if let content = content {
        try fu.write(url: file.infos.storageURL!, content: content, encoding: encoding)
      }
      // remove dirty buffer
      scoreBuffer.removeScore(url: file.infos.storageURL!)
      return
    } catch let error as ABCFailure {
      if ModalAlert().show(question: "File content Save fatal error ", text: "\(String(describing: error.message))", style: .Critical){}
      throw error
    } catch {}
  }
  
  func getScoreType() -> ABCResType {
    file.infos.type = ABCResType.ABCScore
    return file.infos.type!
  }
  
  private func isEndOfLine( _ char: Character ) -> Bool {
    let newLines = CharacterSet.newlines
    if (  ParseUtils.belongs(char: char , to: newLines ) ) {
      return true
    } else {
      return false
    }
  }
  
  func buildXmlLineOfssets() -> [Int] {
    var linesOffsets : [Int] = [0]
    var lineNumber: Int = 1
    
    linesOffsets[0] = 0
    if let content = content {
      for index  in  content.indices {
        if isEndOfLine(( content[index]) ) {
          let distance = content.distance(from: content.startIndex, to: index)+1
          linesOffsets.insert(distance, at:lineNumber )
          lineNumber += 1
        }
      }
    }
    linesOffsets.insert(-1, at:lineNumber ) // end of lines
    return linesOffsets
  }
  
}
