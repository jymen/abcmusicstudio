//
//  main.swift
//  ABCStudio
//
//  Created by jymengant on 5/31/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

// Global lumberjack level
var ddLogLevel: DDLogLevel = DDLogLevel.debug
var mainWindowController : MainWindowController


// init LumberJack
DDLog.add(DDOSLogger.sharedInstance)
DDLogVerbose("Verbose")
DDLogDebug("Debug")
DDLogInfo("Info")
DDLogWarn("Danger, Will Robinson", level: ddLogLevel)
DDLogDebug("CocoaLumberjack has been inited", level: ddLogLevel)

do {
  if ( preferences.isDarkMode() ) {
    DDLogDebug("we're DARK")
  } else {
    DDLogDebug("we're LIGHT")
  }
  try preferences.checkPreferences()
} catch {
  let myError : ABCFailure = error as! ABCFailure
  _ = ModalAlert().show(question: "Severe INIT error", text: myError.message! , style: .Critical )
  exit(-1)
}


DDLogDebug("Cocoa App Startup")
let myApp: NSApplication = NSApplication.shared
let del = AppDelegate()
mainWindowController = MainWindowController() // This is the owner MainMenu NIB file
// Load MainMenu NSDocument NIB file
let mainBundle: Bundle = Bundle.main
mainBundle.loadNibNamed("MainMenu" , owner: mainWindowController, topLevelObjects: nil )
let mainMenu = mainWindowController.getMainMenu()
NSApplication.shared.mainMenu = mainMenu
NSApplication.shared.delegate = del
// First instance becomes the shared document controller
// _ = ABCMusicDocumentController()

DDLogDebug("Cocoa App Startup Ended ==> Launching NSApplication")
_=loc.tr("")
NSApplication.shared.run()

