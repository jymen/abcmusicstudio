//
//  AppDelegate.swift
//  ABCStudio
//
//  Created by jymengant on 5/26/18 week 21.
//  Copyright © 2018 jymengant. All rights reserved.
//
import Cocoa
import CocoaLumberjack
import Preferences

class AppDelegate: NSObject, NSApplicationDelegate {

  var mainWindowController : MainWindowController?

  func applicationDidFinishLaunching(_ aNotification: Notification) {
    DDLogDebug("entering delegate FinishLaunching ")
    UserDefaults.standard.set(true, forKey: "NSConstraintBasedLayoutVisualizeMutuallyExclusiveConstraints") //Bool
    DDLogDebug("leaving delegate FinishLaunching ")
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
    DDLogDebug("entering delegate WillTerminate ")
    DDLogDebug("leaving delegate WillTerminate ")
  }

  func applicationShouldOpenUntitledFile(_ sender: NSApplication) -> Bool {
    DDLogDebug("entering delegate applicationShouldOpenUntitledFile ")

    let documentController = NSDocumentController.shared
    DDLogDebug("document type : \(documentController.defaultType!) ")
    if let mostRecentDocument = documentController.recentDocumentURLs.first {
      documentController.openDocument(withContentsOf: mostRecentDocument, display: true, completionHandler: { (document, documentWasAlreadyOpen, errorWhileOpening) in
        // Handle opened document or error here
        DDLogDebug("TODO :Handle opened document or error here ")
        if ( errorWhileOpening != nil )  {
          DDLogDebug("document \(document!.displayName ?? "UNKNOWN") failed to open : \(errorWhileOpening?.localizedDescription ?? "UNKNOWN ERROR")")
        }
      })
      return false
    } else {
      return true
    }
  }


}

