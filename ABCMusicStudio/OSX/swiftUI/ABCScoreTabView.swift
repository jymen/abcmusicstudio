//
//  ABCScoreTabView.swift
//  ABCStudio
//
//  Created by jymengant on 12/27/19 week 52.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI


struct ABCScoreTabView: View {
  var body: some View {
    TabView {
      Text("Editor")
        .tabItem {
          Text("Editor")
      }.tag(0)
      Text("Score")
        .tabItem {
          Text("Score")
      }.tag(1)
      Text("Extra")
        .tabItem {
          Text("Extra")
      }.tag(2)
      Text("Recording")
        .tabItem {
          Text("Recording")
      }.tag(3)
    }
  }
}

struct ABCScoreTabView_Previews: PreviewProvider {
  static var previews: some View {
    ABCScoreTabView()
  }
}
