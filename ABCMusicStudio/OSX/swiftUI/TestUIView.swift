//
//  TestUIView.swift
//  ABCMusicStudio
//
//  Created by jymengant on 4/5/20 week 14.
//  Copyright © 2020 jymengant. All rights reserved.
//

import SwiftUI

struct TestUIView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TestUIView_Previews: PreviewProvider {
    static var previews: some View {
        TestUIView()
    }
}
