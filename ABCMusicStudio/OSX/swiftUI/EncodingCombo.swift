//
//  EncodingCombo.swift
//  ABCStudio
//
//  Created by jymengant on 8/16/19 week 33.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import SwiftUI


struct EncodingCombo: View {
  
  
  @Binding var selected : Int 
    
  
  var body: some View {
    SwiftUIComboBox(
      content: Unicode.sortedEncodingsKeys,
      nbLines:5,
      selected: $selected)
  }
}

#if DEBUG
struct EncodingCombo_Preview: PreviewProvider {
  @State static var myEncoding = Unicode.encodingIndex(encoding:.utf8)
  
  static var previews: some View {
    return EncodingCombo(selected:$myEncoding)
  }
}
#endif


