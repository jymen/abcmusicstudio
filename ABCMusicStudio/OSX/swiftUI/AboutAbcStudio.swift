//
//  AboutAbcStudio.swift
//  ABCStudio
//
//  Created by jymengant on 1/11/20 week 2.
//  Copyright © 2020 jymengant. All rights reserved.
//

import SwiftUI

struct AboutAbcStudio: View {
  
  @State var aboutData : AboutContent
  
    var body: some View {
      VStack {
        Spacer().frame(height: 10)
        HStack {
          Image("logo")
          .resizable()
          .aspectRatio(contentMode: .fit)
            .frame(width: 128.0,height: 128.0)
        }.padding()
        Spacer().frame(height: 10)

        Text(aboutData.version).font(.headline)
        Spacer().frame(height: 10)
        HStack {
          Spacer().frame(width: 20)
          ScrollView(.vertical) {
            Text(aboutData.description)
             .lineLimit(nil)
             .lineSpacing(2)
             .multilineTextAlignment(.leading)
             .font(.caption)
           }.frame(width: 480.0, height:100)
          Spacer().frame(width: 20)
        }
        Spacer().frame(height: 20)
        Text(aboutData.copyright).font(.footnote)
        Spacer().frame(height: 20)
      }
    }
}

struct AboutAbcStudio_Previews: PreviewProvider {
  static var about = AboutContent (resource:"About")
    static var previews: some View {
      AboutAbcStudio(aboutData: about)
    }
}
