//
//  SwiftUITextField.swift
//  ABCStudio
//
//  Created by jymengant on 11/28/19 week 48.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import SwiftUI


struct ContentView: View {
  @State var text = "Hello Text"
  
  var body: some View {
    VStack {
      Text("text is: \(text)")
      SwiftUITextView(
        text: $text
      ) .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
    }
    
  }
}

struct SwiftUITextView: NSViewRepresentable {
  
  typealias NSViewType = NSTextView
  
  @Binding var text: String
  
  func makeCoordinator() -> Coordinator {
    Coordinator(self)
  }
  
  func makeNSView(context: Context) -> NSTextView {
    
    let myTextView = NSTextView()
    myTextView.delegate = context.coordinator
    
    //myTextView.font = NSFont(name: "HelveticaNeue", size: 14)
    myTextView.isEditable = true
    myTextView.backgroundColor = NSColor(white: 0.9, alpha: 0.05)
    
    return myTextView
  }
  
  private func color(_ nsView: NSTextView ) {
    let txtColoring = TextColoring(text: nsView)
    _ = txtColoring.colorAbcText(offset: 0, length: nsView.string.count, type: .Comment)
  }
  
  func updateNSView(_ nsView: NSTextView, context: Context) {
    nsView.string = text
    self.color(nsView)
  }
  
  class Coordinator : NSObject, NSTextViewDelegate {
    
    var parent: SwiftUITextView
    
    init(_ uiTextView: SwiftUITextView) {
      self.parent = uiTextView
    }
    
    func textView(_ textView: NSTextView, shouldChangeTextInRanges affectedRanges: [NSValue], replacementStrings: [String]?) -> Bool {
      return true
    }
    
    func textViewDidChange(_ textView: NSTextView) {
      print("text now: \(String(describing: textView.string))")
      self.parent.text = textView.string
      self.parent.color(textView)
    }
  }
}


#if DEBUG
struct SwiftUITextView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
#endif
