//
//  SpeedViewPanel.swift
//  ABCStudio
//
//  Created by jymengant on 12/23/19 week 52.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI

private struct Constants {
  static let _SPEED = loc.tr("Speed")
}

struct SpeedViewPanel: View {
  
  @Binding var speed : Double
  
  private func rounded(_ value: Double) -> String {
    let roundedValue = round(value * 100) / 100
    return String(roundedValue)
  }
  
  init( speed: Binding<Double> ) {
    self._speed = speed
  }
  
  
  var body: some View {
    VStack {
      Text(Constants._SPEED)
      Text(rounded(speed))
      HStack {
        Text("0.10")
          .foregroundColor(.blue)
        Slider(value: $speed, in: 0.10...3.0, step: 0.10)
        Text("3.0")
          .foregroundColor(.blue)
      } .frame(width: 500, height: 30 , alignment: .topLeading)
    }
  }
}

struct SpeedViewPanel_Previews: PreviewProvider {
  @State static var mySpeed : Double = 1.0
  static var previews: some View {
    SpeedViewPanel(speed : $mySpeed )
  }
}
