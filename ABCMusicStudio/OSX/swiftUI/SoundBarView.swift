//
//  SoundBar.swift
//  ABCStudio
//
//  Created by jymengant on 11/17/19 week 46.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI
import Combine

class ObservableArray<T>: ObservableObject {
  
  @Published var array:[T] = []
  var cancellables = [AnyCancellable]()
  
  init(array: [T]) {
    self.array = array
    
  }
  
  func observeChildrenChanges<K>(_ type:K.Type) throws ->ObservableArray<T> where K : ObservableObject{
    let array2 = array as! [K]
    array2.forEach({
      let c = $0.objectWillChange.sink(
        receiveValue: { _ in
          self.objectWillChange.send()
        }
      )
      
      // Important: You have to keep the returned value allocated,
      // otherwise the sink subscription gets cancelled
      self.cancellables.append(c)
    })
    return self
  }
}

struct SoundBarView: View {
  
  @ObservedObject var barData: ObservableArray<BarData>

  init( data : [BarData] = [BarData]() ) {
    barData = try! ObservableArray(array: data).observeChildrenChanges(BarData.self)
  }

  var body: some View {
    GeometryReader { geomety in
      ZStack {
        SoundBarBackground(image: Image("fiddles"))
        ForEach( self.barData.array , id: \.id ) {
          bar in
          SoundBarElement(
            data: BarData(
              index : bar.index ,
              relativeHeight: bar.relativeHeight ,
              color : bar.color
            )
          )
        }

      }
    }
    .frame(width: 320.0, height: 132.0)
  }
  
}

#if DEBUG
struct SoundBar_Previews: PreviewProvider {
  static var testArray : [BarData] = [
    BarData(index : 0.0 , relativeHeight: 0.5 , color : Color.red) ,
    BarData(index : 1.0 , relativeHeight: 0.25 , color : Color.red) ,
    BarData(index : 2.0 , relativeHeight: 0.30 , color : Color.red) ,
    BarData(index : 3.0 , relativeHeight: 0.75 , color : Color.red) ,
    BarData(index : 4.0 , relativeHeight: 0.95 , color : Color.red) ,
    BarData(index : 5.0 , relativeHeight: 0.5 , color : Color.red) ,
    BarData(index : 6.0 , relativeHeight: 0.25 , color : Color.red) ,
    BarData(index : 7.0 , relativeHeight: 0.30 , color : Color.red) ,
    BarData(index : 8.0 , relativeHeight: 0.75 , color : Color.red) ,
    BarData(index : 9.0 , relativeHeight: 0.95 , color : Color.red) ,
    BarData(index : 10.0 , relativeHeight: 0.15 , color : Color.red),
    BarData(index : 11.0 , relativeHeight: 0.5 , color : Color.red) ,
    BarData(index : 12.0 , relativeHeight: 0.25 , color : Color.red) ,
    BarData(index : 13.0 , relativeHeight: 0.30 , color : Color.red) ,
    BarData(index : 14.0 , relativeHeight: 0.75 , color : Color.red) ,
    BarData(index : 15.0 , relativeHeight: 0.95 , color : Color.red) ,
    BarData(index : 16.0 , relativeHeight: 0.15 , color : Color.red)

  ]
    static var previews: some View {
      Group {
        SoundBarView(data: testArray)
          .environment(\.colorScheme, .light)
        SoundBarView(data: testArray)
          .environment(\.colorScheme, .dark)
      }
    }
}
#endif
