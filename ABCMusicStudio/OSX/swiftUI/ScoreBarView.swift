//
//  ScoreBarView.swift
//  ABCStudio
//
//  Created by jymengant on 10/25/19 week 43.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI
import Combine

 
struct ScoreBarView: View {
  
  //
  // Shared sound data
  //
  @ObservedObject var sound: Sound

  @ObservedObject var selectedEncoding : ComboSelection
  @ObservedObject var selectedInstrument : ComboSelection
  
  
  var body: some View {
    HStack {
      Section {
        EncodingCombo(selected:$selectedEncoding.index)
        .frame(width: 140, height: nil)
        MidiInstrumentCombo(selected:$selectedInstrument.index)
        .frame(width:200,height:nil)
        Button(action: {
          print("play button pressed")
          self.sound.play()
        }){
          Image(self.sound.isPlaying ? "pause" : "play")
            .resizable()
            .frame(width:16,height:16)
        }
        .scaledToFit()
        .disabled(!self.sound.isRecordAvailable())
        SoundSlider(data: sound)
      }
      Spacer()
    }
  }
}

#if DEBUG
struct ScoreBarView_Previews: PreviewProvider {
  static var myEncoding = ComboSelection(index: 5)
  static var myInstrument = ComboSelection(index: 0)
  static var example = Sound(
                              url: nil,
                              image: nil,
                              title: "The Old gray goose",
                              description: "Exemple de description"
                             )

  // static var data = ScoreBarDataModel()
  
  static var previews: some View {
    ScoreBarView(sound: example, selectedEncoding: myEncoding,selectedInstrument:myInstrument )
  }
}
#endif
