//
//  SoundSlider.swift
//  ABCStudio
//
//  Created by jymengant on 12/14/19 week 50.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI
import Combine


struct SoundSlider: View {
  
  
  @ObservedObject var data : PlayerObserver
  
  var body: some View {
    HStack {
      Text(data.getCurrentTime())
      Slider(value: $data.relativePosition,
          in:data.minimumValue...data.maximumvalue)
        .disabled(data.isPlaying) // valid when paused only
      Text(data.getRemindingTime())
    }
  }
}

#if DEBUG
struct SoundSlider_Previews: PreviewProvider {
  @State static var example = PlayerObserver ()
  static var previews: some View {
    SoundSlider(data: example)
  }
}
#endif
