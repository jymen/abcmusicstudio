//
//  SwiftUIComboBox.swift
//  ABCStudio
//
//  Created by jymengant on 11/2/19 week 44.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI

class ComboSelection : ObservableObject {
  
  @Published var index : Int
  
  init( index: Int ) {
    self.index = index
  }
  
  func newSelection( newIndex : Int ) {
    index = newIndex
  }
}

//
// SwiftUI NSComboBox component interface
//
struct SwiftUIComboBox : NSViewRepresentable {
  
  typealias NSViewType = NSComboBox
  
  var content : [String]
  var nbLines : Int
  @Binding var selected : Int
  
  final class Coordinator : NSObject ,
  NSComboBoxDelegate {
    
    var selected : Binding<Int>
    
    
    init( selected : Binding<Int> ) {
      self.selected = selected
    }
    
    func comboBoxSelectionDidChange(_ notification: Notification) {
      print ("entering coordinator selection did change")
      if let combo = notification.object as? NSComboBox ,
       selected.wrappedValue != combo.indexOfSelectedItem {
        selected.wrappedValue = combo.indexOfSelectedItem
      }
    }
  }
  
  func makeCoordinator() -> SwiftUIComboBox.Coordinator {
    return Coordinator( selected: $selected)
  }
  
  func makeNSView(context: NSViewRepresentableContext<SwiftUIComboBox>) -> NSComboBox {
    let returned = NSComboBox()
    returned.numberOfVisibleItems = nbLines
    returned.hasVerticalScroller = true
    returned.usesDataSource = false
    returned.delegate = context.coordinator // Important : not forget to define delegate
    for key in content{
      returned.addItem(withObjectValue: key)
    }
    return returned
  }
  
  func updateNSView(_ combo: NSComboBox, context: NSViewRepresentableContext<SwiftUIComboBox>) {
    if selected != combo.indexOfSelectedItem {
        DispatchQueue.main.async {
          combo.selectItem(at: self.selected)
            print("populating index change \(self.selected) to Combo : \(String(describing: combo.objectValue))")
        }
    }
  }
}

#if DEBUG
struct SwiftUIComboBox_Previews: PreviewProvider {
  static let content = ["riri","fifi","loulou","toto"]
  
  @State static var selected  =  1
    static var previews: some View {
      SwiftUIComboBox(content: content,nbLines:3, selected: $selected)
    }
}
#endif

