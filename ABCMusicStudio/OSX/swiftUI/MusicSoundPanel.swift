//
//  MusicSoundRow.swift
//  ABCStudio
//
//  Created by jymengant on 11/7/19 week 45.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI
import Combine

private struct Constants {
  static let _SPEED = loc.tr("Speed")
  static let _COMMENTS = loc.tr("Comments")
  static let _REMOVE_TRACK = loc.tr("Remove track")
}


struct MusicSoundPanel: View {
  
  @ObservedObject var sound: Sound
  @State var speed : Double = 1.0
  
  var bars : [BarData]
  
  var body: some View {
    ScrollView(.vertical) {
      VStack {
        Text(sound.title)
          .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
        
        SoundBarView(data: bars)
        
        SpeedViewPanel(speed: $sound.speed)
        
        Text(Constants._COMMENTS)
        
        SwiftUITextView(text: $sound.description )
          .lineLimit(nil)
          .frame(width: 500, height: nil , alignment: .topLeading)
        
        TextField("url" ,text: $sound.urlStr )
          .frame(width: 500, height: 20 , alignment: .topLeading)
        HStack {
          Button(action: {
            print("Remove Sound Pressed")
            self.sound.soundDeleted() // just notify deletion rquest
          }
          ) {
            Text(Constants._REMOVE_TRACK)
          }
        }
        Spacer()
          .frame(height: 10)
        
      }
    }
  }
}

#if DEBUG
struct MusicSoundRow_Previews: PreviewProvider {
  static var example = Sound(
    url: nil,
    image: nil,
    title: "The Old gray goose",
    description: "Exemple de description"
  )
  static var testArray : [BarData] = [
    BarData(index : 0.0 , relativeHeight: 0.5 , color : Color.red) ,
    BarData(index : 1.0 , relativeHeight: 0.25 , color : Color.red) ,
    BarData(index : 2.0 , relativeHeight: 0.30 , color : Color.red) ,
    BarData(index : 3.0 , relativeHeight: 0.75 , color : Color.red) ,
    BarData(index : 4.0 , relativeHeight: 0.95 , color : Color.red) ,
    BarData(index : 5.0 , relativeHeight: 0.5 , color : Color.red) ,
    BarData(index : 6.0 , relativeHeight: 0.25 , color : Color.red) ,
    BarData(index : 7.0 , relativeHeight: 0.30 , color : Color.red) ,
    BarData(index : 8.0 , relativeHeight: 0.75 , color : Color.red) ,
    BarData(index : 9.0 , relativeHeight: 0.95 , color : Color.red) ,
    BarData(index : 10.0 , relativeHeight: 0.15 , color : Color.red),
    BarData(index : 11.0 , relativeHeight: 0.5 , color : Color.red) ,
    BarData(index : 12.0 , relativeHeight: 0.25 , color : Color.red) ,
    BarData(index : 13.0 , relativeHeight: 0.30 , color : Color.red) ,
    BarData(index : 14.0 , relativeHeight: 0.75 , color : Color.red) ,
    BarData(index : 15.0 , relativeHeight: 0.95 , color : Color.red) ,
    BarData(index : 16.0 , relativeHeight: 0.15 , color : Color.red)
  ]
  
  static var previews: some View {
    Group {
      MusicSoundPanel(sound: example, bars: testArray)
        .environment(\.colorScheme, .dark)
      MusicSoundPanel(sound: example, bars: testArray)
        .environment(\.colorScheme, .light)
    }
  }
}
#endif
