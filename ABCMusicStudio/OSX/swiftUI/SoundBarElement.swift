//
//  SoundBarElement.swift
//  ABCStudio
//
//  Created by jymengant on 11/17/19 week 46.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI

class BarData : ObservableObject , Identifiable {
  var id : UUID = UUID()
  @Published var color : Color
  @Published var relativeHeight : CGFloat
  var index : CGFloat
  
  init(  index : CGFloat , relativeHeight : CGFloat , color : Color ) {
    self.color = color
    self.relativeHeight = relativeHeight
    self.index = index
  }
}

struct SoundBarElement: View {

  @ObservedObject  var data : BarData
  
  var body: some View {
    GeometryReader { geometry in
        Path { path in
          let height = geometry.size.height
          let y = height * self.data.relativeHeight
          path.move(to: CGPoint(x: self.data.index, y: y))
          path.addLine(to: CGPoint(x: self.data.index, y: geometry.size.height))
        }
        .stroke()
        .fill(self.data.color)
        .opacity(0.6)
    }
  }
}

#if DEBUG
struct SoundBarElement_Previews: PreviewProvider {
  static var previews: some View {
    SoundBarElement(data :
      BarData(index : 5.0 , relativeHeight: 0.5 , color : Color.red)
    )
  }
}
#endif
