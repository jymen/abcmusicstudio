//
//  MidiInstrumentComboView.swift
//  ABCStudio
//
//  Created by jymengant on 11/2/19 week 44.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI

struct MidiInstrumentCombo: View {
  
  @Binding var selected : Int
    
  var body: some View {
    SwiftUIComboBox( content: MIDIInstruments.instruments,
                     nbLines:15,
                     selected: $selected)
  }
}

#if DEBUG
struct MidiInstrumentComboView_Previews: PreviewProvider {
  @State static var myInstrument = 0

  static var previews: some View {
    // let unicode = Unicode(encoding:"utf8")
    // let data = Unicode.encodings.map{$0.key}
    return MidiInstrumentCombo(selected:$myInstrument)
  }
}
#endif

