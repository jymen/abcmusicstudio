//
//  SoundBarBackground.swift
//  ABCStudio
//
//  Created by jymengant on 11/17/19 week 46.
//  Copyright © 2019 jymengant. All rights reserved.
//

import SwiftUI
import URLImage

struct SoundBarBackground: View {
  
  var image: Image
  var url : URL?
  
  var body: some View {
    containedView()
  }
  
  func containedView() -> AnyView {
    if let url = url {
      return AnyView(URLImage(
        url ,
        content: {
            $0.image
                .resizable()
        }
      ))
    } else {
      return AnyView(image.resizable())
    }
  }
}

#if DEBUG
struct SoundBarBackground_Previews: PreviewProvider {
  static var previews: some View {
    SoundBarBackground(image : Image("fiddles"))
  }
}
#endif
