//
//  OsxSessionController.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/16/20 week 20.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import Cocoa
import MultipeerConnectivity


class ABCSessionController : PeerSessionController {
  
  private var controller : NSViewController
  
  init( controller : NSViewController ) {
    self.controller = controller
  }
  
  
  func present(sessionController: MCBrowserViewController) {
    controller.presentAsSheet(sessionController)
  }
  
  func dismiss() {
    controller.dismiss(self)
  }
  
}
