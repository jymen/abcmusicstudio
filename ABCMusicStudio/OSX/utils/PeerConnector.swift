//
//  PeerConnector.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/16/20 week 20.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

class PeerConnectorButton : NSButton  {
  
  var dropper : URLViewDropper?
  var highlightForDragging : Bool = false {
    didSet {
      self.needsDisplay = true
    }
  }
  var dropIsReady : Bool  = false
  
  var scoreSender : PeerConnector?
  
  override init(frame frameRect: NSRect) {
    super.init(frame: frameRect)
    commonInit()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder:coder)
    commonInit()
  }
  
  func commonInit() {
    dropper = URLViewDropper(view: self)
    self.registerForDraggedTypes(([
      NSPasteboard.PasteboardType(rawValue: DRAGDROP_PASTEBOARD_TYPE),
      NSPasteboard.PasteboardType.fileURL
                                        ]))
  }
  
  //
  // Drop destination
  //
  
  override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
    if ( dropIsReady ) {
      highlightForDragging = true
    }
    return sender.draggingSourceOperationMask
  }
  
  override func draggingExited(_ sender: NSDraggingInfo?) {
    highlightForDragging = false
  }
  
  override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
    return dropIsReady
  }
  
  override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
    // read from pasteboard
    let pasteboard = sender.draggingPasteboard
    if let urls = pasteboard.readObjects(forClasses: [NSURL.self], options:nil) as? [URL], urls.count > 0 {
      DDLogDebug("Url drop for : \(urls[0].absoluteString) has been accepted")
      return true
    }
    if let str =  pasteboard.string(forType: NSPasteboard.PasteboardType(rawValue: DRAGDROP_PASTEBOARD_TYPE)) {
      DDLogDebug("String drop for : \(str) has been accepted")
      scoreSender?.scoreTransfert(source: str)
      return true
    }
    return false
  }
  
  override func concludeDragOperation(_ sender: NSDraggingInfo?) {
    highlightForDragging = false
  }
  
  override func draw(_ dirtyRect: NSRect) {
    super.draw(dirtyRect)
    self.dropper?.colorBorder(highlight: highlightForDragging, stdWidth: 1.0 , highlightWidth: 3.0)
  }
  
}

class PeerConnectorToolbaItem : NSToolbarItem {

  var dropIsReady : Bool {
    set {
      let button =  self.view as! PeerConnectorButton
      button.dropIsReady = newValue
    }
    
    get {
      let button =  self.view as! PeerConnectorButton
      return button.dropIsReady
    }
  }
  
  var scoreSender : PeerConnector? {
    get {
      let button =  self.view as! PeerConnectorButton
      return button.scoreSender
    }
    set {
      let button =  self.view as! PeerConnectorButton
      button.scoreSender = newValue
    }
  }
  
  
  private func switchImage(name: String) {
    self.image = NSImage(named: name)
  }
  
  override func awakeFromNib() {
    DDLogDebug("PeerConnectorToolbaItem awaking from nib")
    self.image = NSImage(named: "session")
    self.toolTip =  loc.tr("Listening for incoming sollicitors")
    self.isBordered = true
  }
  
  func sessionChanges(state: PeerSessionState , service : ABCScoreService? ) {
    switch state {
    case .Dormant :
      switchImage(name: "session")
      self.toolTip =  loc.tr("Tablet connection inactive")
      break
    case .Started , .Disconnected :
      switchImage(name: "session-orange")
      self.toolTip =  loc.tr("Listening for incoming sollicitors")
      break
    case .Connected:
      switchImage(name: "session-green")
      self.toolTip =  loc.tr("connected to %" , service!.currentPeerName ?? "UNKNOWN")
      break
    }
  }
  
}

class PeerConnector :  PeerSessionObserver {
  
  let item : PeerConnectorToolbaItem
  var service : ABCScoreService?
  var pingOK : Bool = false
  
  private func switchImage(name: String) {
    item.image = NSImage(named: name)
  }
  
  func sessionChanges(state: PeerSessionState) {
    item.sessionChanges(state: state , service: service)
    if ( state == .Connected ) {
      self.ping()
      item.dropIsReady = true
    } else {
      item.dropIsReady = true
    }
  }
  
  init( item : PeerConnectorToolbaItem , deviceName: String) {
    self.item = item
    item.scoreSender = self
    service = ABCScoreService(deviceName: deviceName, observer: self)
  }
  
  func start() {
    service!.startHosting()
  }
  
  func ping() {
    let args = EntryPointArgs(className: "Ping",
                              fx: "ping",
                              args: ["ABCScoreService host sent : HELLO please reply : 'OK' "],
                              fxRet: nil )
    service?.sendMessage(args: args)
  }
  
  func scoreTransfert( source : String ) {
    DDLogDebug("score transfert entered with : \(source)")
    let toTransfer = prepareTransfer(jsonSource: source)
    let args = EntryPointArgs(className: "ScoreTransfert",
                              fx: "scoreTransfert",
                              args: [toTransfer as Any],
                              fxRet: nil )
    service?.sendMessage(args: args)
  }
  
  private func prepareTransfer( jsonSource : String ) -> String? {
    do {
      let decoded = try ABCTreeJsonDataStore.decodeTreeElement(jsonSource: jsonSource)
      var exportedFolder : Folder?
      if let file = decoded as? File {
        // Wrap folder around single file
        let leaf = Leaf(title: "EXPORT", creation: Date(), encoding: .utf8, type: .Group)
        exportedFolder = Folder(infos: leaf)
        exportedFolder!.files = [File]()
        exportedFolder!.files!.append(file)
      }
      if let folder = decoded as? Folder {
        exportedFolder = folder
      }
      let exporter = ExportedABCSource(source: exportedFolder!)
      return exporter.prepareForExport(exportType: ExportOptions())
    } catch {
      DDLogError("transfer Decoding ERROR: \(error.localizedDescription)")
      assert(false) ;  // TO BE IMROVED
    }
    return nil
  }
  
  private func hasError ( ret: FxRet? ) -> Bool {
    if let ret = ret {
      DDLogDebug("ping  responded with : \(ret)")
      if ( ret.error == nil ) {
        self.pingOK = true
        return false
      } else {
        DDLogError("ping  in error : \(ret.error!)")
      }
    } else {
      DDLogDebug("ping  NIL response received !!!")
    }
    return true
  }
  
  func newMessageReceived(args: EntryPointArgs) {
    switch args.fx {
     case "ping" :
      if hasError(ret: args.fxRet) {
        if ModalAlert().show(question: "MultiPeer exchange error : ", text: "\(args.fxRet!.error!)", style: .Critical){}
      }
  
    default:
      DDLogDebug("NOT IMPLEMENTED YET fx : \(args.fx)")
      assert(false)
    }
  }
  
  
  
  func stop() {
    service!.stop()
    self.service = nil
    DDLogDebug("PeerConnector cleaned up")
  }
}
