//
//  XmlMusicConverter.swift
//  ABCMusicStudio
//
//  Created by jymengant on 11/27/20 week 48.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import PythonKit
import CocoaLumberjack
// import JavaScriptCore
import SwiftyXMLParser
import ZIPFoundation
import PromiseKit


class MusicConverters {
  
  private func pyInit() {
    let sys = Python.import("sys")
    DDLogInfo("PYTHON version \(sys.version_info.major).\(sys.version_info.minor) sys loaded )")
    // let bundle = Bundle(for: type(of: self))
    let pyPath = Bundle.main.path(forResource: "ping", ofType: "py", inDirectory: "pythonstuff")
    if let pyPath = pyPath {
      let components = pyPath.split(separator: "/")
      let pyDirectory = components.dropLast(1).map(String.init).joined(separator: "/")
      DDLogDebug("PYTHON Stuff path : \(pyDirectory))")
      sys.path.append(pyDirectory)
      DDLogDebug("PYTHON path : \(sys.path))")
    }
  }
  
  
  func ping(echo : String ) -> String {
    pyInit()
    let converterModule = Python.import("abcstudioconverter")
    let converter = converterModule.AbcStudioConverter()
    return String(converter.ping(echo))!
  }

  private func handleTuple( instance: Array<PythonObject> ) throws -> String {
    let converted = Array<String>(instance[0])
    let error = String(instance[1])
    if error == nil {
      return converted!.joined(separator: "\n")
    }
    throw ABCFailure(message:"abc2xml conversion error \(error!)",kind: .ABCParsing )
  }

  /*
   From Abc to XmlMusic and back
   */
  
  
  //
  // use python util for ABC to XMLMusic conversion
  //
  func abcToXmlMusic( abcContent : String ) throws -> String?  {
    pyInit()
    let converterModule = Python.import("abcstudioconverter")
    let converter = converterModule.AbcStudioConverter()
    let tuple = (Array<PythonObject>)(converter.toXML(abcContent,true))
    if let tuple = tuple {
      return try handleTuple(instance: tuple)
    }
    throw ABCFailure(message:"abc2xml UNEXPECTED ERROR",kind: .ABCParsing )
  }
  
  
  func xmlMusicToABC( xmlStuff : String ) throws -> String? {
    let parser = XmlMusicParser()
    try parser.parse(xmlSource:xmlStuff)
    return try parser.emitABC()
  }
  
  /*
   From MuseScore
   */
  
  func musescoreParse(musescoreStuff : String)  throws -> MuseScoreParser {
    let parser = MuseScoreParser()
    try parser.parse(xmlSource:musescoreStuff)
    return parser
  }
  
  
  func musescoreToABC( musescoreStuff : String ) throws -> String? {
    let parser = try self.musescoreParse(musescoreStuff: musescoreStuff)
    return try parser.emitABC()
  }
  
  
  /**
   from standard or zipped file
   */
  func read( xmlUrl : URL ) -> Promise<String> {
    return Promise { seal in
      let fExtension = xmlUrl.pathExtension
      // var buffers = [String]()
      if ( (fExtension == "mxl") ||
            (fExtension == "xlm") ||
            ( fExtension == "mscz") ){
        var buffer = ""
        let progress = Progress()
        guard let archive = Archive(url: xmlUrl, accessMode: .read) else  {
          let error = ABCFailure(message:"xmlmusic failing to unzip musicxml file : \(xmlUrl.absoluteString)",kind:.XMLMusicError)
          seal.reject(error)
          return
        }
        for entry in archive {
          // reinit buffer => just keep last archive entry content
          buffer = ""
          if entry.type == Entry.EntryType.file {
            _ = try archive.extract(entry,
            progress : progress,
            consumer: { (data) in
             print(data.count)
             buffer += data.string
             // if ( progress.totalUnitCount == (progress.completedUnitCount + Int64(data.count)) ){
             // buffers.append(buffer)
             //}
            })
          }
        }
        // populate the last one
        seal.fulfill(buffer)
      }
      else {
        let fu = FileUtils()
        let buffer = try fu.read(url: xmlUrl)
        seal.fulfill(buffer.string)
      }
    }
  }
  

}
