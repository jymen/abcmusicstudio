//
//  SyntaxColoring.swift
//  ABCStudio
//
//  Created by jymengant on 9/15/18 week 37.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import Highlightr


enum ColoringTheme : Int , Codable {
  case dark  = 0
  case light = 1
  
  static func currentTheme() -> ColoringTheme {
    if ( preferences.isDarkMode() ) {
      return .dark
    }
    return.light
  }
  
}

struct ColoringStyles : Codable {
  var abc : [ColoringType : Style]
  var xml : String?
}


//
// Debug NSColor Hex dumper 
//
extension NSColor {
  
  var hexString: String {
    // NSColorSpaceName.calibratedRGB
    guard let rgbColor = usingColorSpace(NSColorSpace.genericRGB) else {
      return "FFFFFF"
    }
    let red = Int(round(rgbColor.redComponent * 0xFF))
    let green = Int(round(rgbColor.greenComponent * 0xFF))
    let blue = Int(round(rgbColor.blueComponent * 0xFF))
    let hexString = NSString(format: "#%02X%02X%02X", red, green, blue)
    return hexString as String
  }
  
}

class ColoringRules {
  
  let prefName = "SyntaxColoring.json"
  
  // Editor Coloring strategy
  // var style : [ColoringType : Style] = [ColoringType : Style]()
  var currentStyles : ColoringStyles {
    get {
      let curTheme = ColoringTheme.currentTheme()
      return Self.allStyles[curTheme]!
    }
  }
  
  var selectedTheme : ColoringTheme = .dark
  var selectedStyles : ColoringStyles {
    get {
      return Self.allStyles[selectedTheme]!
    }
  }
  
  // SINGLETON styles table
  private static var allStyles : [ColoringTheme:ColoringStyles] = [:]
  private static var inited : Bool = false

  //
  // update current selected theme
  //
  func update( style: Style , forKey : ColoringType ) {
    if let _ = Self.allStyles[selectedTheme] {
      Self.allStyles[selectedTheme]!.abc[forKey] = style
    }
  }
  
  func debugFgColor( style : Style ) {
    let stColor = style.getAttribute(key: .foregroundColor) as! NSColor
    DDLogDebug("dbgcolor curStyle info : \(style.info) has color: \(stColor.hexString)")
  }
  
  
  func populateRules(theme : ColoringTheme) {
    switch ( theme ) {
    case .dark :
      let abcDefStyle : [ColoringType : Style] = [ColoringType : Style]()
      var curStyles = ColoringStyles(abc: abcDefStyle, xml: nil)

      curStyles.abc[.Text] =  Style(inf:loc.tr("standard text"), [StyleElement(key: .foregroundColor , value : NSColor(red:0.290, green:0.635, blue:0.345, alpha: 1))] )
      curStyles.abc[.Comment] = Style(inf:loc.tr("Comments"),[StyleElement(key: .foregroundColor , value : NSColor.gray)])
      curStyles.abc[.Directive] =  Style(inf:loc.tr("ABC directives"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.043, green:0.699, blue:0.982, alpha: 1))] )
      curStyles.abc[.InformationField] =  Style(inf:loc.tr("ABC Information field"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.290, green:0.635, blue:0.345, alpha: 1))] )
      curStyles.abc[.Lyrics] =  Style(inf:loc.tr("Lyrics"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.588, green:0.804, blue:0.984, alpha: 1))] )
      curStyles.abc[.Error] = Style(inf:loc.tr("Error messages"),[StyleElement(key: .foregroundColor , value : NSColor.red )])
      curStyles.abc[.Warning] = Style(inf:loc.tr("Warning messages"),[StyleElement(key: .foregroundColor , value : NSColor.orange )])
      curStyles.abc[.InfoMsg] = Style(inf:loc.tr("Informational messages"),[StyleElement(key: .foregroundColor , value : NSColor.blue )])
      curStyles.abc[.PlaceHolder] = Style(inf:loc.tr("Placeholder"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.622, green:0.822, blue:0.601, alpha:1))])
      curStyles.abc[.Note] = Style(inf:loc.tr("Note"),[StyleElement(key: .foregroundColor , value : NSColor.lightGray )])
      curStyles.abc[.Title] = Style(inf:loc.tr("ABC music title"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.173, green:0.973, blue:0.596, alpha:1)),
                             StyleElement(key: .font , value : NSFont.boldSystemFont(ofSize: 12))
                            ])
      curStyles.abc[.X] = Style(inf:loc.tr("X score numbering"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.867, green:0.867, blue:0.867, alpha:1)),
                         StyleElement(key: .font , value : NSFont.boldSystemFont(ofSize: 15))
                        ])
      //
      // Tree element coloring
      //
      curStyles.abc[.DirtyScoreFlag] = Style(inf:loc.tr("Dirty score flag"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.931, green:0.494, blue:0.196, alpha:1))])
      curStyles.abc[.ScoreTreeItem] = Style(inf:loc.tr("Score tree item"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.500, green:0.496, blue:0.508, alpha:1))])
      curStyles.abc[.FolderTreeItem] = Style(inf:loc.tr("Folder tree item"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.500, green:0.496, blue:0.508, alpha:1))])
      curStyles.abc[.TuneTreeItem] = Style(inf:loc.tr("Tune tree item"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.500, green:0.496, blue:0.508, alpha:1))])
      Self.allStyles[.dark] = curStyles
      
    case .light :
      let abcDefStyle : [ColoringType : Style] = [ColoringType : Style]()
      var curStyles = ColoringStyles(abc: abcDefStyle, xml: nil)

      curStyles.abc[.Text] = Style(inf:loc.tr("standard text"),[StyleElement(key: .foregroundColor , value: NSColor(red:0.047, green:0.259, blue: 0.329, alpha:1 ))])
      curStyles.abc[.Comment] = Style(inf:loc.tr("Comments"),[StyleElement(key: .foregroundColor , value : NSColor.blue )])
      curStyles.abc[.Directive] = Style(inf:loc.tr("ABC directives"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.055,green: 0.224, blue: 0.079, alpha:1))])
      curStyles.abc[.InformationField] = Style(inf:loc.tr("ABC Information field"),[StyleElement(key: .foregroundColor , value : NSColor.blue)])
      curStyles.abc[.Lyrics] = Style(inf:loc.tr("Lyrics"),[StyleElement(key: .foregroundColor , value : NSColor.darkGray )])
      curStyles.abc[.Error] = Style(inf:loc.tr("Error messages"),[StyleElement(key: .foregroundColor , value : NSColor.red )])
      curStyles.abc[.Warning] = Style(inf:loc.tr("Warning messages"),[StyleElement(key: .foregroundColor , value : NSColor.orange )])
      curStyles.abc[.InfoMsg] = Style(inf:loc.tr("Informational messages"),[StyleElement(key: .foregroundColor , value : NSColor.blue )])
      curStyles.abc[.PlaceHolder] = Style(inf:loc.tr("Placeholder"),[StyleElement(key: .foregroundColor , value : NSColor.blue )])
      curStyles.abc[.Note] = Style(inf:loc.tr("Note"),[StyleElement(key: .foregroundColor , value : NSColor.darkGray )])
      curStyles.abc[.Title] = Style(inf:loc.tr("ABC Music title"),[StyleElement(key: .foregroundColor , value : NSColor.black),  StyleElement(key: .font , value : NSFont.boldSystemFont(ofSize: 12))])
      curStyles.abc[.X] = Style(inf:loc.tr("X score numbering"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.867, green:0.867, blue:0.867, alpha:1)),
                         StyleElement(key: .font , value : NSFont.boldSystemFont(ofSize: 15))
        ]
      )
      //
      // Tree element coloring
      //
      curStyles.abc[.DirtyScoreFlag] = Style(inf:loc.tr("Dirty score flag"),[StyleElement(key: .foregroundColor , value : NSColor(red:0.931, green:0.494, blue:0.196, alpha:1))])
      curStyles.abc[.ScoreTreeItem] = Style(inf:loc.tr("Score tree item"),[StyleElement(key: .foregroundColor , value : NSColor.black)])
      curStyles.abc[.FolderTreeItem] = Style(inf:loc.tr("Folder tree item"),[StyleElement(key: .foregroundColor , value : NSColor.black)])
      curStyles.abc[.TuneTreeItem] = Style(inf:loc.tr("Tune tree item"),[StyleElement(key: .foregroundColor , value : NSColor.black)])
      Self.allStyles[.light] = curStyles
    }
  }

  //
  // Store coloring preferences
  //
  func serialize() throws {
    let encoder = JSONEncoder()
    encoder.outputFormatting = .prettyPrinted
    let encoded = try! encoder.encode(Self.allStyles)
    let str = String(decoding: encoded, as: UTF8.self)
    // DDLogDebug("data1 : \(str)")
    let fu = FileUtils()
    try fu.storeTextFile(parent: preferences.prefsPath!, name: prefName, content: str, encoding: .utf8)
  }
  
  private func load( theme : ColoringTheme , from : [ColoringTheme:ColoringStyles] ) {
    if let current = from[theme] {
      if let _ = Self.allStyles[theme] {
        for item in current.abc {
          Self.allStyles[theme]!.abc[item.key] = item.value
        }
      }
    }
  }
  
  //
  // load  coloring preferences if preferences file is
  //
  func deserialize()  {
    let fu = FileUtils()
    var encoding : String.Encoding = .utf8
    var str = ""
    do {
      populateRules( theme : .dark ) // init from scratch
      populateRules( theme : .light )
      (str,encoding) = try fu.loadTextFile(parent: preferences.prefsPath!, name: prefName, encoding: encoding)
      let decoder = JSONDecoder()
      let data = str.data(using: .utf8)
      let overRider = try decoder.decode([ColoringTheme:ColoringStyles].self,from:data!)
      load(theme: .dark, from: overRider)
      load(theme: .light, from: overRider)
    } catch {
      DDLogDebug("Failed to deserialize syntax coloring :: BUT This may be normal on initial startup")
    }
  }
  
  func getForegroundColor( tkType : ColoringType ) -> NSColor? {
    guard let style = getStyle(tkType: tkType) else {
      return nil
    }
    return style.getAttribute(key: .foregroundColor) as? NSColor
  }
  
  func getStyle( tkType : ColoringType ) -> Style? {
    let ret = currentStyles.abc[tkType]
    return ret
  }
  
  func color ( string: String , type: ColoringType , range : NSRange? = nil ) -> NSMutableAttributedString {
    let attrStr = NSMutableAttributedString( string: string )
    if ( string.count > 0 ) {
      var localRange = range
      if ( localRange == nil ) {
        localRange = NSRange(0...string.count-1)
      }
      currentStyles.abc[type]!.addAttributes(string: attrStr, range: localRange!)
    }
    return attrStr
  }
  
  init() {
    if ( !Self.inited ) {
      // SINGLETON init only once
      deserialize()
      Self.inited = true
    }
  }
}


class TextColoring {
  
  var syntaxColor = ColoringRules()
  let view : NSTextView

  init ( text : NSTextView ) {
    self.view = text
  }
  
  //
  // cleanup attributes
  //
  private func cleanupAttributes() {
    let fullRange = NSRange(location: 0, length: view.textStorage!.length)
    // let newStorege = NSTextStorage(NSAttributedString(string: ""))
    let defAttrs = syntaxColor.getStyle(tkType:.InformationField)?.toAttributes()
    view.textStorage!.setAttributes([:], range: fullRange)
    view.textStorage!.setAttributes(defAttrs, range: fullRange)
  }
  
  func colorAbcText( offset : Int , length : Int , type : ColoringType ) -> NSRange {
    let curRange = NSRange(location: offset, length: length)
    if let styles = syntaxColor.getStyle(tkType:type) {
      styles.addAttributes(string: view.textStorage!, range: curRange)
    }
    return curRange
  }
  
  func abcColoring( ir: ABCIR ) {
    cleanupAttributes() // reset previous coloring
    let coloring = ir.colors
    // let str : String = self.view.string
    // DDLogDebug("colored Source = \(str)" )
    for  curColor in coloring {
      curColor.debug(src: self.view.string)
      _ = colorAbcText(offset: curColor.offset , length: curColor.length , type : curColor.type)
    }
  }
  
}

