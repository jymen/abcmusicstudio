//
//  ABCSpinner.swift
//  ABCStudio
//
//  Created by jymengant on 8/10/19 week 32.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

class ABCSpinner : ABCSpinnerProtocol {
  
  let view : NSView
  var spinner : SpinnerView?
  var started : Bool = false
  
  init( view: NSView ) {
    self.view = view
  }
  
  //
  // Waiting for completion animated icon starter
  //
  func start() -> Bool {
    if ( started ) {
      return false
    }
    let f = self.view.frame
    let position = NSPoint(x: f.width/2, y: f.height/2)
    self.spinner = SpinnerView(color: NSColor.lightGray , position: position)
    self.view.addSubview(spinner!)
    self.spinner?.startAnimation()
    self.started = true
    DDLogDebug("SPINNER started")
    return self.started
  }
  
  //
  // end waiting animation
  //
  func stop() {
    if let spinner = self.spinner {
      if ( self.started ) {
        DDLogDebug("SPINNER Stopped")
        spinner.stopAnimation()
        spinner.removeFromSuperview()
        self.spinner = nil
        self.started = false
      }
    }
  }
}
