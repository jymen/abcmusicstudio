//
//  URLViewDropper.swift
//  ABCStudio
//
//  Created by jymengant on 9/11/19 week 37.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

/**
   provide URL dropping facilities to candidate view
*/
class URLViewDropper {
  
  let view : NSView
  var draggedUrl : URL? = nil

  
  private func registerDrop() {
    // Register accepted dragged stuff
    let types = [NSPasteboard.PasteboardType.fileURL]
    view.registerForDraggedTypes(types)
    DDLogDebug("URLViewDropper registeredDraggedTypes: \(view.registeredDraggedTypes)")
  }
  

  init( view : NSView ) {
    self.view = view
    self.registerDrop()
  }
  
  /**
   returns true if URL drop is accepted 
   */
  func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
    var canAccept = false
    //2.Got a reference to the dragging pasteboard from the dragging session info.
    let pasteBoard = draggingInfo.draggingPasteboard
    
    //3.Asked pasteboard if it has any URLs
    if pasteBoard.canReadObject(forClasses: [NSURL.self], options: nil) {
      self.draggedUrl = NSURL(from: pasteBoard) as URL?
      DDLogDebug("dragged fileUrl : \(String(describing: draggedUrl))")
      canAccept = true
      DDLogDebug("ABCFileTreeView drop accepted")
    }
    return canAccept
  }
  
  func colorBorder( highlight : Bool , stdWidth : CGFloat , highlightWidth : CGFloat ) {
    DDLogDebug("ENTERING borderColoring")
    NSColor.lightGray.set()
    let path = NSBezierPath(rect:view.bounds)
    path.lineWidth = stdWidth
    path.stroke()
    
    if highlight {
      DDLogDebug("CHANGING Interface for drop notification")
      if ( preferences.isDarkMode() ) {
        NSColor.white.set()
      } else {
        NSColor(red: 0.153, green: 0.600, blue: 0.976, alpha: 1).set()
      }
      let path = NSBezierPath(rect:view.bounds)
      path.lineWidth = highlightWidth
      path.stroke()
    }

  }
  
  
}
