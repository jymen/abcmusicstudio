//
//  ABCTabViewItem.swift
//  ABCStudio
//
//  Created by jymengant on 9/7/19 week 36.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa

class ABCTabViewItem : NSTabViewItem
{
  let iconWidth:CGFloat = 16
  
  override func sizeOfLabel(_ shouldTruncateLabel: Bool) -> NSSize
  {
    var superSize: NSSize = super.sizeOfLabel(shouldTruncateLabel)
    superSize.width += iconWidth
    return superSize
  }
  
  override func drawLabel(_ shouldTruncateLabel: Bool, in tabRect: NSRect)
  {
    let icon: NSImage? = super.image
    if let icon = icon {
      self.label = "" // clear label (image only)
      let opacity:CGFloat = 1.0
      icon.draw(
        in: NSMakeRect(tabRect.origin.x + tabRect.width - iconWidth - 2, 8, iconWidth, iconWidth),
        from: NSZeroRect,
        operation: .sourceOver, fraction: opacity)
    }
    super.drawLabel(shouldTruncateLabel, in: tabRect)
  }
}
