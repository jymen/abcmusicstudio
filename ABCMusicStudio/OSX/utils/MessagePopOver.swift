//
//  MessagePopOver.swift
//  ABCStudio
//
//  Created by jymengant on 1/5/19 week 1.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack


class MessagePopoverController : NSViewController {
  
  @IBOutlet weak var popupMessage: NSTextField!
  private var msgArea : ABCMessageArea? = nil
  private let type : ColoringType
  private let text : String
  
  init(text : String , tkType: ColoringType ) {
    self.type = tkType
    self.text = text
    super.init(nibName: "MessagePopup", bundle: nil)
  }
  
  required init( coder: NSCoder ) {
    self.type = .Unknown
    self.text = ""
    super.init(coder: coder)!
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // popupMessage.stringValue = self.text
    msgArea = ABCMessageArea(msgArea: popupMessage)
    switch ( self.type ) {
    case .Error :
      msgArea?.displayError(self.text)
      break
    case .InfoMsg :
      msgArea?.displayMessage(self.text)
      break
    case .Warning :
      msgArea?.displayWarning(self.text)
      break
    default:
      break
    }
  }
}

class ABCPopOver {

  let sender: NSView
  let popover : NSPopover
  
  init( sender : NSView) {
    self.sender = sender
    self.popover = NSPopover()
  }
  
  func show( content : NSViewController , bounds: NSRect? = nil ) {
    // popover.contentSize = NSSize(width: 200, height: 60)
    popover.behavior = .applicationDefined
    popover.animates = true
    popover.contentViewController = content
    
    // Show popover
    if ( !popover.isShown ) {
      var subSet : NSRect? = bounds
      if ( subSet == nil ) {
        subSet = sender.bounds
      }
      popover.show(relativeTo: subSet!, of: self.sender, preferredEdge: NSRectEdge.maxY)
    }
  }
  
  func dismiss() {
    popover.close()
  }
}
