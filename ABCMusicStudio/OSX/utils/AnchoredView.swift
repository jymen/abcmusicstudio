//
//  AnchoredView.swift
//  ABCStudio
//
//  Created by jymengant on 6/8/18 week 23.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

/**
 Override mainly used for Debug purposes
*/
class AnchoredView : NSView {
  
  

  override func viewWillDraw() {
    //DDLogDebug("UITests entering viewWillDraw")
    //DDLogDebug("parent bound : \(String(describing: self.superview?.bounds))")
    //DDLogDebug("parent frame : \(String(describing: self.superview?.frame))")
    //DDLogDebug("initial bound : \(self.bounds)")
    //DDLogDebug("initial frame : \(self.frame)")
    self.frame =  (superview?.frame)!  //     NSRect(x:0,y:0,width:50,height:50)
    super.viewWillDraw()
  }
  
  override func resize(withOldSuperviewSize oldSize: NSSize) {
    // DDLogDebug("UITests entering resize")
    self.frame = (superview?.frame)! //     NSRect(x:0,y:0,width:50,height:50)
    super.resize(withOldSuperviewSize: oldSize)
  }
  
}
