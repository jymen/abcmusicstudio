//
//  Sound.swift
//  ABCStudio
//
//  Created by jymengant on 12/5/19 week 49.
//  Copyright © 2019 jymengant. All rights reserved.
//

import Foundation
import Combine



class PlayerObserver : ObservableObject {
  
  let minimumValue = 0.0
  let maximumvalue = 100.0
  var duration : Double = 0.0 // in seconds
  
  @Published var isPlaying : Bool = false
  @Published var countDown : Double = 0.0
  @Published var countUp : Double = 0.0
  @Published var relativePosition : Double = 0.0 {
    didSet {
      print("slider position changed to : \(relativePosition)")
      if ( !isPlaying ) {
        // new player position requested by user
        let absolute = convertFromRelative()
        player.seek(to: Float(absolute))
      }
    }
  }
  
  @Published  var speed : Double = 1.0  {
    didSet {
      print ("new speed value : \(speed)")
      player.speedhasChanged(newSpeed:Float(self.speed))
    }
  }
  
  var timePosition : Double = 0.0

  lazy var player = AudioPlayer(observer: self)
  
  func play() {
    player.playTapped()
    isPlaying = player.isPlaying()
    print ("playing status changed : \(isPlaying)")
  }

  private enum TimeConstant {
    static let secsPerMin = 60
    static let secsPerHour = TimeConstant.secsPerMin * 60
  }
    
  var tuneSize : Double = 0.0 {
    didSet {
      duration = tuneSize / tuneRate
    }
  }
  var tuneRate : Double = 44100 { // defaulted value
    didSet {
      duration = tuneSize / tuneRate
    }
  }
  
  @Published  var tunePosition : Double = 0.0 {
    didSet {
      timePosition = Double(tunePosition) / Double(tuneRate)
      countUp = timePosition
      countDown = duration - timePosition
      relativePosition = convertToRelative()
    }
  }

  private func formatted(time: Double) -> String {
    var secs = Int(ceil(time))
    var hours = 0
    var mins = 0
    
    if secs > TimeConstant.secsPerHour {
      hours = secs / TimeConstant.secsPerHour
      secs -= hours * TimeConstant.secsPerHour
    }
    
    if secs > TimeConstant.secsPerMin {
      mins = secs / TimeConstant.secsPerMin
      secs -= mins * TimeConstant.secsPerMin
    }
    
    var formattedString = ""
    if hours > 0 {
      formattedString = "\(String(format: "%02d", hours)):"
    }
    formattedString += "\(String(format: "%02d", mins)):\(String(format: "%02d", secs))"
    return formattedString
  }

  func getCurrentTime()  -> String {
    return formatted(time:countUp)
  }
  
  func getRemindingTime() -> String {
    return formatted(time:countDown)
  }
  
  func convertToRelative() -> Double {
    let dimension = maximumvalue - minimumValue
    return tunePosition / tuneSize * dimension
  }
  
  func convertFromRelative() -> Double {
    let dimension = maximumvalue - minimumValue
    return  relativePosition * tuneSize  / dimension
  }
}


class Sound : PlayerObserver {
  
  let objectdidChange = ObservableObjectPublisher()
  let soundDidDelete = ObservableObjectPublisher()

  let id : UUID
  var url : URL? = nil {
    didSet {
      print ("url: \(String(describing: url?.absoluteString))")
      // stop previous
      stopPlaying()
      if let url = url {
        urlStr = url.absoluteString
        // sync player with new audio
        player.audioFileURL = url
      } else {
        urlStr = ""
      }
      objectdidChange.send()
    }
  }
    
  @Published  var urlStr : String
  @Published  var image : URL?
  @Published  var title : String
  @Published  var description : String
  @Published var meterLevel : Float = 0
    
  init ( url: URL? ,
         image : URL? ,
         title : String ,
         description : String )
  {
    self.id = UUID()
    self.url = url
    if let url = url {
      urlStr = url.absoluteString
    } else {
      urlStr = ""
    }
    self.image = image
    self.title = title
    self.description = description
  }

  //
  // removal request 
  //
  func soundDeleted () {
    soundDidDelete.send()
  }
  

   
  func setTunePosition( offset : Double ) {
    super.tunePosition = offset
  }
  
  func isRecordAvailable() -> Bool {
    if let _ = self.url {
      return player.isRecordAvailable()
    }
    return false
  }
  
  func initAudioPlayer() {
    player.setupAudio()
  }
  
  func stopPlaying() {
    player.cancelCurrentAudio()
    isPlaying = false
  }
  
  
  func reset() {
    self.url = nil
    self.image = nil
    self.title = ""
    self.description = ""
    self.speed = 0
    self.meterLevel = 1.0
  }
}

