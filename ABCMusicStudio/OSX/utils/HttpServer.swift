//
//  HttpServer.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/15/20 week 20.
//  Copyright © 2020 jymengant. All rights reserved.
//

import CocoaLumberjack

import Embassy
import EnvoyAmbassador

class HttpServer {
  
  let port : Int
  
  init(port : Int ) {
    self.port = port
  }
  
  
  //
  // Start tiny JSON server
  //
  func start() {
    let loop = try! SelectorEventLoop(selector: try! KqueueSelector())
    let router = Router()
    let server = DefaultHTTPServer(eventLoop: loop, port: self.port, app: router.app)

    router["/abcmusic/api"] = DelayResponse(JSONResponse(handler: { data -> Any in
      
        return [
            ["id": "01", "name": "john"],
            ["id": "02", "name": "tom"]
        ]
    }))

    // Start HTTP server to listen on the port
    try! server.start()

    // Run event loop
    loop.runForever()
  }
  
}


