//
//  CustomizedButton.swift
//  ABCStudio
//
//  Created by jymengant on 6/8/18 week 23.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack

/**
 ABCStudio Button customization
 */
class CustomizedButton : NSButton {
  
  var trackingArea : NSTrackingArea? = nil
  
  
  required init?(coder : NSCoder) {
    super.init(coder:coder)
    if ( preferences.isDarkMode() ) {
      
    } else {
      self.layer?.backgroundColor = CGColor(red:0.925, green:0.925, blue:0.925,alpha:1)
      // prevent default button blue background (default selected button pattern)
      self.focusRingType = NSFocusRingType.none
      self.updateTrackingAreas()
    }
  }
  
  /**
   enable mouseEntered and mouse leave
  */
  override func updateTrackingAreas() {
    super.updateTrackingAreas()
    if ( trackingArea != nil ) {
      self.removeTrackingArea(trackingArea!)
    }
    let opts = UInt8(NSTrackingArea.Options.mouseEnteredAndExited.rawValue) | UInt8(NSTrackingArea.Options.activeAlways.rawValue)
    trackingArea = NSTrackingArea(
      rect: self.bounds ,
      options : NSTrackingArea.Options(rawValue: NSTrackingArea.Options.RawValue(opts)) ,
      owner: self ,
      userInfo: nil )
    self.addTrackingArea(trackingArea!)
  }
  
  override func mouseEntered(with event: NSEvent) {
    //DDLogDebug("MouseEntered  ")
    if ( preferences.isDarkMode()) {
      self.layer?.backgroundColor = CGColor.black

    } else {
      self.layer?.backgroundColor = CGColor.white
    }
    super.mouseEntered(with: event)

  }
  
  
  override func mouseExited(with event: NSEvent) {
    //DDLogDebug("MouseExited  ")
    if ( preferences.isDarkMode()) {
      self.layer?.backgroundColor = CGColor(red:0.173, green:0.176, blue:0.188,alpha: 1)
    } else {
      self.layer?.backgroundColor = CGColor(red:0.925, green:0.925, blue:0.925,alpha:1)
    }
    super.mouseExited(with: event)
  }
  
  override func mouseDown(with event: NSEvent) {
    //DDLogDebug("MouseDown  ")
    if ( preferences.isDarkMode() ) {
      self.layer?.backgroundColor = CGColor(red:0.420, green:0.421, blue:0.427, alpha:1)
    } else {
      self.layer?.backgroundColor = CGColor(red:0.314, green:0.773, blue:0.780,alpha:1)
    }
    super.mouseDown(with: event)
  }
  
  override func mouseUp(with event: NSEvent) {
    if ( preferences.isDarkMode() ) {
      self.layer?.backgroundColor = CGColor(red:0.173, green:0.176, blue:0.188,alpha: 1)
    } else {
      //DDLogDebug("MouseUp  ")
      self.layer?.backgroundColor = CGColor(red:0.925, green:0.925, blue:0.925,alpha:1)
    }
    super.mouseUp(with: event)
  }
}
