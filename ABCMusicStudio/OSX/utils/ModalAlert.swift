//
//  ModalAlert.swift
//  ABCStudio
//
//  Created by jymengant on 6/1/18 week 22.
//  Copyright © 2018 jymengant. All rights reserved.
//

import Cocoa
import CocoaLumberjack
import PromiseKit

enum RemovalType {
  case StopRemoval
  case Remove
  case Delete
}

class ModalAlert  {
  
  let parent : NSWindow?
  
  init() {
    self.parent = NSApplication.shared.mainWindow
  }
  
  /**
   Just popup dialog box
  */
  func show( question: String, text: String , style: ABCAlertStyle ) -> Bool {
    var alertStyle : NSAlert.Style = .informational
    switch style {
    case .Critical :
      alertStyle = .critical
      break
    case .Warning :
      alertStyle = .warning
      break
    case .Informational :
      alertStyle = .informational
      break
    }
    
    let alert = NSAlert()
    alert.messageText = question
    alert.informativeText = text
    alert.alertStyle = alertStyle
    alert.addButton(withTitle: "OK")
    alert.addButton(withTitle: loc.tr("Cancel"))
    if let parent = parent {
      alert.beginSheetModal(for: parent) { (returnCode: NSApplication.ModalResponse) -> Void in
        print ("returnCode: ", returnCode)
      }
    } else {
      return alert.runModal() == .alertFirstButtonReturn
    }
    return true
  }

  
  private func confirm( leaf: Leaf? ,alert : NSAlert ) -> Promise<RemovalType> {
    return Promise {
      seal in
        alert.beginSheetModal(for: parent! , completionHandler: {
        ( response: NSApplication.ModalResponse ) in
          switch response {
            
          case .alertFirstButtonReturn:
            seal.fulfill(.Remove)
          case .alertSecondButtonReturn:
            if ( leaf!.storageURL != nil ) {
              seal.fulfill(.Delete)
            }
            seal.fulfill(.StopRemoval)
          default:
            seal.fulfill(.StopRemoval)
          }
      })
    }
  }
  
  func confirmTreeRemoval( leaf : Leaf ) -> Promise<RemovalType> {
    let alert = NSAlert()
    // alert.showsSuppressionButton = true
    
    alert.messageText = loc.tr("tree item removal")
    alert.informativeText = loc.tr("Confirm removal of % ?" , leaf.title)
    alert.alertStyle = NSAlert.Style.warning
    alert.addButton(withTitle: loc.tr("Remove from tree"))
    if ( leaf.storageURL != nil ) {
      alert.addButton(withTitle: loc.tr("Also delete on disk"))
    }
    alert.addButton(withTitle: loc.tr("Cancel"))
    return Promise {
      seal in
      alert.beginSheetModal(for: parent! , completionHandler: {
        ( response: NSApplication.ModalResponse ) in
        switch response {
          
        case .alertFirstButtonReturn:
          seal.fulfill(.Remove)
        case .alertSecondButtonReturn:
          if ( leaf.storageURL != nil ) {
            seal.fulfill(.Delete)
          }
          seal.fulfill(.StopRemoval)
        default:
          seal.fulfill(.StopRemoval)
        }
      })
    }
  }
  
}
