//
//  TemplateHeaderFile.swift
//  ABCMusicStudio
//
//  Created by jymengant on 2/18/21 week 7.
//  Copyright © 2021 jymengant. All rights reserved.
//

import Foundation

class TemplateHeaderFile {
  
  private let _HEADER_FNAME_ = "HeaderTemplate"
  private var template : String =
"""
% Default header template begins
% can be changed in preferences
%%vocalfont Bookman-Light 12
%%historyfont Bookman-Light 10
%%wordsfont Bookman-Light 14
%%composerfont Bookman-Light 12
%%titlefont Luminari 26
% Default header template ends
"""
  private var templateFile : File!
  
  var content : String {
    get {
      return template
    }
    set {
      template = newValue
    }
  }
  
  
  func setup() {
    let leaf = buildLeaf()
    templateFile = File(infos: leaf)
    let fu = FileUtils()
    let headerFUrl = leaf.storageURL
    if ( !fu.existFile(url: headerFUrl!, directory: false)) {
      write()
    } else {
      read()
    }
  }
  
  private func buildLeaf() -> Leaf {
    let leaf = Leaf(title: "abc template header", encoding: .utf8, type: .ABCScore)
    let headerFUrl = preferences.defStorage?.appendingPathComponent(_HEADER_FNAME_)
    leaf.storageURL = headerFUrl
    return leaf
  }
  
  func write()  {
    do {
      let fu = FileUtils()
      let leaf = buildLeaf()
      let headerFUrl = leaf.storageURL
      try fu.write(url: headerFUrl!, content: template, encoding: .utf8)
    } catch {
      let err = error as! ABCFailure
      if ModalAlert().show(question: "HeaderTemplate write error  ", text: "error \(err)  ", style: .Critical){}
    }

  }
  
  func read()  {
    let fu = FileUtils()
    let leaf = buildLeaf()
    let headerFUrl = leaf.storageURL
    do {
    (template,_)  = try fu.read(url: headerFUrl!, backupEncoding: .utf8 )
    } catch {
      let err = error as! ABCFailure
      if ModalAlert().show(question: "HeaderTemplate write error  ", text: "error \(err)  ", style: .Critical){}
    }
  }
  
  func importTemplate( inSource : String ) -> String {
    var returned = ""
    let prefInclusion = preferences.includeTemplate
    if ( prefInclusion ) {
      read()
      returned.append(template)
      returned.append("\n")
    }
    returned.append(inSource)
    return returned
  }
  
}

