//
//  MusicUrlImporter.swift
//  ABCMusicStudio
//
//  Created by jymengant on 2/6/21 week 5.
//  Copyright © 2021 jymengant. All rights reserved.
//

import Foundation
import CocoaLumberjack

private enum ABCImporFileType {
  case MuseScore
  case Abc
  case MusicXml
  case Undefined
}

class MusicUrlImportExport {
  
  private var fType : ABCImporFileType = .Undefined

  let parent : ABCFileTreeController
  let url : URL
  var abcSrc : String?
  var encoding : String.Encoding
  var destFile : URL?
  var curDocumentName : String? {
    let parentParent  = parent.parentController
    if let parentParent = parentParent {
      return parentParent.documentName
    }
    return nil
  }
  
  init (url : URL , parent : ABCFileTreeController ) {
    self.url = url
    encoding = .utf8
    self.parent = parent
  }
  
  private func addToTree( url : URL , src: String ) throws {
    var data : ABCResourcePanelData?
    let parser = ABCParser(src: src)
    let ir = ABCIR(parser: parser)
    ir.parse()
    // just try to fetch title if error exists and title is ok
    // we just bypass them : they will be shown in the editor later
    if let desc = ir.curDescription {
      let title = desc.title
      data = ABCResourcePanelData(type: .ABCScore,
      url: url, iconUrl: nil, imageUrl: nil, soundUrl: nil, title: title, creation: Date(), encoding: encoding, existing: false)
      parent.addTreeElement(data: data, content: src)
      // save tree content
      _ = try parent.getSerialized()
    } else {
      if let errors = ir.errors {
        throw ABCFailure(message: "abc parser error : \(errors.message)", kind: .ABCParsing, token: nil)
      } else {
        throw ABCFailure(message: "import error : Unpredictable problem when parsing imported stuff", kind: .ABCParsing, token: nil)
      }
    }
  }
  
  private func writeToDest() throws {
    // did file exist in dest
    let fu = FileUtils()
    while ( fu.existFile(url: destFile!, directory: false) ) {
      destFile = fu.changeFName(url: destFile!)
    }
    try fu.write(url: destFile!, content: abcSrc!, encoding: .utf8)
  }
  
  private func checkDest( forRepo : String , fName : String ) throws {
    // prepare Storage For export
    let fu = FileUtils()
    let destDir = preferences.defStorage?.appendingPathComponent(forRepo)
    if ( !fu.existFile(url: destDir!, directory: true) ) {
      try fu.createLocalFolder(url: destDir!)
    }
    destFile = destDir!.appendingPathComponent("\(fName).abc")
  }
  
  //
  // called in asynch way
  //
  private func handleImport(src: String , type: ABCImporFileType ) {
    do {
    switch type {
    case .Abc :
      abcSrc = src // nothing else to be done here
      break
    case .MuseScore :
      let mscoreImporter = MusicConverters()
      abcSrc = try mscoreImporter.musescoreToABC(musescoreStuff: src)
    case .MusicXml :
      let xmlImporter = MusicConverters()
      abcSrc = try xmlImporter.xmlMusicToABC(xmlStuff: src)
      break
    case .Undefined:
      break
    }
    // Conversion is done => check for template insertion
    let tmplFile = TemplateHeaderFile()
    abcSrc = tmplFile.importTemplate(inSource: abcSrc!)
    //  => write abcFile to dest repo
    try writeToDest()
    try addToTree(url: destFile!, src: abcSrc!)
    } catch  {
      // delete created file
      let fu = FileUtils()
      if ( fu.existFile(url: self.destFile!, directory: false)) {
        do {
          try fu.deleteOnDisk(path: self.destFile!.path)
        } catch {}
      }
      let err = error as! ABCFailure
      if ModalAlert().show(question: "MusicFile Abc conversion error", text: "\(err.message!)", style: .Critical){}
      
    }
  }
  
  func abcImport() throws  {
    // check file types
    // accept abc , mscx / mscz , musicxml / Mxl
    //    destFile = preferences.defStorage?.appendingPathComponent("\(fileName).abc")
    let fileExtension = url.pathExtension
    let fileName = url.deletingPathExtension().lastPathComponent
    if let curDocumentName = curDocumentName {
      try checkDest(forRepo: curDocumentName , fName : fileName  )
    } else {
      if ModalAlert().show(question: "current Repository missing", text: "Unable to get current repository name", style: .Critical){}
    }
    switch fileExtension {
    case "abc" :
      fType = .Abc
      break
    case "mscx" , "mscz" :
      fType = .MuseScore
      break
    case "musicxml" , "mxl" , "xml" :
      fType = .MusicXml
      break
    default :
      throw ABCFailure(message: "Unsuported File Type \(fileExtension)", kind: .IOError )
    }
    switch fType {
    case .Abc :
      let fu = FileUtils()
      fu.asyncRead(url: url, backupEncoding: .utf8).done({
        response in
        self.encoding = response.1
        self.handleImport(src: response.0 , type: .Abc)
      }).catch({ error  in
        let err = error as! ABCFailure
        if ModalAlert().show(question: "MusicFile Abc read error", text: "\(err.message!)", style: .Critical){}
      })
      break
    case .MuseScore, .MusicXml :
      let cnvrtr = MusicConverters()
      cnvrtr.read(xmlUrl: url ).done({ data in
        self.handleImport(src: data , type: self.fType)
      }).catch({ error  in
        let err = error as! ABCFailure
        if ModalAlert().show(question: "MusicFile Xml/MuseScore read error", text: "\(err.message!)", style: .Critical){}
      })
      break
    default :
      throw ABCFailure(message: "UnExpected File Type for import : \(fileExtension)", kind: .IOError )
    }
    
  }
  
  
}
