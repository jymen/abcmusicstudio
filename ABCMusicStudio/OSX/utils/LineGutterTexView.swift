import Cocoa
import CocoaLumberjack

/// Defines the width of the gutter view.
private let GUTTER_WIDTH: CGFloat = 40.0
private let _FULL_DEBUG_ : Bool = false

struct VisibleLines {
  let start : Int
  let length : Int
}

/// Adds line numbers to a NSTextField.
class LineNumberGutter: NSRulerView {
  
  private var errorColor : NSColor = .red // Foreground color for lines in error
  private var lineInError : Int = -1    // line number in error if not -1
  private var font : NSFont? = nil
  // private var popOver : ABCPopOver? = nil // Popover for line in error if any

  // Lines / sourceOffsets start location provided by loader/parser
  var linesOffsets : [Int]? = nil
  var firstLine : Int = 0 // First line number in scope
  var lastLine  : Int = 0 // last line number in scope
  var firstLineOffset : Int = 0
  var linesLength : Int = 0
  
  /// Holds the background color.
  internal var backgroundColor: NSColor {
    didSet {
      self.needsDisplay = true
    }
  }
  
  /// Holds the text color.
  internal var foregroundColor: NSColor {
    didSet {
      self.needsDisplay = true
    }
  }
  
  func getVisibleLines() -> VisibleLines {
    return VisibleLines(start: linesLength , length: linesLength )
  }
  
  ///  Initializes a LineNumberGutter with the given attributes.
  ///
  ///  - parameter textView:        NSTextView to attach the LineNumberGutter to.
  ///  - parameter foregroundColor: Defines the foreground color.
  ///  - parameter backgroundColor: Defines the background color.
  ///
  ///  - returns: An initialized LineNumberGutter object.
  init(withTextView textView: NSTextView, foregroundColor: NSColor, backgroundColor: NSColor) {
    // Set the color preferences.
    self.backgroundColor = backgroundColor
    self.foregroundColor = foregroundColor
    
    // Make sure everything's set up properly before initializing properties.
    super.init(scrollView: textView.enclosingScrollView, orientation: .verticalRuler)
    
    // Set the rulers clientView to the supplied textview.
    self.clientView = textView
    self.font = textView.font
    // Define the ruler's width.
    self.ruleThickness = GUTTER_WIDTH
  }
  
  ///  Initializes a default LineNumberGutter, attached to the given textView.
  ///  Default foreground color: hsla(0, 0, 0, 0.55);
  ///  Default background color: hsla(0, 0, 0.95, 1);
  ///
  ///  - parameter textView: NSTextView to attach the LineNumberGutter to.
  ///
  ///  - returns: An initialized LineNumberGutter object.
  convenience init(withTextView textView: NSTextView) {
    let fg = NSColor(calibratedHue: 0, saturation: 0, brightness: 0, alpha: 1)
    let bg = NSColor(calibratedHue: 0, saturation: 0, brightness: 0, alpha: 1)
    // Call the designated initializer.
    self.init(withTextView: textView, foregroundColor: fg, backgroundColor: bg)
  }
  
  struct ScopedLines {
    let first : Int
    let last : Int
  }
  
  /**
   return back visible lines numbers array
   */
  private func getVisibleLines( startOffset : Int , length : Int ) -> ScopedLines? {
    if (linesOffsets == nil) {
      return nil
    }
    var firstLine : Int = -1
    var lastLine  : Int = 0
    let highBound : Int = startOffset + length
    var curLine : Int = 0
    var pos : Int = 0
    while curLine < linesOffsets!.count {
      if ( linesOffsets![curLine] >= startOffset ) {
        if ( firstLine == -1 ) {
          firstLine = curLine
        }
      }
      if ( firstLine > -1 ) {
        // proceed to last line
        if ( linesOffsets![curLine] < highBound ) {
          if ( curLine == linesOffsets!.count-1 ) {
            // reach array size => break
            break
          }
          pos += 1
        } else {
          break
        }
      }
      curLine += 1
    }
    lastLine = curLine-1 // the last selected isgreater highbound
    return ScopedLines(first: firstLine, last: lastLine)
  }
  
  private struct GutterResources {
    let layoutManager : NSLayoutManager
    let textcontainer : NSTextContainer
  }
  
  private func updateLineScope() -> GutterResources? {
    // Unwrap the clientView, the layoutManager and the textContainer, since we'll
    // need them sooner or later.
    guard let textView = self.clientView as? NSTextView ,
      let layoutManager = textView.layoutManager ,
      let textContainer = textView.textContainer else {
        return nil
    }

    let vRect : NSRect  = textView.visibleRect
    if ( _FULL_DEBUG_) {
      DDLogDebug("visible text rect : \(vRect)" )
    }
    let visibleRange = layoutManager.glyphRange(forBoundingRect: vRect, in: textContainer)
    firstLineOffset = visibleRange.location
    linesLength = visibleRange.length
    if let visibleLines = getVisibleLines(startOffset: firstLineOffset, length: linesLength)  {
      self.firstLine = visibleLines.first
      self.lastLine = visibleLines.last
      self.needsDisplay = true
    }
    // DDLogDebug("NBGlyphs : \(length)")
    return GutterResources(layoutManager: layoutManager, textcontainer: textContainer)
  }
  
  func setError(line: Int , color : NSColor = .red) {
    lineInError = line
    errorColor = color
    self.needsDisplay = true
  }
  
  func clearError() {
    if ( lineInError != 0 ) {
      lineInError = -1
      self.needsDisplay = true
    }
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  ///  Draws the line numbers.
  ///
  ///  - parameter rect: NSRect to draw the gutter view in.
  override func drawHashMarksAndLabels(in rect: NSRect) {
    let textView = self.clientView as? NSTextView
    // Set the current background color...
    self.backgroundColor.set()
    // ...and fill the given rect.
    rect.fill()

    let res = updateLineScope()
    if ( _FULL_DEBUG_ ) {
      DDLogDebug("drawHashMarksAndLabels First : \(firstLine) last : \(lastLine) ")
    }
    
    if ( (self.lastLine != -1 ) && ( firstLine >= 0 ) && ( linesOffsets != nil )) {
      var curYPos : CGFloat = 0
      for curLine in  self.firstLine...self.lastLine {
        // The effective range of glyphs within the current line.
        let startOffset = linesOffsets![curLine]
        
        if ( startOffset == -1 ) {
          let usedRect = self.visibleRect
          if ( curYPos <= usedRect.size.height ) {
            // room available
            self.drawLineNumber(num: linesOffsets!.count, atYPosition: curYPos)
          }
        } else {
          var effectiveRange = NSRange(location: startOffset, length: 1)
          // Get the rect for the current line fragment.
          let lineRect = res?.layoutManager.lineFragmentRect(forGlyphAt: startOffset, effectiveRange: &effectiveRange, withoutAdditionalLayout: true)
          if ( lineRect?.size.height == 0 ) { // Beyond text ( may be \n case )
            let usedRect = self.visibleRect
            if ( curYPos <= usedRect.size.height ) {
              // room available
              self.drawLineNumber(num: curLine+1, atYPosition: curYPos)
            }
          } else {
            curYPos += lineRect!.height
            if ( _FULL_DEBUG_ ) {
              DDLogDebug("line \(curLine+1) starting with: \(textView!.string[startOffset])/\(startOffset) wil be at : \(lineRect!.minY)")
            }
            self.drawLineNumber(num: curLine+1, atYPosition: lineRect!.minY)
          }
        }
      }
    }
  }
  
  private func drawLineNumber(num: Int, atYPosition yPos: CGFloat) {
    // DDLogDebug("drawing line number ")

    // Define attributes for the attributed string.
    var attrs = [NSAttributedString.Key.font: font , NSAttributedString.Key.foregroundColor: self.foregroundColor ]
    if ( num == lineInError ) {
      attrs = [ NSAttributedString.Key.font: font , NSAttributedString.Key.foregroundColor: self.errorColor ]
    }
    // Define the attributed string.
    let attributedString = NSAttributedString(string: "\(num)", attributes: attrs as [NSAttributedString.Key : Any])
    // Get the NSZeroPoint from the text view.
    let relativePoint    = self.convert(NSZeroPoint, from: clientView)
    // Calculate the x position, within the gutter.
    let xPosition        = GUTTER_WIDTH - (attributedString.size().width + 5)
    // Draw the attributed string to the calculated point.
    if ( _FULL_DEBUG_ ) {
      DDLogDebug("LineGutter line :\(num) drawn at x:\(xPosition) y: \(relativePoint.y + yPos)  ")
    }
    attributedString.draw(at:NSPoint(x: xPosition, y: relativePoint.y + yPos))
  }
}
