
/*
ExternalAPI test suite
*/

let abcScore = `
X: 1
T:Money Lost
M:3/4
L:1/8
Q:1/4=100
C:Paul Rosen
S:Copyright 2007, Paul Rosen
R:Klezmer
K:Dm
Ade|:"Dm"(f2d)e gf|"A7"e2^c4|"Gm"B>>^c BA BG|"A"A3Ade|"Dm"(f2d)e gf|"A7"e2^c4|
"Gm"A>>B "A7"AG FE|1"Dm"D3Ade:|2"Dm"D3DEF||:"Gm"(G2D)E FG|"Dm"A2F4|"Gm"B>>c "A7"BA BG|
"Dm"A3 DEF|"Gm"(G2D)EFG|"Dm"A2F4|"E°"E>>Fy "(A7)"ED^C2|1"Dm"D3DEF:|2"Dm"D6||
`

// internal utility functions

function buildArgs(className,entryPoint, args) {
  let fxArguments = {
    'className': className,
    'fx': entryPoint,
    'fxArgs': args,
  };
  return fxArguments;
}

function callExt(args) {
  let returned = abcStudioAPI.externalEntryPoint(args)
  return returned.fxRet
}

describe('ExternalApi', function () {

  it('should return sum of arguments', function () {
    chai.expect(1+ 2).to.equal(3);
  });


  it('abcGlobals is an existing object containing externalAPI singleton', function () {
    chai.expect(window.abcGlobals).to.be.an('object').and.is.ok;
    chai.expect(window.abcGlobals).to.have.property('externalAPI');
  })

  // TestSample call empty function
  it('call testSample through externalEntryPoint', function () {
    let ret = callExt(buildArgs('abcjs','test'), [])
    chai.expect(ret).to.be.an('object').and.is.ok
    chai.expect(ret).not.to.have.property('error')
    chai.expect(ret).to.have.property('data')
    let returned = ret.data
    //chai.expect(returned).to.be.string()
    chai.expect(returned).to.startWith('test API from AbcStudioApi')
  })

  // Show sample abc score API
  it('aShow sampleABC score', function () {
    let args = [
      abcScore,
      function() {
        // callback when dom is ready
        let parent = $('#testID');
        chai.expect(parent.find('svg').length).to.be.at.least(1)
      } ,
    ];
    let ret = callExt(buildArgs('abcjs','showScore', args));
    chai.expect(ret).not.to.have.property('error')
    chai.expect(ret).to.be.an('object').and.is.ok
    let returned = ret.data
    chai.expect(returned).to.be.string
    chai.expect(returned).to.startWith('Done')
  })

  it('set abcjs arguments', function () {
    let args = [{
      staffWidth : 900,
    }];
    let ret = callExt(buildArgs('abcjs','setParams', args))
    chai.expect(ret).to.be.an('object').and.is.ok
    let returned = ret.data
    chai.expect(returned).to.be.string
    chai.expect(returned).to.startWith('Done')
  })

});
