/*
ExternalAPI test suite
*/
let sampleScore =
'<?xml version="1.0" encoding="UTF-8"?>' +
'<score-partwise version="3.1">'+
'  <part-list>'+
'    <score-part id="P1">'+
'      <part-name>Music</part-name>'+
'    </score-part>'+
'  </part-list>'+
'  <part id="P1">'+
'    <measure number="1">'+
'      <attributes>'+
'        <divisions>1</divisions>'+
'        <key>'+
'          <fifths>0</fifths>'+
'        </key>'+
'        <time>'+
'          <beats>4</beats>'+
'          <beat-type>4</beat-type>'+
'        </time>'+
'        <clef>'+
'          <sign>G</sign>'+
'          <line>2</line>'+
'        </clef>'+
'      </attributes>'+
'      <note>'+
'        <pitch>'+
'          <step>C</step>'+
'          <octave>4</octave>'+
'        </pitch>'+
'        <duration>4</duration>'+
'        <type>whole</type>'+
'      </note>'+
'    </measure>'+
'  </part>'+
'</score-partwise>'

describe('XmlMusicApi', function () {

  function validateResponse(response) {
    if (!response.ok) {
      chai.assert(false , response.statusText)
      throw Error(response.statusText)
    }
    return response;
  }

  function getXmlSource(response) {
    let toParse = response.text()
    return toParse
  }

  function parseXmlMusic(xmlSrc) {
    chai.expect(window.abcGlobals).to.be.an('object').and.is.ok
    chai.expect(window.abcGlobals).to.have.property('musicXmlParser')
    let musicXmlParser = window.abcGlobals.musicXmlParser
    let ret = musicXmlParser.parse(xmlSrc)
    let abcConver = musicXmlParser.convertToAbc(xmlSrc)
    console.log(abcConver)
    return ret
  }

  it('sampleMusicXml test', function() {
    return fetch('./tests/testData/sampleMusicXml.xml')
      .then(validateResponse)
      .then(getXmlSource)
      .then(parseXmlMusic)
      .then(function(ret) {
        chai.expect(ret).not.to.be.undefined
      })
  })

  it('sampleMusic Schumann test', function() {
    return fetch('./tests/testData/sampleMusicSchumann.xml')
      .then(validateResponse)
      .then(getXmlSource)
      .then(parseXmlMusic)
      .then(function(ret) {
        chai.expect(ret).not.to.be.undefined
      })
  })

  it('should return sum of arguments', function () {
    chai.expect(1+ 2).to.equal(3);
  });

  it('should parse XML music document through JQUERY and OpenSheetMusic API', function () {
    chai.expect(window.abcGlobals).to.be.an('object').and.is.ok
    chai.expect(window.abcGlobals).to.have.property('xmlMusic')
    let xmlMusic = window.abcGlobals.xmlMusic
    chai.expect(xmlMusic).to.be.an('object').and.is.ok
    let ret = xmlMusic.parseXML(sampleScore)
    chai.expect(ret).to.be.an('object').and.is.ok
    let ret1 = xmlMusic.parseMusicXml(sampleScore)
    chai.expect(ret1).to.be.an('object').and.is.ok
  });

  it('should parse XML music document through Xml2Abc parser', function () {
    chai.expect(window.abcGlobals).to.be.an('object').and.is.ok
    chai.expect(window.abcGlobals).to.have.property('xmlMusic')
    let xmlMusicParser = window.abcGlobals.xmlMusicParser
    chai.expect(xmlMusicParser).to.be.an('object').and.is.ok
    let ret = xmlMusicParser.parse(sampleScore)
    chai.expect(ret).to.be.an('object').and.is.ok
  });

});
