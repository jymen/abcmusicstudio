//
//  ABCMongoCTests.m
//  ABCMongoCTests
//
//  Created by jymengant on 3/28/20 week 13.
//  Copyright © 2020 jymengant. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AbcMongo.h"

@interface ABCMongoCTests : XCTestCase

@end

@implementation ABCMongoCTests

- (void)setUp {
  // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
  // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testSampleConnection  {
  // This is an example of a functional test case.
  // Use XCTAssert and related functions to verify your tests produce the correct results.
  AbcMongo *pMongo = [[AbcMongo alloc] init] ;
  [pMongo initMongo] ;
  [pMongo terminateMongo] ;
  XCTAssert(true) ;
  NSLog(@"testSampleConnection is over");
}


@end
