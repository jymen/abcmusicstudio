//
//  main.swift
//  SampleAbcMongo
//
//  Created by jymengant on 3/26/20 week 13.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import ABCMongoC.Mongo

let mongo = AbcMongo()
mongo.initMongo()
print("Hello, World!")
let connector = mongo.connect("mongodb://localhost:27000")
mongo.terminateMongo()
