//
//  ABCMongoC.h
//  ABCMongoC
//
//  Created by jymengant on 3/26/20 week 13.
//  Copyright © 2020 jymengant. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ABCMongoC.
FOUNDATION_EXPORT double ABCMongoCVersionNumber;

//! Project version string for ABCMongoC.
FOUNDATION_EXPORT const unsigned char ABCMongoCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ABCMongoC/PublicHeader.h>
#import "AbcMongo.h"


