//
//  AbcMongo.h
//  CMongoSample
//
//  Created by jymengant on 3/26/20 week 13.
//  Copyright © 2020 jymengant. All rights reserved.
//
#ifndef AbcMongo_h
#define AbcMongo_h


@interface AbcMongo : NSObject 

@property (strong, nonatomic) NSString* errMessage  ;


// just check library is functional
- (void) check;

// init mongoc driver
- (void) initMongo;

// terminate mongoC (only call once when parent program is ending)
- (void) terminateMongo ;

// connection and misc opening
- (BOOL) connect :(NSString *) connectionUrlStr ;
- (BOOL) createDataBase :(NSString *) dbName ;
- (BOOL) dropDataBase :(NSString *) dbName ;

// collection allocator / deallocator
- (BOOL) getCollection :(NSString *) dbName :(NSString *) collection ;
- (BOOL) releaseCollection :(NSString *) db :(NSString *) collection ;

// get a plain new oid back
- (NSString *) newOid ;

// Data Manipulation stuff
- (NSString *) insert :(NSString *) dbName
                      :(NSString *) collection
                      :(NSString *) jsonData
                      :(NSString *) pOid ; // may be null then computed by mongo

// read using OID return allocated cursor or -1
- (long) select :(NSString *) dbName
                           :(NSString *) collection
                           :(NSString *) selection ;
//
- (NSString *) fetch :(long) cursor  ;
- (BOOL) closeCursor :(long) cursor  ;

// generate oid
- (int64_t) oidGenerate ;
@end

#endif /* AbcMongo_h */
