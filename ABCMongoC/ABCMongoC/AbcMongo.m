//
//  AbcMongo.m
//  MongoCLib
//
//  Created by jymengant on 3/26/20 week 13.
//  Copyright © 2020 jymengant. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AbcMongo.h"

//
// Mongo C driver stuff
//
#import "bson/bson.h"
#import "mongoc/mongoc.h"

#define ABC_APPNAME "ABCMusicStudio"

#pragma mark Mongo Cursor wrapper

@interface AbcMongoCursor : NSObject {
@public
  mongoc_cursor_t *pCursor ;
}

- (instancetype)initWithCursor :(mongoc_cursor_t *) cursor ;
@end

@implementation AbcMongoCursor
- (instancetype)initWithCursor :(mongoc_cursor_t *) cursor {
  self = [super init];
  if (self) {
    self->pCursor = cursor ;
  }
  return self;
}

@end

#pragma mark Mongo Collection wrapper

@interface AbcMongoCollection : NSObject {
@public
  mongoc_collection_t * pCollection ;
}

- (instancetype)initWithCollection :(mongoc_collection_t *) collection ;
@end

@implementation AbcMongoCollection

- (instancetype)initWithCollection :(mongoc_collection_t *) collection {
  self = [super init];
  if (self) {
    self->pCollection = collection ;
  }
  return self;
}

+ (NSString *) buildKey :(NSString *) dbName :(NSString *) collection {
  NSMutableString *  dbcol = [[NSMutableString alloc] initWithCapacity:50] ;
  [dbcol appendString :dbName];
  [dbcol appendString :@"."] ;
  [dbcol appendString :collection] ;
  return dbcol ;
}

@end

#pragma mark Mongo main interface
@implementation AbcMongo

#pragma mark private data
{
@private
  
  mongoc_client_t  * pConnector ; // internal connector to Mongo server
  
  NSMutableDictionary * pCollectionDict ; // Opened collection storage
  NSMutableArray * pCursorsList ; // Opened cursors
}

#pragma mark private methods

- (void) setErrorMessage : (const char * ) pErr
                         : (bson_error_t) mongoError {
  char errorStr[256] = ""  ;
  sprintf ( errorStr ,
           "Message: %s\n"
           "error: %s\n",
           pErr,
           mongoError.message);
  self.errMessage = [[NSString alloc] initWithUTF8String:pErr];
}

//
// from JSON string
//
- (bson_t *) convertToBson :(NSString *) jsonData {
  bson_error_t error;
  const char * jsonStr = [jsonData UTF8String];
  bson_t *doc = bson_new_from_json ((const uint8_t *)jsonStr, -1, &error);
  if (!doc) {
    [self setErrorMessage:"mongo BSON parsing error " :error ] ;
  }
  return doc ;
}


//
// get cursor back from array
//
- (mongoc_cursor_t *) getCursor :(long) cursor {

  if (!self->pCursorsList) {
    self.errMessage = @"No cursors defined" ;
    return NULL;
  }
  AbcMongoCursor * pcurs = self->pCursorsList[cursor] ;
  if (!pcurs) {
    self.errMessage = [ NSString stringWithFormat
                       :@"inexisting cursor location : %li" , cursor];
    return NULL;
  }
  return pcurs->pCursor ;
}

//
// convert to mongo URI internal format
//
- (mongoc_uri_t *) mongoUriFromString :(NSString *) pSource {
  bson_error_t error ;
  char errorStr[256] = ""  ;
  const char * uri_string = [ pSource UTF8String ];
  
  mongoc_uri_t * returnedUri = mongoc_uri_new_with_error (uri_string, &error);
  if (!returnedUri) {
    sprintf ( errorStr ,
             "failed to parse URI: %s\n",
             uri_string );
    [self setErrorMessage :errorStr :error] ;
  }
  return returnedUri ;
}

- (mongoc_collection_t *) getCurrentCollection :(NSString *) db :(NSString *) collection {
  NSString * pKey = [AbcMongoCollection buildKey:db :collection];
  AbcMongoCollection * pAbcCollection = self->pCollectionDict[pKey];
  if (!pAbcCollection) {
    self.errMessage = [ NSString stringWithFormat
                       :@"undefined collection : %@" , collection];
    return NULL ;
  }
  return pAbcCollection->pCollection ;
}

//
// try to connect and ping the server
//
- (mongoc_client_t *) ping :(mongoc_uri_t *) uri {
  mongoc_client_t * pClient = mongoc_client_new_from_uri (uri);
  if (pClient) {
    // Register ABCMusicStudion as mongo connector
    mongoc_client_set_appname (pClient, ABC_APPNAME);
    bson_error_t error;
    bson_t reply ;
    // Try to ping to validate connection
    bson_t * pCommand = BCON_NEW ("ping", BCON_INT32 (1));
    if ( mongoc_client_command_simple ( pClient, "admin",
                                       pCommand,
                                       NULL,
                                       &reply,
                                       &error) ) {
      char * pRetStr = bson_as_json (&reply, NULL);
      NSLog(@"ping to mongo returned : %@" , [[NSString alloc] initWithUTF8String: pRetStr]) ;
      bson_destroy (&reply); // cleanup
    } else {
      [self setErrorMessage:"ping database error " :error ] ;
      return NULL ;
    }
  }
  return pClient;
}

- (mongoc_database_t *) getDatabase :(NSString *) dbName {
  if (!pConnector) {
    self.errMessage = @"No mongo connectors set => call connect prior openDatabase" ;
    return NULL;
  }
  const char * dbCstring = [ dbName UTF8String ];
  return mongoc_client_get_database (pConnector, dbCstring);
}

// convert Oid to String
- (NSString *) convertOidToString :(bson_oid_t *) pOid {
  char str[26] ;
  bson_oid_to_string (pOid, str);
  return [[NSString alloc] initWithUTF8String:str] ;
}

#pragma mark public methods

- (void) check {
  NSLog(@"ABCMongo is functional");
}

- (void) initMongo {
  NSLog(@"ABCMongo has inited");
  mongoc_init();
}


- (void) terminateMongo {
  mongoc_cleanup();
  NSLog(@"ABCMongo has cleanup");
}

//
// connect and keep connector
//
- (BOOL) connect : ( NSString * ) connectionUrlStr {
  mongoc_uri_t * pMongo_uri ;
  self.errMessage = nil ; // cleanup
  
  if ( connectionUrlStr == nil ) {
    return NO ;
  }
  pMongo_uri = [self mongoUriFromString: connectionUrlStr];
  if ( pMongo_uri ) {
    pConnector  = [self ping: pMongo_uri] ;
    if (!pConnector) {
      self.errMessage = [ NSString stringWithFormat
                         :@"Unable to ping mongoserver : %@" , connectionUrlStr];
      return NO;
    }
    return YES;
  }
  return NO;
}

//
// access db and keep db handle
//
- (BOOL) createDataBase :( NSString *) dbName {
  self.errMessage = nil ; // cleanup

  mongoc_database_t *pDatabase = [self getDatabase :dbName] ;
  
  mongoc_database_destroy (pDatabase); // cleanup db resources
  
  if ( pDatabase ) {
    return YES;
  } else {
    return NO ;
  }
}

- (BOOL) dropDataBase :(NSString *) dbName {
  self.errMessage = nil ; // cleanup

  mongoc_database_t *pDatabase = [self getDatabase :dbName] ;
  bson_error_t error;
  
  bool succes = mongoc_database_drop_with_opts (pDatabase,NULL,&error);
  mongoc_database_destroy (pDatabase); // cleanup db resources
  
  if (succes) {
    return YES ;
  } else {
    [self setErrorMessage:"drop database error " :error ] ;
    return NO ;
  }
}


- (BOOL) getCollection :(NSString *) dbName :(NSString *) collection {
  const char * dbCstring = [dbName UTF8String];
  const char * pCcolName = [collection  UTF8String];
  mongoc_collection_t * pCurCollection = NULL;
  self.errMessage = nil ; // cleanup

  
  if (! self->pCollectionDict ) {
    self->pCollectionDict = [[NSMutableDictionary alloc] init];
  }
  NSString * pKey = [AbcMongoCollection buildKey:dbName :collection ] ;
  AbcMongoCollection * pAbcCollection = self->pCollectionDict[pKey];
  if ( pAbcCollection) {
    return YES;
  } else {
    // new allocation
    pCurCollection = mongoc_client_get_collection ( pConnector,
                                                   dbCstring,
                                                   pCcolName );
    if (!pCurCollection) {
      self.errMessage = @"Mongo collection allocation failure" ;
      return NO;
    }
    AbcMongoCollection * colWrapper = [[ AbcMongoCollection alloc ]
                                       initWithCollection:pCurCollection] ;
    self->pCollectionDict[pKey] = colWrapper;
    return YES ;
  }
}

- (BOOL) releaseCollection :(NSString *) dbName :(NSString *) collection {
  if (! self->pCollectionDict ) {
    return NO ;
  }
  NSString * pKey = [AbcMongoCollection buildKey:dbName :collection ] ;
  AbcMongoCollection * pCol = self->pCollectionDict[pKey] ;
  if (pCol) {
    mongoc_collection_destroy (pCol->pCollection);
    [self->pCollectionDict removeObjectForKey :collection ];
    return YES ;
  }
  return NO ;
}

// get a plain new oid back
- (NSString *) newOid {
  bson_oid_t oid ;
  bson_oid_init (&oid, NULL);
  return [self convertOidToString :&oid];
}

//
// json data insertion in given collection
// return inserted _id
//
- (NSString *) insert :(NSString *) dbName
                      :(NSString *) collection
                      :(NSString *) jsonData
                      :(NSString *) pOid {
  bson_error_t error;
  bson_oid_t oid;
  self.errMessage = nil ; // cleanup

  mongoc_collection_t * pCollection = [self getCurrentCollection :dbName :collection];
  if (!pCollection) {
    return nil ;
  }
  bson_t *doc = [self convertToBson:jsonData];
  if (!doc) {
    return nil ;
  }
  // deal with Object id
  if (pOid) {
    const char * strOid = [pOid UTF8String];
    bson_oid_init_from_string(&oid, strOid) ;
  } else {
    bson_oid_init (&oid, NULL);
  }
  BSON_APPEND_OID (doc, "_id", &oid);
  char * string = bson_as_canonical_extended_json (doc, NULL);
  NSLog (@"BSON data : %s\n", string);
  
  // proceed with insert
  bool success = mongoc_collection_insert_one ( pCollection, doc, NULL, NULL, &error ) ;
  if ( success ) {
    return [self convertOidToString :&oid] ;
  } else {
    [self setErrorMessage:"collection insert error " :error ] ;
    return nil ;
  }
}

// select data and return cursor offset or -1 when not found or error
- (long) select :(NSString *) dbName
                :(NSString *) collection
                :(NSString *) selection {
  bson_t *query;
  self.errMessage = nil ; // cleanup

  mongoc_collection_t * pCollection = [self getCurrentCollection :dbName :collection];
  
  if (!self->pCursorsList) {
    self->pCursorsList = [[NSMutableArray alloc] init] ;
  }
  
  if (!pCollection) {
    return -1 ;
  }
  if (selection) {
    query = [self convertToBson:selection];
    if (!query) {
      return -1 ;
    }
  } else {
    // scan all collection
    query = bson_new ();
  }
  mongoc_cursor_t * cursor = mongoc_collection_find_with_opts (pCollection, query, NULL, NULL);
  bson_destroy (query);

  if (cursor ) {
    // store it in array and return its index
    AbcMongoCursor * pcurs = [[AbcMongoCursor alloc] initWithCursor:cursor ];
    [self->pCursorsList addObject:pcurs] ;
    return [self->pCursorsList count] -1;
  }
  
  return -1;
}

//
// get selection instances back
//
- (NSString *) fetch :(long) cursor {
  const bson_t *doc;
  char *str;
  NSString * returned ;
  self.errMessage = nil ; // cleanup

  mongoc_cursor_t * mcursor = [self getCursor :cursor] ;
  if ( !mcursor ) {
    return nil ;
  }
  returned = nil ;
  if (mongoc_cursor_next (mcursor, &doc)) {
    str = bson_as_canonical_extended_json (doc, NULL);
    returned = [[NSString alloc] initWithUTF8String :str ];
    bson_free (str);
  }
  return returned ;
}

//
// release cursor resource
//
- (BOOL) closeCursor :(long) cursor  {
  self.errMessage = NULL ; // cleanup

  mongoc_cursor_t * mcursor = [self getCursor :cursor] ;
  if ( !mcursor ) {
    return NO ;
  }
  mongoc_cursor_destroy (mcursor);
  [self->pCursorsList removeObjectAtIndex :cursor] ;
  return YES ;
}

//
// oid generator
//
- (int64_t) oidGenerate {
  // bson_oid_t oid;
  int64_t oid;
  bson_oid_init ((bson_oid_t *)&oid, NULL);
  return oid; 
}




@end
