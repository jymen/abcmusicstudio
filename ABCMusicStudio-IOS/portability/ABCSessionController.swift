//
//  IOSSessionController.swift
//  ABCMusicStudio
//
//  Created by jymengant on 5/16/20 week 20.
//  Copyright © 2020 jymengant. All rights reserved.
//

import UIKit

import MultipeerConnectivity


class ABCSessionController : PeerSessionController {
  
  private var controller : UIViewController
  
  init( controller : UIViewController ) {
    self.controller = controller
  }
  
  
  func present(sessionController: MCBrowserViewController) {
    controller.present(sessionController,animated:true)
  }
  
  func dismiss() {
    self.controller.dismiss(animated:true)
  }
}
