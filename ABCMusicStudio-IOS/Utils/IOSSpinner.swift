//
//  IOSSpinner.swift
//  ABCMusicStudio-IOS
//
//  Created by jymengant on 5/14/20 week 20.
//  Copyright © 2020 jymengant. All rights reserved.
//

import UIKit

class SpinnerViewController: UIViewController {
  var spinner = UIActivityIndicatorView(style: .whiteLarge)
  
  override func loadView() {
    view = UIView()
    view.backgroundColor = UIColor(white: 0, alpha: 0.7)
    
    spinner.translatesAutoresizingMaskIntoConstraints = false
    spinner.startAnimating()
    view.addSubview(spinner)
    
    spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
  }
}

class IOSSpinner : ABCSpinnerProtocol {

  var started : Bool = false
  var child : SpinnerViewController?
  let view : UIView
  
  init( view : UIView ) {
    self.view = view
  }
  
  func start() -> Bool {
    if let _ = self.child {
      return false
    }
    child = SpinnerViewController()
    child!.view.frame = view.frame
    view.addSubview(child!.view)
    return true
  }
  
  func stop() {
    if let child = child {
      child.view.removeFromSuperview()
    }
  }
  
  
}
