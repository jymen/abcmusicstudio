//
//  AbcIOSPeerAPI.swift
//  ABCMusicStudio-IOS
//
//  Created by jymengant on 5/19/20 week 21.
//  Copyright © 2020 jymengant. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjack

class AbcIOSPeerAPI : PeerSessionObserver {
    
  var peerService : ABCScoreService?
  let webView : ABCWebView

  init( webView : ABCWebView ) {
    self.webView = webView
  }
  
  //
  // simple communication checker
  //
  func newMessageReceived(args: EntryPointArgs) {
    switch args.fx {
    case "ping" :
      let fxRet = FxRet(data: ["response": "OK"])
      let ret = EntryPointArgs(className: "Ping", fx: "ping", args: nil, fxRet: fxRet)
      peerService?.sendMessage(args: ret)
      
    case "scoreTransfert" :
      sendTransferedScore(args:args)
    default:
      assert(false)
    }
  }
  
  //
  // Browse for ABCScoreservice and connect with it
  //
  func bind() {
    if ( peerService == nil ) {
      peerService = ABCScoreService(deviceName: UIDevice.current.name , observer: self)
      peerService?.joinSession()
    } else {
      peerService?.stop()
      peerService = nil 
    }
  }
  
  //
  // UI Api to be called on main thread wrapper
  //
  func swiftEntryPoint( className : String , fx : String , args : [Any]? , completion: @escaping (JsReturned)->() ){
    if Thread.current.isMainThread {
      webView.swiftEntryPoint( className: className, fx: fx, args: args, completion: completion)
    } else {
      DispatchQueue.main.sync {
        webView.swiftEntryPoint( className: className, fx: fx, args: args, completion: completion)
      }
    }
  }
  
  //
  // notify session state changes
  //
  func sessionChanges(state: PeerSessionState ) {
    DDLogDebug("Peer session status changed")
    let strState = state.rawValue
    swiftEntryPoint( className: "abcIOSScoreTransferAPI" ,
                     fx: "newState",
                     args: [strState],
                     completion: {
                        result in
                        assert( result.isDone())
                        DDLogDebug("newState has been done")
                     })
  }
  
  func sendActionCompleted( action : String , jsonSource : String?) {
    DDLogDebug("action completed")
    swiftEntryPoint( className: "abcIOSScoreTransferAPI" ,
                     fx: "actionCompleted",
                     args: [action , jsonSource ?? "{}"],
                     completion: {
                        result in
                        assert( result.isDone())
                        DDLogDebug("actionCompleted has been sent")
                     })
  }
  
  func sendActionInError( action : String , error : String ) {
    DDLogDebug("action in error")
    swiftEntryPoint( className: "abcIOSScoreTransferAPI" ,
                     fx: "actionInError",
                     args: [action , error],
                     completion: {
                        result in
                        assert( result.isDone())
                        DDLogDebug("actionInError has been sent")
                     })

  }
  
  func sendTransferedScore( args: EntryPointArgs ) {
    DDLogDebug("Send Score")
    swiftEntryPoint( className: "abcIOSScoreTransferAPI" ,
                     fx: args.fx,
                     args: args.fxArgs,
                     completion: {
                        result in
                        assert( result.isDone())
                        DDLogDebug("score has been transfered to JS")
                     })
  }
  

}
