//
//  ModalAlert.swift
//  ABCStudio
//
//  Created by jymengant on 8/16/19 week 33.
//  Copyright © 2019 jymengant. All rights reserved.
//

import UIKit
import CocoaLumberjack
import PromiseKit

enum RemovalType {
  case StopRemoval
  case Remove
  case Delete
}


class ModalAlert : UIViewController {
  
  var okAction : ((UIAlertAction) -> Void)? = nil
  var cancelAction : ((UIAlertAction) -> Void)? = nil

  /**
   Just popup dialog box with messages
   */
  func show( question: String, text: String , style: ABCAlertStyle) -> Bool {
    
    var alertStyle : UIAlertController.Style = .actionSheet
    switch style {
    case .Critical , .Warning :
      alertStyle = .alert
      break
    case .Informational :
      alertStyle = .actionSheet
      break
    }

    let alert = UIAlertController(title: question, message: text, preferredStyle: alertStyle)
    if ( style == .Informational )  {
      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: okAction ))
      alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: cancelAction ))
    } else {
      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: okAction ))
    }
    self.present(alert, animated: true)
    return true
  }
  
  func confirmTreeRemoval( leaf : Leaf ) -> Promise<RemovalType> {
    // NOT NEEDED ON IOS SIDE yet
    return Promise {_ in
    }
  }
  
  
}
