//
//  ContentView.swift
//  ABCMusicStudio-IOS
//
//  Created by jymengant on 2/3/20 week 6.
//  Copyright © 2020 jymengant. All rights reserved.
//

import SwiftUI

struct ABCMainView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ABCMainView_Previews: PreviewProvider {
    static var previews: some View {
        ABCMainView()
    }
}
