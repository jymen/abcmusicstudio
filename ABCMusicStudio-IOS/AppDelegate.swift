//
//  AppDelegate.swift
//  ABCMusicStudio-IOS
//
//  Created by jymengant on 2/3/20 week 6.
//  Copyright © 2020 jymengant. All rights reserved.
//

import UIKit
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Global lumberjack level
    let ddLogLevel: DDLogLevel = DDLogLevel.debug
    
    // init LumberJack
    DDLog.add(DDOSLogger.sharedInstance)
    DDLogVerbose("Verbose")
    DDLogDebug("Debug")
    DDLogInfo("Info")
    DDLogWarn("Danger, Will Robinson", level: ddLogLevel)
    DDLogDebug("CocoaLumberjack has been inited", level: ddLogLevel)
    
    do {
      if ( preferences.isDarkMode() ) {
        DDLogDebug("we're DARK")
      } else {
        DDLogDebug("we're LIGHT")
      }
      try preferences.checkPreferences()
    } catch {
      let myError : ABCFailure = error as! ABCFailure
      _ = ModalAlert().show(question: "Severe INIT error", text: myError.message! , style: .Critical )
      exit(-1)
    }
    
    // Override point for customization after application launch.
    // Main window startup
    self.window = self.window ?? UIWindow()
    if let mainWindow = self.window {
      mainWindow.backgroundColor = .white
      mainWindow.rootViewController = ABCMainViewController()
      mainWindow.makeKeyAndVisible()
    }
    return true
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }


}

