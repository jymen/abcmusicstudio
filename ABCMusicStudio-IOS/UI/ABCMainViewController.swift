//
//  ABCMainViewController.swift
//  ABCMusicStudio-IOS
//
//  Created by jymengant on 4/15/20 week 16.
//  Copyright © 2020 jymengant. All rights reserved.
//

import UIKit
import WebKit

import CocoaLumberjack

class ABCMainViewController : UIViewController ,
                              ABCWebViewPortabilityAdapter
{
  
  
  private var htmlView : ABCWebView? = nil
  private var apiClass : AbcIOSPeerAPI?


  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  init() {
    super.init(nibName: nil, bundle: nil)
    self.view = WKWebView()
    self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    let htmlView = self.view as? WKWebView
    self.htmlView = ABCWebView(view: htmlView! , webHandler: self)
    //
    // Important : since tabView selection may be lazy , it's important to force
    // Javascript global definition initialisation before tab is selected else
    // RPC javacript call will fail ; this is done by calling initialHtml on next
    // statement initialHtml may be called multiple times; it just proceeds
    // with JS init and displays the ABC welcome page
    initialHtml()
    apiClass = AbcIOSPeerAPI(webView: self.htmlView!)
  }
  
  private func initialHtml() {
    htmlView!.load(name: "index", type: "html")
  }

  
  // configure our interface
  override func viewDidLoad() {
    super.viewDidLoad()
    // self.view.backgroundColor = .yellow
  }
  
  
  private func repository( fx : DatabaseAction ,
                           // Default value just used when no db arguments are
                           // requested
                           jsonRepo : String =  """
                            {
                              "name" : "defaultRepository" ,
                              "type" : "SqlLite"
                            }
                            """ ) {
    do {
      let request = ABCDbJsonRequest()
      let jsonDest = try request.doRequest(fx: fx , jsonRepo:jsonRepo)
      apiClass?.sendActionCompleted(action: fx.rawValue, jsonSource: jsonDest)
    } catch {
      let decodeError = error.localizedDescription
      handleJSCallbackError(title: fx.rawValue, message:  " Action Repository error : \(decodeError)")
    }
  }
  
  private func checkArgs( args : EntryPointArgs ) -> String? {
    if let args = args.fxArgs {
      if let toDecode = args[0].value as? String {
        return toDecode
      }
    }
    return nil
  }

  //
  // JS Callback return handler
  //
  func handleJSCallbackReturn( content : EntryPointArgs ) {
    DDLogDebug("entering handleJSCallbackReturn")
    if let className = content.className {
      switch className {
      
      // Peer communication
      case "AbcIOSPeerAPI" :
        switch content.fx {
        case "bind" :
          apiClass!.bind()
          break
        default :
          break
      }
      // DB repository communication
      case "AbcRepositoryAPI" :
        switch content.fx {
        case "storageOperation" :
          if let args =  checkArgs(args: content) {
            repository(fx: .StorageOperation , jsonRepo: args)
          } else {
            handleJSCallbackError(title: className, message: " UNEXPECTED error on args")
          }
        case "getScoreRepoList" :
          repository(fx: .ListDatabase )
          break
        case "createRepository" :
          if let args =  checkArgs(args: content) {
            repository(fx: .CreateDatabase , jsonRepo: args)
          } else {
            handleJSCallbackError(title: className, message: " UNEXPECTED error on args")
          }
        case "deleteRepository" :
          if let args =  checkArgs(args: content) {
            repository(fx: .DeleteDatabase , jsonRepo: args)
          } else {
            handleJSCallbackError(title: className, message: " UNEXPECTED error on args")
          }
          break
        default :
          handleJSCallbackError(title: className, message: " \(content.fx) : UNKNOWN API")
          break
        }

      default:
        handleJSCallbackError(title: className, message: " handler NOT IMPLEMENTED YET")
      }
    }
  }
  
  func handleJSCallbackError(title: String, message: String) {
    apiClass?.sendActionInError(action: title, error: message)
  }
  


  
}
